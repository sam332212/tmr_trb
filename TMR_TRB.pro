#-------------------------------------------------
#
# Project created by QtCreator 2019-09-09T20:51:27
#
#-------------------------------------------------

QT       += core gui sql serialport concurrent printsupport multimedia

include(qextSerialPort/qextserialport.pri)
include(xlsx/qtxlsx.pri)

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TMR_TRB
TEMPLATE = app
RC_FILE = myapp.rc

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

#If the QString contains non-ASCII Unicode characters, using this function can
#lead to loss of information. You can disable this function by defining QT_NO_CAST_TO_ASCII
#when you compile your applications.
#DEFINES += QT_NO_CAST_TO_ASCII

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11
CONFIG += qt warn_on debug

#CONFIG(release, debug|release) {
#        DEFINES += QT_NO_DEBUG_OUTPUT
#}

SOURCES += \
        B64ED/Base64.cpp \
        PCI/1758/Pci1758.cpp \
        PCI/7444/Pci7444.cpp \
        Report/PDF_API.cpp \
        Report/report_generator.cpp \
        Serials/DMM.cpp \
        Serials/DevicesHandler.cpp \
        Serials/IRMeter.cpp \
        Serials/PowerSupply.cpp \
        Serials/SerialPort.cpp \
        Support/Database.cpp \
        Support/Global.cpp \
        Support/Home.cpp \
        Support/Timer.cpp \
        Support/Utility.cpp \
        TestCases/EmiEmc.cpp \
        TestCases/InsulationP2C.cpp \
        TestCases/IsolationP2C.cpp \
        TestCases/IsolationP2P.cpp \
        TestCases/IsolationP2P_V1.cpp \
        TestCases/JigSelfTest.cpp \
        TestCases/LevelShifter.cpp \
        TestCases/Load.cpp \
        TestCases/ManualIsolation.cpp \
        TestCases/ManualPolarity.cpp \
        TestCases/Polarity.cpp \
        TestCases/RelayTest.cpp \
        TestCases/SelfTest.cpp \
        UserControl/EditUser.cpp \
        UserControl/Login.cpp \
        UserControl/SignUp.cpp \
        main.cpp \

HEADERS += \
        B64ED/Base64.h \
        PCI/1758/Pci1758.h \
        PCI/1758/bdaqctrl.h \
        PCI/7444/Dask.h \
        PCI/7444/Pci7444.h \
        Report/PDF_API.h \
        Report/report_generator.h \
        Serials/DMM.h \
        Serials/DevicesHandler.h \
        Serials/IRMeter.h \
        Serials/PowerSupply.h \
        Serials/SerialPort.h \
        Support/Database.h \
        Support/Global.h \
        Support/Home.h \
        Support/Timer.h \
        Support/Utility.h \
        Support/debugflag.h \
        TestCases/EmiEmc.h \
        TestCases/InsulationP2C.h \
        TestCases/IsolationP2C.h \
        TestCases/IsolationP2P.h \
        TestCases/IsolationP2P_V1.h \
        TestCases/JigSelfTest.h \
        TestCases/LevelShifter.h \
        TestCases/Load.h \
        TestCases/ManualIsolation.h \
        TestCases/ManualPolarity.h \
        TestCases/Polarity.h \
        TestCases/RelayTest.h \
        TestCases/SelfTest.h \
        UserControl/EditUser.h \
        UserControl/Login.h \
        UserControl/SignUp.h \

LIBS += ./PCI-DASK.lib

FORMS += \
        Support/Home.ui \
        TestCases/JigSelfTest.ui \
        TestCases/ManualIsolation.ui \
        TestCases/RelayTest.ui \
        TestCases/SelfTest.ui \
        UserControl/EditUser.ui \
        UserControl/Login.ui \
        UserControl/SignUp.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    images.qrc

#win32:CONFIG(release, debug|release): LIBS += -L$$PWD/PCI/ -lPCI-Dask
#else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/PCI/ -lPCI-Daskd

#INCLUDEPATH += PCI\

#INCLUDEPATH += $$PWD/PCI
#DEPENDPATH += $$PWD/PCI

#======== UNUSED ===========
#DISTFILES += \
#    PCI/PCI-Dask.dll \
#    PCI/PCI-Dask.lib
#===========================
