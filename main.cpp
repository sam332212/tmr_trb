/*************************************  FILE HEADER  *****************************************************************
*
*  File name -main.cpp
*  Creation date and time - 21-AUG-2019 , SAT 03:00 PM IST
*  Author: - Manoj
*  Purpose of the file - It is the start up function.
*
**********************************************************************************************************************/

/********************************************************
 Inclusion of header file
 ********************************************************/
#include <QApplication>

#include "Support/Home.h"
#include "Support/debugflag.h"

/********************************************************
 Function Definition
 ********************************************************/
/*************************************************************************
 Function Name - main
 Parameter(s)  - int,char*[]
 Return Type   - int
 Action        - It is the start up function.
 *************************************************************************/
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    Home* homeObj = new Home;

    if(!homeObj->initHome())
    {
        QMessageBox::warning(nullptr,"TMR_TRB!!\n","Oops Unable To open Application.\nContact to Administrator");
    }
    else
    {
        return a.exec();
    }

#if MAIN_DEBUG
        qDebug("Main End.");
#endif

    return 0;
}

