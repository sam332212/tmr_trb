#ifndef RELAYTEST_H
#define RELAYTEST_H

#include <QWidget>
#include "Support/Utility.h"

namespace Ui {
class RelayTest;
}

class RelayTest : public QWidget
{
    Q_OBJECT

public:
    explicit RelayTest(QWidget *parent = nullptr);
    ~RelayTest();
    void setMsgTitle(const QString &value);
    bool checkPS();
    bool resetPS();

signals:
    void close();

private:
    void closeEvent (QCloseEvent *event);

private slots:
    void on_pushButton_onoff_clicked();
    void on_pushButton_read_clicked();
    void on_pushButton_clear_clicked();
    void on_pushButton_ps1onoff_clicked();
    void on_pushButton_ps2onoff_clicked();
    void on_pushButton_ps3onoff_clicked();
    void on_pushButton_ps4onoff_clicked();
    void on_pushButton_ps5onoff_clicked();
    void on_pushButton_connectDisconnect_clicked();
    void on_pushButton_onoff1758_clicked();
    void on_pushButton_readDmmResistance_clicked();
    void on_pushButton_readDmmVoltage_clicked();

    void on_pushButton_onoffDMM_clicked();

    void on_pushButton_reset7444_clicked();

    void on_pushButton_reset1758_clicked();

    void on_pushButton_readDmmCurrent_clicked();

private:
    Ui::RelayTest *ui;
    QString msgTitle;
    Utility utilityObj;
    QVector<int>psPrevOff;
    QVector<int>psPrevOn;
};

#endif // RELAYTEST_H
