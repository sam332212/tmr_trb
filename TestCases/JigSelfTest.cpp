/*************************************  FILE HEADER  *****************************************************************
*
*  File name - JigSelfTest.cpp
*  Creation date and time - 01-AUG-2021 , SUN 08:50 AM IST
*  Author: - Manoj
*  Purpose of the file - All "JigSelfTest" modules will be defined.
*
**********************************************************************************************************************/
/********************************************************
 Inclusion of header file
 ********************************************************/
#include "JigSelfTest.h"
#include "ui_JigSelfTest.h"
#include "Support/debugflag.h"

#include <QCloseEvent>
#include <QScreen>
#include <QFileDialog>

#include "Report/report_generator.h"

/********************************************************
 MACRO Definition
 ********************************************************/
#if JIG_SELFTEST_DEBUG
    #define _qDebug(s) qDebug()<<"\n========"<<"Inside "<<s<<"========"
#else
    #define _qDebug(s); {}
#endif

#if JIG_SELFTEST_DEBUG_E
    #define _qDebugE(p,q,r,s) qDebug()<<p<<q<<r<<s
#else
    #define _qDebugE(p,q,r,s); {}
#endif

/********************************************************
 CONSTRUCTOR
 ********************************************************/
 JigSelfTest::JigSelfTest(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::JigSelfTest),dbObj(new Database),
    timerObj(new Timer),testTimerObj(new QTimer)
 {
    _qDebug("JigSelfTest Constructor");
    ui->setupUi(this);
    //move(QGuiApplication::screens().at(0)->geometry().center() - frameGeometry().center());
    move(pos() + (QGuiApplication::primaryScreen()->geometry().center() - geometry().center()));

    //set Font for table_Header
    font.setPointSize(14);
    font.setBold(true);
    font.setFamily("Times New Roman");
 }

/********************************************************
 DESTRUCTOR
 ********************************************************/
 JigSelfTest::~JigSelfTest()
 {
    _qDebug("JigSelfTest Destructor");
    clearmem(timerObj);
    clearmem(testTimerObj);
    delete ui;
 }

/*************************************************************************
 Function Name - closeEvent
 Parameter(s)  - QCloseEvent*
 Return Type   - void
 Action        - This function handles this windows close action.
 *************************************************************************/
 void JigSelfTest::closeEvent (QCloseEvent *event)
 {
     _qDebug("void JigSelfTest::closeEvent (QCloseEvent *event)");

     if(ui->pushButton_run_stop->text() == "STOP") {
        PLAY QMessageBox::warning(nullptr,"Error!!\n","Please stop the test before closing Window.");
        event->ignore();
     }
     else {
         QMessageBox::StandardButton resBtn = QMessageBox::question( this, msgTitle,tr("Do you Want to Close this Window?\n"),QMessageBox::No | QMessageBox::Yes);
         if (resBtn != QMessageBox::Yes)
         {
             event->ignore();
         }
         else
         {
             G::msgBox = new QMessageBox;
             G::msgBox->setWindowTitle(msgTitle);
             G::msgBox->setText("Please wait!! "+msgTitle+" Window is being closed...  ");
             G::msgBox->setWindowFlags(Qt::CustomizeWindowHint);
             G::msgBox->setStandardButtons(nullptr);
             G::msgBox->show();
             timerObj->delayInms(1);

             emit close();
         }
     }
 }

/*************************************************************************
 Function Name - setMsgTitle
 Parameter(s)  - const QString &
 Return Type   - void
 Action        - This function modifies the windows tille variable with the given value.
 *************************************************************************/
 void JigSelfTest::setMsgTitle(const QString &value)
 {
    msgTitle = value;
 }

/*************************************************************************
 Function Name - startUpdateTimerLabel
 Parameter(s)  - void
 Return Type   - void
 Action        - This function starts timer to update test_timer_label.
 *************************************************************************/
 void JigSelfTest::startUpdateTimerLabel()
 {
    testTimerObj->start(1000);
 }

/*************************************************************************
 Function Name - stopUpdateTimerLabel
 Parameter(s)  - void
 Return Type   - void
 Action        - This function stops the test timer.
 *************************************************************************/
 void JigSelfTest::stopUpdateTimerLabel()
 {
    testTimerObj->stop();
 }

/*************************************************************************
 Function Name - updateTestTimer
 Parameter(s)  - void
 Return Type   - void
 Action        - This function updates the test_timer_label when test runs.
 *************************************************************************/
 void JigSelfTest::updateTestTimer()
 {
     QString text;
     timeElapSec++;
     if(timeElapSec > 59)
     {
         timeElapMin++;
         timeElapSec = 0;
     }
     if(timeElapMin > 59)
     {
         timeElapHour++;
         timeElapMin = 0;
         timeElapSec = 0;
     }
     text.sprintf("%02d H : %02d M : %02d S",timeElapHour,timeElapMin,timeElapSec);
     ui->label_timer->setText(text);
}

/*************************************************************************
 Function Name - initSelfTest
 Parameter(s)  - QString, QString, bool
 Return Type   - bool
 Action        - This function gets data from database and loads data in tables.
                 If any failure returns false else returns true.
 *************************************************************************/
 bool JigSelfTest::initSelfTest(QString userRep, QString QARep, bool autoGenerateReport) {
     _qDebug("bool JigSelfTest::initSelfTest(QString userRep, QString QARep, bool autoGenerateReport)");
     this->autoGenerateReport = autoGenerateReport;

     if(!dbObj->openDB("Self_Test.db","localHost","SELF_TEST","self_test_pw","selftest"))
     {
         PLAY QMessageBox::warning(nullptr,"Error!!","SelfTest Database open Failed.");
         return false;
     }
     else {
         _qDebugE("SelfTest Database opened successfully.","","","");

         if(!getTableData(DIPS_DB_TABLE,   dipMap      )) return false;
         if(!getTableData(DOPS_DB_TABLE,   dopMap      )) return false;
         if(!getTableData(INPUTS_DB_TABLE, inputMap    )) return false;
         if(!getTableData(J8_DB_TABLE,     J8_Data     )) return false;
         if(!getTableData(J9_DB_TABLE,     J9_Data     )) return false;
         if(!getTableData(J10_DB_TABLE,    J10_Data    )) return false;
         if(!getTableData(J11_J17_DB_TABLE,J11_J17_Data)) return false;
         if(!getTableData(J12_J18_DB_TABLE,J12_J18_Data)) return false;
         if(!getTableData(J15A_13_DB_TABLE,J15A_13_Data)) return false;
         if(!getTableData(J15B_14_DB_TABLE,J15B_14_Data)) return false;
         if(!getTableData(MESSAGE_DB_TABLE,testMessage )) return false;
         else testMessage.pop_front();//because we don't need table header
         if(!getTableData(REPORT_DB_TABLE,reportData)) return false;
         else {
             reportHeader = reportData.at(0);
             reportData.pop_front();//because we don't need table header
         }

         if(testMessage.count() != TOTAL_TEST) {
             PLAY QMessageBox::warning(nullptr,"Error!!","Invalid data in ."+MESSAGE_DB_TABLE);
             return false;
         }
         if(reportData.count() != TOTAL_TEST) {
             PLAY QMessageBox::warning(nullptr,"Error!!","Invalid data in ."+REPORT_DB_TABLE);
             return false;
         }

         ui->lineEdit_UserPerson->setText(userRep);
         ui->lineEdit_QAperson->setText(QARep);
         ui->tabWidget->setCurrentIndex(0);
         loadTest();
     }

     return setPS();
 }

/*************************************************************************
 Function Name - loadTest
 Parameter(s)  - void
 Return Type   - void
 Action        - This function loads data onto the test tables.
 *************************************************************************/
 void JigSelfTest::loadTest() {
     _qDebug("void JigSelfTest::loadTest()");

     if(!uploadTableData(J8_DB_TABLE,ui->tableWidget_J8,J8_Data,
                     J8_hiddenData,J8_hiddenHeaders)) { return; }

     if(!uploadTableData(J9_DB_TABLE,ui->tableWidget_J9,J9_Data,
                     J9_hiddenData,J9_hiddenHeaders)) { return; }

     if(!uploadTableData(J10_DB_TABLE,ui->tableWidget_J10,J10_Data,
                     J10_hiddenData,J10_hiddenHeaders)) { return; }

     if(!uploadTableData(J11_J17_DB_TABLE,ui->tableWidget_J11_J17,J11_J17_Data,
                     J11_J17_hiddenData,J11_J17_hiddenHeaders)) { return; }

     if(!uploadTableData(J12_J18_DB_TABLE,ui->tableWidget_J12_J18,J12_J18_Data,
                     J12_J18_hiddenData,J12_J18_hiddenHeaders)) { return; }

    if(!uploadTableData(J15A_13_DB_TABLE,ui->tableWidget_J15A_13,J15A_13_Data,
                    J15A_13_hiddenData,J15A_13_hiddenHeaders)) { return; }

    if(!uploadTableData(J15B_14_DB_TABLE,ui->tableWidget_J15B_14,J15B_14_Data,
                    J15B_14_hiddenData,J15B_14_hiddenHeaders)) { return; }

    connect(testTimerObj,SIGNAL(timeout()),this,SLOT(updateTestTimer()));
 }

/*************************************************************************
 Function Name - getTableData
 Parameter(s)  - bool
 Return Type   - const QString ,QVector<QStringList>&
 Action        - This function reads data from given table in database and
                 stores data in given buffer.
 *************************************************************************/
 bool JigSelfTest::getTableData(const QString table,QVector<QStringList>& buffer) {
     _qDebug("bool JigSelfTest::getTableData(const QString table,QVector<QStringList>& buffer)");

     QStringList tableHeaders;
     buffer.clear();
     tableHeaders.clear();

     if(!dbObj->getTableData(table,buffer))
     {
         PLAY QMessageBox::warning(nullptr,"Error!!",QString("Fetching table:[%1] failed.").arg(table));
         return false;
     }

     if(!dbObj->getColumnNames(table,tableHeaders))
     {
         PLAY QMessageBox::warning(nullptr,"Error!!\n",QString("Fetching headers of table:[%1] failed.").arg(table));
         return false;
     }

     buffer.prepend(tableHeaders);

     _qDebugE(table," data: [",buffer,"]");
     return true;
 }

/*************************************************************************
 Function Name - getTableData
 Parameter(s)  - bool
 Return Type   - const QString ,QMap<QString, uint32>&
 Action        - This function reads data from given table in database and
                 stores data in given buffer in QMap<key, value> format.
 *************************************************************************/
 bool JigSelfTest::getTableData(const QString table,QMap<QString, uint32>& buffer) {
     _qDebug("bool JigSelfTest::getTableData(const QString table,QMap<QString, uint32>& buffer)");

     QVector<QStringList> tempData;
     buffer.clear();

     tempData.clear();
     if(!dbObj->getTableData(table,tempData))
     {
         PLAY QMessageBox::warning(nullptr,"Error!!",QString("Fetching table:[%1] failed.").arg(table));
         return false;
     }

     for(const QStringList& s : tempData) {
         if(s.size() != 2) {
             PLAY QMessageBox::warning(nullptr,"Error!!",QString("The number of data columns in table:[%1] is not 2.").arg(table));
             return false;
         }
         buffer.insert(s.at(0),s.at(1).toUInt());
     }

     _qDebugE(table," data: [",buffer,"]");
     return true;
 }

/*************************************************************************
 Function Name - uploadTableData
 Parameter(s)  - const QString,QTableWidget*,const QVector<QStringList>,
                 QVector<QStringList>& ,QStringList&
 Return Type   - bool
 Action        - This function uploads given data to given table. It retrives
                 RHS hidden data and stores in hiddenHeader and hiddenData.
 *************************************************************************/
 bool JigSelfTest::uploadTableData(const QString tableName,QTableWidget* table,
                                   const QVector<QStringList> tableData,
                                   QVector<QStringList>& hiddenData,QStringList& hiddenHeader) {
    QTableWidgetItem* item = nullptr;

    int rowNo;
    int columnNo;
    int rowCount;
    int columnCount;
    int dispColumnCount;
    QStringList data;

    table->setRowCount(0);
    timerObj->delayInms(10);//table stable time

    if(tableData.size() < 1) return false;

    hiddenData.clear();
    hiddenHeader.clear();

    //NOTE:The index 0 of tableData contains table header names
    dispColumnCount = (tableData.at(0).indexOf("Remarks") + 1);
    rowCount = tableData.count();
    columnCount = tableData.at(0).count();

    _qDebugE("Rows:",rowCount,", Columns:",columnCount);
    _qDebugE("dispColumns:",dispColumnCount,"","");

    if(rowCount == 0 || columnCount == 0)
    {
        PLAY QMessageBox::warning(nullptr,"Error!!\n",QString("No Data Found in table[%1]").arg(tableName));
        return false;
    }

    table->setColumnCount(dispColumnCount);
    for(int i = 0; i < dispColumnCount;i++)
    {
        item = new QTableWidgetItem(tableData.at(0).at(i));
        item->setFont(font);
        item->setForeground(Qt::blue);
        item->setBackground(QColor("lightblue"));
        table->setHorizontalHeaderItem(i, item);
    }
    table->horizontalHeader()->show();
    G::enableTableHeaderUnderline(table);

    for(int i = dispColumnCount;i < columnCount;i++) hiddenHeader.push_back(tableData.at(0).at(i));

    for(int rowNo = 1;rowNo < rowCount;rowNo++) {
        table->insertRow(rowNo-1);
        for(columnNo = 0;columnNo < dispColumnCount;columnNo++) {
            item = new QTableWidgetItem(tableData.at(rowNo).at(columnNo));
            item->setFont(G::myFont);
            table->setItem(rowNo-1,columnNo,item);
        }
    }

    for(rowNo = 1;rowNo < rowCount;rowNo++) {
        data.clear();
        for(columnNo = dispColumnCount;columnNo < columnCount;columnNo++) {
            data.push_back(tableData.at(rowNo).at(columnNo));
        }
        hiddenData.push_back(data);
    }

    table->resizeRowsToContents();
    table->resizeColumnsToContents();
    table->horizontalHeader()->setStretchLastSection(true);
    table->setColumnWidth(0,60);
    table->update();

    return true;
 }

/*************************************************************************
 Function Name - disableComponents
 Parameter(s)  - void
 Return Type   - void
 Action        - This function disables checkboxes from user.
 *************************************************************************/
 void JigSelfTest::disableComponents(){
     ui->checkBox_J16A->setDisabled(true);
     ui->checkBox_J16B->setDisabled(true);
     ui->checkBox_J15A_J13->setDisabled(true);
     ui->checkBox_J15B_J14->setDisabled(true);
     ui->checkBox_J8->setDisabled(true);
     ui->checkBox_J9->setDisabled(true);
     ui->checkBox_J10->setDisabled(true);
     ui->checkBox_J11_J17->setDisabled(true);
     ui->checkBox_J12_J18->setDisabled(true);

     ui->pushButton_generateReport->setDisabled(true);
 }

/*************************************************************************
 Function Name - enableComponents
 Parameter(s)  - void
 Return Type   - void
 Action        - This function enables checkboxes.
 *************************************************************************/
 void JigSelfTest::enableComponents(){
     ui->checkBox_J16A->setEnabled(true);
     ui->checkBox_J16B->setEnabled(true);
     ui->checkBox_J15A_J13->setEnabled(true);
     ui->checkBox_J15B_J14->setEnabled(true);
     ui->checkBox_J8->setEnabled(true);
     ui->checkBox_J9->setEnabled(true);
     ui->checkBox_J10->setEnabled(true);
     ui->checkBox_J11_J17->setEnabled(true);
     ui->checkBox_J12_J18->setEnabled(true);

     ui->pushButton_generateReport->setEnabled(true);
 }

/*************************************************************************
 Function Name - cleanUI
 Parameter(s)  - void
 Return Type   - void
 Action        - This function cleans/resets UI components.
 *************************************************************************/
 void JigSelfTest::cleanUI() {
    const QString colorStr = "QLabel { background-color: rgb( 221, 226, 228 ); }";
    ui->label_TestStatus ->setStyleSheet(colorStr);
    ui->label_TestStatus ->clear();
    ui->textEdit_dipTestJ16A->clear();
    ui->textEdit_dipTestJ16B->clear();

    for(int i = 0;i <= J12_J18_INDEX;i++) {
        ui->tabWidget->tabBar()->setTabTextColor(i,QColor(Qt::black));
    }

    ui->progressBar->setValue(0);

    clearTable(ui->tableWidget_J8);
    clearTable(ui->tableWidget_J9);
    clearTable(ui->tableWidget_J10);
    clearTable(ui->tableWidget_J11_J17);
    clearTable(ui->tableWidget_J12_J18);
    clearTable(ui->tableWidget_J15A_13);
    clearTable(ui->tableWidget_J15B_14);

    ui->label_timer->setText("00 H : 00 M : 00 S");
 }

/*************************************************************************
 Function Name - clearTable
 Parameter(s)  - QTableWidget*
 Return Type   - bool
 Action        - This function cleans/resets given table.
 *************************************************************************/
 bool JigSelfTest::clearTable(QTableWidget* table) {
     uint8_t resultIndex = 0;
     uint8_t remarkIndex = 0;
     QTableWidgetItem* item = nullptr;

     for(uint8_t i = 0;i < table->columnCount();i++)
     {
         if(     table->horizontalHeaderItem(i)->text().compare(utilityObj._headers1.result) == 0) resultIndex  = i;
         else if(table->horizontalHeaderItem(i)->text().compare(utilityObj._headers1.remark) == 0) remarkIndex  = i;
     }

     if(resultIndex == 0) {
         PLAY QMessageBox::warning(nullptr,"Error!!",QString("SelfTest:[%1] not present in Table Header.")
                              .arg(utilityObj._headers1.result));
         return false;
     }
     if(remarkIndex == 0) {
         PLAY QMessageBox::warning(nullptr,"Error!!",QString("SelfTest:[%1] not present in Table Header.")
                              .arg(utilityObj._headers1.remark));
         return false;
     }

     for(int rowNo = 0;rowNo < table->rowCount();rowNo++)
     {
         item = table->item(rowNo,remarkIndex);
         if(item) table->item(rowNo,remarkIndex)->setText("");

         item = table->item(rowNo,resultIndex);
         if(item) {
             item = table->takeItem(rowNo,resultIndex);
             if(item != nullptr) clearmem(item);
             item = new QTableWidgetItem("");
             item->setFont(G::myFont);
             table->setItem(rowNo,resultIndex,item);
         }
     }

     return true;
 }

/*************************************************************************
 Function Name - dipTest
 Parameter(s)  - uint8_t, int
 Return Type   - bool
 Action        - This function runs DIP test for given card.
 *************************************************************************/
 bool JigSelfTest::dipTest(uint8_t cardNo,int testIndex) {
    _qDebug("bool JigSelfTest::dipTest(uint8_t cardNo,int testIndex)");

    uint8_t data = 0x00;
    QString msg;
    bool isPassed = true;
    QString failedDips, expected, actual;
    int count = 1;

    ui->tabWidget->setCurrentIndex(testIndex);
    msg = testMessage.at(testIndex).at(1);

    stopUpdateTimerLabel();
    QMessageBox::StandardButton resBtn = QMessageBox::question( nullptr, msgTitle,msg,QMessageBox::No | QMessageBox::Yes);
    if (resBtn == QMessageBox::No) return true;
    startUpdateTimerLabel();

    //For 96 pins total 128/8=12 ports
    //After connection Test
    for(int32 port = 0;port < 12;port++) {
        failedDips.clear();
        expected.clear();
        actual.clear();
#if PCI_1758_EN
        if(G::pci1758Obj->readPort(cardNo,port,data,false) != 1) {
            PLAY QMessageBox::warning(nullptr,"Error!!",G::pci1758Obj->getErrorLog());
            return false;
        }
#endif
        if(data != 0xFF) {
            for(int i = 0;i < 8;i++) {
                if(((data >> i) & 1) == 0) {
                    int dipNo = getDipNo(cardNo,port,i);
                    _qDebugE("Failed dipNo:",dipNo,"Expected:",1);

                    if(failedDips.size() == 0) {
                        failedDips = dipMap.key(dipNo,"");
                        expected = "1";actual = "0";
                    }
                    else {
                        failedDips += QString(",%1").arg(dipMap.key(dipNo,""));
                        expected += ",1";actual += ",0";
                    }
                }
            }
            if(isPassed) {
                isPassed = false;
                ui->tabWidget->tabBar()->setTabTextColor(testIndex,QColor(Qt::red));
            }
            switch (testIndex) {
                case J16A_INDEX :
                                  ui->textEdit_dipTestJ16A->append(QString("%1:DIPS:[%2],E:[%3],A:[%4]")
                                                               .arg(count++).arg(failedDips).arg(expected).arg(actual));
                                  break;
                case J16B_INDEX :
                                  ui->textEdit_dipTestJ16B->append(QString("%1:DIPS:[%2],E:[%3],A:[%4]")
                                                               .arg(count++).arg(failedDips).arg(expected).arg(actual));
                default         : break;
            }

            G::updateResultStatus(ui->label_TestStatus,false,isPassUpdated,isFailUpdated);
        }
        else G::updateResultStatus(ui->label_TestStatus,true,isPassUpdated,isFailUpdated);
    }

    msg = testMessage.at(testIndex).at(2);

    stopUpdateTimerLabel();
    resBtn = QMessageBox::question( nullptr, msgTitle,msg,QMessageBox::No | QMessageBox::Yes);
    if (resBtn == QMessageBox::Yes) {
        startUpdateTimerLabel();

        //After connection Removal Test
        for(int32 port = 0;port < 12;port++) {
            failedDips.clear();
            expected.clear();
            actual.clear();
#if PCI_1758_EN
            if(G::pci1758Obj->readPort(cardNo,port,data,false) != 1) {
                PLAY QMessageBox::warning(nullptr,"Error!!",G::pci1758Obj->getErrorLog());
                return false;
            }
#endif
            if(data != 0x00) {
                for(int i = 0;i < 8;i++) {
                    if((data >> i) & 1) {
                        int dipNo = getDipNo(cardNo,port,i);
                        _qDebugE("Failed dipNo:",dipNo,", Expected:",0);

                        if(failedDips.size() == 0) {
                            failedDips = dipMap.key(dipNo,"");
                            expected = "0";actual = "1";
                        }
                        else {
                            failedDips += QString(",%1").arg(dipMap.key(dipNo,""));
                            expected += ",0";actual += ",1";
                        }
                    }
                }
                if(isPassed) {
                    isPassed = false;
                    ui->tabWidget->tabBar()->setTabTextColor(testIndex,QColor(Qt::red));
                }

                switch (testIndex) {
                    case J16A_INDEX :
                                      ui->textEdit_dipTestJ16A->append(QString("%1:DIPS:[%2],E:[%3],A:[%4]")
                                                                   .arg(count++).arg(failedDips).arg(expected).arg(actual));
                                      break;
                    case J16B_INDEX :
                                      ui->textEdit_dipTestJ16B->append(QString("%1:DIPS:[%2],E:[%3],A:[%4]")
                                                                   .arg(count++).arg(failedDips).arg(expected).arg(actual));
                    default         : break;
                }

                G::updateResultStatus(ui->label_TestStatus,false,isPassUpdated,isFailUpdated);
            }
            else G::updateResultStatus(ui->label_TestStatus,true,isPassUpdated,isFailUpdated);
        }
    }
    else {
        switch (testIndex) {
            case J16A_INDEX :
                              ui->textEdit_dipTestJ16A->append("DIP Off state couldn't be tested.");
                              break;
            case J16B_INDEX :
                              ui->textEdit_dipTestJ16B->append("DIP Off state couldn't be tested.");
            default         : break;
        }
    }

    if(isPassed) {
        switch (testIndex) {
            case J16A_INDEX :
                              ui->textEdit_dipTestJ16A->append("ALL Passed");
                              testData[J16A_INDEX][REPORT_RESULT_INDEX] = PASS;
                              testData[J16A_INDEX][REPORT_REMARK_INDEX] = "";
                              break;
            case J16B_INDEX :
                              ui->textEdit_dipTestJ16B->append("ALL Passed");
                              testData[J16B_INDEX][REPORT_RESULT_INDEX] = PASS;
                              testData[J16B_INDEX][REPORT_REMARK_INDEX] = "";
            default         : break;
        }
    }
    else {
        switch (testIndex) {
            case J16A_INDEX :
                              testData[J16A_INDEX][REPORT_RESULT_INDEX] = FAIL;
                              testData[J16A_INDEX][REPORT_REMARK_INDEX] = ui->textEdit_dipTestJ16A->toPlainText();
                              break;
            case J16B_INDEX :
                              testData[J16B_INDEX][REPORT_RESULT_INDEX] = FAIL;
                              testData[J16B_INDEX][REPORT_REMARK_INDEX] = ui->textEdit_dipTestJ16B->toPlainText();
            default         : break;
        }
    }

    G::updateProgressBar(ui->progressBar,progressCount++,totalTestCount);
    return true;
 }

/*************************************************************************
 Function Name - getDipNo
 Parameter(s)  - uint8_t, int32, uint8_t
 Return Type   - int
 Action        - This function calculates and returns dip number.
 *************************************************************************/
 int JigSelfTest::getDipNo(uint8_t cardNo,int32 port,uint8_t bit) {
     return ((cardNo*128) + (port*8) + (bit+1));
 }

/*************************************************************************
 Function Name - getTestCount
 Parameter(s)  - void
 Return Type   - int
 Action        - This function returns number of tests to be performed.
 *************************************************************************/
 int JigSelfTest::getTestCount() {
    int count = 0;

    if(ui->checkBox_J8->isChecked()      ) count += ui->tableWidget_J8     ->rowCount();
    if(ui->checkBox_J9->isChecked()      ) count += ui->tableWidget_J9     ->rowCount();
    if(ui->checkBox_J10->isChecked()     ) count += ui->tableWidget_J10    ->rowCount();
    if(ui->checkBox_J11_J17->isChecked() ) count += ui->tableWidget_J11_J17->rowCount();
    if(ui->checkBox_J12_J18->isChecked() ) count += ui->tableWidget_J12_J18->rowCount();
    if(ui->checkBox_J15A_J13->isChecked()) count += ui->tableWidget_J15A_13->rowCount();
    if(ui->checkBox_J15B_J14->isChecked()) count += ui->tableWidget_J15B_14->rowCount();
	if(ui->checkBox_J16A->isChecked()    ) count++;
    if(ui->checkBox_J16B->isChecked()    ) count++;
	
    return count;
 }

 /*************************************************************************
  Function Name - isOnlyJ16
  Parameter(s)  - void
  Return Type   - bool
  Action        - This function returns true if only J16 test selected, else
                  returns false.
  *************************************************************************/
 bool JigSelfTest::isOnlyJ16() {
     if(ui->checkBox_J8->isChecked()      ||
        ui->checkBox_J9->isChecked()      ||
        ui->checkBox_J10->isChecked()     ||
        ui->checkBox_J11_J17->isChecked() ||
        ui->checkBox_J12_J18->isChecked() ||
        ui->checkBox_J15A_J13->isChecked()||
        ui->checkBox_J15B_J14->isChecked() ) return false;
     else return true;
 }
 
/*************************************************************************
 Function Name - inputAndDopTest
 Parameter(s)  - QTableWidget*,const QStringList,const QVector<QStringList>,
                 const QString,int
 Return Type   - bool
 Action        - This function runs Input and DOP test.
 *************************************************************************/
 bool JigSelfTest::inputAndDopTest(QTableWidget* table,const QStringList hiddenHeader,const QVector<QStringList> hiddenData,
                                   const QString hint,int testIndex) {
     _qDebug("bool JigSelfTest::inputAndDopTest(QTableWidget* table,const QStringList hiddenHeader,const QVector<QStringList> hiddenData,"
             "const QString hint,int testIndex)");
     uint8_t        dopHighIndex = 0;
     uint8_t         dopLowIndex = 0;
     uint8_t            dopIndex = 0;
     uint8_t expValueBeforeIndex = 0;
     uint8_t  expValueAfterIndex = 0;
     uint8_t         resultIndex = 0;
     uint8_t         remarkIndex = 0;

     uint32_t dopToHigh,dopToLow,dopNo;
     uint32_t highDop,lowDop;

     bool testStatus = true;
     double msrdValue = 0.0;

     QString msg;
     QString inToHigh,inToLow,dop,expValueBefore,expValueAfter;
     QTableWidgetItem* item = nullptr;

     ui->tabWidget->setCurrentIndex(testIndex);

     stopUpdateTimerLabel();
     msg = testMessage.at(testIndex).at(1);
     QMessageBox::StandardButton resBtn = QMessageBox::question( nullptr, msgTitle,msg,QMessageBox::No | QMessageBox::Yes);
     if (resBtn == QMessageBox::No) return true;
     startUpdateTimerLabel();

     for(uint8_t i = 0;i < table->columnCount();i++)
     {
         if(     table->horizontalHeaderItem(i)->text().compare(utilityObj._headers1.result) == 0) resultIndex  = i;
         else if(table->horizontalHeaderItem(i)->text().compare(utilityObj._headers1.remark) == 0) remarkIndex  = i;
     }

     for(uint8_t i = 0;i < hiddenHeader.count();i++)
     {
         if(     hiddenHeader.at(i).compare(utilityObj._hidden2.dopHigh            ) == 0) dopHighIndex        = i;
         else if(hiddenHeader.at(i).compare(utilityObj._hidden2.dopLow             ) == 0) dopLowIndex         = i;
         else if(hiddenHeader.at(i).compare(utilityObj._hidden2.dop                ) == 0) dopIndex            = i;
         else if(hiddenHeader.at(i).compare(utilityObj._hidden2.expectedValueBefore) == 0) expValueBeforeIndex = i;
         else if(hiddenHeader.at(i).compare(utilityObj._hidden2.expectedValueAfter ) == 0) expValueAfterIndex  = i;
         else {
             PLAY QMessageBox::warning(nullptr,"Error!!",hint+": Invalid Table(RHS) Header ["+hiddenHeader.at(i)+"]");
             return false;
         }
     }

#if(DMM_PRESENT)
     G::dmmObj->resetDMM();//Reset DMM
     if(!G::dmmObj->setVoltageMode()) {//Set Voltage Mode
         PLAY QMessageBox::warning(nullptr,"Error!!",hint+": Unable to set DMM in Voltage Mode.");
         return false;
     }
#endif

     for(int row = 0;row < table->rowCount();row++) {
         if(G::stopTest) break;
         G::manageScrolling(table,row,row);

         bool isPassed = true;

         inToHigh       = hiddenData.at(row).at(dopHighIndex);
         inToLow        = hiddenData.at(row).at(dopLowIndex);
         dop            = hiddenData.at(row).at(dopIndex);
         expValueBefore = hiddenData.at(row).at(expValueBeforeIndex);
         expValueAfter  = hiddenData.at(row).at(expValueAfterIndex);

         if(!inputMap.contains(inToHigh)) {
             PLAY QMessageBox::warning(nullptr,"Error!!\n",hint+": The UUT_con/Pin:["+inToHigh+"] couldn't be found in Database.");
             return false;
         }
         else {
             dopToHigh = inputMap.value(inToHigh);
         }

         if(!inputMap.contains(inToLow)) {
             PLAY QMessageBox::warning(nullptr,"Error!!\n",hint+": The UUT_con/Pin:["+inToLow+"] couldn't be found in Database.");
             return false;
         }
         else {
             dopToLow = inputMap.value(inToLow);
         }

         if((!dopMap.contains(dop)) && (dop.size() > 0) ) {
             PLAY QMessageBox::warning(nullptr,"Error!!\n",hint+": The UUT_con/Pin:["+dop+"] couldn't be found in Database.");
             return false;
         }
         else {
             dopNo = dopMap.value(dop);
         }

         utilityObj.getLineDops(&dopToHigh,&dopToLow,&highDop,&lowDop);

         //Close Line
         if(G::writeDOP({dopToHigh,highDop,dopToLow,lowDop},ON)!= 1) return false;
         timerObj->delayInms(TEST_DELAY_MS);//Relay Stabilize Delay

         msrdValue = G::dmmObj->getStableMeterValue(DMM_READ_MAX_DELAY_MS);
         _qDebugE("msrdValue Before: [",QString::number(msrdValue, 'g', 8),"]","");

         if(utilityObj.getResult(expValueBefore,msrdValue) == false) {
            isPassed = false;
            msg = QString("Input Failed:[%1,%2],E:[%3],A:[%4]").arg(inToHigh).arg(inToLow).
                    arg(expValueBefore).arg(utilityObj.getDisplayValue(expValueBefore,msrdValue));

            item = table->item(row,resultIndex);
            item->setBackground(Qt::red);
            item->setText(FAIL);

            table->item(row,remarkIndex)->setText(msg);

            G::updateResultStatus(ui->label_TestStatus,false,isPassUpdated,isFailUpdated);
            if(testStatus) {
                ui->tabWidget->tabBar()->setTabTextColor(testIndex,QColor(Qt::red));
                testData[testIndex][REPORT_RESULT_INDEX] = FAIL;
                testStatus = false;
            }
         }
         else {
             G::updateResultStatus(ui->label_TestStatus,true,isPassUpdated,isFailUpdated);
         }

         if(dop.size() > 0 && isPassed) {
             //ON DOP
             if(G::writeDOP(dopNo,ON,G::PCI::P7444)!= 1) return false;
             timerObj->delayInms(TEST_DELAY_MS);//Relay Stabilize Delay

             msrdValue = G::dmmObj->getStableMeterValue(DMM_READ_MAX_DELAY_MS);
             _qDebugE("msrdValue After: [",QString::number(msrdValue, 'g', 8),"]","");

             if(utilityObj.getResult(expValueAfter,msrdValue) == false) {
                 msg = QString("DOP Failed:[%1],E:[%2],A:[%3]").arg(dop).
                         arg(expValueAfter).arg(utilityObj.getDisplayValue(expValueAfter,msrdValue));

                 if(isPassed) {
                     isPassed = false;
                     item = table->item(row,resultIndex);
                     item->setText(FAIL);
                     item->setBackground(Qt::red);

                     if(testStatus) {
                         ui->tabWidget->tabBar()->setTabTextColor(testIndex,QColor(Qt::red));
                         testData[testIndex][REPORT_RESULT_INDEX] = FAIL;
                         testStatus = false;
                     }
                 }

                 item = table->item(row,remarkIndex);
                 QString str = item->text();
                 if(str.size() > 0) str += "\n";
                 str += msg;
                 item->setText(str);

                 G::updateResultStatus(ui->label_TestStatus,false,isPassUpdated,isFailUpdated);
             }
             else {
                 G::updateResultStatus(ui->label_TestStatus,true,isPassUpdated,isFailUpdated);
             }
         }

         if(isPassed) {
             item = table->item(row,resultIndex);
             item->setText(PASS);
             item->setBackground(Qt::green);
         }

         table->resizeRowToContents(row);
         table->update();

         //Off DOP
         if((dop.size() > 0) && (G::writeDOP(dopNo,OFF,G::PCI::P7444)!= 1)) return false;
         //Open Line
         if(G::writeDOP({dopToHigh,highDop,dopToLow,lowDop},OFF)!= 1) return false;

         G::updateProgressBar(ui->progressBar,progressCount++,totalTestCount);
     }

     if(testStatus) testData[testIndex][REPORT_RESULT_INDEX] = PASS;

     testData[testIndex][REPORT_REMARK_INDEX] = getTestRemark(table,remarkIndex);

     ui->tabWidget->setCurrentIndex(testIndex);
     return true;
 }

/*************************************************************************
 Function Name - inputTest
 Parameter(s)  - QTableWidget*,const QStringList,const QVector<QStringList>,
                 const QString,int, int
 Return Type   - bool
 Action        - This function runs Input test.
 *************************************************************************/
 bool JigSelfTest::inputTest(QTableWidget* table,const QStringList hiddenHeader,const QVector<QStringList> hiddenData,
                             const QString hint,int testIndex, int mode) {
     _qDebug("bool JigSelfTest::inputTest(QTableWidget* table,const QString hiddenHeader,const QVector<QStringList> hiddenData,"
             "const QString hint,int testIndex, int mode)");
     uint8_t  dopHighIndex = 0;
     uint8_t   dopLowIndex = 0;
     uint8_t expValueIndex = 0;
     uint8_t   resultIndex = 0;
     uint8_t   remarkIndex = 0;

     uint32_t dopToHigh,dopToLow;
     uint32_t highDop,lowDop;

     bool testStatus = true;
     double msrdValue = 0.0;

     QString msg;
     QString inToHigh,inToLow,expValue;
     QTableWidgetItem* item = nullptr;

     ui->tabWidget->setCurrentIndex(testIndex);

     stopUpdateTimerLabel();
     msg = testMessage.at(testIndex).at(1);
     QMessageBox::StandardButton resBtn = QMessageBox::question( nullptr, msgTitle,msg,QMessageBox::No | QMessageBox::Yes);
     if (resBtn == QMessageBox::No) return true;
     startUpdateTimerLabel();

     for(uint8_t i = 0;i < table->columnCount();i++)
     {
         if(     table->horizontalHeaderItem(i)->text().compare(utilityObj._headers1.result) == 0) resultIndex  = i;
         else if(table->horizontalHeaderItem(i)->text().compare(utilityObj._headers1.remark) == 0) remarkIndex  = i;
     }

     for(uint8_t i = 0;i < hiddenHeader.count();i++)
     {
         if(     hiddenHeader.at(i).compare(utilityObj._hidden2.dopHigh      ) == 0) dopHighIndex  = i;
         else if(hiddenHeader.at(i).compare(utilityObj._hidden2.dopLow       ) == 0) dopLowIndex   = i;
         else if(hiddenHeader.at(i).compare(utilityObj._hidden2.expectedValue) == 0) expValueIndex = i;
         else {
             PLAY QMessageBox::warning(nullptr,"Error!!",hint+": Invalid Table(RHS) Header ["+hiddenHeader.at(i)+"]");
             return false;
         }
     }

#if(DMM_PRESENT)
     G::dmmObj->resetDMM();//Reset DMM
     switch(mode) {
        case RESISTANCE :
                         if(!G::dmmObj->setResistanceMode()) {//Set Resistance Mode
                             PLAY QMessageBox::warning(nullptr,"Error!!",hint+": Unable to set DMM in Resistance Mode.");
                             return false;
                         }
                         break;
        case VOLTAGE   :
                         if(!G::dmmObj->setVoltageMode()) {//Set Voltage Mode
                             PLAY QMessageBox::warning(nullptr,"Error!!",hint+": Unable to set DMM in Voltage Mode.");
                             return false;
                         }
                         break;
        default :
                         PLAY QMessageBox::warning(nullptr,"Error!!",hint+": Unable to set DMM in any Mode.");
                         return false;
     }
#endif

     for(int row = 0;row < table->rowCount();row++) {
         if(G::stopTest) break;
         G::manageScrolling(table,row,row);

         bool isPassed = true;

         inToHigh = hiddenData.at(row).at(dopHighIndex);
         inToLow  = hiddenData.at(row).at(dopLowIndex);
         expValue = hiddenData.at(row).at(expValueIndex);

         if(!inputMap.contains(inToHigh)) {
             PLAY QMessageBox::warning(nullptr,"Error!!\n",hint+": The UUT_con/Pin:["+inToHigh+"] couldn't be found in Database.");
             return false;
         }
         else {
             dopToHigh = inputMap.value(inToHigh);
         }

         if(!inputMap.contains(inToLow)) {
             PLAY QMessageBox::warning(nullptr,"Error!!\n",hint+": The UUT_con/Pin:["+inToLow+"] couldn't be found in Database.");
             return false;
         }
         else {
             dopToLow = inputMap.value(inToLow);
         }

         utilityObj.getLineDops(&dopToHigh,&dopToLow,&highDop,&lowDop);

         //Close Line
         if(G::writeDOP({dopToHigh,highDop,dopToLow,lowDop},ON)!= 1) return false;
         timerObj->delayInms(TEST_DELAY_MS);//Relay Stabilize Delay

         msrdValue = G::dmmObj->getStableMeterValue(DMM_READ_MAX_DELAY_MS);
         _qDebugE("msrdValue: [",QString::number(msrdValue, 'g', 8),"]","");
         if(mode == RESISTANCE) {
             msrdValue -= G::calibResistance;
             _qDebugE("msrdValue After Calib: [",QString::number(msrdValue, 'g', 8),"]","");
         }

         //Open Line
         if(G::writeDOP({dopToHigh,highDop,dopToLow,lowDop},OFF)!= 1) return false;

         if(utilityObj.getResult(expValue,msrdValue) == false) {
            isPassed = false;
            msg = QString("[%1,%2],E:[%3],A:[%4]").arg(inToHigh).arg(inToLow).
                    arg(expValue).arg(utilityObj.getDisplayValue(expValue,msrdValue));

            item = table->item(row,resultIndex);
            item->setBackground(Qt::red);
            item->setText(FAIL);

            table->item(row,remarkIndex)->setText(msg);

            G::updateResultStatus(ui->label_TestStatus,false,isPassUpdated,isFailUpdated);
            if(testStatus) {
                ui->tabWidget->tabBar()->setTabTextColor(testIndex,QColor(Qt::red));
                testData[testIndex][REPORT_RESULT_INDEX] = FAIL;
                testStatus = false;
            }
         }
         else {
             G::updateResultStatus(ui->label_TestStatus,true,isPassUpdated,isFailUpdated);
         }

         if(isPassed) {
             item = table->item(row,resultIndex);
             item->setText(PASS);
             item->setBackground(Qt::green);
         }

         table->resizeRowToContents(row);
         table->update();

         G::updateProgressBar(ui->progressBar,progressCount++,totalTestCount);
     }

     if(testStatus) testData[testIndex][REPORT_RESULT_INDEX] = PASS;

     testData[testIndex][REPORT_REMARK_INDEX] = getTestRemark(table,remarkIndex);

     ui->tabWidget->setCurrentIndex(testIndex);
     return true;
 }

/*************************************************************************
 Function Name - inputTest
 Parameter(s)  - QTableWidget*,const QStringList,const QVector<QStringList>,
                 const QString,int
 Return Type   - bool
 Action        - This function runs Input test.
 *************************************************************************/
 bool JigSelfTest::inputTest(QTableWidget* table,const QStringList hiddenHeader,const QVector<QStringList> hiddenData,
                              const QString hint,int testIndex) {
      _qDebug("bool JigSelfTest::inputTest(QTableWidget* table,const QString hiddenHeader,const QVector<QStringList> hiddenData,"
              "const QString hint,int testIndex)");
      uint8_t  dopHighIndex = 0;
      uint8_t   dopLowIndex = 0;
      uint8_t expValueIndex = 0;
      uint8_t   resultIndex = 0;
      uint8_t   remarkIndex = 0;

      char prevType = '*';//Some defined garbage
      char currType;

      uint32_t dopToHigh,dopToLow;
      uint32_t highDop,lowDop;

      bool testStatus = true;
      double msrdValue = 0.0;

      QString msg;
      QString inToHigh,inToLow,expValue;
      QTableWidgetItem* item = nullptr;

      ui->tabWidget->setCurrentIndex(testIndex);

      stopUpdateTimerLabel();
      msg = testMessage.at(testIndex).at(1);
      QMessageBox::StandardButton resBtn = QMessageBox::question( nullptr, msgTitle,msg,QMessageBox::No | QMessageBox::Yes);
      if (resBtn == QMessageBox::No) return true;
      startUpdateTimerLabel();

      for(uint8_t i = 0;i < table->columnCount();i++)
      {
          if(     table->horizontalHeaderItem(i)->text().compare(utilityObj._headers1.result) == 0) resultIndex  = i;
          else if(table->horizontalHeaderItem(i)->text().compare(utilityObj._headers1.remark) == 0) remarkIndex  = i;
      }

      for(uint8_t i = 0;i < hiddenHeader.count();i++)
      {
          if(     hiddenHeader.at(i).compare(utilityObj._hidden2.dopHigh      ) == 0) dopHighIndex  = i;
          else if(hiddenHeader.at(i).compare(utilityObj._hidden2.dopLow       ) == 0) dopLowIndex   = i;
          else if(hiddenHeader.at(i).compare(utilityObj._hidden2.expectedValue) == 0) expValueIndex = i;
          else {
              PLAY QMessageBox::warning(nullptr,"Error!!",hint+": Invalid Table(RHS) Header ["+hiddenHeader.at(i)+"]");
              return false;
          }
      }

      for(int row = 0;row < table->rowCount();row++) {
          if(G::stopTest) break;
          G::manageScrolling(table,row,row);

          bool isPassed = true;

          inToHigh = hiddenData.at(row).at(dopHighIndex);
          inToLow  = hiddenData.at(row).at(dopLowIndex);
          expValue = hiddenData.at(row).at(expValueIndex);

          if(!inputMap.contains(inToHigh)) {
              PLAY QMessageBox::warning(nullptr,"Error!!\n",hint+": The UUT_con/Pin:["+inToHigh+"] couldn't be found in Database.");
              return false;
          }
          else {
              dopToHigh = inputMap.value(inToHigh);
          }

          if(!inputMap.contains(inToLow)) {
              PLAY QMessageBox::warning(nullptr,"Error!!\n",hint+": The UUT_con/Pin:["+inToLow+"] couldn't be found in Database.");
              return false;
          }
          else {
              dopToLow = inputMap.value(inToLow);
          }

          currType = expValue.contains('V',Qt::CaseInsensitive) ? 'V':'R';
          if(currType != prevType) {
              prevType = currType;
#if(DMM_PRESENT)
              G::dmmObj->resetDMM();//Reset DMM

              if(currType == 'V') {
                 if(!G::dmmObj->setVoltageMode()) {//Set Voltage Mode
                     PLAY QMessageBox::warning(nullptr,"Error!!",hint+": Unable to set DMM in Voltage Mode.");
                     return false;
                 }
              }
              else if(currType == 'R') {
                  if(!G::dmmObj->setResistanceMode()) {//Set Resistance Mode
                      PLAY QMessageBox::warning(nullptr,"Error!!",hint+": Unable to set DMM in Resistance Mode.");
                      return false;
                  }
              }
              else {
                  PLAY QMessageBox::warning(nullptr,"Error!!",hint+": Unable to set DMM in any Mode.");
                  return false;
              }
#endif
          }

          utilityObj.getLineDops(&dopToHigh,&dopToLow,&highDop,&lowDop);

          //Close Line
          if(G::writeDOP({dopToHigh,highDop,dopToLow,lowDop},ON)!= 1) return false;
          timerObj->delayInms(TEST_DELAY_MS);//Relay Stabilize Delay

          msrdValue = G::dmmObj->getStableMeterValue(DMM_READ_MAX_DELAY_MS);
          _qDebugE("msrdValue: [",QString::number(msrdValue, 'g', 8),"]","");
          if(currType == 'R') {
              msrdValue -= G::calibResistance;
              _qDebugE("msrdValue After Calib: [",QString::number(msrdValue, 'g', 8),"]","");
          }

          //Open Line
          if(G::writeDOP({dopToHigh,highDop,dopToLow,lowDop},OFF)!= 1) return false;

          if(utilityObj.getResult(expValue,msrdValue) == false) {
             isPassed = false;
             msg = QString("[%1,%2],E:[%3],A:[%4]").arg(inToHigh).arg(inToLow).
                     arg(expValue).arg(utilityObj.getDisplayValue(expValue,msrdValue));

             item = table->item(row,resultIndex);
             item->setBackground(Qt::red);
             item->setText(FAIL);

             table->item(row,remarkIndex)->setText(msg);

             G::updateResultStatus(ui->label_TestStatus,false,isPassUpdated,isFailUpdated);
             if(testStatus) {
                 ui->tabWidget->tabBar()->setTabTextColor(testIndex,QColor(Qt::red));
                 testData[testIndex][REPORT_RESULT_INDEX] = FAIL;
                 testStatus = false;
             }
          }
          else {
              G::updateResultStatus(ui->label_TestStatus,true,isPassUpdated,isFailUpdated);
          }

          if(isPassed) {
              item = table->item(row,resultIndex);
              item->setText(PASS);
              item->setBackground(Qt::green);
          }

          table->resizeRowToContents(row);
          table->update();

          G::updateProgressBar(ui->progressBar,progressCount++,totalTestCount);
      }

      if(testStatus) testData[testIndex][REPORT_RESULT_INDEX] = PASS;

      testData[testIndex][REPORT_REMARK_INDEX] = getTestRemark(table,remarkIndex);

      ui->tabWidget->setCurrentIndex(testIndex);
      return true;
 }

/*************************************************************************
 Function Name - getTestRemark
 Parameter(s)  - QTableWidget*, int
 Return Type   - QString
 Action        - Returns all test remarks of the given table if present.
 *************************************************************************/
 QString JigSelfTest::getTestRemark(QTableWidget* table, int remarkIndex) {
     _qDebug("QString JigSelfTest::getTestRemark(QTableWidget* table, int remarkIndex)");
     QString remarks;
     QString remark;
     remarks.clear();

     if(remarkIndex >= table->columnCount()) {
         _qDebugE("Error!! Given remarkIndex:",remarkIndex," should be less than total table columns:",table->columnCount());
         return remarks;
     }

     for(int row = 0;row < table->rowCount();row++) {
         remark.clear();
         remark = table->item(row,remarkIndex)->text();
         if(remark.size() > 0) {
             if(remarks.size() > 0) remarks.append("\n");
             remarks.append(remark);
         }
     }
     return remarks;
 }

/*************************************************************************
 Function Name - isAnyTestSelected
 Parameter(s)  - void
 Return Type   - bool
 Action        - This function returns true if any one of the test is selected
                 else returns false.
 *************************************************************************/
 bool JigSelfTest::isAnyTestSelected() {
     if(ui->checkBox_J8->isChecked()       |
        ui->checkBox_J9->isChecked()       |
        ui->checkBox_J10->isChecked()      |
        ui->checkBox_J11_J17->isChecked()  |
        ui->checkBox_J12_J18->isChecked()  |
        ui->checkBox_J15A_J13->isChecked() |
        ui->checkBox_J15B_J14->isChecked() |
        ui->checkBox_J16A->isChecked()     |
        ui->checkBox_J16B->isChecked()) return true;
     return false;
 }

/*************************************************************************
 Function Name - isCredentialsEmpty
 Parameter(s)  - QString&
 Return Type   - bool
 Action        - Checks the user entry credential and keeps the message in
                 the given buffer warningText. If any field is missing returns
                 true, else returns false.
 *************************************************************************/
 bool JigSelfTest::isCredentialsEmpty(QString& warningText)
 {
      bool status = false;
      int userRep,qaRep;

      userRep = ui->lineEdit_UserPerson->text().count();
      qaRep   = ui->lineEdit_QAperson->text().count();
      warningText.clear();

      if(!userRep && !qaRep)
      {
          warningText.push_back("\"User Rep\" and \"QA Rep\" fields are empty.\nDo you want to Continue?");
      }
      else if(!userRep)
      {
          warningText.push_back("\"User Rep\" field is empty.\nDo you want to Continue?");
      }
      else if(!qaRep)
      {
          warningText.push_back("\"QA Rep\" field is empty.\nDo you want to Continue?");
      }

      if(warningText.count() > 0) status = true;//Indicates user field(s) is/are empty
      return status;
 }

/*************************************************************************
 Function Name - setPS
 Parameter(s)  - void
 Return Type   - bool
 Action        - set the PS ON which is OFF previously and saved the off PSs
                 to use during PS reset.
 *************************************************************************/
 bool JigSelfTest::setPS() {
    psPrevOff.clear();
#if PS_PRESENT
    if(!G::psObj->isOutputOn(INPUT_POWER_SUPPLY_PS)) psPrevOff.push_back(INPUT_POWER_SUPPLY_PS);
    if(G::psObj->getErrorLog().size() > 0) {
        PLAY QMessageBox::warning(nullptr,"Error!!",G::psObj->getErrorLog());
        return false;
    }
    if(!G::psObj->isOutputOn(JIG_POWER_SUPPLY_PS)) psPrevOff.push_back(JIG_POWER_SUPPLY_PS);
    if(G::psObj->getErrorLog().size() > 0) {
        PLAY QMessageBox::warning(nullptr,"Error!!",G::psObj->getErrorLog());
        return false;
    }
    if(!G::psObj->isOutputOn(LEVEL_SHIFTER_PS)) psPrevOff.push_back(LEVEL_SHIFTER_PS);
    if(G::psObj->getErrorLog().size() > 0) {
        PLAY QMessageBox::warning(nullptr,"Error!!",G::psObj->getErrorLog());
        return false;
    }
    if(!G::psObj->isOutputOn(COIL_SUPPLY_PS)) psPrevOff.push_back(COIL_SUPPLY_PS);
    if(G::psObj->getErrorLog().size() > 0) {
        PLAY QMessageBox::warning(nullptr,"Error!!",G::psObj->getErrorLog());
        return false;
    }
    if(!G::psObj->isOutputOn(STATUS_SUPPLY_PS)) psPrevOff.push_back(STATUS_SUPPLY_PS);
    if(G::psObj->getErrorLog().size() > 0) {
        PLAY QMessageBox::warning(nullptr,"Error!!",G::psObj->getErrorLog());
        return false;
    }

    _qDebugE("PS to ON:[",psPrevOff,"]","");
    if(!G::psObj->setOutputOn(psPrevOff)) {
         PLAY QMessageBox::warning(nullptr,"Error!!",G::psObj->getErrorLog());
         return false;
    }
#endif

     return true;
 }

/************************************************************************
 Function Name - resetPS
 Parameter(s)  - void
 Return Type   - bool
 Action        - set the PS OFF, which was OFF before the selftest was invoked.
 *************************************************************************/
 bool JigSelfTest::resetPS() {
     _qDebugE("PS to OFF:[",psPrevOff,"]","");
#if PS_PRESENT
     if(!G::psObj->setOutputOff(psPrevOff)) {
          PLAY QMessageBox::warning(nullptr,"Error!!",G::psObj->getErrorLog());
          return false;
     }
#endif

      return true;
 }

/*************************************************************************
 Function Name - generateReport
 Parameter(s)  - const QString
 Return Type   - void
 Action        - calls Report_Generator API to generate report.
 *************************************************************************/
 void JigSelfTest::generateReport(const QString path)
 {
     _qDebug("void JigSelfTest::generateReport(const QString path).");

     QString str;
     QStringList data;
     QVector<QVector<QStringList>> testDataToPrint;

     testData.push_front(reportHeader);
     testDataToPrint.push_back(testData);

     //QStringList results = {"PASS", "PASS", "PASS", "PASS"};
     QString result = ui->label_TestStatus->text().trimmed();

     Report_Generator report(TEST,path);

     report.setSoftwareVersion(G::appVersion);
     report.setSoftwareBuildDate(G::builtDate);
     report.setSoftwareCheckSum(G::softwareChecksum);
     report.setStartDate(G::startDate);
     report.setEndTime(G::endDate);
     report.setStartTime(G::startTime);
     report.setEndTime(G::endTime);
     report.setTestDuration(ui->label_timer->text().trimmed());
     report.setUser(ui->label_UserPerson->text());
     report.setTestName(TEST);
     report.setUserRep(extractValueFromLabel(ui->lineEdit_UserPerson->text()));
     report.setQARep(extractValueFromLabel(ui->lineEdit_QAperson->text()));
     report.setTestResult(statusData);

     report.printParameters();
     report.setTables(testDataToPrint);
     report.printSelfTestPdf(1);
 }

/*************************************************************************
 Function Name - on_pushButton_run_stop_clicked
 Parameter(s)  - void
 Return Type   - void
 Action        - Button Action to handel the Test Run and Stop.
 *************************************************************************/
 void JigSelfTest::on_pushButton_run_stop_clicked()
 {
    _qDebug("void JigSelfTest::on_pushButton_run_stop_clicked().");

    ui->pushButton_run_stop->setDisabled(true);
    if(ui->pushButton_run_stop->text() == "RUN") {
        cleanUI();
        if(!isAnyTestSelected()) {
            PLAY QMessageBox::warning(nullptr,"Error!!","Please select atleast one Test.");
            ui->pushButton_run_stop->setEnabled(true);
            return;
        }

        QString text;
        if(isCredentialsEmpty(text)) {
            QMessageBox::StandardButton resBtn = QMessageBox::question( this, MESSAGE_TITLE,text,QMessageBox::No | QMessageBox::Yes);
            if(resBtn == QMessageBox::No) {
                ui->pushButton_run_stop->setEnabled(true);
                return;
            }
        }

        testData = reportData;
        disableComponents();
        ui->pushButton_run_stop->setText("STOP");
        ui->pushButton_run_stop->setToolTip("Click to Stop Test");
        ui->pushButton_run_stop->setEnabled(true);

        G::stopTest = false;
        isPassUpdated = false;
        isFailUpdated = false;
        timeElapSec = 1;
        timeElapMin = 0;
        timeElapHour = 0;
        progressCount = 0;
        bool noCriticalFailure = true;

        totalTestCount = getTestCount();
        utilityObj.setDateTime(G::startDate,G::startTime);
        startUpdateTimerLabel();

		if(!isOnlyJ16()) {
			if(G::writeDOP(RLC_DMM,ON) != 1) noCriticalFailure = false;//Close DMM Path for measurement

			if(ui->checkBox_J8->isChecked() && noCriticalFailure && (!G::stopTest))
				noCriticalFailure = inputTest(ui->tableWidget_J8,J8_hiddenHeaders,J8_hiddenData,"ST:J8Test",J8_INDEX,RESISTANCE);

			if(ui->checkBox_J9->isChecked() && noCriticalFailure && (!G::stopTest))
				noCriticalFailure = inputTest(ui->tableWidget_J9,J9_hiddenHeaders,J9_hiddenData,"ST:J9Test",J9_INDEX,RESISTANCE);

			if(ui->checkBox_J10->isChecked() && noCriticalFailure && (!G::stopTest))
				noCriticalFailure = inputTest(ui->tableWidget_J10,J10_hiddenHeaders,J10_hiddenData,"ST:J10Test",J10_INDEX,RESISTANCE);

			if(ui->checkBox_J11_J17->isChecked() && noCriticalFailure && (!G::stopTest))
				noCriticalFailure = inputTest(ui->tableWidget_J11_J17,J11_J17_hiddenHeaders,J11_J17_hiddenData,"ST:J11Test",J11_J17_INDEX);

			if(ui->checkBox_J12_J18->isChecked() && noCriticalFailure && (!G::stopTest))
				noCriticalFailure = inputTest(ui->tableWidget_J12_J18,J12_J18_hiddenHeaders,J12_J18_hiddenData,"ST:J12Test",J12_J18_INDEX,VOLTAGE);

			if(ui->checkBox_J15A_J13->isChecked() && noCriticalFailure && (!G::stopTest))
				noCriticalFailure = inputAndDopTest(ui->tableWidget_J15A_13,J15A_13_hiddenHeaders,J15A_13_hiddenData,"ST:J15A_J13Test",J15A_J13_INDEX);

			if(ui->checkBox_J15B_J14->isChecked() && noCriticalFailure && (!G::stopTest))
				noCriticalFailure = inputAndDopTest(ui->tableWidget_J15B_14,J15B_14_hiddenHeaders,J15B_14_hiddenData,"ST:J15B_14Test",J15B_J14_INDEX);

			G::writeDOP(RLC_DMM,OFF);//Open DMM Path after measurement
		}

        if(ui->checkBox_J16A->isChecked() && noCriticalFailure && (!G::stopTest))
            noCriticalFailure = dipTest(0,J16A_INDEX);
        if(ui->checkBox_J16B->isChecked() && noCriticalFailure && (!G::stopTest))
            noCriticalFailure = dipTest(1,J16B_INDEX);

        stopUpdateTimerLabel();
        utilityObj.setDateTime(G::endDate,G::endTime);
        ui->progressBar->setValue(100);
        ui->pushButton_run_stop->setText("RUN");
        ui->pushButton_run_stop->setToolTip("Click to Run Test");
        ui->pushButton_run_stop->setEnabled(true);
        clearmem(stopMsgBox);
        enableComponents();
        DMM::isreadBusy = false;
        PowerSupply::isBusy = false;

    }
    else if(ui->pushButton_run_stop->text() == "STOP") {
        QMessageBox::StandardButton resBtn = QMessageBox::question( this, MESSAGE_TITLE,tr("Do you Want to Stop the Test?\n"),QMessageBox::No | QMessageBox::Yes);
        if (resBtn == QMessageBox::Yes){
            stopMsgBox = new QMessageBox;
            stopMsgBox->setWindowTitle(MESSAGE_TITLE);
            stopMsgBox->setText("Please wait!! Test is being Stopped...  ");
            stopMsgBox->setWindowFlags(Qt::CustomizeWindowHint);
            stopMsgBox->setStandardButtons(nullptr);
            stopMsgBox->show();
            timerObj->delayInms(1);

            G::stopTest = true;
        }
        else{
            ui->pushButton_run_stop->setEnabled(true);
        }
    }
    statusData.push_back(ui->label_TestStatus->text());
    if(autoGenerateReport) on_pushButton_generateReport_clicked();
 }

/*************************************************************************
 Function Name - on_pushButton_generateReport_clicked
 Parameter(s)  - void
 Return Type   - void
 Action        - Button Action to generate Report.
 *************************************************************************/
 void JigSelfTest::on_pushButton_generateReport_clicked()
 {
    _qDebug("void JigSelfTest::on_pushButton_generateReport_clicked()");
    if(reportData.count() == 0)
    {
        PLAY QMessageBox::warning(nullptr,MESSAGE_TITLE,"No Data Found to save.\nPlease Run the Test and Save Again.");
        return;
    }

    QMessageBox* msgBox = new QMessageBox;

    msgBox->setWindowTitle(MESSAGE_TITLE);
    msgBox->setText("Please wait!! Report is being Generated ...");
    msgBox->setWindowFlags(Qt::CustomizeWindowHint);
    msgBox->setStandardButtons(nullptr);
    msgBox->show();
    timerObj->delayInms(1);

    generateReport("");
    timerObj->delayInms(500);

    clearmem(msgBox);
 }

 /*************************************************************************
  Function Name - extractValueFromLabel
  Parameter(s)  - QString
  Return Type   - QString
  Action        - Extracts the required value from label for Report generation.
  *************************************************************************/
  QString JigSelfTest::extractValueFromLabel(QString str)
  {
      QStringList data;

      str = str.trimmed();
      data = str.split(":");
      if(data.count() > 1) str = data.at(1).trimmed().replace(" ","_");

      return str;
  }
