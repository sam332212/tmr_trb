/*************************************  FILE HEADER  *****************************************************************
*
*  File name - ManualIsolation.cpp
*  Creation date and time - 13-NOV-2021 , SAT 16:30 PM IST
*  Author: - Manoj
*  Purpose of the file - All "ManualIsolation Test" modules will be defined.
*
**********************************************************************************************************************/
/********************************************************
 Inclusion of header file
 ********************************************************/
#include "ManualIsolation.h"
#include "ui_ManualIsolation.h"


#include "Support/debugflag.h"
#include "Support/Global.h"

#include <QMessageBox>
#include <QCloseEvent>
#include <QScreen>

/********************************************************
MACRO Definition
********************************************************/
#if MANUALISOLATION_DEBUG
   #define _qDebug(s) qDebug()<<"\n========"<<"Inside "<<s<<"========"
#else
   #define _qDebug(s); {}
#endif

#if MANUALISOLATION_DEBUG_E
   #define _qDebugE(p,q,r,s) qDebug()<<p<<q<<r<<s
#else
   #define _qDebugE(p,q,r,s); {}
#endif

/********************************************************
 CONSTRUCTOR
 ********************************************************/
ManualIsolation::ManualIsolation(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ManualIsolation)
{
    _qDebug("ManualIsolation Constructor");
    ui->setupUi(this);
    move(QGuiApplication::screens().at(0)->geometry().center() - frameGeometry().center());
}

/********************************************************
 DESTRUCTOR
 ********************************************************/
ManualIsolation::~ManualIsolation()
{
    _qDebug("ManualIsolation Destructor");
    delete ui;
}

/*************************************************************************
 Function Name - closeEvent
 Parameter(s)  - QCloseEvent*
 Return Type   - void
 Action        - This function handles this windows close action.
 *************************************************************************/
 void ManualIsolation::closeEvent (QCloseEvent *event)
 {
     _qDebug("void ManualIsolation::closeEvent (QCloseEvent *event).");

     QMessageBox::StandardButton resBtn = QMessageBox::question( this, msgTitle,tr("Do you Want to Close this Window?\n"),QMessageBox::No | QMessageBox::Yes);
     if (resBtn != QMessageBox::Yes)
     {
         event->ignore();
     }
     else
     {
         G::msgBox = new QMessageBox;
         G::msgBox->setWindowTitle(msgTitle);
         G::msgBox->setText("Please wait!! "+msgTitle+" Window is being closed...  ");
         G::msgBox->setWindowFlags(Qt::CustomizeWindowHint);
         G::msgBox->setStandardButtons(nullptr);
         G::msgBox->show();
         QCoreApplication::processEvents(QEventLoop::AllEvents, 100);

         emit closeUI();
     }
 }

 /*************************************************************************
  Function Name - setMsgTitle
  Parameter(s)  - const QString &
  Return Type   - void
  Action        - This function modifies the windows tille variable with the given value.
  *************************************************************************/
 void ManualIsolation::setMsgTitle(const QString &value)
 {
     msgTitle = value;
 }

 /*************************************************************************
  Function Name - initialize
  Parameter(s)  - QString,QString,QString,QString,QString,QString,const QMap<QString, uint32>&
  Return Type   - void
  Action        - This function initializes the required fields.
  *************************************************************************/
 void ManualIsolation::initialize(QString userName, QString unitName,QString uutNumber,
                 QString typeOfTest,QString userRep, QString QARep,
                 const QMap<QString, uint32>& pinMap)
 {
     _qDebug("void ManualIsolation::initialize(QString userName, QString unitName,QString uutNumber,"
             "QString typeOfTest,QString userRep, QString QARep,"
             "const QMap<QString, uint32>& pinMap).");
     QString details = ui->textEdit_Details->toPlainText()
                .arg(userName).arg(unitName).arg(uutNumber)
                .arg(typeOfTest).arg(userRep).arg(QARep);

     ui->textEdit_Details->setText(details);
     this->pinMap = pinMap;
     uutConnPin.clear();
     QStringList connpins = pinMap.keys();

     QStringList connectors;
     QVector<QStringList>pins;
     for(QString connPin : connpins) {
         QStringList list = connPin.split("/");
         //if(list.size() < 2) && (!connPin.contains("Chassis",Qt::CaseInsensitive))) continue;

         if(list.size() >= 2) {
             QString conn = list.at(0);
             QString pin = list.at(1);
             int index = connectors.indexOf(conn);

             if(index == -1) {
                 connectors.push_back(conn);
                 pins.push_back(QStringList(pin));
             }
             else {
                 pins[index].push_back(pin);
             }
         }
         else if(connPin.contains("Chassis",Qt::CaseInsensitive)) {
             chassis = connPin;
         }
     }

     uutConnPin.clear();
     for(int i = 0;i < connectors.count();i++){
         uutConnPin.insert(connectors.at(i),pins.at(i));
     }

     ui->comboBox_SelectConnector->addItems(connectors);
     ui->pushButton_ReadResistance->setDisabled(true);
     ui->textEdit_LogMsg->clear();
 }

 void ManualIsolation::changeSateComboBoxes(bool state)
 {
     ui->comboBox_SelectConnector->setEnabled(state);
     ui->comboBox_SelectHighPin->setEnabled(state);
     ui->comboBox_SelectLowPin->setEnabled(state);
 }

void ManualIsolation::on_comboBox_SelectConnector_currentIndexChanged(const QString &arg1)
{
    QStringList lowPins = uutConnPin.value(arg1);
    if(chassis.size() > 1) lowPins.push_back(chassis);
    ui->comboBox_SelectHighPin->clear();
    ui->comboBox_SelectLowPin->clear();
    ui->comboBox_SelectHighPin->addItems(uutConnPin.value(arg1));
    ui->comboBox_SelectLowPin-> addItems(lowPins);
}

void ManualIsolation::on_pushButton_Clear_clicked()
{
    ui->textEdit_LogMsg->clear();
}

void ManualIsolation::on_pushButton_ConnDisconn_clicked()
{
    uint32_t dopToHigh,dopToLow,highDop,lowDop;
    QString higPin, lowPin;
    int status = 0;
    bool isPassed = true;

    higPin = (ui->comboBox_SelectConnector->currentText().trimmed() + "/" +ui->comboBox_SelectHighPin->currentText().trimmed());
    lowPin = (ui->comboBox_SelectConnector->currentText().trimmed() + "/" +ui->comboBox_SelectLowPin->currentText().trimmed());

    dopToHigh = pinMap.value(higPin);
    dopToLow  = pinMap.value(lowPin);
    _qDebugE("Selected=> dopToHigh:",dopToHigh,", dopToLow:",dopToLow);

    if(dopToHigh == dopToLow) {
        PLAY QMessageBox::warning(nullptr,"Error!!","Error!! From pin and To Pin can't be same.");
    }
    else {
        if(ui->pushButton_ConnDisconn->text() == "Connect") {
            ui->pushButton_ConnDisconn->setText("Disconnect");
            ui->pushButton_ConnDisconn->setToolTip("Click to Disconnect Line");
            ui->pushButton_ReadResistance->setEnabled(true);
            changeSateComboBoxes(false);

            utilityObj.getLineDops(&dopToHigh,&dopToLow,&highDop,&lowDop);
            _qDebugE("dopToHigh:",dopToHigh,", dopToLow:",dopToLow);
            _qDebugE("highDop:"  ,highDop,  ", lowDop:"  ,lowDop);

            status = G::writeDOP(dopToHigh,ON);
            if(status != 1) {
                ui->textEdit_LogMsg->append(QString("INA <%1> ON failed with Error Code <%2>.").arg(dopToHigh).arg(status));
                isPassed = false;
            }

            status = G::writeDOP(dopToLow,ON);
            if(status != 1) {
                ui->textEdit_LogMsg->append(QString("INB <%1> ON failed with Error Code <%2>.").arg(dopToLow).arg(status));
                isPassed = false;
            }

            status = G::writeDOP(highDop,ON);
            if(status != 1) {
                ui->textEdit_LogMsg->append(QString("DOP <%1> ON failed with Error Code <%2>.").arg(highDop).arg(status));
                isPassed = false;
            }

            status = G::writeDOP(lowDop,ON);
            if(status != 1) {
                ui->textEdit_LogMsg->append(QString("DOP <%1> ON failed with Error Code <%2>.").arg(lowDop).arg(status));
                isPassed = false;
            }

            status = G::writeDOP(RLC_DMM,ON);
            if(status != 1) {
                ui->textEdit_LogMsg->append(QString("DMM DOP<%1> ON failed with Error Code <%2>.").arg(RLC_DMM).arg(status));
                isPassed = false;
            }

            if(isPassed) QMessageBox::information(nullptr,"Success!!\n","Line Connecteded successfully.");
        }
        else {
            ui->pushButton_ConnDisconn->setText("Connect");
            ui->pushButton_ConnDisconn->setToolTip("Click to Connect Line");
            ui->pushButton_ReadResistance->setDisabled(true);
            changeSateComboBoxes(true);

            utilityObj.getLineDops(&dopToHigh,&dopToLow,&highDop,&lowDop);
            _qDebugE("dopToHigh:",dopToHigh,", dopToLow:",dopToLow);
            _qDebugE("highDop:"  ,highDop,  ", lowDop:"  ,lowDop);

            status = G::writeDOP(dopToHigh,OFF);
            if(status != 1) {
                ui->textEdit_LogMsg->append(QString("INA <%1> OFF failed with Error Code <%2>.").arg(dopToHigh).arg(status));
                isPassed = false;
            }

            status = G::writeDOP(dopToLow,OFF);
            if(status != 1) {
                ui->textEdit_LogMsg->append(QString("INB <%1> OFF failed with Error Code <%2>.").arg(dopToLow).arg(status));
                isPassed = false;
            }

            status = G::writeDOP(highDop,OFF);
            if(status != 1) {
                ui->textEdit_LogMsg->append(QString("DOP <%1> OFF failed with Error Code <%2>.").arg(highDop).arg(status));
                isPassed = false;
            }

            status = G::writeDOP(lowDop,OFF);
            if(status != 1) {
                ui->textEdit_LogMsg->append(QString("DOP <%1> OFF failed with Error Code <%2>.").arg(lowDop).arg(status));
                isPassed = false;
            }

            status = G::writeDOP(RLC_DMM,OFF);
            if(status != 1) {
                ui->textEdit_LogMsg->append(QString("DMM DOP<%1> OFF failed with Error Code <%2>.").arg(RLC_DMM).arg(status));
                isPassed = false;
            }

            if(isPassed) QMessageBox::information(nullptr,"Success!!\n","Line Disconnecteded successfully.");
        }
    }
}

void ManualIsolation::on_pushButton_ReadResistance_clicked()
{
    double msrdValue = 0.0;
    QString dispValue;

#if(DMM_PRESENT)
       if(!G::dmmObj->setResistanceMode()) {
           PLAY QMessageBox::warning(nullptr,"Error!!","Unable to set DMM to Resistance mode.");
           return;
       }
#endif
       ui->textEdit_LogMsg->append(utilityObj.getCurrentDateTime() + "\n-------------------------");
       ui->textEdit_LogMsg->append("Reading DMM...");
       msrdValue = G::dmmObj->getStableMeterValue(DMM_READ_MAX_DELAY_MS);
       dispValue = utilityObj.getResistanceDisplayValue(msrdValue);
       _qDebugE("msrdValue: [",QString::number(msrdValue, 'g', 8),"], dispValue:",dispValue);

       ui->textEdit_LogMsg->setText(ui->textEdit_LogMsg->toPlainText()+" ...done");
       ui->textEdit_LogMsg->append("Resistance Value :["+dispValue+"]");
}

