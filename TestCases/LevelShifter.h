/*************************************  FILE HEADER  *****************************************************************
*
*  File name - LevelShifter.h
*  Creation date and time - 02-JUNE-2020 , TUE 08:07 AM IS
*  Author: - Manoj
*  Purpose of the file - All "LevelShifter Test" modules will be declared .
*
**********************************************************************************************************************/
#ifndef LEVELSHIFTER_H
#define LEVELSHIFTER_H

/********************************************************
 Inclusion of header file
 ********************************************************/
#include "Support/Global.h"

/********************************************************
 Class Declaration
 ********************************************************/
class LevelShifter
{
public:
    LevelShifter();
    ~LevelShifter();

    static void runTest(const QMap<QString, uint32>& pinMap,
                        const QStringList& hiddenHeader,
                        const QVector<QStringList>& hiddenData);

private:
    bool isParameterInitialized();

    bool measValueAndUpdate(const QMap<QString, uint32>& pinMap,const QString& INsToHigh,const QString& INsToLow,
                            const QString& expectedValue,int rowNo,int msrdValIndex,int resultIndex,bool& isPassed);
    bool setDMM();

    static LevelShifter *thisObj;
};

#endif // LEVELSHIFTER_H
