/*************************************  FILE HEADER  *****************************************************************
*
*  File name - EmiEmc.h
*  Creation date and time - 12-NOV-2021 , FRI 22:57 PM IST
*  Author: - Manoj
*  Purpose of the file - All "EmiEmc Test" modules will be declared .
*
**********************************************************************************************************************/
#ifndef EMIEMC_H
#define EMIEMC_H

#define EMIEMC_TEST_TIMEOUT_SEC 10

/********************************************************
 Inclusion of header file
 ********************************************************/
#include "Support/Global.h"

class EmiEmc
{
public:
    EmiEmc();
    ~EmiEmc();

    static void runTest(const QMap<QString, uint32>& pinMap,
                        const QMap<QString, uint32>& dopMap,
                        const QMap<QString, uint32>& dipMap,
                        const QStringList& hiddenHeader,
                        const QVector<QStringList>& hiddenData,
                        QVector<QVector<QStringList>>& testData,
                        QStringList& testResult
                        );

    bool isParameterInitialized();
    void cleanTable(int msrdValIndex, int resultIndex, int remarkIndex, bool isBegin = true);
    bool readDipsAndUpdate(const QMap<QString, uint32>& dipMap,const QString& connPins,const QString& expectedDipValue,int rowNo,int remarkIndex,int resultIndex,bool isBefore,bool& isPassed);
    bool measValueAndUpdate(const QMap<QString, uint32>& pinMap,const QString& INsToHigh,const QString& INsToLow,
                            const QString& expectedValue,int rowNo,int msrdValIndex,int resultIndex,bool& isPassed);
    bool measValueWithoutUpdate(const QMap<QString, uint32>& pinMap,const QString& INsToHigh,const QString& INsToLow,
                                const QString& expectedValue,int rowNo,int remarkIndex,int resultIndex,bool& isPassed);
    void storeData(QVector<QVector<QStringList>>& testData, QStringList& testResult,int resultIndex);
    bool setPSAndDMM();
    bool resetPS();

    static EmiEmc *thisObj;
};

#endif // EMIEMC_H
