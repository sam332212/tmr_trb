/*************************************  FILE HEADER  *****************************************************************
*
*  File name - JigSelfTest.h
*  Creation date and time - 01-AUG-2021 , SUN 08:50 AM IST
*  Author: - Manoj
*  Purpose of the file - All "JigSelfTest" modules will be declared .
*
**********************************************************************************************************************/
#ifndef JIGSELFTEST_H
#define JIGSELFTEST_H

/********************************************************
 Inclusion of header file
 ********************************************************/
#include <QWidget>
#include <QMessageBox>
#include <QCheckBox>
#include "Support/Timer.h"
#include "Support/Utility.h"
#include "Support/Database.h"
#include "Support/Global.h"

/********************************************************
 MACRO Definition
 ********************************************************/

/********************************************************
 Class Declaration
 ********************************************************/
namespace Ui {
class JigSelfTest;
}

class JigSelfTest : public QWidget
{
    Q_OBJECT

public:
    explicit JigSelfTest(QWidget *parent = nullptr);
    ~JigSelfTest();
    void setMsgTitle(const QString &value);
    bool initSelfTest(QString userRep, QString QARep, bool autoGenerateReport);
    bool resetPS();

signals:
    void close();

private slots:
    void updateTestTimer();
    void on_pushButton_run_stop_clicked();
    void on_pushButton_generateReport_clicked();

private:
    bool uploadTableData(const QString tableName,QTableWidget* table,
                         const QVector<QStringList> tableData,
                         QVector<QStringList>& hiddenData,QStringList& hiddenHeader);
    bool getTableData(const QString table,QVector<QStringList>& buffer);
    bool getTableData(const QString table,QMap<QString, uint32>& buffer);
    void loadTest();
    void cleanUI();
    bool clearTable(QTableWidget* table) ;
    void disableComponents();
    void enableComponents();
    void selectAllTest();
    bool isAnyTestSelected();
    bool isCredentialsEmpty(QString& warningText);
    bool dipTest(uint8_t cardNo,int testIndex);
    int  getDipNo(uint8_t cardNo,int32 port,uint8_t bit);
    bool inputAndDopTest(QTableWidget* table,const QStringList hiddenHeader,const QVector<QStringList> hiddenData,
                         const QString hint,int testIndex);
    bool inputTest(QTableWidget* table,const QStringList hiddenHeader,const QVector<QStringList> hiddenData,
                   const QString hint,int testIndex, int mode);
    bool inputTest(QTableWidget* table,const QStringList hiddenHeader,const QVector<QStringList> hiddenData,
                   const QString hint,int testIndex);
    bool setPS();
	bool isOnlyJ16();
    void closeEvent (QCloseEvent *event);
    void startUpdateTimerLabel();
    void stopUpdateTimerLabel();
    int  getTestCount();
    void generateReport(const QString path);
    QString getTestRemark(QTableWidget* table,int remarkIndex);
    QString extractValueFromLabel(QString str);

private:
    Ui::JigSelfTest *ui;
    Database* dbObj = nullptr;
    QFont font;
    QString msgTitle;
    Utility utilityObj;
    Timer* timerObj = nullptr;
    QTimer* testTimerObj = nullptr;
    QMessageBox* stopMsgBox = nullptr;
    uint timeElapSec;
    uint timeElapMin;
    uint timeElapHour;
    bool isPassUpdated;
    QVector<int>psPrevOff;
    bool isFailUpdated;
    bool autoGenerateReport;
    int progressCount;
    int totalTestCount;
    QVector<QStringList> testData;
    QStringList statusData;

    const QString TEST = "SelfTest";
    const uint8 TOTAL_TEST = 9;
    const uint8 REPORT_RESULT_INDEX = 1;
    const uint8 REPORT_REMARK_INDEX = 2;

    const QString    DIPS_DB_TABLE = "DIPS_Mapping";
    const QString    DOPS_DB_TABLE = "DOPS_Mapping";
    const QString  INPUTS_DB_TABLE = "Inputs_Mapping";
    const QString J15A_13_DB_TABLE = "SelfTestInputConnectorAndDOPS_J15A_J13";
    const QString J15B_14_DB_TABLE = "SelfTestInputConnectorAndDOPS_J15B_J14";
    const QString      J8_DB_TABLE = "SelfTestInputConnector_J8";
    const QString      J9_DB_TABLE = "SelfTestInputConnector_J9";
    const QString     J10_DB_TABLE = "SelfTestInputConnector_J10";
    const QString J11_J17_DB_TABLE = "SelfTestInputConnector_J11_J17";
    const QString J12_J18_DB_TABLE = "SelfTestInputConnector_J12_J18";
    const QString MESSAGE_DB_TABLE = "TestMessage";
    const QString  REPORT_DB_TABLE = "Report";

    QMap<QString, uint32> dipMap;
    QMap<QString, uint32> dopMap;
    QMap<QString, uint32> inputMap;
    QVector<QStringList> J15A_13_Data;
    QVector<QStringList> J15B_14_Data;
    QVector<QStringList> J8_Data;
    QVector<QStringList> J9_Data;
    QVector<QStringList> J10_Data;
    QVector<QStringList> J11_J17_Data;
    QVector<QStringList> J12_J18_Data;
    QVector<QStringList> testMessage;
    QVector<QStringList> reportData;
    QStringList reportHeader;

    QVector<QStringList> J15A_13_hiddenData;
    QVector<QStringList> J15B_14_hiddenData;
    QVector<QStringList> J8_hiddenData;
    QVector<QStringList> J9_hiddenData;
    QVector<QStringList> J10_hiddenData;
    QVector<QStringList> J11_J17_hiddenData;
    QVector<QStringList> J12_J18_hiddenData;

    QStringList J15A_13_hiddenHeaders;
    QStringList J15B_14_hiddenHeaders;
    QStringList J8_hiddenHeaders;
    QStringList J9_hiddenHeaders;
    QStringList J10_hiddenHeaders;
    QStringList J11_J17_hiddenHeaders;
    QStringList J12_J18_hiddenHeaders;

    enum TEST_INDEX {
       J8_INDEX = 0,
       J9_INDEX,
       J10_INDEX,
       J11_J17_INDEX,
       J12_J18_INDEX,
       J15A_J13_INDEX,
       J15B_J14_INDEX,
       J16A_INDEX,
       J16B_INDEX
    };

    enum MODE {
       VOLTAGE = 0,
       RESISTANCE
    };
};

#endif // JIGSELFTEST_H
