#ifndef SELFTEST_H
#define SELFTEST_H

/********************************************************
 Inclusion of header file
 ********************************************************/
#include <QWidget>
#include <QCheckBox>
#include <QMessageBox>
#include "Support/Timer.h"
#include "Support/Utility.h"

/********************************************************
 MACRO Definition
 ********************************************************/
#define RELAY_CARDS     5
#define RELAYS_PER_CARD 100

/********************************************************
 Class Declaration
 ********************************************************/
namespace Ui {
class SelfTest;
}

class SelfTest : public QWidget
{
    Q_OBJECT

public:
    explicit SelfTest(QWidget *parent = nullptr);
    ~SelfTest();
    void setMsgTitle(const QString &value);

signals:
    void close();

private slots:
    void on_pushButton_run_stop_clicked();
    void updateTestTimer();

private:
    void initializeUi();
    void setColumnHeader(int column, QString str);
    void closeEvent (QCloseEvent *event);
    void runTest();
    void getStartAndEndDop(int relayCardNo,int& startLine, int& endLine);
    void disableCardSelection();
    void enableCardSelection();
    void updateResult(int rowNo, bool status);
    void startUpdateTimerLabel();
    void stopUpdateTimerLabel();

    int connectLine(int lineNo);
    int disConnectLine(int lineNo);
    int getLinesToTest();

private:
    Ui::SelfTest *ui;
    QString msgTitle;
    Utility utilityObj;
    Timer* timerObj = nullptr;
    QTimer* testTimerObj = nullptr;
    QVector<QCheckBox*> checkBoxLists;
    QMessageBox* stopMsgBox = nullptr;
    bool isPassed = true;
    bool isUpdated = false;
    uint timeElapSec;
    uint timeElapMin;
    uint timeElapHour;
};

#endif // SELFTEST_H
