/*************************************  FILE HEADER  *****************************************************************
*
*  File name - Load.h
*  Creation date and time - 11-JUNE-2020 , THU 10:00 AM IST
*  Author: - Manoj
*  Purpose of the file - All "Load Test" modules will be declared .
*
**********************************************************************************************************************/
#ifndef LOAD_H
#define LOAD_H

/********************************************************
 Inclusion of header file
 ********************************************************/
#include "Support/Global.h"

/********************************************************
 Class Declaration
 ********************************************************/
class Load
{
public:
    Load();
    ~Load();

    static void runTest(const QMap<QString, uint32>& pinMap,
                        const QMap<QString, uint32>& dopMap,
                        const QMap<QString, uint32>& dipMap,
                        const QStringList& hiddenHeader,
                        const QVector<QStringList>& hiddenData,
                        const Utility::ResetData& resetData
                        );

private:
    bool isParameterInitialized();
    bool onLoadRelays();
    bool offLoadRelays();
    bool readDipsAndUpdate(const QMap<QString, uint32>& dipMap,const QString& connPins,const QString& expectedDipValue,int rowNo,int remarkIndex,int resultIndex,bool isBefore,bool& isPassed);
    bool measValueAndUpdate(const QMap<QString, uint32>& pinMap,const QString& INsToHigh,const QString& INsToLow,
                            const QString& expectedValue,int rowNo,int msrdValIndex,int resultIndex,bool& isPassed);
    bool measValueWithoutUpdate(const QMap<QString, uint32>& pinMap,const QString& INsToHigh,const QString& INsToLow,
                                const QString& expectedValue,int rowNo,int remarkIndex,int resultIndex,bool& isPassed);
    bool setDMM();
    bool resetDevice(const QMap<QString, uint32>& dopMap,
                     const QMap<QString, uint32>& dipMap,
                     const Utility::ResetData& resetData);

    static Load *thisObj;
};

#endif // LOAD_H
