/*************************************  FILE HEADER  *****************************************************************
*
*  File name - ManualPolarity.h
*  Creation date and time - 03-DEC-2021 , FRI 08:38 AM IST
*  Author: - Manoj
*  Purpose of the file - All "ManualPolarity Test" modules will be declared.
*
**********************************************************************************************************************/
/********************************************************
 Inclusion of header file
 ********************************************************/
#ifndef MANUALPOLARITY_H
#define MANUALPOLARITY_H

/********************************************************
 Inclusion of header file
 ********************************************************/
#include "Support/Global.h"

/********************************************************
 Class Declaration
 ********************************************************/
class ManualPolarity
{
public:
    ManualPolarity();
    ~ManualPolarity();

    static void runTest(const QMap<QString, uint32>& pinMap,
                        const QMap<QString, uint32>& dopMap,
                        const QMap<QString, uint32>& dipMap,
                        const QStringList& hiddenHeader,
                        const QVector<QStringList>& hiddenData
                        );

private:
    bool isParameterInitialized();
    bool readDipsAndUpdate(const QMap<QString, uint32>& dipMap,const QString& connPins,const QString& expectedDipValue,int rowNo,int remarkIndex,int resultIndex,bool isBefore,bool& isPassed);
    bool measValueAndUpdate(const QMap<QString, uint32>& pinMap,const QString& INsToHigh,const QString& INsToLow,
                            const QString& expectedValue,int rowNo,int msrdValIndex,int resultIndex,bool& isPassed);
    bool measValueWithoutUpdate(const QMap<QString, uint32>& pinMap,const QString& INsToHigh,const QString& INsToLow,
                                const QString& expectedValue,int rowNo,int remarkIndex,int resultIndex,bool& isPassed);
    bool setDMM();

    static ManualPolarity *thisObj;
};

#endif // MANUALPOLARITY_H
