#include "SelfTest.h"
#include "ui_SelfTest.h"
#include "Support/debugflag.h"
#include "Support/Global.h"

#include <QCloseEvent>

/********************************************************
 MACRO Definition
 ********************************************************/
#if SELFTEST_DEBUG
    #define _qDebug(s) qDebug()<<"\n========"<<"Inside "<<s<<"========"
#else
    #define _qDebug(s); {}
#endif

#if SELFTEST_DEBUG_E
    #define _qDebugE(p,q,r,s) qDebug()<<p<<q<<r<<s
#else
    #define _qDebugE(p,q,r,s); {}
#endif

/********************************************************
 CONSTRUCTOR
 ********************************************************/
SelfTest::SelfTest(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SelfTest),timerObj(new Timer),testTimerObj(new QTimer)
{
    _qDebug("SelfTest Constructor");
    ui->setupUi(this);
    initializeUi();
}

/********************************************************
 DESTRUCTOR
 ********************************************************/
SelfTest::~SelfTest()
{
    _qDebug("SelfTest Destructor");
    for(QCheckBox* chkBox:checkBoxLists) {
        clearmem(chkBox);
    }
    clearmem(timerObj);
    clearmem(testTimerObj);
    delete ui;
}

/*************************************************************************
 Function Name - closeEvent
 Parameter(s)  - QCloseEvent*
 Return Type   - void
 Action        - This function handles this windows close action.
 *************************************************************************/
 void SelfTest::closeEvent (QCloseEvent *event)
 {
     _qDebug("void SelfTest::closeEvent (QCloseEvent *event)");

     if(ui->pushButton_run_stop->text() == "STOP") {
        PLAY QMessageBox::warning(nullptr,"Error!!\n","Please stop the test before closing Window.");
        event->ignore();
     }
     else {
         QMessageBox::StandardButton resBtn = QMessageBox::question( this, msgTitle,tr("Do you Want to Close this Window?\n"),QMessageBox::No | QMessageBox::Yes);
         if (resBtn != QMessageBox::Yes)
         {
             event->ignore();
         }
         else
         {
             emit close();
         }
     }
 }

/*************************************************************************
 Function Name - setMsgTitle
 Parameter(s)  - const QString &
 Return Type   - void
 Action        - This function modifies the windows tille variable with the given value.
 *************************************************************************/
 void SelfTest::setMsgTitle(const QString &value)
 {
     msgTitle = value;
 }

/*************************************************************************
 Function Name - initializeUi
 Parameter(s)  - void
 Return Type   - void
 Action        - This function initializes the GUI components.
 *************************************************************************/
 void SelfTest::initializeUi()
 {
     _qDebug("void SelfTest::initializeUi()");
     QCheckBox* checkBox = nullptr;
     QTableWidgetItem* item = nullptr;

     ui->label_TestStatus->clear();
     connect(testTimerObj,SIGNAL(timeout()),this,SLOT(updateTestTimer()));

     //set Header
     ui->tableWidget->horizontalHeader()->show();
     ui->tableWidget->setColumnCount(2);
     setColumnHeader(0," Relay Card No ");
     setColumnHeader(1,"    Result    ");

     checkBoxLists.clear();
     for(int rowNo = 0;rowNo < RELAY_CARDS;rowNo++) {
         ui->tableWidget->insertRow(rowNo);

         checkBox = new QCheckBox(QString::number(rowNo + 1));
         checkBox->setChecked(true);
         checkBox->setFont(G::myFont);
         ui->tableWidget->setCellWidget(rowNo,0,checkBox);
         checkBoxLists.push_back(checkBox);

         ui->tableWidget->resizeRowToContents(rowNo);

         item = new QTableWidgetItem("");
         item->setFont(G::myFont);
         ui->tableWidget->setItem(rowNo,1, item);
     }
     ui->tableWidget->resizeRowsToContents();
     ui->tableWidget->resizeColumnsToContents();
     ui->tableWidget->horizontalHeader()->setStretchLastSection(true);
     ui->tableWidget->update();
 }

 void SelfTest::setColumnHeader(int column, QString str) {
     QTableWidgetItem* item = new QTableWidgetItem(str);
     item->setForeground(Qt::blue);
     item->setBackground(QColor("lightblue"));
     ui->tableWidget->setHorizontalHeaderItem(column, item);
 }

 void SelfTest::runTest() {
    int startLine, endLine;
    int relayCardNo;
    int count = 0;
    double msrdValue = 0.0;
    QString shorted = "< 2 Ω";
    QString open    = "> 20 MΩ";

    ui->textEdit->clear();
    ui->progressBar->setValue(0);
    ui->label_TestStatus->clear();
    ui->label_TestStatus->setStyleSheet("background-color: rgb(247, 247, 247); ");

    if(G::writeDOP(RLC_DMM,ON) != 1) return;

    int linesToTest = getLinesToTest();

    disableCardSelection();

    startUpdateTimerLabel();
    //check and clear result if present
    for(relayCardNo = 1;relayCardNo <= checkBoxLists.count();relayCardNo++) {
        QTableWidgetItem* item = ui->tableWidget->item(relayCardNo-1,1);
        if(item) {
            item = ui->tableWidget->takeItem(relayCardNo-1,1);
            if(item != nullptr) clearmem(item);
            item = new QTableWidgetItem("");
            item->setFont(G::myFont);
            ui->tableWidget->setItem(relayCardNo-1,1,item);
        }
    }

    for(relayCardNo = 1;relayCardNo <= checkBoxLists.count();relayCardNo++) {
        if(G::stopTest) break;

        if(checkBoxLists.at(relayCardNo-1)->isChecked()) {
            getStartAndEndDop(relayCardNo,startLine,endLine);

            for(;startLine <= endLine;startLine++,count++) {
                if(G::stopTest) break;

                if(connectLine(startLine) != 1) {
                    ui->textEdit->append(QString("Connecting Line IN%1 in card %1 failed.").arg(startLine).arg(relayCardNo));
                    break;
                }
                timerObj->delayInms(TEST_DELAY_MS);//Relay Stabilize Delay
                msrdValue = G::dmmObj->getStableMeterValue(DMM_READ_MAX_DELAY_MS);
                _qDebugE("IN",startLine,":Continuity Value:",utilityObj.getDisplayValue(shorted,msrdValue));
                //should be short now
                if(utilityObj.getResult(shorted,msrdValue) == false) {
                    ui->textEdit->append(QString("Line IN%1 Failed in Continuity.").arg(startLine));
                    updateResult(relayCardNo-1,false);
                }

                if(G::writeDOP(startLine,OFF) != 1) break;
                timerObj->delayInms(TEST_DELAY_MS);//Relay Stabilize Delay
                msrdValue = G::dmmObj->getStableMeterValue(DMM_READ_MAX_DELAY_MS);
                _qDebugE("IN",startLine,":Isolation Value1:",utilityObj.getDisplayValue(open,msrdValue));
                //should be open now
                if(utilityObj.getResult(open,msrdValue) == false) {
                    ui->textEdit->append(QString("Line IN%1 Failed in Isolation1.").arg(startLine));
                    updateResult(relayCardNo-1,false);
                }

                if(G::writeDOP(startLine,ON) != 1) break;
                if((startLine % 2) == 1) {
                    if(G::writeDOP(startLine+1,OFF) != 1) break;
                }
                else {
                    if(G::writeDOP(startLine-1,OFF) != 1) break;
                }
                timerObj->delayInms(TEST_DELAY_MS);//Relay Stabilize Delay
                msrdValue = G::dmmObj->getStableMeterValue(DMM_READ_MAX_DELAY_MS);
                _qDebugE("IN",startLine,":Isolation Value2:",utilityObj.getDisplayValue(open,msrdValue));
                //should be open now
                if(utilityObj.getResult(open,msrdValue) == false) {
                    ui->textEdit->append(QString("Line IN%1 Failed in Isolation2.").arg(startLine));
                    updateResult(relayCardNo-1,false);
                }

                if(disConnectLine(startLine) != 1) {
                    _qDebugE("Disconnecting Line Failed.","","","");
                    break;
                }
                updateResult(relayCardNo-1,true);
                ui->progressBar->setValue(static_cast<int>(100.00 * (count + 1)/linesToTest));
                timerObj->delayInms(5);
            }
        }
        if(startLine <= endLine) break;
    }

    G::writeDOP(RLC_DMM,OFF);

    if(G::stopTest) {
        clearmem(stopMsgBox);
    }
    else if(relayCardNo <= checkBoxLists.count()) {
        ui->label_TestStatus->setText("FAIL");
        ui->label_TestStatus->setStyleSheet("QLabel { background-color : red;}");
    }

    stopUpdateTimerLabel();

    enableCardSelection();
    ui->progressBar->setValue(100);
    ui->pushButton_run_stop->setText("RUN");
    ui->pushButton_run_stop->setToolTip("Click to Run Test");
    ui->pushButton_run_stop->setEnabled(true);
 }

/*************************************************************************
 Function Name - initializeUi
 Parameter(s)  - void
 Return Type   - void
 Action        - This function makes relays ON to connect the one end of
                 given line to high and other end to low.
                 Returns 1 if success.
 *************************************************************************/
 int SelfTest::connectLine(int lineNo){
     QVector<uint32_t>dops;
     dops.clear();
     if((lineNo % 2) == 1) {//ODD
         dops.append(lineNo); dops.append(lineNo+1);
         dops.append(RLC_P2A);dops.append(RLC_R2B);
     }
     else {//EVEN
         dops.append(lineNo-1); dops.append(lineNo);
         dops.append(RLC_Q2A);  dops.append(RLC_S2B);
     }

     return G::writeDOP(dops,ON);
 }

 int SelfTest::disConnectLine(int lineNo){
     QVector<uint32_t>dops;
     dops.clear();
     if((lineNo % 2) == 1) {//ODD
         dops.append(lineNo); dops.append(lineNo+1);
         dops.append(RLC_P2A);dops.append(RLC_R2B);
     }
     else {//EVEN
         dops.append(lineNo-1); dops.append(lineNo);
         dops.append(RLC_Q2A);  dops.append(RLC_S2B);
     }

     return G::writeDOP(dops,OFF);
 }

 void SelfTest::getStartAndEndDop(int relayCardNo,int& startLine, int& endLine) {
     switch(relayCardNo) {
         case 1 :
                  startLine = 1;
                  endLine = 100;
                  break;
         case 2 :
                  startLine = 101;
                  endLine = 200;
                  break;
         case 3 :
                  startLine = 201;
                  endLine = 300;
                  break;
         case 4 :
                  startLine = 301;
                  endLine = 400;
                  break;
         case 5 :
                  startLine = 401;
                  endLine = 500;
                  break;
     }
 }

 void SelfTest::disableCardSelection() {
     for(QCheckBox* checkBox : checkBoxLists) checkBox->setDisabled(true);
 }

 void SelfTest::enableCardSelection() {
     for(QCheckBox* checkBox : checkBoxLists) checkBox->setEnabled(true);
 }

 int SelfTest::getLinesToTest() {
     int count = 0;
     for(QCheckBox* checkBox : checkBoxLists) {
         if(checkBox->isChecked()) {
             count += 100;
         }
     }
     return count;
 }

/*************************************************************************
 Function Name - updateResult
 Parameter(s)  - int, bool
 Return Type   - void
 Action        - Updates result in table as well as in status.
 *************************************************************************/
 void SelfTest::updateResult(int rowNo, bool status) {
     if(!isUpdated) {
         if(status) {
             ui->tableWidget->item(rowNo,1)->setText(PASS);
             ui->tableWidget->item(rowNo,1)->setBackground(Qt::green);
             ui->label_TestStatus->setText(PASS);
             ui->label_TestStatus->setStyleSheet("QLabel { background-color : #3AEA02;}");
         }
         else {
             ui->tableWidget->item(rowNo,1)->setText(FAIL);
             ui->tableWidget->item(rowNo,1)->setBackground(Qt::red);
             ui->label_TestStatus->setText("FAIL");
             ui->label_TestStatus->setStyleSheet("QLabel { background-color : red;}");
         }
         isUpdated = true;
     }
     else if(!status && ui->tableWidget->item(rowNo,1)->text() != FAIL) {
         ui->tableWidget->item(rowNo,1)->setText(FAIL);
         ui->tableWidget->item(rowNo,1)->setBackground(Qt::red);
         ui->label_TestStatus->setText("FAIL");
         ui->label_TestStatus->setStyleSheet("QLabel { background-color : red;}");
     }
 }

 /*************************************************************************
  Function Name - startUpdateTimerLabel
  Parameter(s)  - void
  Return Type   - void
  Action        - This function starts timer to update test_timer_label.
  *************************************************************************/
 void SelfTest::startUpdateTimerLabel()
 {
    timeElapSec = 0;
    timeElapMin = 0;
    timeElapHour = 0;

    testTimerObj->start(1000);
 }

/*************************************************************************
 Function Name - stopUpdateTimerLabel
 Parameter(s)  - void
 Return Type   - void
 Action        - This function stops the test timer.
 *************************************************************************/
 void SelfTest::stopUpdateTimerLabel()
 {
   testTimerObj->stop();
 }

 /*************************************************************************
 Function Name - updateTestTimer
 Parameter(s)  - void
 Return Type   - void
 Action        - This function updates the test_timer_label when test runs.
 *************************************************************************/
 void SelfTest::updateTestTimer()
 {
    QString text;
    timeElapSec++;
    if(timeElapSec > 59)
    {
        timeElapMin++;
        timeElapSec = 0;
    }
    if(timeElapMin > 59)
    {
        timeElapHour++;
        timeElapMin = 0;
        timeElapSec = 0;
    }
    text.sprintf("%02d H : %02d M : %02d S",timeElapHour,timeElapMin,timeElapSec);
    ui->label_timer->setText(text);
 }


 void SelfTest::on_pushButton_run_stop_clicked()
 {
    ui->pushButton_run_stop->setDisabled(true);

    if(ui->pushButton_run_stop->text() == "RUN") {
        ui->pushButton_run_stop->setText("STOP");
        ui->pushButton_run_stop->setToolTip("Click to Stop Test");
        ui->pushButton_run_stop->setEnabled(true);
        G::stopTest = false;
        runTest();
    }
    else if(ui->pushButton_run_stop->text() == "STOP") {
        QMessageBox::StandardButton resBtn = QMessageBox::question( this, MESSAGE_TITLE,tr("Do you Want to Stop the Test?\n"),QMessageBox::No | QMessageBox::Yes);
        if (resBtn == QMessageBox::Yes)
        {
            stopMsgBox = new QMessageBox;
            stopMsgBox->setWindowTitle(msgTitle);
            stopMsgBox->setText("Please wait!! Test is being Stopped...  ");
            stopMsgBox->setWindowFlags(Qt::CustomizeWindowHint);
            stopMsgBox->setStandardButtons(nullptr);
            stopMsgBox->show();
            timerObj->delayInms(1);

            G::stopTest = true;
        }
        else
        {
            ui->pushButton_run_stop->setEnabled(true);
        }
    }
 }

