/*************************************  FILE HEADER  *****************************************************************
*
*  File name - LevelShifter.cpp
*  Creation date and time - 02-JUNE-2020 , TUE 08:07 AM IST
*  Author: - Manoj
*  Purpose of the file - All "LevelShifter Test" modules will be defined .
*
**********************************************************************************************************************/
/********************************************************
 Inclusion of header file
 ********************************************************/
#include "LevelShifter.h"
#include "Support/debugflag.h"
#include "Serials/DevicesHandler.h"
#include "Support/Utility.h"
#include "Support/Timer.h"
#include "Support/Global.h"

/********************************************************
 MACRO Definition
 ********************************************************/
#if LEVELSHIFTER_DEBUG
    #define _qDebug(s) qDebug()<<"\n-------- Inside "<<s<<" --------"
#else
    #define _qDebug(s); {}
#endif

#if LEVELSHIFTER_DEBUG_E
    #define _qDebugE(p,q,r,s) qDebug()<<p<<q<<r<<s
#else
    #define _qDebugE(p,q,r,s); {}
#endif

LevelShifter* LevelShifter::thisObj = nullptr;
const QString TITLE = "Level Shifter Test";

/********************************************************
 Function Definition
 ********************************************************/
/*************************************************************************
 Function Name - LevelShifter
 Parameter(s)  - void
 Return Type   - void
 Action        - Creates the object.
 *************************************************************************/
 LevelShifter::LevelShifter()
 {
    _qDebug("LevelShifter Constructor");
    thisObj = this;
 }

/*************************************************************************
 Function Name - ~LevelShifter
 Parameter(s)  - void
 Return Type   - void
 Action        - Destroys the object.
 *************************************************************************/
 LevelShifter::~LevelShifter()
 {
     _qDebug("LevelShifter Destructor");
 }

/*************************************************************************
 Function Name - runTest
 Parameter(s)  - const QMap<QString, uint32>&, const QStringList&, const QVector<QStringList>&
 Return Type   - void
 Action        - Runs the test.
 *************************************************************************/
 void LevelShifter::runTest(const QMap<QString, uint32>& pinMap,const QStringList& hiddenHeader,const QVector<QStringList>& hiddenData)
 {
     _qDebug("void LevelShifter::runTest(const QMap<QString, uint32>& pinMap,const QStringList& hiddenHeader,const QVector<QStringList>& hiddenData)");

     uint8_t   inHighIndex = 0;
     uint8_t    inLowIndex = 0;
     uint8_t expValueIndex = 0;
     uint8_t  msrdValIndex = 0;
     uint8_t   resultIndex = 0;
     uint8_t   remarkIndex = 0;

     int rowNo;
     int pausePoint = UINT_MAX;

     QString insToHigh,insToLow,expValue,expValueAfter;
     QStringList connPins;
     QVector<int> selectedRowNumbers;
     QTableWidgetItem* item = nullptr;
     QCheckBox* chkBox = nullptr;
     QString msgTitle = "LevelShifter Test";

     Utility utilityObj;
     Timer   timerObj;
     bool       isPassed = true;
     bool  isPassUpdated = false;
     bool  isFailUpdated = false;
     bool isRepeatedTest = false;

     if(thisObj->isParameterInitialized() == false)
     {
         PLAY QMessageBox::warning(nullptr,"Error!!","LevelShifter: All Test Parameters are not Initialized.");
     }
     else
     {
         isRepeatedTest = (G::testResultStatus->text().size() > 0);
         G::cleanUi();

         //Getting table header index
         for(uint8_t i = 0;i < G::testTable->columnCount();i++)
         {
             if(     G::testTable->horizontalHeaderItem(i)->text().compare(utilityObj._headers1.measuredValue) == 0) msrdValIndex = i;
             else if(G::testTable->horizontalHeaderItem(i)->text().compare(utilityObj._headers1.result)        == 0) resultIndex  = i;
             else if(G::testTable->horizontalHeaderItem(i)->text().compare(utilityObj._headers1.remark)        == 0) remarkIndex  = i;
         }
         if(msrdValIndex == 0) {
             PLAY QMessageBox::warning(nullptr,"Error!!",QString("LevelShifter:[%1] not present in Table Header.")
                                  .arg(utilityObj._headers1.measuredValue));
             return;
         }
         if(resultIndex == 0) {
             PLAY QMessageBox::warning(nullptr,"Error!!",QString("LevelShifter:[%1] not present in Table Header.")
                                  .arg(utilityObj._headers1.result));
             return;
         }
         if(remarkIndex == 0) {
             PLAY QMessageBox::warning(nullptr,"Error!!",QString("LevelShifter:[%1] not present in Table Header.")
                                  .arg(utilityObj._headers1.remark));
             return;
         }
         if((hiddenHeader.count() == 0) || (hiddenData.count() == 0)) {
             PLAY QMessageBox::warning(nullptr,"Error!!","LevelShifter: RHS data is not present in Database.");
             _qDebugE("hiddenHeader:",hiddenHeader,"\nhiddenData:",hiddenData);
             return;
         }

         for(uint8_t i = 0;i < hiddenHeader.count();i++)
         {
             if(     hiddenHeader.at(i).compare(utilityObj._hidden1.INsToHigh)     == 0) inHighIndex   = i;
             else if(hiddenHeader.at(i).compare(utilityObj._hidden1.INsToLow)      == 0) inLowIndex    = i;
             else if(hiddenHeader.at(i).compare(utilityObj._hidden1.expectedValue) == 0) expValueIndex = i;
             else {
                 PLAY QMessageBox::warning(nullptr,"Error!!","LevelShifter: Invalid Table(RHS) Header ["+hiddenHeader.at(i)+"]");
                 return;
             }
         }

         G::showInfoMessage(TITLE,"Please wait!! Preparing Test...  ");
         timerObj.delayInms(10);

         //Get selected row Number and clean the table Columns
         selectedRowNumbers.clear();
         for(int rowNo = 0;rowNo < G::testTable->rowCount();rowNo++)
         {
            chkBox = static_cast<QCheckBox*>(G::testTable->cellWidget(rowNo,0));
            if(chkBox == nullptr) {
                pausePoint = rowNo;
                selectedRowNumbers.push_back(rowNo);
                continue;
            }
            else if(chkBox->isChecked())  selectedRowNumbers.push_back(rowNo);

            if(isRepeatedTest) {
                item = G::testTable->item(rowNo,msrdValIndex);
                if(item) G::testTable->item(rowNo,msrdValIndex)->setText("");

                item = G::testTable->item(rowNo,remarkIndex);
                if(item) G::testTable->item(rowNo,remarkIndex)->setText("");

                item = G::testTable->item(rowNo,resultIndex);
                if(item) {
                    item = G::testTable->takeItem(rowNo,resultIndex);
                    if(item != nullptr) clearmem(item);
                    item = new QTableWidgetItem("");
                    item->setFont(G::myFont);
                    G::testTable->setItem(rowNo,resultIndex,item);
                }
            }
         }
         if(selectedRowNumbers.at(selectedRowNumbers.count()-1) == pausePoint) selectedRowNumbers.pop_back();
         G::closeTestMessage();

         if(pausePoint != selectedRowNumbers.at(0)) {
             G::showInfoMessage(G::testStartMessage);
             INFO if (QMessageBox::No == QMessageBox(QMessageBox::Information, "LevelShifter Test",
                                                "Do you want to continue the test ?",
                                                QMessageBox::Yes|QMessageBox::No).exec()) return;
         }

         if(!thisObj->setDMM()) return;
         G::enableRunButton(true);

         utilityObj.setDateTime(G::startDate,G::startTime);
         if(G::writeDOP(RLC_DMM,ON) == 1)//Close DMM Path for LevelShifter measurement
         {
              G::testTimer->start(1000);
              _qDebugE("selectedRowNumbers:",selectedRowNumbers.count(),", pausePoint:",pausePoint);
              //Operate other Dops
              for(int index = 0;index < selectedRowNumbers.count();index++)
              {
                  if(G::stopTest) break;
                  isPassed = true;//Start of the tests in a row

                  G::testRunStatus->setStyleSheet("QLabel { background-color : #3AEA02;}");

                  rowNo = selectedRowNumbers[index];
                  G::manageScrolling(index,rowNo);
                  _qDebugE("selectedRow:",rowNo+1,"","");

                  //Decide to show popup for next test slot
                  if(rowNo == pausePoint) {
                      G::testTimer->stop();
                      //show popup here
                      G::showInfoMessage(G::testInterMediateMessage);
                      INFO if (QMessageBox::No == QMessageBox(QMessageBox::Information, "LevelShifter Test",
                                                         "Do you want to continue the test ?",
                                                         QMessageBox::Yes|QMessageBox::No).exec()) break;
                      G::testTimer->start(1000);
                      continue;
                  }

                  insToHigh = hiddenData.at(inHighIndex  ).at(rowNo);
                  insToLow  = hiddenData.at(inLowIndex   ).at(rowNo);
                  expValue  = hiddenData.at(expValueIndex).at(rowNo);

                  if(!thisObj->measValueAndUpdate(pinMap,insToHigh,insToLow,expValue,rowNo,msrdValIndex,resultIndex,isPassed)) break;
                  if(!G::updateResultStatus(isPassed, isPassUpdated, isFailUpdated)) {
                      PLAY QMessageBox::warning(nullptr,"Error!!","LevelShifter: Couldn't update the result status in UI.");
                  }

                  if(isPassed) {
                      G::testTable->item(rowNo,resultIndex)->setText(PASS);
                      G::testTable->item(rowNo,resultIndex)->setBackground(Qt::green);
                  }

                  G::progressBar->setValue(static_cast<int>(100.00 * (index + 1)/selectedRowNumbers.count()));
                  G::testRunStatus->setStyleSheet("QLabel { background-color : red;}");

                  timerObj.delayInms(5);
              }//outer for loop runs for number of table rows
              G::progressBar->setValue(100);
              G::testRunStatus->setStyleSheet("QLabel { background-color : red;}");

              G::writeDOP(RLC_DMM,OFF);//Open DMM Path after measurement

              G::testTimer->stop();
         }//if block when DMM Path close successfull for measurement
         utilityObj.setDateTime(G::endDate,G::endTime);
         DMM::isreadBusy = false;
      }
 }

/*************************************************************************
 Function Name - isParameterInitialized
 Parameter(s)  - void
 Return Type   - bool
 Action        - Checks each parameter's initializations. It returns false
                 if one of the parameter is not initialized else returns true.
 *************************************************************************/
 bool LevelShifter::isParameterInitialized()
 {
     if((G::testTable == nullptr)  || (G::testResultStatus == nullptr) || (G::testRunStatus == nullptr) ||
       (G::progressBar == nullptr) || (G::dmmObj == nullptr)           || (G::psObj == nullptr)         ||
       (G::pci1758Obj == nullptr)  || (G::testTimer == nullptr))
     {
        return false;
     }
     else
     {
         return true;
     }
 }

/*************************************************************************
  Function Name - measValueAndUpdate
  Parameter(s)  - const QMap<QString, uint32>&, const QString&, const QString&,
                  const QString&, int, int, int, bool&
  Return Type   - bool
  Action        - Opens the High and Low DOP lines, measures the voltage and
                  comapres with given expectedValue . Returns true if the process
                  is pass else returns false. The value of isPassed is true
                  if all the measured values match with the expectedValue and
                  else the value of isPassed will be false.
  *************************************************************************/
  bool LevelShifter::measValueAndUpdate(const QMap<QString, uint32>& pinMap,const QString& INsToHigh,const QString& INsToLow,
                                     const QString& expectedValue,int rowNo,int msrdValIndex,int resultIndex,bool& isPassed)
  {
       _qDebug("bool LevelShifter::measValueAndUpdate(const QMap<QString, uint32>& dopMap,const QString& INsToHigh,const QString& INsToLow,"
               "const QString& expectedValue,int rowNo,int msrdValIndex,int resultIndex,int remarkIndex,bool& isPassed)");
       Utility utilityObj;
       Timer timerObj;
       uint32_t dopToHigh,dopToLow;
       uint32_t highDop,lowDop;
       double msrdValue = 0.0;

        if(INsToHigh.size() == 0 || INsToLow.count() == 0) {
            PLAY QMessageBox::warning(nullptr,"Error!!",QString("LevelShifter: Empty Data : Number of INs To High:[%1], Number of INs To Low:[%2] at ROW:[%3].")
                                 .arg(INsToHigh.size()).arg(INsToLow.count()).arg(rowNo+1));
            return false;
        }

        if(rowNo > G::testTable->rowCount()-1) {
           PLAY QMessageBox::warning(nullptr,"Error!!\n","Invalid Row Number.\nGiven ROW:["+QString(rowNo+1)+"].");
           return false;
        }
        if(msrdValIndex > G::testTable->columnCount()-1) {
           PLAY QMessageBox::warning(nullptr,"Error!!\n","Invalid index to update Measured value.\nGiven index:["+QString::number(msrdValIndex) +"] and Given ROW:["+QString::number(rowNo+1)+"].");
           return false;
        }
        if(resultIndex > G::testTable->columnCount()-1) {
            PLAY QMessageBox::warning(nullptr,"Error!!\n","Invalid index to update Result.\nGiven index:["+QString::number(resultIndex) +"] and Given ROW:["+QString::number(rowNo+1)+"].");
            return false;
        }

        if(!pinMap.contains(INsToHigh)) {
            PLAY QMessageBox::warning(nullptr,"Error!!\n","The UUT_con/Pin:["+INsToHigh+"] couldn't be found in Database.");
            return false;
        }
        else {
            dopToHigh = pinMap.value(INsToHigh);
        }

        if(!pinMap.contains(INsToLow)) {
            PLAY QMessageBox::warning(nullptr,"Error!!\n","The UUT_con/Pin:["+INsToLow+"] couldn't be found in Database.");
            return false;
        }
        else {
            dopToLow = pinMap.value(INsToLow);
        }

        utilityObj.getLineDops(&dopToHigh,&dopToLow,&highDop,&lowDop);

        //Dop ON
        if(G::writeDOP({dopToHigh,highDop,dopToLow,lowDop},ON)!= 1) return false;
        timerObj.delayInms(TEST_DELAY_MS);//Relay Stabilize Delay

        msrdValue = G::dmmObj->getStableMeterValue(DMM_READ_MAX_DELAY_MS);
        _qDebugE("msrdValue: [",QString::number(msrdValue, 'g', 8),"]","");
        msrdValue -= G::calibVoltage;
        msrdValue = utilityObj.mod(msrdValue);

        //Dop OFF
        if(G::writeDOP({dopToHigh,highDop,dopToLow,lowDop},OFF)!= 1) return false;
        timerObj.delayInms(TEST_DELAY_MS);//Relay Stabilize Delay

        //Show Measured Value
        QString displayValueStr = utilityObj.getDisplayValue(expectedValue,msrdValue);
        G::testTable->item(rowNo,msrdValIndex)->setText(displayValueStr);
        G::testTable->resizeRowToContents(rowNo);
        G::testTable->update();

        if(utilityObj.getResult(expectedValue,msrdValue) == false)
        {
            if(!G::updateResult(false,rowNo,resultIndex,isPassed)) {
                PLAY QMessageBox::warning(nullptr,"Error!!",QString("LevelShifter: Couldn't update the result in table at ROW:[%1].")
                                     .arg(rowNo+1));
                return false;
            }
            isPassed = false;
        }

        return true;
  }

  /*************************************************************************
   Function Name - setDMM
   Parameter(s)  - void
   Return Type   - bool
   Action        - Sets DMM to corresponding mode.
   *************************************************************************/
  bool LevelShifter::setDMM()
  {
      Timer timer;

      G::showInfoMessage(TITLE,"Please wait!! DMM is being set to Voltage Mode...  ");
      timer.delayInms(1);

      DMM::isreadBusy = true;
      while(!G::handlerObj->isDMMReady) timer.delayInms(1);

#if(DMM_PRESENT)
         G::dmmObj->resetDMM();//Reset DMM
         if(!G::dmmObj->setVoltageMode()) {//Set Voltage Mode for LevelShifter Test
             G::closeTestMessage();
             PLAY QMessageBox::warning(nullptr,"Error!!","LevelShifter: Unable to set DMM in Voltage Mode.");
             return false;
         }
#endif
      G::closeTestMessage();
      return true;
  }
