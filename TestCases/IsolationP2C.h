/*************************************  FILE HEADER  *****************************************************************
*
*  File name - IsolationP2C.h
*  Creation date and time - 24-MAY-2020 , SUN 05:39 PM IST
*  Author: - Manoj
*  Purpose of the file - All "Isolation Pin To Chassis" modules will be declared .
*
**********************************************************************************************************************/
#ifndef ISOLATIONP2C_H
#define ISOLATIONP2C_H

/********************************************************
 Inclusion of header file
 ********************************************************/
#include "Support/Global.h"

/********************************************************
 Class Declaration
 ********************************************************/
class IsolationP2C
{
public:
    IsolationP2C();
    ~IsolationP2C();

    static void runTest(const QMap<QString, uint32>& pinMap,
                        const QStringList& hiddenHeader,
                        const QVector<QStringList>& hiddenData);

private:
    bool isParameterInitialized();

    bool measValueAndUpdate(const QMap<QString, uint32>& pinMap,const QString& INsToHigh,
                            const QString& expectedValue,int rowNo,int msrdValIndex,int resultIndex,bool& isPassed);
    bool setDMM();

    static IsolationP2C *thisObj;
};

#endif // ISOLATIONP2C_H
