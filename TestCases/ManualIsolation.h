/*************************************  FILE HEADER  *****************************************************************
*
*  File name - ManualIsolation.h
*  Creation date and time - 13-NOV-2021 , SAT 16:30 PM IST
*  Author: - Manoj
*  Purpose of the file - All "ManualIsolation Test" modules will be declared.
*
**********************************************************************************************************************/
#ifndef MANUALISOLATION_H
#define MANUALISOLATION_H

#include <QWidget>

#include "Support/Global.h"

namespace Ui {
class ManualIsolation;
}

class ManualIsolation : public QWidget
{
    Q_OBJECT

public:
    explicit ManualIsolation(QWidget *parent = nullptr);
    ~ManualIsolation();
    void setMsgTitle(const QString &value);
    void initialize(QString userName, QString unitName,QString uutNumber,
                    QString typeOfTest,QString userRep, QString QARep,
                    const QMap<QString, uint32>& pinMap);
signals:
    void closeUI();

private slots:
    void on_comboBox_SelectConnector_currentIndexChanged(const QString &arg1);
    void on_pushButton_Clear_clicked();
    void on_pushButton_ConnDisconn_clicked();
    void on_pushButton_ReadResistance_clicked();

private:
    void closeEvent (QCloseEvent *event);
    void getDataIndex(QString data);
    void changeSateComboBoxes(bool state);

private:
    Ui::ManualIsolation *ui;
    QString msgTitle;
    QString chassis = "";
    Utility utilityObj;
    QMap<QString, uint32> pinMap;
    QMap<QString, QStringList> uutConnPin;
};

#endif // MANUALISOLATION_H
