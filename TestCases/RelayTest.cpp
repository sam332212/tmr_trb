#include "RelayTest.h"
#include "ui_RelayTest.h"
#include "Support/debugflag.h"
#include "Support/Global.h"

#include <QMessageBox>
#include <QCloseEvent>
#include <QScreen>

/********************************************************
 MACRO Definition
 ********************************************************/
#if RELAYTEST_DEBUG
    #define _qDebug(s) qDebug()<<"\n========"<<"Inside "<<s<<"========"
#else
    #define _qDebug(s); {}
#endif

#if RELAYTEST_DEBUG_E
    #define _qDebugE(p,q,r,s) qDebug()<<p<<q<<r<<s
#else
    #define _qDebugE(p,q,r,s); {}
#endif

/********************************************************
 CONSTRUCTOR
 ********************************************************/
RelayTest::RelayTest(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RelayTest)
{
    _qDebug("RelayTest Constructor");
    ui->setupUi(this);
    move(QGuiApplication::screens().at(0)->geometry().center() - frameGeometry().center());
}

/********************************************************
 DESTRUCTOR
 ********************************************************/
RelayTest::~RelayTest()
{
    _qDebug("RelayTest Destructor");
    delete ui;
}

/*************************************************************************
 Function Name - closeEvent
 Parameter(s)  - QCloseEvent*
 Return Type   - void
 Action        - This function handles this windows close action.
 *************************************************************************/
 void RelayTest::closeEvent (QCloseEvent *event)
 {
     _qDebug("void RelayTest::closeEvent (QCloseEvent *event).");

     QMessageBox::StandardButton resBtn = QMessageBox::question( this, msgTitle,tr("Do you Want to Close this Window?\n"),QMessageBox::No | QMessageBox::Yes);
     if (resBtn != QMessageBox::Yes)
     {
         event->ignore();
     }
     else
     {
         G::msgBox = new QMessageBox;
         G::msgBox->setWindowTitle(msgTitle);
         G::msgBox->setText("Please wait!! "+msgTitle+" Window is being closed...  ");
         G::msgBox->setWindowFlags(Qt::CustomizeWindowHint);
         G::msgBox->setStandardButtons(nullptr);
         G::msgBox->show();
         QCoreApplication::processEvents(QEventLoop::AllEvents, 100);

         emit close();
     }
 }

 /*************************************************************************
  Function Name - setMsgTitle
  Parameter(s)  - const QString &
  Return Type   - void
  Action        - This function modifies the windows tille variable with the given value.
  *************************************************************************/
 void RelayTest::setMsgTitle(const QString &value)
 {
     msgTitle = value;
 }

/************************************************************************
 Function Name - checkPS
 Parameter(s)  - void
 Return Type   - bool
 Action        - checks the default state of PS and sets Button text accordingly.
 *************************************************************************/
 bool RelayTest::checkPS() {
     psPrevOn.clear();
     psPrevOff.clear();

     if(G::psObj->isOutputOn(INPUT_POWER_SUPPLY_PS)) {
         ui->pushButton_ps1onoff->setText("PS1 OFF");
         psPrevOn.push_back(INPUT_POWER_SUPPLY_PS);
     }
     else {
         ui->pushButton_ps1onoff->setText("PS1 ON");
         psPrevOff.push_back(INPUT_POWER_SUPPLY_PS);
     }
     if(G::psObj->getErrorLog().size() > 0) {
         PLAY QMessageBox::warning(nullptr,"Error!!",G::psObj->getErrorLog());
         return false;
     }

     if(G::psObj->isOutputOn(JIG_POWER_SUPPLY_PS)  ) {
         ui->pushButton_ps2onoff->setText("PS2 OFF");
         psPrevOn.push_back(JIG_POWER_SUPPLY_PS);
     }
     else {
         ui->pushButton_ps2onoff->setText("PS2 ON");
         psPrevOff.push_back(JIG_POWER_SUPPLY_PS);
     }
     if(G::psObj->getErrorLog().size() > 0) {
         PLAY QMessageBox::warning(nullptr,"Error!!",G::psObj->getErrorLog());
         return false;
     }

     if(G::psObj->isOutputOn(LEVEL_SHIFTER_PS)     ) {
         ui->pushButton_ps3onoff->setText("PS3 OFF");
         psPrevOn.push_back(LEVEL_SHIFTER_PS);
     }
     else {
         ui->pushButton_ps3onoff->setText("PS3 ON");
         psPrevOff.push_back(LEVEL_SHIFTER_PS);
     }
     if(G::psObj->getErrorLog().size() > 0) {
         PLAY QMessageBox::warning(nullptr,"Error!!",G::psObj->getErrorLog());
         return false;
     }

     if(G::psObj->isOutputOn(COIL_SUPPLY_PS)       ) {
         ui->pushButton_ps4onoff->setText("PS4 OFF");
         psPrevOn.push_back(COIL_SUPPLY_PS);
     }
     else {
         ui->pushButton_ps4onoff->setText("PS4 ON");
         psPrevOff.push_back(COIL_SUPPLY_PS);
     }
     if(G::psObj->getErrorLog().size() > 0) {
         PLAY QMessageBox::warning(nullptr,"Error!!",G::psObj->getErrorLog());
         return false;
     }

     if(G::psObj->isOutputOn(STATUS_SUPPLY_PS)     ) {
         ui->pushButton_ps5onoff->setText("PS5 OFF");
         psPrevOn.push_back(STATUS_SUPPLY_PS);
     }
     else {
         ui->pushButton_ps5onoff->setText("PS5 ON");
         psPrevOff.push_back(STATUS_SUPPLY_PS);
     }
     if(G::psObj->getErrorLog().size() > 0) {
         PLAY QMessageBox::warning(nullptr,"Error!!",G::psObj->getErrorLog());
         return false;
     }

     _qDebugE("PS ON:",psPrevOn,", PS OFF:",psPrevOff);
     return true;
 }

/************************************************************************
 Function Name - resetPS
 Parameter(s)  - void
 Return Type   - bool
 Action        - set the PS to default state (ON/OFF), which was before
                 the selftest was invoked.
 *************************************************************************/
 bool RelayTest::resetPS() {
     _qDebug("bool RelayTest::resetPS()");
     _qDebugE("PS to ON:[",psPrevOn,"]","");
     _qDebugE("PS to OFF:[",psPrevOff,"]","");
#if PS_PRESENT
     if(!G::psObj->setOutputOn(psPrevOn)) {
          PLAY QMessageBox::warning(nullptr,"Error!!",G::psObj->getErrorLog());
          return false;
     }

     if(!G::psObj->setOutputOff(psPrevOff)) {
          PLAY QMessageBox::warning(nullptr,"Error!!",G::psObj->getErrorLog());
          return false;
     }
#endif

      return true;
 }

 void RelayTest::on_pushButton_onoff_clicked()
 {
     _qDebug("void RelayTest::on_pushButton_onoff_clicked()");
     int status = 0;
     uint32_t dopNo;
     QString str;
     QStringList dops;

     if(ui->lineEdit_dopNumber->text().isEmpty()) {
         str = "Error: Dop Number field can't be empty!!";
         ui->textEdit->append(str);
         PLAY QMessageBox::warning(nullptr,"Error!!",str);
     }
     else {
         dops = ui->lineEdit_dopNumber->text().split(",");

         if(ui->pushButton_onoff->text() == "ON") {
            ui->pushButton_onoff->setText("OFF");
            ui->pushButton_onoff->setToolTip("Click to make DOP OFF");

            for(QString dop : dops) {
                dopNo = static_cast<uint32_t>(dop.toInt());
                status = G::writeDOP(dopNo,ON,G::PCI::P7444);
                if(status != 1) str = QString("7444 DOP <%1> ON failed with Error Code <%2>.").arg(dopNo).arg(status);
                else            str = QString("7444 DOP <%1> ON passed.").arg(dopNo);
                ui->textEdit->append(str);

            }
         }
         else if(ui->pushButton_onoff->text() == "OFF") {
            ui->pushButton_onoff->setText("ON");
            ui->pushButton_onoff->setToolTip("Click to make DOP ON");

            for(QString dop : dops) {
                dopNo = static_cast<uint32_t>(dop.toInt());
                status = G::writeDOP(dopNo,OFF,G::PCI::P7444);
                if(status != 1) str = QString("7444 DOP <%1> OFF failed with Error Code <%2>.").arg(dopNo).arg(status);
                else            str = QString("7444 DOP <%1> OFF passed.").arg(dopNo);
                ui->textEdit->append(str);
            }
         }
     }
     ui->textEdit->append("=========================================================================");
 }

 void RelayTest::on_pushButton_onoff1758_clicked()
 {
     _qDebug("void RelayTest::on_pushButton_onoff1758_clicked()");
     int status = 0;
     uint32_t dopNo;
     QString str;
     QStringList dops;

     if(ui->lineEdit_dopNumber1758->text().isEmpty()) {
         str = "Error: Dop Number field can't be empty!!";
         ui->textEdit->append(str);
         PLAY QMessageBox::warning(nullptr,"Error!!",str);
     }
     else {
         dops = ui->lineEdit_dopNumber1758->text().split(",");

         if(ui->pushButton_onoff1758->text() == "ON") {
            ui->pushButton_onoff1758->setText("OFF");
            ui->pushButton_onoff1758->setToolTip("Click to make DOP OFF");

            for(QString dop : dops) {
                dopNo = static_cast<uint32_t>(dop.toInt());
                status = G::writeDOP(dopNo,ON);
                if(status != 1) str = QString("1758 DOP <%1> ON failed with Error Code <%2>.").arg(dopNo).arg(status);
                else            str = QString("1758 DOP <%1> ON passed.").arg(dopNo);
                ui->textEdit->append(str);

            }
         }
         else if(ui->pushButton_onoff1758->text() == "OFF") {
            ui->pushButton_onoff1758->setText("ON");
            ui->pushButton_onoff1758->setToolTip("Click to make DOP ON");

            for(QString dop : dops) {
                dopNo = static_cast<uint32_t>(dop.toInt());
                status = G::writeDOP(dopNo,OFF);
                if(status != 1) str = QString("1758 DOP <%1> OFF failed with Error Code <%2>.").arg(dopNo).arg(status);
                else            str = QString("1758 DOP <%1> OFF passed.").arg(dopNo);
                ui->textEdit->append(str);
            }
         }
     }
     ui->textEdit->append("=========================================================================");
 }

 void RelayTest::on_pushButton_read_clicked()
 {
     _qDebug("void RelayTest::on_pushButton_read_clicked()");
     int status = 0;
     int32 dipNumber;
     QString str;
     QString dipsStr    = "DIPNO :[";
     QString dipsStatus = "STATUS:[";
     uint8_t state = 0;

     if(ui->lineEdit_dipNumber->text().isEmpty()) {
         str = "Error: Dip Number field can't be empty!!";
         ui->textEdit->append(str);
         PLAY QMessageBox::warning(nullptr,"Error!!",str);
     }
     else {
         QStringList dips = ui->lineEdit_dipNumber->text().split(",");
         dipsStr += (ui->lineEdit_dipNumber->text() + "]");

         for(QString dip : dips) {
             dipNumber = static_cast<int32>(dip.toInt());
             status = G::readDIP(dipNumber, state);
             if(status != 1) {
                str = QString("DIP <%1> Read failed with Error Code <%2>.").arg(dipNumber).arg(status);
                ui->textEdit->append(str);
              }
              dipsStatus.append(QString("%1,").arg(state));
         }
         dipsStatus.remove(dipsStatus.length()-1,1);
         dipsStatus.append("]");
         ui->textEdit->append(dipsStr);
         ui->textEdit->append(dipsStatus);
     }

     ui->textEdit->append("=========================================================================");
 }

 void RelayTest::on_pushButton_connectDisconnect_clicked()
 {
     _qDebug("void RelayTest::on_pushButton_connectDisconnect_clicked()");
     uint32_t dopToHigh,dopToLow,highDop,lowDop;
     int status = 0;
     QString str;
     str.clear();
     dopToHigh = ui->spinBox_inA->text().trimmed().toUInt();
     dopToLow  = ui->spinBox_inB->text().trimmed().toUInt();
     _qDebugE("Entered=> dopToHigh:",dopToHigh,", dopToLow:",dopToLow);

     if(dopToHigh == dopToLow) {
         str = QString("%1 and %2 can't be same.").arg(ui->label_inA->text().remove(ui->label_inA->text().length()-1,1)).
                 arg(ui->label_inB->text().remove(ui->label_inB->text().length()-1,1));
         ui->textEdit->append(str);
         PLAY QMessageBox::warning(nullptr,"Error!!",str);
     }
     else {
         if(ui->pushButton_connectDisconnect->text() == "Connect Line") {
             ui->pushButton_connectDisconnect->setText("Disconnect Line");
             ui->pushButton_connectDisconnect->setToolTip("Click to Disconnect Line");

             utilityObj.getLineDops(&dopToHigh,&dopToLow,&highDop,&lowDop);
             _qDebugE("dopToHigh:",dopToHigh,", dopToLow:",dopToLow);
             _qDebugE("highDop:"  ,highDop,  ", lowDop:"  ,lowDop);

             status = G::writeDOP(dopToHigh,ON);
             if(status != 1) str = QString("INA <%1> ON failed with Error Code <%2>.").arg(dopToHigh).arg(status);
             else            str = QString("INA <%1> ON passed.").arg(dopToHigh);
             ui->textEdit->append(str);

             status = G::writeDOP(dopToLow,ON);
             if(status != 1) str = QString("INB <%1> ON failed with Error Code <%2>.").arg(dopToLow).arg(status);
             else            str = QString("INB <%1> ON passed.").arg(dopToLow);
             ui->textEdit->append(str);

             status = G::writeDOP(highDop,ON);
             if(status != 1) str = QString("DOP <%1> ON failed with Error Code <%2>.").arg(highDop).arg(status);
             else            str = QString("DOP <%1> ON passed.").arg(highDop);
             ui->textEdit->append(str);

             status = G::writeDOP(lowDop,ON);
             if(status != 1) str = QString("DOP <%1> ON failed with Error Code <%2>.").arg(lowDop).arg(status);
             else            str = QString("DOP <%1> ON passed.").arg(lowDop);
             ui->textEdit->append(str);
        }
        else {
             ui->pushButton_connectDisconnect->setText("Connect Line");
             ui->pushButton_connectDisconnect->setToolTip("Click to Connect Line");

             utilityObj.getLineDops(&dopToHigh,&dopToLow,&highDop,&lowDop);
             _qDebugE("dopToHigh:",dopToHigh,", dopToLow:",dopToLow);
             _qDebugE("highDop:"  ,highDop,  ", lowDop:"  ,lowDop);

             status = G::writeDOP(dopToHigh,OFF);
             if(status != 1) str = QString("INA <%1> OFF failed with Error Code <%2>.").arg(dopToHigh).arg(status);
             else            str = QString("INA <%1> OFF passed.").arg(dopToHigh);
             ui->textEdit->append(str);

             status = G::writeDOP(dopToLow,OFF);
             if(status != 1) str = QString("INB <%1> OFF failed with Error Code <%2>.").arg(dopToLow).arg(status);
             else            str = QString("INB <%1> OFF passed.").arg(dopToLow);
             ui->textEdit->append(str);

             status = G::writeDOP(highDop,OFF);
             if(status != 1) str = QString("DOP <%1> OFF failed with Error Code <%2>.").arg(highDop).arg(status);
             else            str = QString("DOP <%1> OFF passed.").arg(highDop);
             ui->textEdit->append(str);

             status = G::writeDOP(lowDop,OFF);
             if(status != 1) str = QString("DOP <%1> OFF failed with Error Code <%2>.").arg(lowDop).arg(status);
             else            str = QString("DOP <%1> OFF passed.").arg(lowDop);
             ui->textEdit->append(str);
        }
     }

     ui->textEdit->append("=========================================================================");
 }

 void RelayTest::on_pushButton_clear_clicked()
 {
     _qDebug("void RelayTest::on_pushButton_clear_clicked()");
    ui->textEdit->clear();
    ui->lineEdit_dmmVoltage->clear();
    ui->lineEdit_dmmResistance->clear();
 }

 void RelayTest::on_pushButton_readDmmResistance_clicked()
 {
     _qDebug("void RelayTest::on_pushButton_readDmmResistance_clicked()");
     double msrdValue = 0.0;
     QString dispValue;

#if(DMM_PRESENT)
        if(!G::dmmObj->setResistanceMode()) {
            PLAY QMessageBox::warning(nullptr,"Error!!","Unable to set DMM to Resistance mode.");
            return;
        }
#endif

     ui->textEdit->append("Reading DMM...");
     msrdValue = G::dmmObj->getStableMeterValue(DMM_READ_MAX_DELAY_MS);
     dispValue = utilityObj.convertToStd(msrdValue);
     _qDebugE("msrdValue: [",QString::number(msrdValue, 'g', 8),"], dispValue:",dispValue);

     ui->textEdit->append("...done");
     ui->textEdit->append("Measured Value :["+dispValue+"]");
     ui->lineEdit_dmmResistance->clear();
     ui->lineEdit_dmmResistance->setText(dispValue);

     ui->textEdit->append("=========================================================================");
 }

 void RelayTest::on_pushButton_readDmmCurrent_clicked()
 {
     _qDebug("void RelayTest::on_pushButton_readDmmCurrent_clicked()");
     double msrdValue = 0.0;
     QString dispValue;

#if(DMM_PRESENT)
        if(!G::dmmObj->setCurrentMode()) {
            PLAY QMessageBox::warning(nullptr,"Error!!","Unable to set DMM to Current mode.");
            return;
        }
#endif

     ui->textEdit->append("Reading DMM...");
     msrdValue = G::dmmObj->getStableMeterValue(DMM_READ_MAX_DELAY_MS);
     dispValue = utilityObj.convertToStd(msrdValue);
     _qDebugE("msrdValue: [",QString::number(msrdValue, 'g', 8),"], dispValue:",dispValue);

     ui->textEdit->append("...done");
     ui->textEdit->append("Measured Value :["+dispValue+"]");
     ui->lineEdit_dmmCurrent->clear();
     ui->lineEdit_dmmCurrent->setText(dispValue);

     ui->textEdit->append("=========================================================================");
 }

 void RelayTest::on_pushButton_readDmmVoltage_clicked()
 {
     _qDebug("void RelayTest::on_pushButton_readDmmVoltage_clicked()");
     double msrdValue = 0.0;
     QString dispValue;

#if(DMM_PRESENT)
        if(!G::dmmObj->setVoltageMode()) {
            PLAY QMessageBox::warning(nullptr,"Error!!","Unable to set DMM to voltage mode.");
            return;
        }
#endif

     ui->textEdit->append("Reading DMM...");
     msrdValue = G::dmmObj->getStableMeterValue(DMM_READ_MAX_DELAY_MS);
     dispValue = utilityObj.convertToStd(msrdValue);
     _qDebugE("msrdValue: [",QString::number(msrdValue, 'g', 8),"], dispValue:",dispValue);

     ui->textEdit->append("...done");
     ui->textEdit->append("Measured Value :["+dispValue+"]");
     ui->lineEdit_dmmVoltage->clear();
     ui->lineEdit_dmmVoltage->setText(dispValue);

     ui->textEdit->append("=========================================================================");
 }

void RelayTest::on_pushButton_ps1onoff_clicked()
{
    _qDebug("void RelayTest::on_pushButton_ps1onoff_clicked()");
    QString str;
    if(ui->pushButton_ps1onoff->text() == "PS1 ON") {
        if(!G::psObj->setOutputOn(INPUT_POWER_SUPPLY_PS)) {
            str = "Unable to make PS1 ON.";
            ui->textEdit->append(str);
            PLAY QMessageBox::warning(nullptr,"Error!!",str);
        }
        else {
            ui->textEdit->append("PS1 ON Success.");
            ui->pushButton_ps1onoff->setText("PS1 OFF");
            ui->pushButton_ps1onoff->setToolTip("Click to OFF PS1");
        }
    }
    else {
        if(!G::psObj->setOutputOff(INPUT_POWER_SUPPLY_PS)) {
            str = "Unable to make PS1 OFF.";
            ui->textEdit->append(str);
            PLAY QMessageBox::warning(nullptr,"Error!!",str);
        }
        else {
            ui->textEdit->append("PS1 OFF Success.");
            ui->pushButton_ps1onoff->setText("PS1 ON");
            ui->pushButton_ps1onoff->setToolTip("Click to ON PS1");
        }
    }
    ui->textEdit->append("=========================================================================");
}

void RelayTest::on_pushButton_ps2onoff_clicked()
{
    _qDebug("void RelayTest::on_pushButton_ps2onoff_clicked()");
    QString str;
    if(ui->pushButton_ps2onoff->text() == "PS2 ON") {
        if(!G::psObj->setOutputOn(JIG_POWER_SUPPLY_PS)) {
            str = "Unable to make PS2 ON.";
            ui->textEdit->append(str);
            PLAY QMessageBox::warning(nullptr,"Error!!",str);
        }
        else {
            ui->textEdit->append("PS2 ON Success.");
            ui->pushButton_ps2onoff->setText("PS2 OFF");
            ui->pushButton_ps2onoff->setToolTip("Click to OFF PS2");
        }
    }
    else {
        if(!G::psObj->setOutputOff(JIG_POWER_SUPPLY_PS)) {
            str = "Unable to make PS2 OFF.";
            ui->textEdit->append(str);
            PLAY QMessageBox::warning(nullptr,"Error!!",str);
        }
        else {
            ui->textEdit->append("PS2 OFF Success.");
            ui->pushButton_ps2onoff->setText("PS2 ON");
            ui->pushButton_ps2onoff->setToolTip("Click to ON PS2");
        }
    }
    ui->textEdit->append("=========================================================================");
}

void RelayTest::on_pushButton_ps3onoff_clicked()
{
    _qDebug("void RelayTest::on_pushButton_ps3onoff_clicked()");
    QString str;
    if(ui->pushButton_ps3onoff->text() == "PS3 ON") {
        if(!G::psObj->setOutputOn(LEVEL_SHIFTER_PS)) {
            str = "Unable to make PS3 ON.";
            ui->textEdit->append(str);
            PLAY QMessageBox::warning(nullptr,"Error!!",str);
        }
        else {
            ui->textEdit->append("PS3 ON Success.");
            ui->pushButton_ps3onoff->setText("PS3 OFF");
            ui->pushButton_ps3onoff->setToolTip("Click to OFF PS3");
        }
    }
    else {
        if(!G::psObj->setOutputOff(LEVEL_SHIFTER_PS)) {
            str = "Unable to make PS3 OFF.";
            ui->textEdit->append(str);
            PLAY QMessageBox::warning(nullptr,"Error!!",str);
        }
        else {
            ui->textEdit->append("PS3 OFF Success.");
            ui->pushButton_ps3onoff->setText("PS3 ON");
            ui->pushButton_ps3onoff->setToolTip("Click to ON PS3");
        }
    }
    ui->textEdit->append("=========================================================================");
}

void RelayTest::on_pushButton_ps4onoff_clicked()
{
    _qDebug("void RelayTest::on_pushButton_ps4onoff_clicked()");
    QString str;
    if(ui->pushButton_ps4onoff->text() == "PS4 ON") {
        if(!G::psObj->setOutputOn(COIL_SUPPLY_PS)) {
            str = "Unable to make PS4 ON.";
            ui->textEdit->append(str);
            PLAY QMessageBox::warning(nullptr,"Error!!",str);
        }
        else {
            ui->textEdit->append("PS4 ON Success.");
            ui->pushButton_ps4onoff->setText("PS4 OFF");
            ui->pushButton_ps4onoff->setToolTip("Click to OFF PS4");
        }
    }
    else {
        if(!G::psObj->setOutputOff(COIL_SUPPLY_PS)) {
            str = "Unable to make PS4 OFF.";
            ui->textEdit->append(str);
            PLAY QMessageBox::warning(nullptr,"Error!!",str);
        }
        else {
            ui->textEdit->append("PS4 OFF Success.");
            ui->pushButton_ps4onoff->setText("PS4 ON");
            ui->pushButton_ps4onoff->setToolTip("Click to ON PS4");
        }
    }
    ui->textEdit->append("=========================================================================");
}

void RelayTest::on_pushButton_ps5onoff_clicked()
{
    _qDebug("void RelayTest::on_pushButton_ps5onoff_clicked()");
    QString str;
    if(ui->pushButton_ps5onoff->text() == "PS5 ON") {
        if(!G::psObj->setOutputOn(STATUS_SUPPLY_PS)) {
            str = "Unable to make PS5 ON.";
            ui->textEdit->append(str);
            PLAY QMessageBox::warning(nullptr,"Error!!",str);
        }
        else {
            ui->textEdit->append("PS5 ON Success.");
            ui->pushButton_ps5onoff->setText("PS5 OFF");
            ui->pushButton_ps5onoff->setToolTip("Click to OFF PS5");
        }
    }
    else {
        if(!G::psObj->setOutputOff(STATUS_SUPPLY_PS)) {
            str = "Unable to make PS5 OFF.";
            ui->textEdit->append(str);
            PLAY QMessageBox::warning(nullptr,"Error!!",str);
        }
        else {
            ui->textEdit->append("PS5 OFF Success.");
            ui->pushButton_ps5onoff->setText("PS5 ON");
            ui->pushButton_ps5onoff->setToolTip("Click to ON PS5");
        }
    }
    ui->textEdit->append("=========================================================================");
}

void RelayTest::on_pushButton_onoffDMM_clicked()
{
    _qDebug("void RelayTest::on_pushButton_onoffDMM_clicked()");
    int status = 0;
    QString str;

    if(ui->pushButton_onoffDMM->text() == "ON DMM") {
        status = G::writeDOP(RLC_DMM,ON);
        if(status != 1) str = QString("DMM DOP<%1> ON failed with Error Code <%2>.").arg(RLC_DMM).arg(status);
        else {
            str = QString("DMM DOP<%1> ON passed.").arg(RLC_DMM);
            ui->textEdit->append(str);
            ui->pushButton_onoffDMM->setText("OFF DMM");
            ui->pushButton_onoffDMM->setToolTip("Click to make DMM OFF");
        }
     }
     else {
        status = G::writeDOP(RLC_DMM,OFF);
        if(status != 1) str = QString("DMM DOP<%1> OFF failed with Error Code <%2>.").arg(RLC_DMM).arg(status);
        else {
            str = QString("DMM DOP<%1> OFF passed.").arg(RLC_DMM);
            ui->textEdit->append(str);
            ui->pushButton_onoffDMM->setText("ON DMM");
            ui->pushButton_onoffDMM->setToolTip("Click to make DMM ON");
        }
     }
    ui->textEdit->append("=========================================================================");
}

void RelayTest::on_pushButton_reset7444_clicked()
{
    _qDebug("void RelayTest::on_pushButton_reset7444_clicked()");
    if(G::pci7444Obj->resetPCI() != 1) {
        ui->textEdit->append("PCI7444 Reset failed.");
    }
    else {
        ui->textEdit->append("PCI7444 Reset successfull.");
    }
    ui->textEdit->append("=========================================================================");
}

void RelayTest::on_pushButton_reset1758_clicked()
{
    _qDebug("void RelayTest::on_pushButton_reset1758_clicked()");
    if(G::pci1758Obj->resetPCI() != 1) {
        ui->textEdit->append("PCI1758 Reset failed.");
    }
    else {
        ui->textEdit->append("PCI1758 Reset successfull.");
    }

    ui->textEdit->append("=========================================================================");
}

