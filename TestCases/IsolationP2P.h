/*************************************  FILE HEADER  *****************************************************************
*
*  File name - IsolationP2P.h
*  Creation date and time - 18-JUL-2020 , SAT 07:22 PM IST
*  Author: - Manoj
*  Purpose of the file - All "Isolation Pin To Pin" modules will be declared .
*
**********************************************************************************************************************/

#ifndef ISOLATIONP2P_H
#define ISOLATIONP2P_H

/********************************************************
 Inclusion of header file
 ********************************************************/
#include "Support/Global.h"

/********************************************************
 Class Declaration
 ********************************************************/
class IsolationP2P
{
public:
    IsolationP2P();
    ~IsolationP2P();

    static void runTest(const QMap<QString, uint32> pinMap,const QStringList& hiddenHeader,const QVector<QStringList>& hiddenData);

private:
    bool isParameterInitialized();
    bool measValueAndUpdate(const QMap<QString, uint32>& pinMap,const QString& INsToHigh,const QStringList& INsToLow,
                            const QString& expectedValue,int rowNo,int msrdValIndex,int resultIndex,int remarkIndex,bool& isPassed);
    bool getConnPins(QString rawData,QStringList& connPins);
    bool setDMM();

    static IsolationP2P *thisObj;
};

#endif // ISOLATIONP2P_H
