/*************************************  FILE HEADER  *****************************************************************
*
*  File name - Load.cpp
*  Creation date and time - 11-JUNE-2020 , THU 10:00 AM IST
*  Author: - Manoj
*  Purpose of the file - All "Load Test" modules will be defined.
*
**********************************************************************************************************************/
/********************************************************
 Inclusion of header file
 ********************************************************/
#include "Load.h"
#include "Support/debugflag.h"
#include "Support/Utility.h"
#include "Support/Timer.h"
#include "Support/Global.h"

/********************************************************
 MACRO Definition
 ********************************************************/
#if LOAD_DEBUG
    #define _qDebug(s) qDebug()<<"\n-------- Inside "<<s<<" --------"
#else
    #define _qDebug(s); {}
#endif

#if LOAD_DEBUG_E
    #define _qDebugE(p,q,r,s) qDebug()<<p<<q<<r<<s
#else
    #define _qDebugE(p,q,r,s); {}
#endif

Load* Load::thisObj = nullptr;
const QString TITLE = "Load Test";

/********************************************************
 Function Definition
 ********************************************************/
/*************************************************************************
 Function Name - Load
 Parameter(s)  - void
 Return Type   - void
 Action        - Creates the object.
 *************************************************************************/
 Load::Load()
 {
    _qDebug("Load Constructor.");
    thisObj = this;
 }

/*************************************************************************
 Function Name - ~Load
 Parameter(s)  - void
 Return Type   - void
 Action        - Destroys the object.
 *************************************************************************/
 Load::~Load()
 {
    _qDebug("Load Destructor.");
 }

/*************************************************************************
 Function Name - runTest
 Parameter(s)  - const QMap<QString, uint32>&, const QMap<QString, uint32>&,
                 const QMap<QString, uint32>&,const QStringList&,const QVector<QStringList>&,
                 const Utility::ResetData&
Return Type   - void
 Action        - Runs the test.
 *************************************************************************/
  void Load::runTest(const QMap<QString, uint32>& pinMap,const QMap<QString, uint32>& dopMap,const QMap<QString, uint32>& dipMap,
                     const QStringList& hiddenHeader,const QVector<QStringList>& hiddenData,const Utility::ResetData& resetData)
 {
    _qDebug("void Load::runTest(const QMap<QString, uint32>& pinMap,const QMap<QString, uint32>& dopMap,const QMap<QString, uint32>& dipMap,"
            "const QStringList& hiddenHeader,const QVector<QStringList>& hiddenData,const Utility::ResetData& resetData)");

    uint8_t          dopHigh1Index = 0;
    uint8_t           dopLow1Index = 0;
    uint8_t          dopHigh2Index = 0;
    uint8_t           dopLow2Index = 0;
    uint8_t            inHighIndex = 0;
    uint8_t             inLowIndex = 0;
    uint8_t          expValueIndex = 0;
    uint8_t              dipsIndex = 0;
    uint8_t      dipsExpValueIndex = 0;
    uint8_t dipsExpValueAfterIndex = 0;
    uint8_t     expValueAfterIndex = 0;
    uint8_t           msrdValIndex = 0;
    uint8_t            resultIndex = 0;
    uint8_t            remarkIndex = 0;

    int rowNo;
    int pausePoint = UINT_MAX;

    QString dopsToOn1,dopsToOff1,dopsToOn2,dopsToOff2;
    QString insToHigh,insToLow,expValue,expValueAfter;
    QString dipsToRead,dipsExpValue,dipsExpValueAfter;
    QVector<int> selectedRowNumbers;
    QTableWidgetItem* item = nullptr;
    QCheckBox* chkBox = nullptr;
    QString msgTitle = "Load Test";

    Utility utilityObj;
    Timer   timerObj;
    bool       isPassed = true;
    bool  isPassUpdated = false;
    bool  isFailUpdated = false;
    bool isRepeatedTest = false;

    G::measCount = 0;

    if(thisObj->isParameterInitialized() == false)
    {
        PLAY QMessageBox::warning(nullptr,"Error!!","Load: All Test Parameters are not Initialized.");
    }
    else
    {
        isRepeatedTest = (G::testResultStatus->text().size() > 0);
        G::cleanUi();

        //Getting table header index
        for(uint8_t i = 0;i < G::testTable->columnCount();i++)
        {
            if(     G::testTable->horizontalHeaderItem(i)->text().compare(utilityObj._headers1.measuredValue) == 0) msrdValIndex = i;
            else if(G::testTable->horizontalHeaderItem(i)->text().compare(utilityObj._headers1.result)        == 0) resultIndex  = i;
            else if(G::testTable->horizontalHeaderItem(i)->text().compare(utilityObj._headers1.remark)        == 0) remarkIndex  = i;
        }
        if(msrdValIndex == 0) {
            PLAY QMessageBox::warning(nullptr,"Error!!",QString("Load:[%1] not present in Table Header.")
                                 .arg(utilityObj._headers1.measuredValue));
            return;
        }
        if(resultIndex == 0) {
            PLAY QMessageBox::warning(nullptr,"Error!!",QString("Load:[%1] not present in Table Header.")
                                 .arg(utilityObj._headers1.result));
            return;
        }
        if(remarkIndex == 0) {
            PLAY QMessageBox::warning(nullptr,"Error!!",QString("Load:[%1] not present in Table Header.")
                                 .arg(utilityObj._headers1.remark));
            return;
        }
        if((hiddenHeader.count() == 0) || (hiddenData.count() == 0)) {
            PLAY QMessageBox::warning(nullptr,"Error!!","Load: RHS data is not present in Database.");
            _qDebugE("hiddenHeader:",hiddenHeader,"\nhiddenData:",hiddenData);
            return;
        }

        for(uint8_t i = 0;i < hiddenHeader.count();i++)
        {
            if(     hiddenHeader.at(i).compare(utilityObj._hidden1.dopHigh1)         == 0) dopHigh1Index          = i;
            else if(hiddenHeader.at(i).compare(utilityObj._hidden1.dopLow1)          == 0) dopLow1Index           = i;
            else if(hiddenHeader.at(i).compare(utilityObj._hidden1.dopHigh2)         == 0) dopHigh2Index          = i;
            else if(hiddenHeader.at(i).compare(utilityObj._hidden1.dopLow2)          == 0) dopLow2Index           = i;
            else if(hiddenHeader.at(i).compare(utilityObj._hidden1.INsToHigh)        == 0) inHighIndex            = i;
            else if(hiddenHeader.at(i).compare(utilityObj._hidden1.INsToLow)         == 0) inLowIndex             = i;
            else if(hiddenHeader.at(i).compare(utilityObj._hidden1.expectedValue)    == 0) expValueIndex          = i;
            else if(hiddenHeader.at(i).compare(utilityObj._hidden1.dips)             == 0) dipsIndex              = i;
            else if(hiddenHeader.at(i).compare(utilityObj._hidden1.dipsExpValue)     == 0) dipsExpValueIndex      = i;
            else if(hiddenHeader.at(i).compare(utilityObj._hidden1.dipsValueAfter)   == 0) dipsExpValueAfterIndex = i;
            else if(hiddenHeader.at(i).compare(utilityObj._hidden1.outputValueAfter) == 0) expValueAfterIndex     = i;
            else {
                PLAY QMessageBox::warning(nullptr,"Error!!","Load: Invalid Table(RHS) Header ["+hiddenHeader.at(i)+"]");
                return;
            }
        }

        G::showInfoMessage(TITLE,"Please wait!! Preparing Test...  ");
        timerObj.delayInms(10);

       //Get selected row Number and clean the table Columns
       selectedRowNumbers.clear();
       for(int rowNo = 0;rowNo < G::testTable->rowCount();rowNo++)
       {
          chkBox = static_cast<QCheckBox*>(G::testTable->cellWidget(rowNo,0));
          if(chkBox == nullptr) {
              pausePoint = rowNo;
              selectedRowNumbers.push_back(rowNo);
              continue;
          }
          else if(chkBox->isChecked()) selectedRowNumbers.push_back(rowNo);

          if(isRepeatedTest) {
              item = G::testTable->item(rowNo,msrdValIndex);
              if(item) G::testTable->item(rowNo,msrdValIndex)->setText("");

              item = G::testTable->item(rowNo,remarkIndex);
              if(item) G::testTable->item(rowNo,remarkIndex)->setText("");

              item = G::testTable->item(rowNo,resultIndex);
              if(item) {
                  item = G::testTable->takeItem(rowNo,resultIndex);
                  if(item != nullptr) clearmem(item);
                  item = new QTableWidgetItem("");
                  item->setFont(G::myFont);
                  G::testTable->setItem(rowNo,resultIndex,item);
              }
          }
       }
       if(selectedRowNumbers.at(selectedRowNumbers.count()-1) == pausePoint) selectedRowNumbers.pop_back();
       G::closeTestMessage();

       if(pausePoint != selectedRowNumbers.at(0)) {
           G::showInfoMessage(G::testStartMessage);
           INFO if (QMessageBox::No == QMessageBox(QMessageBox::Information, "Load Test",
                                              "Do you want to continue the test ?",
                                              QMessageBox::Yes|QMessageBox::No).exec()) return;
       }

       if(!thisObj->setDMM()) return;
        G::enableRunButton(true);

        utilityObj.setDateTime(G::startDate,G::startTime);
        if(thisObj->onLoadRelays())
        {
            G::testTimer->start(1000);
            _qDebugE("selectedRowNumbers:",selectedRowNumbers.count(),", pausePoint:",pausePoint);
            //Operate other Dops
            for(int index = 0;index < selectedRowNumbers.count();index++)
            {
                if(G::stopTest) break;
                isPassed = true;//Start of the tests in a row

                G::testRunStatus->setStyleSheet("QLabel { background-color : #3AEA02;}");

                rowNo = selectedRowNumbers[index];
                G::manageScrolling(index,rowNo);
                _qDebugE("selectedRow:",rowNo+1,"","");

                //Decide to show popup for next test slot
                if(rowNo == pausePoint) {
                    G::testTimer->stop();
                    //show popup here
                    G::showInfoMessage(G::testInterMediateMessage);
                    INFO if (QMessageBox::No == QMessageBox(QMessageBox::Information, "Load Test",
                                                       "Do you want to continue the test ?",
                                                       QMessageBox::Yes|QMessageBox::No).exec()) break;
                    G::testTimer->start(1000);
                    continue;
                }

                dopsToOn1         = hiddenData.at(dopHigh1Index         ).at(rowNo);
                dopsToOff1        = hiddenData.at(dopLow1Index          ).at(rowNo);
                dopsToOn2         = hiddenData.at(dopHigh2Index         ).at(rowNo);
                dopsToOff2        = hiddenData.at(dopLow2Index          ).at(rowNo);
                insToHigh         = hiddenData.at(inHighIndex           ).at(rowNo);
                insToLow          = hiddenData.at(inLowIndex            ).at(rowNo);
                dipsToRead        = hiddenData.at(dipsIndex             ).at(rowNo);
                dipsExpValue      = hiddenData.at(dipsExpValueIndex     ).at(rowNo);
                dipsExpValueAfter = hiddenData.at(dipsExpValueAfterIndex).at(rowNo);
                expValue          = hiddenData.at(expValueIndex         ).at(rowNo);
                expValueAfter     = hiddenData.at(expValueAfterIndex    ).at(rowNo);

                if(!G::onDops(dopMap,dopsToOn1,G::PCI::P7444)) break;
                if(dopsToOn1.count() > 0) timerObj.delayInms(LATCH_RELAY_DELAY_MS);

                if(!G::offDops(dopMap,dopsToOff1,G::PCI::P7444)) break;
                if(dopsToOff1.count() > 0) timerObj.delayInms(LATCH_RELAY_DELAY_MS);

                //Check DIPs here before Relays ON. Compare DIP value with the expected value
                if(!thisObj->readDipsAndUpdate(dipMap,dipsToRead,dipsExpValue,rowNo,remarkIndex,resultIndex,true,isPassed)) break;
                if(!G::updateResultStatus(isPassed, isPassUpdated, isFailUpdated)) {
                    PLAY QMessageBox::warning(nullptr,"Error!!","Load: Couldn't update the result status in UI.");
                }

                //On Dops,Measure Current and Compare with Expected value
                if(!thisObj->measValueAndUpdate(pinMap,insToHigh,insToLow,expValue,rowNo,msrdValIndex,resultIndex,isPassed)) break;
                if(!G::updateResultStatus(isPassed, isPassUpdated, isFailUpdated)) {
                    PLAY QMessageBox::warning(nullptr,"Error!!","Load: Couldn't update the result status in UI.");
                }

                if(!G::onDops(dopMap,dopsToOn2,G::PCI::P7444)) break;
                if(dopsToOn2.count() > 0) timerObj.delayInms(LATCH_RELAY_DELAY_MS);

                if(!G::offDops(dopMap,dopsToOff2,G::PCI::P7444)) break;
                if(dopsToOff2.count() > 0) timerObj.delayInms(LATCH_RELAY_DELAY_MS);

                //Check DIPs here after Test. Compare DIP value with the expected value
                if(dipsExpValueAfter.size() > 0) {
                    if(!thisObj->readDipsAndUpdate(dipMap,dipsToRead,dipsExpValueAfter,rowNo,remarkIndex,resultIndex,false,isPassed)) break;
                    if(!G::updateResultStatus(isPassed, isPassUpdated, isFailUpdated)) {
                        PLAY QMessageBox::warning(nullptr,"Error!!","Load: Couldn't update the result status in UI.");
                    }
                }

                //After Test, On Dops,Measure Current and Compare with Expected value
                if(expValueAfter.size() > 0 ) {
                    if(!thisObj->measValueWithoutUpdate(pinMap,insToHigh,insToLow,expValueAfter,rowNo,remarkIndex,resultIndex,isPassed)) break;
                    if(!G::updateResultStatus(isPassed, isPassUpdated, isFailUpdated)) {
                        PLAY QMessageBox::warning(nullptr,"Error!!","Load: Couldn't update the result status in UI.");
                    }
                }

                if(isPassed) {
                    G::testTable->item(rowNo,resultIndex)->setText(PASS);
                    G::testTable->item(rowNo,resultIndex)->setBackground(Qt::green);
                }

                G::progressBar->setValue(static_cast<int>(100.00 * (index + 1)/selectedRowNumbers.count()));
                G::testRunStatus->setStyleSheet("QLabel { background-color : red;}");

                timerObj.delayInms(5);
            }//outer for loop runs for number of table rows
            G::testTimer->stop();
            if((selectedRowNumbers.count() != G::testTable->rowCount()) || G::stopTest) {
                _qDebugE("Resetting Device ..","","","");
                thisObj->resetDevice(dopMap,dipMap,resetData);
            }

            G::progressBar->setValue(100);
            G::testRunStatus->setStyleSheet("QLabel { background-color : red;}");

            thisObj->offLoadRelays();
         }//if block when Load Path close successfull for measurement
         utilityObj.setDateTime(G::endDate,G::endTime);
         DMM::isreadBusy = false;
    }
  }

/*************************************************************************
 Function Name - isParameterInitialized
 Parameter(s)  - void
 Return Type   - bool
 Action        - Checks each parameter's initializations. It returns false
                 if one of the parameter is not initialized else returns true.
 *************************************************************************/
 bool Load::isParameterInitialized()
 {
     if((G::testTable == nullptr)  || (G::testResultStatus == nullptr) || (G::testRunStatus == nullptr) ||
       (G::progressBar == nullptr) || (G::dmmObj == nullptr)           || (G::psObj == nullptr)         ||
       (G::pci1758Obj == nullptr)  || (G::pci7444Obj == nullptr)       || (G::testTimer == nullptr))
     {
        return false;
    }
    else
    {
        return true;
    }
 }

/*************************************************************************
 Function Name - onLoadRelays
 Parameter(s)  - void
 Return Type   - bool
 Action        - On the relays to on the path to DMM for Load Test.
 *************************************************************************/
 bool Load::onLoadRelays()
 {
     if(G::writeDOP({RLC_LT1,RLC_LT2,RLC_LT3},ON) != 1) return false;
     return true;
 }

/*************************************************************************
 Function Name - offLoadRelays
 Parameter(s)  - void
 Return Type   - bool
 Action        - Off the relays to close the path to DMM for Load Test.
 *************************************************************************/
 bool Load::offLoadRelays()
 {
    if(G::writeDOP({RLC_LT1,RLC_LT2,RLC_LT3},OFF) != 1) return false;
    return true;
 }

 /*************************************************************************
  Function Name - readDipsAndUpdate
  Parameter(s)  - const QMap<QString, uint32>&, const QString&, const QString&,
                  int, int, int, bool, bool&
  Return Type   - bool
  Action        - "dipMap" contains all dipcon_pin-dipNo pairs.
                  "dips"   contains the dipcon_pins to be tested.
                  Reads the given Dips and compare the read value with the
                  expectedValue. If comparision fails then updates the
                  fail status in the table at index remarkIndex and returns false.
                  If any critical error(shown in error prompt) then returns false
                  else returns true.
  *************************************************************************/
  bool Load::readDipsAndUpdate(const QMap<QString, uint32>& dipMap,const QString& connPins,const QString& expectedDipValue,
                                   int rowNo,int remarkIndex,int resultIndex,bool isBefore,bool& isPassed)
  {
      _qDebug("bool Load::readDipsAndUpdate(const QMap<QString, uint32>& dipMap,const QString& dips,const QString& expectedValue,int rowNo,int updateIndex,int resultIndex,bool isBefore,bool& isPassed)");
       QVector<uint32_t> dips;
       QString str,remarkStr;
       QTableWidgetItem* item = nullptr;
       QStringList uutConnPins = connPins.split(",");
       QStringList expDipValues = expectedDipValue.split(",");
       QVector<uint8_t> dipValues;

       if(connPins.size() == 0) return true;

       if(uutConnPins.count() != expDipValues.count()) {
            PLAY QMessageBox::warning(nullptr,"Error!!",QString("Load: Mismatched: Number of DIPs to read:[%1], Number of Expected DIPs values:[%2] at ROW:[%3].")
                                 .arg(uutConnPins.count()).arg(expDipValues.count()).arg(rowNo+1));
            return false;
        }

        if(rowNo > G::testTable->rowCount()-1) {
            PLAY QMessageBox::warning(nullptr,"Error!!\n","Invalid Row Number.\nGiven ROW:["+QString(rowNo+1)+"].");
            return false;
        }
        if(remarkIndex > G::testTable->columnCount()-1) {
           PLAY QMessageBox::warning(nullptr,"Error!!\n","Invalid index to update Remark.\nGiven index:["+QString::number(remarkIndex) +"] and Given ROW:["+QString::number(rowNo+1)+"].");
           return false;
        }
        if(resultIndex > G::testTable->columnCount()-1) {
           PLAY QMessageBox::warning(nullptr,"Error!!\n","Invalid index to update Result.\nGiven index:["+QString::number(resultIndex) +"] and Given ROW:["+QString::number(rowNo+1)+"].");
           return false;
        }

        str.clear();
        dips.clear();
        for(QString connPin : uutConnPins) {
          if(!dipMap.contains(connPin)) {
              PLAY QMessageBox::warning(nullptr,"Error!!\n","The UUT_con/Pin:["+connPin+"] couldn't be found in Database.");
              return false;
          }
          else {
              dips.push_back(dipMap.value(connPin));
          }
        }
        if(G::readDIP(dips,dipValues) != 1) {
            _qDebugE("DIP failed at ROW:",rowNo+1,"","");
            return false;
        }

        _qDebugE("DIPs:[",connPins,"]\nDIP values:",dipValues);

        //Compare DipStatus
        for(int i = 0;i < expDipValues.count();i++) {
            if(expDipValues.at(i).toInt() != dipValues.at(i)) {
                if(str.size() > 0) str += ",";
                str += uutConnPins.at(i) + " E:[" +expDipValues.at(i) + "] A:[" +
                        QString::number(dipValues.at(i)) + "]";
            }
        }

        if(str.size() > 0)
        {
           item = G::testTable->item(rowNo,remarkIndex);
           remarkStr = item->text();
           if(remarkStr.size() > 0) remarkStr += "\n";

           if(isBefore) {
               remarkStr += ("DIPs {" + str + "} failed after Command.");
           }
           else {
               remarkStr += ("DIPs {" + str + "} failed before Command.");
           }

           if(!G::updateResult(false,rowNo,resultIndex,isPassed)) {
               PLAY QMessageBox::warning(nullptr,"Error!!",QString("Load: Couldn't update the result in table at ROW:[%1].")
                                    .arg(rowNo+1));
               return false;
           }
           isPassed = false;
           item->setText(remarkStr);

           G::testTable->resizeRowToContents(rowNo);
        }

       return true;
  }

 /*************************************************************************
  Function Name - measValueAndUpdate
  Parameter(s)  - const QMap<QString, uint32>&, const QString&, const QString&,
                  const QString&, int, int, int, bool&
  Return Type   - bool
  Action        - Opens the High and Low DOP lines, measures the Current and
                  comapres with given expectedValue . Returns true if the process
                  is pass else returns false. The value of isPassed is true
                  if all the measured values match with the expectedValue and
                  else the value of isPassed will be false.
  *************************************************************************/
  bool Load::measValueAndUpdate(const QMap<QString, uint32>& pinMap,const QString& INsToHigh,const QString& INsToLow,
                                    const QString& expectedValue,int rowNo,int msrdValIndex,int resultIndex,bool& isPassed)
  {
      _qDebug("bool Load::measValueAndUpdate(const QMap<QString, uint32>& dopMap,const QString& INsToHigh,const QString& INsToLow,"
              "const QString& expectedValue,int rowNo,int msrdValIndex,int resultIndex,bool& isPassed)");
      Utility utilityObj;
      Timer timerObj;
      uint32_t dopToHigh,dopToLow;
      uint32_t highDop,lowDop;
      double msrdValue = 0.0;
      QString displayValueStr;
      QVector<double> msrdValues;
      QStringList insHigh = INsToHigh.split(",");
      QStringList insLow  = INsToLow.split(",");
      bool toUpdate = false;

      if(INsToHigh.size() == 0 || INsToLow.size() == 0) return true;

       if(insHigh.count() != insLow.count()) {
           PLAY QMessageBox::warning(nullptr,"Error!!",QString("Load: Mismatched: Number of INs To High:[%1], Number of INs To Low:[%2] at ROW:[%3].")
                                .arg(insHigh.count()).arg(insLow.count()).arg(rowNo+1));
           return false;
       }

       if(rowNo > G::testTable->rowCount()-1) {
          PLAY QMessageBox::warning(nullptr,"Error!!\n","Invalid Row Number.\nGiven ROW:["+QString(rowNo+1)+"].");
          return false;
       }
       if(msrdValIndex > G::testTable->columnCount()-1) {
          PLAY QMessageBox::warning(nullptr,"Error!!\n","Invalid index to update Measured value.\nGiven index:["+QString::number(msrdValIndex) +"] and Given ROW:["+QString::number(rowNo+1)+"].");
          return false;
       }
       if(resultIndex > G::testTable->columnCount()-1) {
           PLAY QMessageBox::warning(nullptr,"Error!!\n","Invalid index to update Result.\nGiven index:["+QString::number(resultIndex) +"] and Given ROW:["+QString::number(rowNo+1)+"].");
           return false;
       }

       displayValueStr.clear();
       msrdValues.clear();

       for(int i = 0;i < insHigh.count();i++)
       {
           if(G::stopTest) break;

           if(!pinMap.contains(insHigh[i])) {
               PLAY QMessageBox::warning(nullptr,"Error!!\n","The UUT_con/Pin:["+insHigh[i]+"] couldn't be found in Database.");
               return false;
           }
           else {
               dopToHigh = pinMap.value(insHigh[i]);
           }
           if(!pinMap.contains(insLow[i])) {
               PLAY QMessageBox::warning(nullptr,"Error!!\n","The UUT_con/Pin:["+insHigh[i]+"] couldn't be found in Database.");
               return false;
           }
           else {
               dopToLow = pinMap.value(insLow[i]);
           }

           utilityObj.getLineDops(&dopToHigh,&dopToLow,&highDop,&lowDop);

           //Dop ON
           if(G::writeDOP({dopToHigh,highDop,dopToLow,lowDop},ON)!= 1) return false;
           timerObj.delayInms(TEST_DELAY_MS);//Relay Stabilize Delay
           if(i == 0) { // Additional delay for first measurement
               timerObj.delayInms(TEST_DELAY_MS);
           }
           timerObj.delayInms(LOAD_DELAY_MS);
           msrdValue = G::dmmObj->getStableMeterValue(DMM_READ_MAX_DELAY_MS);
           msrdValue -= G::calibVoltage;
           msrdValue = utilityObj.mod(msrdValue);
           msrdValues.push_back(msrdValue);
           _qDebugE("msrdValue: [",QString::number(msrdValue, 'g', 8),"]","");

           _qDebugE("Load: Measurement Count: [",G::measCount++,"]","");

           //Dop OFF
           if(G::writeDOP({dopToHigh,highDop,dopToLow,lowDop},OFF)!= 1) return false;

           //Show Measured Value
           if(displayValueStr.size() > 0) displayValueStr += ",";
           displayValueStr += utilityObj.getDisplayValue(expectedValue,msrdValue);
           G::testTable->item(rowNo,msrdValIndex)->setText(displayValueStr);

           _qDebugE("ExpectedValue: ",expectedValue,", MeasuredValue:",displayValueStr);

           if(utilityObj.getResult(expectedValue,msrdValue) == false)
           {
               if(!G::updateResult(false,rowNo,resultIndex,isPassed)) {
                   PLAY QMessageBox::warning(nullptr,"Error!!",QString("Load: Couldn't update the result in table at ROW:[%1].")
                                        .arg(rowNo+1));
                   return false;
               }
               isPassed = false;
           }
           if((i > 1) && (i%2 == 0)) { G::testTable->resizeRowToContents(rowNo); toUpdate = true; }
     }

     if(isPassed) {
 #if(AVERAGE_ENABLE)
             double average = 0.0;
             for(double val : msrdValues) average += val;
             average = (average /(double)msrdValues.count());

             G::testTable->item(rowNo,msrdValIndex)->setText(utilityObj.getDisplayValue(expectedValue,average));
 #endif
             if(toUpdate) G::testTable->resizeRowToContents(rowNo);
      }
     else {
         G::testTable->resizeRowToContents(rowNo);
     }

      return true;
  }

/*************************************************************************
  Function Name - measValueWithoutUpdate
  Parameter(s)  - const QMap<QString, uint32>&, const QString&, const QString&,
                   const QString&, int, int, int, bool&
  Return Type   - bool
  Action        - Opens the High and Low DOP lines, measures the Current and
                  comapres with given expectedValue . Returns true if the process
                  is pass else returns false. The value of isPassed is true
                  if all the measured values match with the expectedValue and
                  else the value of isPassed will be false.
  *************************************************************************/
  bool Load::measValueWithoutUpdate(const QMap<QString, uint32>& pinMap,const QString& INsToHigh,const QString& INsToLow,
                                         const QString& expectedValue,int rowNo,int remarkIndex,int resultIndex,bool& isPassed)
  {
       _qDebug("bool Load::measValueWithoutUpdate(const QMap<QString, uint32>& pinMap,const QString& INsToHigh,const QString& INsToLow,"
               "const QString& expectedValue,int rowNo,int remarkIndex,int resultIndex,bool& isPassed)");
       Utility utilityObj;
       Timer timerObj;
       uint32_t dopToHigh,dopToLow;
       uint32_t highDop,lowDop;
       double msrdValue = 0.0;
       QString displayValueStr, remarkStr;
       QStringList insHigh = INsToHigh.split(",");
       QStringList insLow  = INsToLow.split(",");

       if(INsToHigh.size() == 0 || INsToLow.size() == 0) return true;

       if(insHigh.count() != insLow.count()) {
           PLAY QMessageBox::warning(nullptr,"Error!!",QString("Load: Mismatched: Number of INs To High:[%1], Number of INs To Low:[%2] at ROW:[%3].")
                                .arg(insHigh.count()).arg(insLow.count()).arg(rowNo+1));
           return false;
       }

       displayValueStr.clear();

       for(int i = 0;i < insHigh.count();i++)
       {
           if(G::stopTest) break;

           if(!pinMap.contains(insHigh[i])) {
               PLAY QMessageBox::warning(nullptr,"Error!!\n","The UUT_con/Pin:["+insHigh[i]+"] couldn't be found in Database.");
               return false;
           }
           else {
               dopToHigh = pinMap.value(insHigh[i]);
           }
           if(!pinMap.contains(insLow[i])) {
               PLAY QMessageBox::warning(nullptr,"Error!!\n","The UUT_con/Pin:["+insHigh[i]+"] couldn't be found in Database.");
               return false;
           }
           else {
               dopToLow = pinMap.value(insLow[i]);
           }

           utilityObj.getLineDops(&dopToHigh,&dopToLow,&highDop,&lowDop);

           //Dop ON
           if(G::writeDOP({dopToHigh,highDop,dopToLow,lowDop},ON)!= 1) return false;
           timerObj.delayInms(TEST_DELAY_MS);//Relay Stabilize Delay

           msrdValue = G::dmmObj->getStableMeterValue(DMM_READ_MAX_DELAY_MS);
           _qDebugE("msrdValue: [",QString::number(msrdValue, 'g', 8),"]","");
           msrdValue -= G::calibVoltage;
           msrdValue = utilityObj.mod(msrdValue);

           //Dop OFF
           if(G::writeDOP({dopToHigh,highDop,dopToLow,lowDop},OFF)!= 1) return false;

           _qDebugE("ExpectedValue: ",expectedValue,", MeasuredValue:",displayValueStr);

           if(utilityObj.getResult(expectedValue,msrdValue) == false) {
               displayValueStr = QString("Failed: pair <%1><%2>, Expected <%3>, Measured <%4>")
                       .arg(insHigh[i]).arg(insLow[i]).arg(expectedValue).arg(utilityObj.getDisplayValue(expectedValue,msrdValue));
               remarkStr = G::testTable->item(rowNo,remarkIndex)->text();
               if(remarkStr.size() > 0) remarkStr += "\n";
               remarkStr += displayValueStr;

               G::testTable->item(rowNo,remarkIndex)->setText(remarkStr);
               G::testTable->resizeRowToContents(rowNo);

               if(!G::updateResult(false,rowNo,resultIndex,isPassed)) {
                   PLAY QMessageBox::warning(nullptr,"Error!!",QString("Load: Couldn't update the result in table at ROW:[%1].")
                                        .arg(rowNo+1));
                   return false;
               }
               isPassed = false;
           }
     }
     return  true;
  }

  /*************************************************************************
   Function Name - setDMM
   Parameter(s)  - void
   Return Type   - bool
   Action        - Sets DMM to corresponding mode.
   *************************************************************************/
  bool Load::setDMM()
  {
      Timer timer;

      G::showInfoMessage(TITLE,"Please wait!! DMM is being set to Current Mode...  ");
      timer.delayInms(1);

      DMM::isreadBusy = true;
      while(!G::handlerObj->isDMMReady) timer.delayInms(1);

#if(DMM_PRESENT)
       G::dmmObj->resetDMM();//Reset DMM
       if(!G::dmmObj->setCurrentMode()) {//Set Current Mode for Load Test
           G::closeTestMessage();
           PLAY QMessageBox::warning(nullptr,"Error!!","Load: Unable to set DMM in Current Mode.");
           return false;
       }
#endif
     G::closeTestMessage();
     return true;
  }


  /*************************************************************************
  Function Name - resetDevice
  Parameter(s)  - const QMap<QString, uint32>&, const QMap<QString, uint32>&,
                  const Utility::ResetData&
  Return Type   - bool
  Action        - Resets device at the test end.
  *************************************************************************/
  bool Load::resetDevice(const QMap<QString, uint32>& dopMap, const QMap<QString, uint32>& dipMap, const Utility::ResetData& resetData)
  {
      _qDebug("bool Load::resetDevice(const QMap<QString, uint32>& dopMap,const QMap<QString, uint32>& dipMap,const Utility::ResetData& resetData)");

      QStringList uutConnPins;
      QString expDipValues;
      QString dipValues;
      QVector<uint32_t> dips;
      Timer timerObj;

      for(int index = 0;index < resetData.count;index++) {
         _qDebugE(index,"-----------\n","Dips:",resetData.dipsList.at(index));

         uutConnPins = resetData.dipsList.at(index).split(",");
         expDipValues = resetData.dipsExpectedValueList.at(index);

         dips.clear();
         for(QString connPin : uutConnPins) {
           if(!dipMap.contains(connPin)) {
               PLAY QMessageBox::warning(nullptr,"Error!!\n","ResetDevice:The UUT_con/Pin:["+connPin+"] couldn't be found in Database.");
               return false;
           }
           else {
               dips.push_back(dipMap.value(connPin));
           }
         }
         if(G::readDIP(dips,dipValues) != 1) {
             return false;
         }

         _qDebugE("ExpectedValue:",resetData.dipsExpectedValueList.at(index),"\ndipValues:",dipValues);

         //Compare DipStatus
         if(expDipValues != dipValues) {
             _qDebugE("DopsHigh2:",resetData.dopsHigh2List.at(index),"\nDopsLow2:",resetData.dopsLow2List.at(index));

             if(!G::onDops(dopMap,resetData.dopsHigh2List.at(index),G::PCI::P7444)) return false;
             if(resetData.dopsHigh2List.count() > 0) timerObj.delayInms(LATCH_RELAY_DELAY_MS);

             if(!G::offDops(dopMap,resetData.dopsLow2List.at(index),G::PCI::P7444)) return false;
             if(resetData.dopsLow2List.count() > 0) timerObj.delayInms(LATCH_RELAY_DELAY_MS);
         }
      }
      return true;
  }
