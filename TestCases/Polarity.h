/*************************************  FILE HEADER  *****************************************************************
*
*  File name - Polarity.h
*  Creation date and time - 08-JUNE-2020 , MON 08:50 PM IST
*  Author: - Manoj
*  Purpose of the file - All "Polarity Test" modules will be declared .
*
**********************************************************************************************************************/
#ifndef POLARITY_H
#define POLARITY_H

/********************************************************
 Inclusion of header file
 ********************************************************/
#include "Support/Global.h"

/********************************************************
 Class Declaration
 ********************************************************/
class Polarity
{
public:
    Polarity();
    ~Polarity();

    static void runTest(const QMap<QString, uint32>& pinMap,
                        const QMap<QString, uint32>& dopMap,
                        const QMap<QString, uint32>& dipMap,
                        const QStringList& hiddenHeader,
                        const QVector<QStringList>& hiddenData,
                        const Utility::ResetData& resetData
                        );

private:
    bool isParameterInitialized();
    bool readDipsAndUpdate(const QMap<QString, uint32>& dipMap,const QString& connPins,const QString& expectedDipValue,int rowNo,int remarkIndex,int resultIndex,bool isBefore,bool& isPassed);
    bool measValueAndUpdate(const QMap<QString, uint32>& pinMap,const QString& INsToHigh,const QString& INsToLow,
                            const QString& expectedValue,int rowNo,int msrdValIndex,int resultIndex,bool& isPassed);
    bool measValueWithoutUpdate(const QMap<QString, uint32>& pinMap,const QString& INsToHigh,const QString& INsToLow,
                                const QString& expectedValue,int rowNo,int remarkIndex,int resultIndex,bool& isPassed);
    bool setDMM();
    bool resetDevice(const QMap<QString, uint32>& dopMap,
                     const QMap<QString, uint32>& dipMap,
                     const Utility::ResetData& resetData);

    static Polarity *thisObj;
};

#endif // POLARITY_H
