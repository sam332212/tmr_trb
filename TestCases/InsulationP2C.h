/*************************************  FILE HEADER  *****************************************************************
*
*  File name - InsulationP2C.h
*  Creation date and time - 05-JUN-2020 , FRI 07:14 AM IST
*  Author: - Manoj
*  Purpose of the file - All "Insulation Pin To Chassis" modules will be declared .
*
**********************************************************************************************************************/

#ifndef INSULATIONP2C_H
#define INSULATIONP2C_H

/********************************************************
 Inclusion of header file
 ********************************************************/
#include "Support/Global.h"

/********************************************************
 Class Declaration
 ********************************************************/
class InsulationP2C
{
public:
    InsulationP2C();
    ~InsulationP2C();

    static void runTest(const QMap<QString, uint32>& pinMap,
                        const QStringList& hiddenHeader,
                        const QVector<QStringList>& hiddenData);

private:
    bool isParameterInitialized();

    bool measValueAndUpdate(const QMap<QString, uint32>& pinMap,const QString& INsToHigh,
                            const QString& expectedValue,int rowNo,int msrdValIndex,int resultIndex,bool& isPassed);
    bool setIrMeter();

    static InsulationP2C *thisObj;
};

#endif // INSULATIONP2C_H
