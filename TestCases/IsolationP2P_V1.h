/*************************************  FILE HEADER  *****************************************************************
*
*  File name - IsolationP2P_V1.h
*  Creation date and time - 21-NOV-2021 , SAT 12:39 PM IST
*  Author: - Manoj
*  Purpose of the file - All "Isolation Pin To Pin" modules will be declared .
*
**********************************************************************************************************************/

#ifndef ISOLATIONP2P_V1_H
#define ISOLATIONP2P_V1_H

/********************************************************
 Inclusion of header file
 ********************************************************/
#include "Support/Global.h"

class IsolationP2P_V1
{
public:
    IsolationP2P_V1();
    ~IsolationP2P_V1();

    static void runTest(const QMap<QString, uint32> pinMap,const QStringList& hiddenHeader,const QVector<QStringList>& hiddenData);

private:
    bool isParameterInitialized();
    bool measValueAndUpdate(const QMap<QString, uint32>& pinMap,const QString& INsToHigh,const QString& INsToLow,
                            const QString& expectedValue,int rowNo,int msrdValIndex,int resultIndex,bool& isPassed);
    bool setDMM();

    static IsolationP2P_V1 *thisObj;
};

#endif // ISOLATIONP2P_V1_H
