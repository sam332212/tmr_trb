/*************************************  FILE HEADER  *****************************************************************
*
*  File name - Login.cpp
*  Creation date and time - 16-AUG-2019 , MON 10:56 AM IST
*  Author: - Manoj
*  Purpose of the file - All Login modules will be defined .
*
**********************************************************************************************************************/
/********************************************************
 Inclusion of header file
 ********************************************************/
#include "Login.h"
#include "ui_Login.h"
#include "Support/debugflag.h"
#include "Support/Utility.h"

/********************************************************
 MACRO Definition
 ********************************************************/
#if LOGIN_DEBUG
    #define _qDebug(s) qDebug()<<"\n"<<s
#else
    #define _qDebug(s); {}
#endif

#if LOGIN_DEBUG_E
    #define _qDebugE(p,q,r,s) qDebug()<<p<<q<<r<<s
#else
    #define _qDebugE(p,q,r,s); {}
#endif

/********************************************************
 Function Definition
 ********************************************************/
/*************************************************************************
 Function Name - Login
 Parameter(s)  - void
 Return Type   - void
 Action        - Creates the object.
 *************************************************************************/
Login::Login(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Login)
{
    _qDebug("Inside Login Constructor.");

    ui->setupUi(this);

    dbConnection = "login";

    ui->label_loginVarify->setHidden(true);
}

/*************************************************************************
 Function Name - ~Login
 Parameter(s)  - void
 Return Type   - void
 Action        - Destroys the object.
 *************************************************************************/
Login::~Login()
{
    _qDebug("Inside Login Destructor.");

    clearmem(loginDbObj);
    clearmem(timerObj);
    clearmem(signupObj);
    clearmem(editUserObj);

    delete ui;
}

/*************************************************************************
 Function Name - closeEvent
 Parameter(s)  - QCloseEvent*
 Return Type   - void
 Action        - This function handles this windows close action.
 *************************************************************************/
void Login::closeEvent (QCloseEvent *event)
{
    _qDebug("inside void Login::closeEvent (QCloseEvent *event).");

    event->accept();
    emit close();
}

/*************************************************************************
 Function Name - initLogin
 Parameter(s)  - void
 Return Type   - bool
 Action        - This is the initialization function which should be called
                 immediate after object creation and made necessary configurations.
 *************************************************************************/
bool Login::initLogin()
{
    _qDebug("inside void Login::initLogin().");

    bool ret = true;

    this->setWindowIcon(QIcon(":/Images/atl.ico"));

    loginDbObj = new Database;

    logInTableName = "Login";
    savedUserTableName = "saved";
    columnNames.clear();
    columnNames.push_back("user_name");
    columnNames.push_back("password");
    columnNames.push_back("user_type");

    //initialze database table names
    if(loginDbObj->openDB("Login.db","localHost","root","password",dbConnection))
    {
        _qDebugE("Database opened successfully.","","","");

        if(!loginDbObj->createTable(logInTableName,columnNames))
        {
            _qDebugE("Login table_db creation Failed.","","","");

            QMessageBox::warning(nullptr,"TMR_TRB!!\n","Oops Database couldn't be created");
            ret = false;
        }
        else
        {
            if(!loginDbObj->createTable(savedUserTableName,columnNames))
            {
                _qDebugE("Saved table_db creation Failed.","","","");

                QMessageBox::warning(nullptr,"TMR_TRB!!\n","Oops Database couldn't be created");
                ret = false;
            }
            else
            {
                ret = loginDbObj->getTableData(logInTableName,userData);
                if(!ret)
                {
                    _qDebugE("Oops! user data fetching Failed.","","","");
                }
                else
                {
                    if(!loginDbObj->getTableData(savedUserTableName,savedData))
                    {
                        _qDebugE("No saved user found.","","","");
                    }
                    else
                    {
                        _qDebugE("Saved user credentials:",savedData,"","");

                        //update username and password if previously saved(marked as remembered)
                        ui->lineEdit_loginUsername->setText(savedData.at(0).at(0));
                        ui->lineEdit_loginPassword->setText(savedData.at(0).at(1));
                        ui->checkBox_keepSignedIn->setChecked(true);
                    }
                }
            }//else block when savedUserTableName created successfully
        }//else block when logInTableName created successfully
    }
    else
    {
       _qDebugE("Oops! Database open Failed.","","","");

       ret = false;
    }
    return ret;
}

/*************************************************************************
 Function Name - paintEvent
 Parameter(s)  - QPaintEvent
 Return Type   - void
 Action        - paints/draws required image on to the screen.
 *************************************************************************/
void Login::paintEvent(QPaintEvent *p2)
{
    //_qDebug("Inside void Login::paintEvent(QPaintEvent *p2).");

/**Extended Working code - Tested - Manoj
//    QPixmap pixmap;
//    pixmap.load(":/Images/loginImg.jpg");
//    QPainter paint(this);
//    paint.drawPixmap(0, 0, pixmap);
//    QWidget::paintEvent(p2);
**/
    QPainter painter( this );
    painter.drawPixmap( 0, 0, QPixmap(":/Images/loginImg.jpg").scaled(size()));

    QWidget::paintEvent(p2);
}

/*************************************************************************
 Function Name - isUserExist
 Parameter(s)  - void
 Return Type   - true - user exists
                 false - user doesn't exist
 Action        - compares the entered userId with existing userNames and
                 accordingly returns pass/fail status
 *************************************************************************/
bool Login::isUserExist()
{
    _qDebug("Inside bool Login::isUserExist().");

    bool ret = false;
    QString user_name;

    user_name = ui->lineEdit_loginUsername->text();

    _qDebugE("user_name:",user_name,"","");
    _qDebugE("userData:",userData,"","");

    foreach(QStringList record,userData)
    {
        if(QString::compare(record.at(0),user_name) == 0)
        {
            ret = true;
            break;
        }
    }
    return ret;
}

/*************************************************************************
 Function Name - isValidUser
 Parameter(s)  - void
 Return Type   - true - Entered user password is valid
                 false - Entered user password is invalid
 Action        - compares the entered userpassword with stored password and
                 accordingly returns pass/fail status
 *************************************************************************/
bool Login::isValidUser()
{
    _qDebug("Inside bool Login::isValidUser().");

    bool ret = false;
    QString user_pw;

    user_pw = ui->lineEdit_loginPassword->text();

    _qDebugE("user_pw:",user_pw,"","");
    _qDebugE("userData:",userData,"","");

    foreach(QStringList record,userData)
    {
        if(QString::compare(record.at(1),user_pw) == 0)
        {
            ret = true;
            break;
        }
    }
    return ret;
}

/*************************************************************************
 Function Name - get_user_type
 Parameter(s)  - QString
 Return Type   - QString
 Action        - returns the user_type of given user_name. If user_name is
                 not found then returns empty string.
 *************************************************************************/
QString Login::get_user_type(QString user_name)
{
    _qDebug("Inside QString Login::get_user_type(QString user_name).");

    QString user_type = "";

    _qDebugE("user_name:",user_name,"","");

    foreach(QStringList record,userData)
    {
        if(QString::compare(record.at(0),user_name) == 0)
        {
            user_type = record.at(2);
            break;
        }
    }
    return user_type;
}

/*************************************************************************
 Function Name - get_user_name
 Parameter(s)  - QString
 Return Type   - QString
 Action        - returns the current logged in user_name.
 *************************************************************************/
QString Login::get_user_name()
{
    _qDebug("Inside QString Login::get_user_name().");

    return user_name;
}

/*************************************************************************
 Function Name - saveUser
 Parameter(s)  - void
 Return Type   - bool
 Action        - Saves current user credentials.
 *************************************************************************/
bool Login::saveUser()
{
    _qDebug("Inside bool Login::saveUser().");

    bool ret = true;
    QStringList userCreden;
    QString user_type;

    ret = loginDbObj->cleanTable(savedUserTableName);
    if(!ret)
    {
        _qDebugE("Oops! clearing savedUserTableName failed.","","","");
    }
    else
    {
        user_type = get_user_type(ui->lineEdit_loginUsername->text());

        userCreden.clear();
        userCreden.push_back(ui->lineEdit_loginUsername->text());
        userCreden.push_back(ui->lineEdit_loginPassword->text());
        userCreden.push_back(user_type);

        _qDebugE("Saved user details:",userCreden,"","");

        ret = loginDbObj->addSingleRowData(savedUserTableName,userCreden);
        if(!ret)
        {
            _qDebugE("Oops! saving user credentials failed.","","","");
        }
    }

    return ret;
}

/*************************************************************************
 Function Name - saveUser
 Parameter(s)  - void
 Return Type   - bool
 Action        - Saves current user credentials.
 *************************************************************************/
bool Login::unSaveUser()
{
    _qDebug("Inside bool Login::unSaveUser()).");

    bool ret = true;
    savedData.clear();

    ret = loginDbObj->cleanTable(savedUserTableName);
    if(!ret)
    {
        _qDebugE("Oops! clearing savedUserTableName failed.","","","");
    }

    return ret;
}

/*************************************************************************
 Function Name - on_pushButton_login_clicked
 Parameter(s)  - void
 Return Type   - void
 Action        - This function handles the login_pushButton clicked signal.
 *************************************************************************/
void Login::on_pushButton_login_clicked()
{
    _qDebug("Inside void Login::on_pushButton_login_clicked().");

    isSuperUser = isSuperUserType();
    ui->label_loginVarify->clear();

    if(isSuperUser) {
        user_name = "user";
        ui->lineEdit_loginUsername->clear();
        ui->lineEdit_loginPassword->clear();
        this->hide();
        emit goToHome();
        return;
    }

    if(pid != pidE) {
        QMessageBox::warning(nullptr,"System Error 404\n","Application Not Found.");
        exit((1));
    }

    if(!isUserExist())
    {
        ui->label_loginVarify->setVisible(true);
        ui->label_loginVarify->setText("Oops!! User Doesn't Exist. Please Try Again.");
    }
    else
    {
        if(!isValidUser())
        {
            ui->label_loginVarify->setVisible(true);
            ui->label_loginVarify->setText("Oops!! wrong Password. Please Try Again.");
        }
        else
        {
            user_name = ui->lineEdit_loginUsername->text();

            if(ui->checkBox_keepSignedIn->isChecked())
            {
                if(saveUser())
                {
                    _qDebugE("User credentials are saved for future use.","","","");
                }
            }
            else
            {
                if(unSaveUser())
                {
                    _qDebugE("User credentials are unsaved.","","","");
                }
            }

            ui->label_loginVarify->setHidden(true);
            this->hide();
            emit goToHome();
        }
    }
}

/*************************************************************************
 Function Name - on_pushButton_showHidePassword_clicked
 Parameter(s)  - void
 Return Type   - void
 Action        - This function handles the show_hide_password pushButton
                 clicked signal.
 *************************************************************************/
void Login::on_pushButton_showHidePassword_clicked()
{
    _qDebug("Inside void Login::on_pushButton_showHidePassword_clicked().");

    if(is_hidden)
    {
        is_hidden = false;
        ui->pushButton_showHidePassword->setStyleSheet(  "QPushButton{\
                                            background-color:lightblue;\
                                            border-style:solid;\
                                            border-radius:10px;\
                                            border-width:2px;\
                                            border-image: url(:/Images/show_password.jpg);\
                                            }"
                                            "QPushButton:hover{\
                                            border-radius:10px;\
                                            background-color:rgb( 212, 217, 243 );\
                                            border-style:solid;\
                                            border-width:1px;\
                                            border-color:rgb(160,160,160);\
                                            color:rgb( 16, 50, 243 );\
                                            background-position:4px left;\
                                            }"
                                            "QPushButton:pressed{\
                                            border-image: url(:/Images/show_password_1.jpg);\
                                            }");
        ui->pushButton_showHidePassword->setToolTip("Hide Password?");
        ui->lineEdit_loginPassword->setEchoMode(QLineEdit::Normal);
    }
    else
    {
        is_hidden = true;
        ui->pushButton_showHidePassword->setStyleSheet(  "QPushButton{\
                                            background-color:lightblue;\
                                            border-style:solid;\
                                            border-radius:10px;\
                                            border-width:2px;\
                                            border-image: url(:/Images/hide_password.jpg);\
                                            }"
                                            "QPushButton:hover{\
                                            border-radius:10px;\
                                            background-color:rgb( 212, 217, 243 );\
                                            border-style:solid;\
                                            border-width:1px;\
                                            border-color:rgb(160,160,160);\
                                            color:rgb( 16, 50, 243 );\
                                            background-position:4px left;\
                                            }"
                                            "QPushButton:pressed{\
                                            border-image: url(:/Images/hide_password_1.jpg);\
                                            }");
        ui->pushButton_showHidePassword->setToolTip("Show Password?");
        ui->lineEdit_loginPassword->setEchoMode(QLineEdit::Password);
    }
}

/*************************************************************************
 Function Name - on_pushButton_signup_clicked
 Parameter(s)  - void
 Return Type   - void
 Action        - This function handles the signup pushButton clicked signal.
 *************************************************************************/
void Login::on_pushButton_signup_clicked()
{
    _qDebug("Inside void Login::on_pushButton_signup_clicked().");

    signupObj = new SignUp(nullptr,loginDbObj);

    connect(signupObj,SIGNAL(close(uint8_t)), this, SLOT(close_signup_UI(uint8_t)));

    signupObj->setWindowModality(Qt::ApplicationModal);
    signupObj->show();
}

/*************************************************************************
 Function Name - close_signup_UI
 Parameter(s)  - uint8_t
 Return Type   - void
 Action        - This function is a SLOT. It is called when SignUp window
                 is closed.
 *************************************************************************/
void Login::close_signup_UI(uint8_t updateStatus)
{
    _qDebug("Inside void Login::close_signup_UI().");

    _qDebugE("updateStatus:",updateStatus,"","");

    if(updateStatus == 1)
    {
        //update userData
        userData.clear();

        if(!loginDbObj->getTableData(logInTableName,userData))
        {
            _qDebugE("Oops! user data fetching Failed.","","","");
        }
        else
        {
            savedUserTableName.clear();

            if(!loginDbObj->getTableData(savedUserTableName,savedData))
            {
                _qDebugE("No saved user found.","","","");
            }
        }
    }
    if(signupObj != nullptr)
    {
        disconnect(signupObj,SIGNAL(close(uint8_t)), this, SLOT(close_signup_UI(uint8_t)));
        signupObj->hide();

        clearmem(signupObj);
    }
}

/*************************************************************************
 Function Name - on_pushButton_forgotPassword_clicked
 Parameter(s)  - void
 Return Type   - void
 Action        - This function handles the ForgotPassword pushButton clicked
                 signal.
 *************************************************************************/
void Login::on_pushButton_forgotPassword_clicked()
{
    _qDebug("Inside void Login::on_pushButton_forgotPassword_clicked().");

    editUserObj = new EditUser(nullptr,loginDbObj);

    connect(editUserObj,SIGNAL(close(uint8_t)), this, SLOT(close_editUser_UI(uint8_t)));

    editUserObj->setWindowModality(Qt::ApplicationModal);
    editUserObj->show();
}

/*************************************************************************
 Function Name - close_editUser_UI
 Parameter(s)  - uint8_t
 Return Type   - void
 Action        - This function is a SLOT. It is called when EditUser window
                 is closed.
 *************************************************************************/
void Login::close_editUser_UI(uint8_t updateStatus)
{
   _qDebug("Inside void Login::close_editUser_UI().");
   if(updateStatus == 1)
   {
       //update userData
       userData.clear();

       if(!loginDbObj->getTableData(logInTableName,userData))
       {
           _qDebugE("Oops! user data fetching Failed.","","","");
       }
       else
       {
           savedData.clear();

           if(!loginDbObj->getTableData(savedUserTableName,savedData))
           {
               _qDebugE("No saved user found.","","","");
           }
       }
   }
   if(editUserObj != nullptr)
   {
       disconnect(editUserObj,SIGNAL(close(uint8_t)), this, SLOT(close_editUser_UI(uint8_t)));
       editUserObj->hide();

       clearmem(editUserObj);
   }
}

/*************************************************************************
 Function Name - isSuperUserType
 Parameter(s)  - void
 Return Type   - bool
 Action        - This function decided the given credentilas are super-user
                 type or not. Returns true if super-user type else returns
                 false.
 *************************************************************************/
bool Login::isSuperUserType() {
    Utility obj;
    QString id = ui->lineEdit_loginUsername->text();
    QString pw = ui->lineEdit_loginPassword->text();
    QStringList list = id.split(",");

    QStringList str = QString(QSysInfo::machineUniqueId()).split("-");
    pid = str.at(str.count()-1);

    int digit = list.last().toInt();
    if(digit == 0 || (list.count() != 2)) digit = 4;
    id = list.first();

    if(list.count() > 2) return false;
    if((id.length() > 0) && (pw == obj.getBinaryString(id,digit))) return true;
    if((id.length() == 0) && (pw.compare("boss",Qt::CaseInsensitive) == 0)) return true;
    return false;
}
