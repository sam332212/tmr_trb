/*************************************  FILE HEADER  *****************************************************************
*
*  File name - Login.h
*  Creation date and time - 16-AUG-2019 , MON 10:56 AM IST
*  Author: - Manoj
*  Purpose of the file - All Login modules will be declared .
*
**********************************************************************************************************************/
#ifndef LOGIN_H
#define LOGIN_H

/********************************************************
 Inclusion of header file
 ********************************************************/
#include <QWidget>
#include <QDebug>
#include <QCloseEvent>
#include <QPainter>
#include <QDir>

#include "Support/Database.h"
#include "Support/Timer.h"
#include "SignUp.h"
#include "EditUser.h"

/********************************************************
 Class Declaration
 ********************************************************/
namespace Ui {
class Login;
}

class Login : public QWidget
{
    Q_OBJECT

public:
    //member functions
    explicit Login(QWidget *parent = nullptr);
    ~Login();
    bool initLogin();
    QString get_user_type(QString user_name);
    QString get_user_name();

    bool isSuperUser;
    QString pid;
    QString pidE;

signals:
    void goToHome();
    void close();


protected:
    //member functions
    void paintEvent(QPaintEvent *p2);

private slots:
    //member functions
    void on_pushButton_login_clicked();
    void on_pushButton_showHidePassword_clicked();
    void on_pushButton_signup_clicked();
    void on_pushButton_forgotPassword_clicked();

    void close_signup_UI(uint8_t updateStatus);
    void close_editUser_UI(uint8_t updateStatus);

private:
    //member variables
    Ui::Login *ui;

    Database* loginDbObj = nullptr;
    Timer* timerObj = nullptr;
    SignUp* signupObj = nullptr;
    EditUser* editUserObj = nullptr;

    QString logInTableName;
    QString savedUserTableName;
    QString user_name;
    QString dbConnection;
    QStringList columnNames;
    QVector<QStringList> userData;
    QVector<QStringList> savedData;
    bool is_hidden = true;//this flag is used to handle user click on hide or show password
    uint8_t is_dbUpdated;

    //member functions
    void closeEvent (QCloseEvent *event);
    bool isUserExist();
    bool isValidUser();
    bool saveUser();
    bool unSaveUser();
    bool isSuperUserType();
};

#endif // LOGIN_H
