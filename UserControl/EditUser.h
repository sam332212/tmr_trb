/*************************************  FILE HEADER  *****************************************************************
*
*  File name - EditUser.h
*  Creation date and time - 25-SEP-2019 , WED 10:00 AM IST
*  Author: - Manoj
*  Purpose of the file - All Edituser modules will be declared .
*
**********************************************************************************************************************/
#ifndef EDITUSER_H
#define EDITUSER_H

/********************************************************
 Inclusion of header file
 ********************************************************/
#include <QWidget>
#include <QCloseEvent>
#include <QMessageBox>

#include "Support/Database.h"

namespace Ui {
class EditUser;
}

/********************************************************
 Class Declaration
 ********************************************************/
class EditUser : public QWidget
{
    Q_OBJECT

public:
    explicit EditUser(QWidget *parent = nullptr,Database* db = nullptr);
    ~EditUser();

signals:
    void close(uint8_t updateStatus);//1->user(s) added,0->No user added

private slots:
    void on_pushButton_save_clicked();

    void on_pushButton_adminPwShowHide_clicked();

    void on_pushButton_userOldPwShowHide_clicked();

    void on_pushButton_userNewPwShowHide_clicked();

private:
    Ui::EditUser *ui;
    Database* loginDbObj = nullptr;

    QVector<QStringList> loginData;
    QString loginTableName;
    uint8_t isUserUpdated = 0;
    bool is_adminPwhidden = true;
    bool is_userOldPwhidden = true;
    bool is_userNewPwhidden = true;

    void closeEvent (QCloseEvent *event);
    bool isValidEntry();
};

#endif // EDITUSER_H
