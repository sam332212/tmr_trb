/*************************************  FILE HEADER  *****************************************************************
*
*  File name - SignUp.h
*  Creation date and time - 23-SEP-2019 , MON 01:00 PM IST
*  Author: - Manoj
*  Purpose of the file - All Signup modules will be declared .
*
**********************************************************************************************************************/
#ifndef SIGNUP_H
#define SIGNUP_H

/********************************************************
 Inclusion of header file
 ********************************************************/
#include <QWidget>
#include <QCloseEvent>
#include <QMessageBox>

#include "Support/Database.h"

namespace Ui {
class SignUp;
}

/********************************************************
 Class Declaration
 ********************************************************/
class SignUp : public QWidget
{
    Q_OBJECT

public:
    //member functions
    explicit SignUp(QWidget *parent = nullptr,Database* db = nullptr);
    ~SignUp();

signals:
    void close(uint8_t updateStatus);//1->user(s) added,0->No user added

private slots:
    //member functions
    void on_pushButton_signup_clicked();

    void on_pushButton_adminPwShowHide_clicked();

    void on_pushButton_userPwShowHide_clicked();

private:
    //member variables
    Ui::SignUp *ui;
    Database* loginDbObj = nullptr;

    QVector<QStringList> loginData;
    QString loginTableName;
    uint8_t isUserAdded = 0;
    bool is_adminPwhidden = true;
    bool is_userPwhidden = true;

    void closeEvent (QCloseEvent *event);
    bool isValidEntry();
};

#endif // SIGNUP_H
