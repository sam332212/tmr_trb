/*************************************  FILE HEADER  *****************************************************************
*
*  File name - EditUser.cpp
*  Creation date and time - 25-SEP-2019 , WED 10:00 AM IST
*  Author: - Manoj
*  Purpose of the file - All Forgot Password modules will be defined .
*
**********************************************************************************************************************/
/********************************************************
 Inclusion of header file
 ********************************************************/
#include "EditUser.h"
#include "ui_EditUser.h"
#include "Support/debugflag.h"

/********************************************************
 MACRO Definition
 ********************************************************/
#if EDITUSER_DEBUG
    #define _qDebug(s) qDebug()<<"\n"<<s
#else
    #define _qDebug(s); {}
#endif

#if EDITUSER_DEBUG_E
    #define _qDebugE(p,q,r,s) qDebug()<<p<<q<<r<<s
#else
    #define _qDebugE(p,q,r,s); {}
#endif

/********************************************************
 Function Definition
 ********************************************************/
/*************************************************************************
 Function Name - EditUser
 Parameter(s)  - QWidget*
 Return Type   - void
 Action        - Creates the object.
 *************************************************************************/
EditUser::EditUser(QWidget *parent,Database* db) :
    QWidget(parent),
    ui(new Ui::EditUser)
{
    _qDebug("Inside EditUser Constructor.");

    ui->setupUi(this);

    loginData.clear();

    loginTableName = "Login";
    loginDbObj = db;

    if(loginDbObj != nullptr)
    {
        loginDbObj->getTableData(loginTableName,loginData);

        _qDebugE("loginData:",loginData,"","");
    }
    this->setWindowTitle("Edit");
}

/*************************************************************************
 Function Name - ~EditUser
 Parameter(s)  - void
 Return Type   - void
 Action        - Destroys the object.
 *************************************************************************/
EditUser::~EditUser()
{
    _qDebug("Inside EditUser Destructor.");

    delete ui;
}

/*************************************************************************
 Function Name - closeEvent
 Parameter(s)  - QCloseEvent*
 Return Type   - void
 Action        - This function handles this windows close action.
 *************************************************************************/
void EditUser::closeEvent (QCloseEvent *event)
{
    _qDebug("inside void EditUser::closeEvent (QCloseEvent *event).");

    event->accept();
    emit close(isUserUpdated);
}

/*************************************************************************
 Function Name - isValidEntry
 Parameter(s)  - void
 Return Type   - bool
 Action        - This function checks for valid user entry in UI.
 *************************************************************************/
bool EditUser::isValidEntry()
{
    _qDebug("Inside bool EditUser::isValidEntry().");

    bool ret = true;

    QVector<QStringList> users;
    QVector<QStringList> admins;
    QStringList userIdPw;
    QStringList adminIdPw;

    QString id;
    QString pw;
    QString userActualPw;

    int index;

    //check for any field is blank
    if((ui->lineEdit_adminId->text() == "") || (ui->lineEdit_adminPassword->text() == "") ||
       (ui->lineEdit_userId->text() == "") || (ui->lineEdit_userOldPassword->text() == "") ||
       (ui->lineEdit_userNewPassword->text() == ""))
    {
        QMessageBox::warning(this,"Error!","All fields are mandatory.\nPlease fill all the fields.");
        ret = false;
    }
    else
    {
        users.clear();
        admins.clear();
        for(index = 0;index < loginData.size();index++)
        {
            if(loginData.at(index).at(2) == "User")
            {
                userIdPw.clear();
                userIdPw.push_back(loginData.at(index).at(0));//id
                userIdPw.push_back(loginData.at(index).at(1));//pw
                users.push_back(userIdPw);
            }
            else if(loginData.at(index).at(2) == "Admin")
            {
                adminIdPw.clear();
                adminIdPw.push_back(loginData.at(index).at(0));//id
                adminIdPw.push_back(loginData.at(index).at(1));//pw
                admins.push_back(adminIdPw);
            }
        }

        //check admin credentials
        id = ui->lineEdit_adminId->text();
        pw = ui->lineEdit_adminPassword->text();
        for(index = 0;index < admins.size();index++)
        {
            if(id.compare(admins.at(index).at(0)) == 0) break;
        }
        if(index == admins.size())
        {
            QMessageBox::warning(this,"Error!","Oops! Admin ID doesn't Exist.");
            ret = false;
        }
        else
        {
            if(pw.compare(admins.at(index).at(1)))
            {
                QMessageBox::warning(this,"Error!","Oops! Admin Password is wrong.");
                ret = false;
            }
            else
            {
                //check user credentials
                id = ui->lineEdit_userId->text();
                pw = ui->lineEdit_userOldPassword->text();
                for(index = 0;index < users.size();index++)
                {
                    if(id.compare(users.at(index).at(0)) == 0)
                    {
                        userActualPw = users.at(index).at(1);
                        break;
                    }
                }
                if(index == users.size())
                {
                    QMessageBox::warning(this,"Error!","Oops! User doesn't Existt.");
                    ret = false;
                }
                else
                {
                    if(pw.compare(userActualPw))
                    {
                        QMessageBox::warning(this,"Error!","Oops! User password is wrong.");
                        ret = false;
                    }
                    else
                    {
                        if(pw.compare(ui->lineEdit_userNewPassword->text()) == 0)
                        {
                            QMessageBox::warning(this,"Error!","Oops! User New password can't be same as Old password.");
                            ret = false;
                        }
                    }
                }
            }
        }
    }
    return  ret;
}

/*************************************************************************
 Function Name - on_pushButton_save_clicked
 Parameter(s)  - void
 Return Type   - void
 Action        - This function handles the save pushbutton clicked signal.
 *************************************************************************/
void EditUser::on_pushButton_save_clicked()
{
    _qDebug("Inside void EditUser::on_pushButton_save_clicked().");

    QString oldCellValue;
    QString newCellValue;
    QStringList columnNames;

    if(isValidEntry())
    {
        //save user credentials
        if(loginDbObj->isOpen())
        {
            columnNames.clear();

            if(loginDbObj->getColumnNames(loginTableName,columnNames))
            {
                oldCellValue = ui->lineEdit_userOldPassword->text();
                newCellValue = ui->lineEdit_userNewPassword->text();

                if(loginDbObj->editCell(loginTableName,columnNames.at(1),oldCellValue,newCellValue))
                {
                    QMessageBox::information(this,"Success!","User password updated successfully.");
                    isUserUpdated = 1;
                }
                else
                {
                    QMessageBox::warning(this,"Error!","User couldn't be saved.");
                }
            }
            else
            {
                QMessageBox::warning(this,"Error!","Database Error. User couldn't be saved.");
            }
        }
        else
        {
            QMessageBox::warning(this,"Error!","Database is not Open.");
        }
    }
}

/*************************************************************************
 Function Name - on_pushButton_adminPwShowHide_clicked
 Parameter(s)  - void
 Return Type   - void
 Action        - This function handles the adminPwShowHide pushbutton clicked
                 signal.
 *************************************************************************/
void EditUser::on_pushButton_adminPwShowHide_clicked()
{
    _qDebug("Inside void EditUser::on_pushButton_adminPwShowHide_clicked().");

    if(is_adminPwhidden)
    {
        is_adminPwhidden = false;
        ui->pushButton_adminPwShowHide->setStyleSheet(  "QPushButton{\
                                            background-color:lightblue;\
                                            border-style:solid;\
                                            border-radius:10px;\
                                            border-width:2px;\
                                            border-image: url(:/Images/show_password.jpg);\
                                            }"
                                            "QPushButton:hover{\
                                            border-radius:10px;\
                                            background-color:rgb( 212, 217, 243 );\
                                            border-style:solid;\
                                            border-width:1px;\
                                            border-color:rgb(160,160,160);\
                                            color:rgb( 16, 50, 243 );\
                                            background-position:4px left;\
                                            }"
                                            "QPushButton:pressed{\
                                            border-image: url(:/Images/show_password_1.jpg);\
                                            }");
        ui->pushButton_adminPwShowHide->setToolTip("Hide Password?");
        ui->lineEdit_adminPassword->setEchoMode(QLineEdit::Normal);
    }
    else
    {
        is_adminPwhidden = true;
        ui->pushButton_adminPwShowHide->setStyleSheet(  "QPushButton{\
                                            background-color:lightblue;\
                                            border-style:solid;\
                                            border-radius:10px;\
                                            border-width:2px;\
                                            border-image: url(:/Images/hide_password.jpg);\
                                            }"
                                            "QPushButton:hover{\
                                            border-radius:10px;\
                                            background-color:rgb( 212, 217, 243 );\
                                            border-style:solid;\
                                            border-width:1px;\
                                            border-color:rgb(160,160,160);\
                                            color:rgb( 16, 50, 243 );\
                                            background-position:4px left;\
                                            }"
                                            "QPushButton:pressed{\
                                            border-image: url(:/Images/hide_password_1.jpg);\
                                            }");
        ui->pushButton_adminPwShowHide->setToolTip("Show Password?");
        ui->lineEdit_adminPassword->setEchoMode(QLineEdit::Password);
    }
}

/*************************************************************************
 Function Name - on_pushButton_userOldPwShowHide_clicked
 Parameter(s)  - void
 Return Type   - void
 Action        - This function handles the userOldPwShowHide pushbutton clicked
                 signal.
 *************************************************************************/
void EditUser::on_pushButton_userOldPwShowHide_clicked()
{
    _qDebug("Inside void EditUser::on_pushButton_userOldPwShowHide_clicked().");

    if(is_userOldPwhidden)
    {
        is_userOldPwhidden = false;
        ui->pushButton_userOldPwShowHide->setStyleSheet(  "QPushButton{\
                                            background-color:lightblue;\
                                            border-style:solid;\
                                            border-radius:10px;\
                                            border-width:2px;\
                                            border-image: url(:/Images/show_password.jpg);\
                                            }"
                                            "QPushButton:hover{\
                                            border-radius:10px;\
                                            background-color:rgb( 212, 217, 243 );\
                                            border-style:solid;\
                                            border-width:1px;\
                                            border-color:rgb(160,160,160);\
                                            color:rgb( 16, 50, 243 );\
                                            background-position:4px left;\
                                            }"
                                            "QPushButton:pressed{\
                                            border-image: url(:/Images/show_password_1.jpg);\
                                            }");
        ui->pushButton_userOldPwShowHide->setToolTip("Hide Password?");
        ui->lineEdit_userOldPassword->setEchoMode(QLineEdit::Normal);
    }
    else
    {
        is_userOldPwhidden = true;
        ui->pushButton_userOldPwShowHide->setStyleSheet(  "QPushButton{\
                                            background-color:lightblue;\
                                            border-style:solid;\
                                            border-radius:10px;\
                                            border-width:2px;\
                                            border-image: url(:/Images/hide_password.jpg);\
                                            }"
                                            "QPushButton:hover{\
                                            border-radius:10px;\
                                            background-color:rgb( 212, 217, 243 );\
                                            border-style:solid;\
                                            border-width:1px;\
                                            border-color:rgb(160,160,160);\
                                            color:rgb( 16, 50, 243 );\
                                            background-position:4px left;\
                                            }"
                                            "QPushButton:pressed{\
                                            border-image: url(:/Images/hide_password_1.jpg);\
                                            }");
        ui->pushButton_userOldPwShowHide->setToolTip("Show Password?");
        ui->lineEdit_userOldPassword->setEchoMode(QLineEdit::Password);
    }
}

/*************************************************************************
 Function Name - on_pushButton_userNewPwShowHide_clicked
 Parameter(s)  - void
 Return Type   - void
 Action        - This function handles the userNewPwShowHide pushbutton clicked
                 signal.
 *************************************************************************/
void EditUser::on_pushButton_userNewPwShowHide_clicked()
{
    _qDebug("Inside void EditUser::on_pushButton_userNewPwShowHide_clicked().");

    if(is_userNewPwhidden)
    {
        is_userNewPwhidden = false;
        ui->pushButton_userNewPwShowHide->setStyleSheet(  "QPushButton{\
                                            background-color:lightblue;\
                                            border-style:solid;\
                                            border-radius:10px;\
                                            border-width:2px;\
                                            border-image: url(:/Images/show_password.jpg);\
                                            }"
                                            "QPushButton:hover{\
                                            border-radius:10px;\
                                            background-color:rgb( 212, 217, 243 );\
                                            border-style:solid;\
                                            border-width:1px;\
                                            border-color:rgb(160,160,160);\
                                            color:rgb( 16, 50, 243 );\
                                            background-position:4px left;\
                                            }"
                                            "QPushButton:pressed{\
                                            border-image: url(:/Images/show_password_1.jpg);\
                                            }");
        ui->pushButton_userNewPwShowHide->setToolTip("Hide Password?");
        ui->lineEdit_userNewPassword->setEchoMode(QLineEdit::Normal);
    }
    else
    {
        is_userNewPwhidden = true;
        ui->pushButton_userNewPwShowHide->setStyleSheet(  "QPushButton{\
                                            background-color:lightblue;\
                                            border-style:solid;\
                                            border-radius:10px;\
                                            border-width:2px;\
                                            border-image: url(:/Images/hide_password.jpg);\
                                            }"
                                            "QPushButton:hover{\
                                            border-radius:10px;\
                                            background-color:rgb( 212, 217, 243 );\
                                            border-style:solid;\
                                            border-width:1px;\
                                            border-color:rgb(160,160,160);\
                                            color:rgb( 16, 50, 243 );\
                                            background-position:4px left;\
                                            }"
                                            "QPushButton:pressed{\
                                            border-image: url(:/Images/hide_password_1.jpg);\
                                            }");
        ui->pushButton_userNewPwShowHide->setToolTip("Show Password?");
        ui->lineEdit_userNewPassword->setEchoMode(QLineEdit::Password);
    }
}
