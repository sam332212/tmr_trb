/*************************************  FILE HEADER  *****************************************************************
*
*  File name - SignUp.cpp
*  Creation date and time - 23-SEP-2019 , MON 01:00 PM IST
*  Author: - Manoj
*  Purpose of the file - All Signup modules will be defined .
*
**********************************************************************************************************************/
/********************************************************
 Inclusion of header file
 ********************************************************/
#include "SignUp.h"
#include "ui_SignUp.h"
#include "Support/debugflag.h"

/********************************************************
 MACRO Definition
 ********************************************************/
#ifdef SIGNUP_DEBUG
    #define _qDebug(s) qDebug()<<"\n"<<s
#else
    #define _qDebug(s); {}
#endif

#ifdef SIGNUP_DEBUG_E
    #define _qDebugE(p,q,r,s) qDebug()<<p<<q<<r<<s
#else
    #define _qDebugE(p,q,r,s); {}
#endif

/********************************************************
 Function Definition
 ********************************************************/
/*************************************************************************
 Function Name - SignUp
 Parameter(s)  - QWidget*
 Return Type   - void
 Action        - Creates the object.
 *************************************************************************/
SignUp::SignUp(QWidget *parent,Database* db) :
    QWidget(parent),
    ui(new Ui::SignUp)
{
    _qDebug("Inside SignUp Constructor.");

    ui->setupUi(this);

    loginData.clear();

    loginTableName = "Login";
    loginDbObj = db;

    if(loginDbObj != nullptr)
    {
        loginDbObj->getTableData(loginTableName,loginData);

        _qDebugE("loginData:",loginData,"","");
    }
    this->setWindowTitle("SignUp");
}

/*************************************************************************
 Function Name - ~SignUp
 Parameter(s)  - void
 Return Type   - void
 Action        - Destroys the object.
 *************************************************************************/
SignUp::~SignUp()
{
    _qDebug("Inside SignUp Destructor.");
    delete ui;
}

/*************************************************************************
 Function Name - closeEvent
 Parameter(s)  - QCloseEvent*
 Return Type   - void
 Action        - This function handles this windows close action.
 *************************************************************************/
void SignUp::closeEvent (QCloseEvent *event)
{
    _qDebug("inside void SignUp::closeEvent (QCloseEvent *event).");

    event->accept();
    emit close(isUserAdded);
}

/*************************************************************************
 Function Name - isValidEntry
 Parameter(s)  - void
 Return Type   - bool
 Action        - This function checks for valid user entry in UI.
 *************************************************************************/
bool SignUp::isValidEntry()
{
    _qDebug("Inside bool SignUp::isValidEntry().");

    bool ret = true;

    QVector<QStringList> users;
    QVector<QStringList> admins;
    QStringList userIdPw;
    QStringList adminIdPw;

    QString id;
    QString pw;

    int index;

    //check for any field is blank
    if((ui->lineEdit_adminId->text() == "") || (ui->lineEdit_adminPassword->text() == "") ||
       (ui->lineEdit_userId->text() == "") || (ui->lineEdit_userPassword->text() == ""))
    {
        QMessageBox::warning(this,"Error!","All fields are mandatory.\nPlease fill all the fields.");
        ret = false;
    }
    else
    {
        users.clear();
        admins.clear();
        for(index = 0;index < loginData.size();index++)
        {
            if(loginData.at(index).at(2) == "User")
            {
                userIdPw.clear();
                userIdPw.push_back(loginData.at(index).at(0));//id
                userIdPw.push_back(loginData.at(index).at(1));//pw
                users.push_back(userIdPw);
            }
            else if(loginData.at(index).at(2) == "Admin")
            {
                adminIdPw.clear();
                adminIdPw.push_back(loginData.at(index).at(0));//id
                adminIdPw.push_back(loginData.at(index).at(1));//pw
                admins.push_back(adminIdPw);
            }
        }

        //check admin credentials
        id = ui->lineEdit_adminId->text();
        pw = ui->lineEdit_adminPassword->text();
        for(index = 0;index < admins.size();index++)
        {
            if(id.compare(admins.at(index).at(0)) == 0) break;
        }
        if(index == admins.size())
        {
            QMessageBox::warning(this,"Error!","Oops! Admin ID doesn't Exist.");
            ret = false;
        }
        else
        {
            if(pw.compare(admins.at(index).at(1)))
            {
                QMessageBox::warning(this,"Error!","Oops! Admin Password is wrong.");
                ret = false;
            }
            else
            {
                //check user credentials
                id = ui->lineEdit_userId->text();

                for(index = 0;index < users.size();index++)
                {
                    if(id.compare(users.at(index).at(0)) == 0) break;
                }
                if(index != users.size())
                {
                    QMessageBox::warning(this,"Error!","Oops! User Already Exist.");
                    ret = false;
                }
            }
        }
    }
    return  ret;
}

/*************************************************************************
 Function Name - on_pushButton_signup_clicked
 Parameter(s)  - void
 Return Type   - void
 Action        - This function handles the signup pushbutton clicked signal.
 *************************************************************************/
void SignUp::on_pushButton_signup_clicked()
{
    _qDebug("Inside void SignUp::on_pushButton_signup_clicked().");

    QStringList userData;

    if(isValidEntry())
    {
        //save user credentials
        if(loginDbObj->isOpen())
        {
            userData.clear();
            userData.push_back(ui->lineEdit_userId->text());
            userData.push_back(ui->lineEdit_userPassword->text());
            userData.push_back("User");

            if(loginDbObj->addSingleRowData(loginTableName,userData))
            {
                QMessageBox::information(this,"Success!","User saved successfully.");
                isUserAdded = 1;
            }
            else
            {
                QMessageBox::warning(this,"Error!","User couldn't be saved.");
            }
        }
    }
}

/*************************************************************************
 Function Name - on_pushButton_adminPwShowHide_clicked
 Parameter(s)  - void
 Return Type   - void
 Action        - This function handles the adminPwShowHide pushbutton clicked
                 signal.
 *************************************************************************/
void SignUp::on_pushButton_adminPwShowHide_clicked()
{
    _qDebug("Inside void SignUp::on_pushButton_adminPwShowHide_clicked().");

    if(is_adminPwhidden)
    {
        is_adminPwhidden = false;
        ui->pushButton_adminPwShowHide->setStyleSheet(  "QPushButton{\
                                            background-color:lightblue;\
                                            border-style:solid;\
                                            border-radius:10px;\
                                            border-width:2px;\
                                            border-image: url(:/Images/show_password.jpg);\
                                            }"
                                            "QPushButton:hover{\
                                            border-radius:10px;\
                                            background-color:rgb( 212, 217, 243 );\
                                            border-style:solid;\
                                            border-width:1px;\
                                            border-color:rgb(160,160,160);\
                                            color:rgb( 16, 50, 243 );\
                                            background-position:4px left;\
                                            }"
                                            "QPushButton:pressed{\
                                            border-image: url(:/Images/show_password_1.jpg);\
                                            }");
        ui->pushButton_adminPwShowHide->setToolTip("Hide Password?");
        ui->lineEdit_adminPassword->setEchoMode(QLineEdit::Normal);
    }
    else
    {
        is_adminPwhidden = true;
        ui->pushButton_adminPwShowHide->setStyleSheet(  "QPushButton{\
                                            background-color:lightblue;\
                                            border-style:solid;\
                                            border-radius:10px;\
                                            border-width:2px;\
                                            border-image: url(:/Images/hide_password.jpg);\
                                            }"
                                            "QPushButton:hover{\
                                            border-radius:10px;\
                                            background-color:rgb( 212, 217, 243 );\
                                            border-style:solid;\
                                            border-width:1px;\
                                            border-color:rgb(160,160,160);\
                                            color:rgb( 16, 50, 243 );\
                                            background-position:4px left;\
                                            }"
                                            "QPushButton:pressed{\
                                            border-image: url(:/Images/hide_password_1.jpg);\
                                            }");
        ui->pushButton_adminPwShowHide->setToolTip("Show Password?");
        ui->lineEdit_adminPassword->setEchoMode(QLineEdit::Password);
    }
}

/*************************************************************************
 Function Name - on_pushButton_userPwShowHide_clicked
 Parameter(s)  - void
 Return Type   - void
 Action        - This function handles the userPwShowHide pushbutton clicked
                 signal.
 *************************************************************************/
void SignUp::on_pushButton_userPwShowHide_clicked()
{
    _qDebug("Inside void SignUp::on_pushButton_userPwShowHide_clicked().");

    if(is_userPwhidden)
    {
        is_userPwhidden = false;
        ui->pushButton_userPwShowHide->setStyleSheet(  "QPushButton{\
                                            background-color:lightblue;\
                                            border-style:solid;\
                                            border-radius:10px;\
                                            border-width:2px;\
                                            border-image: url(:/Images/show_password.jpg);\
                                            }"
                                            "QPushButton:hover{\
                                            border-radius:10px;\
                                            background-color:rgb( 212, 217, 243 );\
                                            border-style:solid;\
                                            border-width:1px;\
                                            border-color:rgb(160,160,160);\
                                            color:rgb( 16, 50, 243 );\
                                            background-position:4px left;\
                                            }"
                                            "QPushButton:pressed{\
                                            border-image: url(:/Images/show_password_1.jpg);\
                                            }");
        ui->pushButton_userPwShowHide->setToolTip("Hide Password?");
        ui->lineEdit_userPassword->setEchoMode(QLineEdit::Normal);
    }
    else
    {
        is_userPwhidden = true;
        ui->pushButton_userPwShowHide->setStyleSheet(  "QPushButton{\
                                            background-color:lightblue;\
                                            border-style:solid;\
                                            border-radius:10px;\
                                            border-width:2px;\
                                            border-image: url(:/Images/hide_password.jpg);\
                                            }"
                                            "QPushButton:hover{\
                                            border-radius:10px;\
                                            background-color:rgb( 212, 217, 243 );\
                                            border-style:solid;\
                                            border-width:1px;\
                                            border-color:rgb(160,160,160);\
                                            color:rgb( 16, 50, 243 );\
                                            background-position:4px left;\
                                            }"
                                            "QPushButton:pressed{\
                                            border-image: url(:/Images/hide_password_1.jpg);\
                                            }");
        ui->pushButton_userPwShowHide->setToolTip("Show Password?");
        ui->lineEdit_userPassword->setEchoMode(QLineEdit::Password);
    }
}
