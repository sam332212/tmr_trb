/*************************************  FILE HEADER  *****************************************************************
*
*  File name - PowerSupply.cpp
*  Creation date and time - 17-MAR-2020 , TUE 10:00 AM IST
*  Author: - Manoj
*  Purpose of the file - All Power Supply Handling modules will be defined .
*
**********************************************************************************************************************/
/********************************************************
 Inclusion of header file
 ********************************************************/
#include "PowerSupply.h"
#include "Support/debugflag.h"

/********************************************************
 MACRO Definition
 ********************************************************/
#if PS_DEBUG
    #define _qDebug(s) qDebug()<<"\n-------- Inside "<<s<<" --------"
#else
    #define _qDebug(s); {}
#endif

#if PS_DEBUG_E
    #define _qDebugE(p,q,r,s) qDebug()<<p<<q<<r<<s
#else
    #define _qDebugE(p,q,r,s); {}
#endif

bool PowerSupply::connectionStatus = false;
bool PowerSupply::isBusy = false;

/********************************************************
 Function Definition
 ********************************************************/
/*************************************************************************
 Function Name - PowerSupply
 Parameter(s)  - void
 Return Type   - void
 Action        - Creates the object.
 *************************************************************************/

PowerSupply::PowerSupply():timerObj(new Timer),utilObj(new Utility)
{
    _qDebug("PowerSupply Constructor");
    //Ex- Response:"OK\r"
    psResponse.append(0x0D);
}

/*************************************************************************
 Function Name - ~PowerSupply
 Parameter(s)  - void
 Return Type   - void
 Action        - Destroys the object.
 *************************************************************************/
PowerSupply::~PowerSupply()
{
    _qDebug("PowerSupply Destructor");
    clearmem(timerObj);
    clearmem(utilObj);
}

/*************************************************************************
 Function Name - setDB
 Parameter(s)  - Database*
 Return Type   - void
 Action        - sets DMM db.
 *************************************************************************/
void PowerSupply::setDB(Database* database)
{
    _qDebug("void PowerSupply::setDB(Database* database)");

    psDB = database;
}

/*************************************************************************
 Function Name - setDBTableName
 Parameter(s)  - QString
 Return Type   - void
 Action        - sets DMM db table name.
 *************************************************************************/
void PowerSupply::setDBTableName(QString tableName)
{
   _qDebug("void PowerSupply::setDBTableName(QString tableName)");

   dbTableName = tableName;
}

/*************************************************************************
 Function Name - setRowId
 Parameter(s)  - int
 Return Type   - void
 Action        - sets DMM info containing row number.
 *************************************************************************/
void PowerSupply::setRowId(int rowId)
{
   _qDebug("void PowerSupply::setRowId(int rowId)");

   rowID = rowId;
}

/*************************************************************************
 Function Name - setPortSettings
 Parameter(s)  - QByteArray, QByteArray, int, int, int, int, int, long, int
 Return Type   - void
 Action        - sets DMM port settings.
 *************************************************************************/
void PowerSupply::setPortSettings(QByteArray txData,QByteArray matchData,int BaudRate,int DataBits,
                          int Parity,int StopBits,int FlowControl,long Timeout_Millisec,int COMPARE_FLAG)
{
    _qDebug("void PowerSupply::setPortSettings(QByteArray txData,QByteArray matchData,int BaudRate,int DataBits,\
            int Parity,int StopBits,int FlowControl,long Timeout_Millisec,int COMPARE_FLAG)");

    txData.append(0x0D);
    txData.append(0x0A);

    setPortData(txData,matchData,BaudRate,DataBits,Parity,StopBits,FlowControl,Timeout_Millisec,COMPARE_FLAG);

    portSet = true;
}

/*************************************************************************
 Function Name - showPortSettings
 Parameter(s)  - void
 Return Type   - void
 Action        - prints DMM port settings.
 *************************************************************************/
void PowerSupply::showPortSettings()
{
    _qDebug("void PowerSupply::showPortSettings()");

    printSettings();
}

/*************************************************************************
 Function Name - connectPS
 Parameter(s)  - QStringList
 Return Type   - bool
 Action        - searches and connects the appropriate PORT for PowerSupply.
                 This function returns true if PowerSupply is successfully
                 connected or else returns false.
 *************************************************************************/
bool PowerSupply::connectPS(QStringList discardPorts,QVector<int> psAddress,QVector<bool>& psStatus)
{
    _qDebug("bool PowerSupply::connectPS(QStringList discardPorts,QVector<int> psAddress,QVector<bool>& psStatus)");

    bool status = false;

    QStringList rowContents;
    QStringList columnNames;
    QString str;

    QByteArray dataRecieved = "";

    rowContents.clear();
    columnNames.clear();
    errorLog.clear();

    if(psDB == nullptr)
    {
        QMessageBox::warning(nullptr,"Oops!!\n","PowerSupply DB is not set");
    }
    else if(dbTableName == "")
    {
        QMessageBox::warning(nullptr,"Oops!!\n","PowerSupply Table Name is not set");
    }
    else if(rowID == 0)
    {
        QMessageBox::warning(nullptr,"Oops!!\n","PowerSupply RowID is not set");
    }
    else if(portSet == false)
    {
        QMessageBox::warning(nullptr,"Oops!!\n","PowerSupply PORT is not set");
    }
    else if(psAddress.count() == 0)
    {
        QMessageBox::warning(nullptr,"Oops!!\n","PowerSupply Address(es) can't be blank.");
    }
    else
    {
        /* check portName variable. If contains empty then it indicates application started now.
         * Then get portName from DB. If in DB, port field is empty then it indicates the port
         * has never detected successfully in past. So try to detect the port and if detected
         * successfully then store the port name in DB.
         * If portName is found in DB then directly connect the port using PortName.
         */
        if(portName == "")
        {
            status = psDB->getRowContents(dbTableName,rowID,rowContents);
            _qDebugE("rowContents:[",rowContents,"]","");

            if(status) portName = rowContents.at(1);
            status = false;
        }
        psStatus.clear();
        for(int i = 0;i < psAddress.count();i++) psStatus.push_back(false);//default status is false

        if(portName.length() > 0)
        {
            if(isOpen()) closePort();

            _qDebugE("portName:[",portName,"]","");

            //connect port with port name
            if(connect(portName))//connect with port name
            {
                _qDebugE("connectPS:connect with port name:[",portName,"]"," Success.");
                status = true;
            }
            else
            {
                _qDebugE("connectPS:connect with port name:[",portName,"]"," failed.");
                portName = "";
            }
        }

        if(!status)
        {
            //It indicates either no Port Name has been stored previously or connection to port failed
            if(isOpen()) closePort();

            //Now connect to PowerSupply with available COM PORTs
            if(connect(discardPorts))
            {
                columnNames.clear();
                if(psDB->getColumnNames(dbTableName,columnNames))
                {
                    portName = getPortName();
                    _qDebugE("PowerSupply is connected at ",portName,".","");

                    if(psDB->editCell(dbTableName,static_cast<unsigned>(rowID),columnNames.at(1),portName))
                    {
                        status = true;
                    }
                    else
                    {
                        _qDebugE("Editing Cell failed","","","");
                    }
                }
                else
                {
                    _qDebugE("Getting column names failed","","","");
                }
            }
            else
            {
                _qDebugE("PowerSupply is offline.","!!!","!!!","!!!");
                portName = "";
            }
        }

        if(status == true) status = isConnected(psAddress,psStatus);
    }
    connectionStatus = status;

    return status;
}

/*************************************************************************
 Function Name - disconnectPS
 Parameter(s)  - void
 Return Type   - void
 Action        - disconnects the PowerSupply port.
 *************************************************************************/
void PowerSupply::disconnectPS()
{
    _qDebug("void PowerSupply::disconnectPS()");

    if(isOpen()) closePort();
}

/*************************************************************************
 Function Name - isConnected
 Parameter(s)  - QVector<int>, QVector<bool>&
 Return Type   - bool
 Action        - This function checks the connectivity of the each PowerSupply
                 whose address is given in psAddress and stores the connection
                 status of all powesupplies in buffer psStatus. The status is
                 stored true if PowerSupply is connected or else false is stored.
                 If all ps status is false then only returns false else returns true.
 *************************************************************************/
bool PowerSupply::isConnected(QVector<int> psAddress,QVector<bool>& psStatus)
{
    _qDebug("bool PowerSupply::isConnected(QVector<int> psAddress,QVector<bool>& psStatus))");

    static QByteArray dataRecieved;
    static QVector<QByteArray> commandBuf;
    bool status = false;
    QString str;
    errorLog.clear();

    psStatus.clear();
    commandBuf.clear();
    for(int i = 0;i < psAddress.count();i++) {
        psStatus.push_back(false);//default status is false
        commandBuf.push_back(psCommand+QByteArray::number(psAddress.at(i),10));
        commandBuf[i].push_back(0x0D);
        commandBuf[i].push_back(0x0A);
    }

    for(int i = 0;i < psAddress.count();i++) {
        _qDebugE("Command Buffer:[",QString(commandBuf.at(i)),"]","");
        if(sendCommand(commandBuf.at(i)) == false)
        {
            str = QString("Error!! executing Command[%1] Failed.").arg(QString(commandBuf.at(i)));
            _qDebugE(str,"","","");
            setErrorLog(str);
        }
        else
        {
            dataRecieved = getResponse();
            _qDebugE("dataRecieved:[",dataRecieved,"]","\n");

            if((dataRecieved.length() > 0) && (dataRecieved.contains(psResponse)))
            {
                psStatus[i] = true;
            }
            else
            {
                //Retry
                if(sendCommand(commandBuf.at(i)) == false)
                {
                    str = QString("Error!! Retried Command[%1] Failed.").arg(QString(commandBuf.at(i)));
                    _qDebugE(str,"","","");
                    setErrorLog(str);
                }
                else
                {
                    dataRecieved = getResponse();
                    _qDebugE("Retry dataRecieved:[",dataRecieved,"]","");

                    if((dataRecieved.length() > 0) && (dataRecieved.contains(psResponse)))
                    {
                        psStatus[i] = true;
                    }
                }
            }
        }//else block when sendCommand succeed

        if(!psStatus[i]) {
            str = QString("PS No:%1 Connection Failed").arg(i);
            _qDebugE(str,"","","");
            setErrorLog(str);
        }
    }//for loop

    //If all ps status is false then only sends false else sends true
    for(int i = 0;i < psStatus.count();i++) {
        if(psStatus.at(i)) {
            status = true;
            break;
        }
    }
    connectionStatus = status;
    return status;
}

/*************************************************************************
 Function Name - sendCommand
 Parameter(s)  - QByteArray
 Return Type   - void
 Action        - writes/sends command on port and returns true if write succeeded
                 else returns false.
 *************************************************************************/
bool PowerSupply::sendCommand(QByteArray cmd)
{
    _qDebug("void PowerSupply::sendCommand(QByteArray cmd).");
    _qDebugE("Command To Send:[",QString(cmd),"]","");
    cmd.append(0x0D);
    cmd.append(0x0A);

    bool status = write(cmd);
    if(status) timerObj->delayInms(25);
    return status;
}

/*************************************************************************
 Function Name - sendCommand
 Parameter(s)  - void
 Return Type   - QByteArray
 Action        - reads response from port and returns the response buffer.
 *************************************************************************/
QByteArray PowerSupply::getResponse()
{
    _qDebug("QByteArray PowerSupply::getResponse().");

    return readAll();
}

/*************************************************************************
 Function Name - getPort
 Parameter(s)  - void
 Return Type   - QString
 Action        - returns the portName assigned.
 *************************************************************************/
QString PowerSupply::getPort()
{
    _qDebug("QString PowerSupply::getPort().");

    return portName;
}

/*************************************************************************
 Function Name - getErrorLog
 Parameter(s)  - void
 Return Type   - QString
 Action        - returns the error log.
 *************************************************************************/
QString PowerSupply::getErrorLog() const
{
    return errorLog;
}

/*************************************************************************
 Function Name - setErrorLog
 Parameter(s)  - void
 Return Type   - QString
 Action        - appends the given error log in value.
 *************************************************************************/
void PowerSupply::setErrorLog(const QString &value)
{
    if(errorLog.size() > 0) errorLog.append("\n");
    errorLog.append(value);
}

/*************************************************************************
 Function Name - selectDevice
 Parameter(s)  - uint8_t
 Return Type   - bool
 Action        - sets given address to select a device.
 *************************************************************************/
bool PowerSupply::selectDevice(uint8_t address)
{
    _qDebug("bool PowerSupply::selectDevice(uint8_t address).");
    QString cmd = "ADR "+QString::number(address);

    return sendCommand(cmd.toLocal8Bit());
}

/*************************************************************************
 Function Name - resetPS
 Parameter(s)  - uint8_t
 Return Type   - bool
 Action        - Brings the PowerSupply to a safe and known state.
 *************************************************************************/
bool PowerSupply::resetPS(uint8_t address)
{
    _qDebug("bool PowerSupply::resetPS(uint8_t address).");

    QByteArray bytes = nullptr;
    errorLog.clear();

    if(selectDevice(address) == false) {
        setErrorLog(QString("PS%1 selection failed.").arg(address));
        return false;
    }
    else {
        if(sendCommand("RST")) bytes = readAll();
        else {
            setErrorLog(QString("Unable to reset PS%1.").arg(address));
            return false;
        }

        if(bytes.contains(psResponse)) return true;
        else {
            setErrorLog(QString("Invalid response <%1>.").arg(QString(bytes)));
            return false;
        }
    }
}

/*************************************************************************
 Function Name - resetAll
 Parameter(s)  - void
 Return Type   - bool
 Action        - Brings all the connected PowerSupplies to a safe and known state.
                 No Ack receives for this global command.
 *************************************************************************/
bool PowerSupply::resetAll()
{
    _qDebug("bool PowerSupply::resetAll().");
    errorLog.clear();

    if(sendCommand("GRST")) {
        timerObj->delayInms(25);
        return true;
    }
    else {
        setErrorLog("Unable to reset all PS.");
        return false;
    }
}

/*************************************************************************
 Function Name - getSerialNumber
 Parameter(s)  - uint8_t
 Return Type   - QString
 Action        - returns the serial .
 *************************************************************************/
QString PowerSupply::getSerialNumber(uint8_t address)
{
    _qDebug("QString PowerSupply::getSerialNumber(uint8_t address).");
    QString slNo = nullptr;
    QByteArray bytes = nullptr;
    errorLog.clear();

    if(selectDevice(address) == false) {
        setErrorLog(QString("PS%1 selection failed.").arg(address));
    }
    else {
        if(sendCommand("SN?")){
            bytes = readAll();
            if(bytes.count() > 0) slNo = QString(bytes);
        }
        else setErrorLog(QString("Unable to get Serial Number for PS%1.").arg(address));
    }

    return slNo;
}

/*************************************************************************
 Function Name - setVoltage
 Parameter(s)  - uint8_t, double
 Return Type   - bool
 Action        - sets the output voltage.
 *************************************************************************/
bool PowerSupply::setVoltage(uint8_t address,double value)
{
    _qDebug("bool PowerSupply::setVoltage(uint8_t address,double value).");
    QByteArray bytes = nullptr;
    QString cmd = "PV "+utilObj->convertToQstring(value,4);
    errorLog.clear();

    if(selectDevice(address) == false) {
        setErrorLog(QString("PS%1 selection failed.").arg(address));
        return false;
    }
    else {
        if(sendCommand(cmd.toLocal8Bit())) bytes = readAll();
        else {
            setErrorLog(QString("Unable to set Voltage Mode for PS%1.").arg(address));
            return false;
        }

        if(bytes.contains(psResponse)) return true;
        else {
            setErrorLog(QString("Invalid response <%1>.").arg(QString(bytes)));
            return false;
        }
    }
}

/*************************************************************************
 Function Name - setVoltage
 Parameter(s)  - double
 Return Type   - bool
 Action        - sets the output voltage.  Before calling this function device
                 address must be set by calling selectDevice.
 *************************************************************************/
bool PowerSupply::setVoltage(double value)
{
    _qDebug("bool PowerSupply::setVoltage(double value).");
    QByteArray bytes = nullptr;
    QString cmd = "PV "+utilObj->convertToQstring(value,4);
    errorLog.clear();

    if(sendCommand(cmd.toLocal8Bit())) bytes = readAll();
    else {
        setErrorLog(QString("Unable to set Voltage <%1>.").arg(value));
        return false;
    }

    if(bytes.contains(psResponse)) return true;
    else {
        setErrorLog(QString("Invalid response <%1>.").arg(QString(bytes)));
        return false;
    }
}

/*************************************************************************
 Function Name - getProgrammedVoltage
 Parameter(s)  - uint8_t, double&
 Return Type   - bool
 Action        - If success keeps programmed voltage in value and returns
                 true else returns false.
 *************************************************************************/
bool PowerSupply::getProgrammedVoltage(uint8_t address,double& value)
{
    _qDebug("double PowerSupply::getProgrammedVoltage(uint8_t address,double& value).");
    QByteArray bytes = nullptr;
    value = 0.0;
    errorLog.clear();

    if(selectDevice(address) == false) {
        setErrorLog(QString("PS%1 selection failed.").arg(address));
        return false;
    }
    else {
        if(sendCommand("PV?")) bytes = readAll();
        else {
            setErrorLog(QString("Unable to get Programmed Voltage for PS%1.").arg(address));
            return false;
        }
    }
    if(bytes != nullptr) value = static_cast<double>(utilObj->convertToDouble(bytes));

    return true;
}

/*************************************************************************
 Function Name - getMeasuredVoltage
 Parameter(s)  - uint8_t, double&
 Return Type   - bool
 Action        - If success keeps measured voltage in value and returns
                 true else returns false.
 *************************************************************************/
bool PowerSupply::getMeasuredVoltage(uint8_t address,double& value)
{
    _qDebug("double PowerSupply::getMeasuredVoltage(uint8_t address,double& value).");
    QByteArray bytes = nullptr;
    value = 0.0;
    errorLog.clear();

    if(selectDevice(address) == false) {
        setErrorLog(QString("PS%1 selection failed.").arg(address));
        return false;
    }
    else {
        if(sendCommand("MV?")) bytes = readAll();
        else {
            setErrorLog(QString("Unable to get Measured Voltage for PS%1.").arg(address));
            return false;
        }
    }

    if(bytes != nullptr) value= static_cast<double>(utilObj->convertToDouble(bytes));

    return true;
}

/*************************************************************************
 Function Name - setCurrent
 Parameter(s)  - uint8_t, double
 Return Type   - bool
 Action        - sets the output current.
 *************************************************************************/
bool PowerSupply::setCurrent(uint8_t address,double value)
{
    _qDebug("bool PowerSupply::setCurrent(uint8_t address,double value).");
    QByteArray bytes = nullptr;
    QString cmd = "PC "+utilObj->convertToQstring(value,4);
    errorLog.clear();

    if(selectDevice(address) == false) {
        setErrorLog(QString("PS%1 selection failed.").arg(address));
        return false;
    }
    else {
        if(sendCommand(cmd.toLocal8Bit())) bytes = readAll();
        else {
            setErrorLog(QString("Unable to set Current Mode for PS%1.").arg(address));
            return false;
        }

        if(bytes.contains(psResponse)) return true;
        else {
            setErrorLog(QString("Invalid response <%1>.").arg(QString(bytes)));
            return false;
        }
    }
}

/*************************************************************************
 Function Name - setCurrent
 Parameter(s)  - double
 Return Type   - bool
 Action        - sets the output current. Before calling this function device
                 address must be set by calling selectDevice.
 *************************************************************************/
bool PowerSupply::setCurrent(double value)
{
    _qDebug("bool PowerSupply::setCurrent(double value).");
    QByteArray bytes = nullptr;
    QString cmd = "PC "+utilObj->convertToQstring(value,4);
    errorLog.clear();

    if(sendCommand(cmd.toLocal8Bit())) bytes = readAll();
    else {
        setErrorLog(QString("Unable to set Current <%1>.").arg(value));
        return false;
    }

    if(bytes.contains(psResponse)) return true;
    else {
        setErrorLog(QString("Invalid response <%1>.").arg(QString(bytes)));
        return false;
    }
}

/*************************************************************************
 Function Name - getProgrammedCurrent
 Parameter(s)  - uint8_t, double&
 Return Type   - bool
 Action        - If success keeps programmed current in value and returns
                 true else returns false.
 *************************************************************************/
bool PowerSupply::getProgrammedCurrent(uint8_t address,double& value)
{
    _qDebug("double PowerSupply::getProgrammedCurrent(uint8_t address,double& value).");
    QByteArray bytes = nullptr;
    value = 0.0;
    errorLog.clear();

    if(selectDevice(address) == false) {
        setErrorLog(QString("PS%1 selection failed.").arg(address));
        return false;
    }
    else {
        if(sendCommand("PC?")) bytes = readAll();
        else {
            setErrorLog(QString("Unable to get Programmed Current for PS%1.").arg(address));
            return false;
        }

        if(bytes != nullptr) value = static_cast<double>(utilObj->convertToDouble(bytes));
    }

    return true;
}

/*************************************************************************
 Function Name - getMeasuredCurrent
 Parameter(s)  - uint8_t, double&
 Return Type   - bool
 Action        - If success keeps measured current in value and returns
                 true else returns false.
 *************************************************************************/
bool PowerSupply::getMeasuredCurrent(uint8_t address, double& value)
{
    _qDebug("double PowerSupply::getMeasuredCurrent(uint8_t address, double& value).");
    QByteArray bytes = nullptr;
    value = 0.0;
    errorLog.clear();

    if(selectDevice(address) == false) {
        setErrorLog(QString("PS%1 selection failed.").arg(address));
        return false;
    }
    else {
        if(sendCommand("MC?")) bytes = readAll();
        else {
            setErrorLog(QString("Unable to get Measured Current for PS%1.").arg(address));
            return false;
        }
    }
    if(bytes != nullptr) value = static_cast<double>(utilObj->convertToDouble(bytes));

    return true;
}

/*************************************************************************
 Function Name - getPsData
 Parameter(s)  - uint8_t
 Return Type   - void
 Action        - returns power supply set data .
 *************************************************************************/
void PowerSupply::getPsData(uint8_t address)
{
    _qDebug("PowerSupply::psDetails PowerSupply::getPsData(uint8_t address).");
    QString data = nullptr;
    QByteArray bytes = nullptr;
    errorLog.clear();

    _psDetails.measuredVoltage = nullptr;
    _psDetails.programmedVoltage = nullptr;
    _psDetails.measuredCurrent = nullptr;
    _psDetails.programmedCurrent = nullptr;
    _psDetails.overVoltageSetPoint = nullptr;
    _psDetails.underVoltageSetPoint = nullptr;

    if(selectDevice(address) == true) {
        if(sendCommand("DVC?"))
        {
            bytes = readAll();
            if(bytes.count() > 0) {
                data = QString(bytes);
                QStringList psData = data.split(',');

                if(psData.count() == 6)
                {
                    _psDetails.measuredVoltage = psData.at(0);
                    _psDetails.programmedVoltage = psData.at(1);
                    _psDetails.measuredCurrent = psData.at(2);
                    _psDetails.programmedCurrent = psData.at(3);
                    _psDetails.overVoltageSetPoint = psData.at(4);
                    _psDetails.underVoltageSetPoint = psData.at(5);
                }
            }
        }
    }
    else {
        setErrorLog(QString("Unable to get data from PS%1.").arg(address));
    }
}

/*************************************************************************
 Function Name - getPsData
 Parameter(s)  - void
 Return Type   - void
 Action        - returns power supply set data . Before calling this function
                 device address must be set by calling selectDevice.
 *************************************************************************/
void PowerSupply::getPsData()
{
    _qDebug("void PowerSupply::getPsData()).");
    QString data = nullptr;
    QByteArray bytes = nullptr;

    _psDetails.measuredVoltage = "";
    _psDetails.programmedVoltage = "";
    _psDetails.measuredCurrent = "";
    _psDetails.programmedCurrent = "";
    _psDetails.overVoltageSetPoint = "";
    _psDetails.underVoltageSetPoint = "";

    if(sendCommand("DVC?"))
    {
        bytes = readAll();
        if(bytes.count() > 0) {
            data = QString(bytes);
            QStringList psData = data.split(',');

            if(psData.count() == 6)
            {
                _psDetails.measuredVoltage = psData.at(0);
                _psDetails.programmedVoltage = psData.at(1);
                _psDetails.measuredCurrent = psData.at(2);
                _psDetails.programmedCurrent = psData.at(3);
                _psDetails.overVoltageSetPoint = psData.at(4);
                _psDetails.underVoltageSetPoint = psData.at(5);

                utilObj->setPrecision(_psDetails.measuredVoltage,2,2);
                utilObj->setPrecision(_psDetails.programmedVoltage,2,2);
                utilObj->setPrecision(_psDetails.measuredCurrent,2,2);
                utilObj->setPrecision(_psDetails.programmedCurrent,2,2);
                utilObj->setPrecision(_psDetails.overVoltageSetPoint,2,2);
                utilObj->setPrecision(_psDetails.underVoltageSetPoint,2,2);
            }
        }
    }
    else {
        setErrorLog("Unable to get data from PS.");
    }
}

/*************************************************************************
 Function Name - setOutputOn
 Parameter(s)  - uint8_t
 Return Type   - bool
 Action        - Turns output ON. This function must be called after setting done.
 *************************************************************************/
bool PowerSupply::setOutputOn(uint8_t address)
{
    _qDebug("bool PowerSupply::setOutputOn(uint8_t address).");
    QByteArray bytes = nullptr;
    errorLog.clear();

    if(selectDevice(address) == false) {
        setErrorLog(QString("PS%1 selection failed.").arg(address));
        return false;
    }
    else {
        if(sendCommand("OUT 1")) bytes = readAll();
        else {
            setErrorLog(QString("Unable to make Output ON for PS%1.").arg(address));
            return false;
        }

        if(bytes.contains(psResponse)) return true;
        else {
            setErrorLog(QString("Invalid response <%1> from PS%2.").arg(QString(bytes)).arg(address));
            return false;
        }
    }
}

/*************************************************************************
 Function Name - setOutputOn
 Parameter(s)  - QVector<int>
 Return Type   - bool
 Action        - Turns given outputs ON. This function must be called after setting done.
 *************************************************************************/
bool PowerSupply::setOutputOn(QVector<int> address)
{
    _qDebug("bool PowerSupply::setOutputOn(QVector<int> address).");
    QByteArray bytes = nullptr;
    bool ret = false;
    errorLog.clear();
    _qDebugE("PS Addresses to ON:",address,"","");

    for(uint8_t addr : address) {
        if(selectDevice(addr) == false) {
            setErrorLog(QString("PS%1 selection failed.").arg(addr));
        }
        else {
            if(sendCommand("OUT 1")) {
                bytes = readAll();
                if(bytes.contains(psResponse)) {
                    if(!ret) ret = true;
                }
                else {
                    setErrorLog(QString("Invalid response <%1> from PS%2.").arg(QString(bytes)).arg(addr));
                }
                if(!isOutputOn()) {
                    setErrorLog(QString("Unable to make Output ON for PS%1.").arg(addr));
                    ret = false;
                }
            }
            else setErrorLog(QString("Command send failed to make Output ON for PS%1.").arg(addr));
        }
    }
    return ret;
}

/*************************************************************************
 Function Name - setOutputOn
 Parameter(s)  - uint8_t
 Return Type   - bool
 Action        - Turns output ON. This function must be called after setting
                 done. Before calling this function device address must be
                 set by calling selectDevice.
 *************************************************************************/
bool PowerSupply::setOutputOn()
{
    _qDebug("bool PowerSupply::setOutputOn().");
    QByteArray bytes = nullptr;
    errorLog.clear();

    if(sendCommand("OUT 1")) bytes = readAll();
    else {
        setErrorLog("Unable to make Output ON.");
        return false;
    }

    if(bytes.contains(psResponse)) return true;
    else {
        setErrorLog(QString("Invalid response <%1>.").arg(QString(bytes)));
        return false;
    }
}

/*************************************************************************
 Function Name - setAllOutputOn
 Parameter(s)  - void
 Return Type   - bool
 Action        - Turns output ON for all the connected PowerSupplies.
                 No Ack receives for this global command.
 *************************************************************************/
bool PowerSupply::setAllOutputOn()
{
    _qDebug("bool PowerSupply::setAllOutputOn().");
    errorLog.clear();

    if(sendCommand("GOUT 1")) {
        timerObj->delayInms(25);
        return true;
    }
    else {
        setErrorLog("Unable to make Output ON for all PS.");
        return false;
    }
}

/*************************************************************************
 Function Name - setOutputOff
 Parameter(s)  - uint8_t
 Return Type   - bool
 Action        - Turns output OFF.
 *************************************************************************/
bool PowerSupply::setOutputOff(uint8_t address)
{
    _qDebug("bool PowerSupply::setOutputOff(uint8_t address).");
    QByteArray bytes = nullptr;
    errorLog.clear();

    if(selectDevice(address) == false) {
        setErrorLog(QString("PS%1 selection failed.").arg(address));
        return false;
    }
    else {
        if(sendCommand("OUT 0")) bytes = readAll();
        else {
            setErrorLog(QString("Unable to make Output OFF for PS%1.").arg(address));
            return false;
        }

        if(bytes.contains(psResponse)) return true;
        else {
            setErrorLog(QString("Invalid response <%1> from PS%2.").arg(QString(bytes)).arg(address));
            return false;
        }
    }
}

/*************************************************************************
 Function Name - setOutputOff
 Parameter(s)  - QVector<int>
 Return Type   - bool
 Action        - Turns given outputs OFF. This function must be called after setting done.
 *************************************************************************/
bool PowerSupply::setOutputOff(QVector<int> address)
{
    _qDebug("bool PowerSupply::setOutputOff(QVector<int> address).");
    QByteArray bytes = nullptr;
    bool ret = false;
    errorLog.clear();
    _qDebugE("PS Addresses to OFF:",address,"","");

    for(uint8_t addr : address) {
        if(selectDevice(addr) == false) {
            setErrorLog(QString("PS%1 selection failed.").arg(addr));
        }
        else {
            timerObj->delayInms(25);
            if(sendCommand("OUT 0")) {
                bytes = readAll();
                if(bytes.contains(psResponse)) {
                    if(!ret) ret = true;
                }
                else {
                    setErrorLog(QString("Invalid response <%1> from PS%2.").arg(QString(bytes)).arg(addr));
                }
                if(isOutputOn()) {
                    setErrorLog(QString("Unable to make Output OFF for PS%1.").arg(addr));
                    ret = false;
                }
            }
            else setErrorLog(QString("Command send failed to make Output OFF for PS%1.").arg(addr));
        }
    }
    return ret;
}

/*************************************************************************
 Function Name - setOutputOff
 Parameter(s)  - void
 Return Type   - bool
 Action        - Turns output OFF. Before calling this function device
                 address must be set by calling selectDevice.
 *************************************************************************/
bool PowerSupply::setOutputOff()
{
    _qDebug("bool PowerSupply::setOutputOff().");
    QByteArray bytes = nullptr;
    errorLog.clear();

    if(sendCommand("OUT 0")) bytes = readAll();
    else {
        setErrorLog("Unable to make Output OFF.");
        return false;
    }

    if(bytes.contains(psResponse)) return true;
    else {
        setErrorLog(QString("Invalid response <%1>.").arg(QString(bytes)));
        return false;
    }
}

/*************************************************************************
 Function Name - setAllOutputOff
 Parameter(s)  - void
 Return Type   - bool
 Action        - Turns output OFF for all the connected PowerSupplies.
                 No Ack receives for this global command.
 *************************************************************************/
bool PowerSupply::setAllOutputOff()
{
    _qDebug("bool PowerSupply::setAllOutputOff().");

    if(sendCommand("GOUT 0")) {
        timerObj->delayInms(25);
        return true;
    }
    else {
        setErrorLog("Unable to make Output OFF for all PS.");
        return false;
    }
}

/*************************************************************************
 Function Name - isOutputOn
 Parameter(s)  - uint8_t
 Return Type   - bool
 Action        - Checks the output status. If receives "ON" then returns true
                 and if receives "OFF" or others, then returns false.
 *************************************************************************/
bool PowerSupply::isOutputOn(uint8_t address)
{
    _qDebug("bool PowerSupply::isOutputOn(uint8_t address).");
    QString res = nullptr;
    QByteArray bytes = nullptr;
    errorLog.clear();

    if(selectDevice(address) == false) {
        setErrorLog(QString("PS%1 selection failed.").arg(address));
        return false;
    }
    else {
        if(sendCommand("OUT?")) {
            bytes = readAll();
            if(bytes.count() > 0) res = QString(bytes);
        }
        else {
            setErrorLog(QString("Unable to read Output status for PS%1.").arg(address));
            return false;
        }

        if(res.contains("ON") == true) return true;
        else return false;
    }
}

/*************************************************************************
 Function Name - isOutputOn
 Parameter(s)  - void
 Return Type   - bool
 Action        - Checks the output status. If receives "ON" then returns true
                 and if receives "OFF" or others, then returns false. Before
                 calling this function device address must be set by calling
                 selectDevice.
 *************************************************************************/
bool PowerSupply::isOutputOn()
{
    _qDebug("bool PowerSupply::isOutputOn().");
    QString res = nullptr;
    QByteArray bytes = nullptr;
    errorLog.clear();

    if(sendCommand("OUT?")) {
        bytes = readAll();  
        if(bytes.count() > 0) res = QString(bytes);
        _qDebugE("isOutputOn:Response:bytes:",res,"","");
    }
    else {
        setErrorLog("Unable to read Output status for PS%1.");
        _qDebugE("Unable to read Output status for PS.","","","");
        return false;
    }

    return res.contains("ON");
}

/*************************************************************************
 Function Name - getRemoteMode
 Parameter(s)  - uint8_t
 Return Type   - bool
 Action        - Checks the remote mode. Returns 0 for Local Mode, 1 for Remote
                 Mode and 2 for Local Lockout Mode.
 *************************************************************************/
bool PowerSupply::getRemoteMode(uint8_t address,uint8_t* mode)
{
    _qDebug("bool PowerSupply::getRemoteMode(uint8_t address,uint8_t* mode).");
    QString res = nullptr;
    QByteArray bytes = nullptr;
    errorLog.clear();

    if(selectDevice(address) == false) {
        setErrorLog(QString("PS%1 selection failed.").arg(address));
        return false;
    }
    else {
        if(sendCommand("RMT?")){
            bytes = readAll();
            if(bytes.count() > 0) res = QString(bytes);
        }
        else {
            setErrorLog(QString("Unable to get Remote Mode for PS%1.").arg(address));
            return false;
        }
    }

    if(res.contains("LLO")) *mode = 2;
    else if(res.contains("REM")) *mode = 1;
    else if(res.contains("LOC")) *mode = 0;
    else {
        setErrorLog(QString("Invalid response <%1> from PS%2.").arg(QString(bytes)).arg(address));
        return false;
    }

    return true;
}

/*************************************************************************
 Function Name - setLocalMode
 Parameter(s)  - uint8_t
 Return Type   - bool
 Action        - Sets the PowerSupply to Local Mode.
 *************************************************************************/
bool PowerSupply::setLocalMode(uint8_t address)
{
    _qDebug("bool PowerSupply::setLocalMode(uint8_t address).");
    QByteArray bytes = nullptr;
    errorLog.clear();

    if(selectDevice(address) == false) {
        setErrorLog(QString("PS%1 selection failed.").arg(address));
        return false;
    }
    else {
        if(sendCommand("RMT 0")) bytes = readAll();
        else {
            setErrorLog(QString("Unable to get Remote Mode for PS%1.").arg(address));
            return false;
        }

        if(bytes.contains(psResponse)) return true;
        else {
            setErrorLog(QString("Invalid response <%1> from PS%2.").arg(QString(bytes)).arg(address));
            return false;
        }
    }
}

/*************************************************************************
 Function Name - setLocalMode
 Parameter(s)  - void
 Return Type   - bool
 Action        - Sets the PowerSupply to Local Mode. Before calling this function device
                 address must be set by calling selectDevice.
 *************************************************************************/
bool PowerSupply::setLocalMode()
{
    _qDebug("bool PowerSupply::setLocalMode().");
    QByteArray bytes = nullptr;
    errorLog.clear();

    if(sendCommand("RMT 0")) bytes = readAll();
    else {
        setErrorLog("Unable to get Remote Mode.");
        return false;
    }

    if(bytes.contains(psResponse)) return true;
    else {
        setErrorLog(QString("Invalid response <%1>.").arg(QString(bytes)));
        return false;
    }
}

/*************************************************************************
 Function Name - setRemoteMode
 Parameter(s)  - uint8_t
 Return Type   - bool
 Action        - Sets the PowerSupply to Remote Mode.
 *************************************************************************/
bool PowerSupply::setRemoteMode(uint8_t address)
{
    _qDebug("bool PowerSupply::setRemoteMode(uint8_t address).");
    QByteArray bytes = nullptr;
    errorLog.clear();

    if(selectDevice(address) == false) {
        setErrorLog(QString("PS%1 selection failed.").arg(address));
        return false;
    }
    else {
        if(sendCommand("RMT 1")) bytes = readAll();
        else {
            setErrorLog(QString("Unable to set Remote Mode for PS%1.").arg(address));
            return false;
        }

        if(bytes.contains(psResponse)) return true;
        else {
            setErrorLog(QString("Invalid response <%1>.").arg(QString(bytes)));
            return false;
        }
    }
}

/*************************************************************************
 Function Name - setRemoteMode
 Parameter(s)  - void
 Return Type   - bool
 Action        - Sets the PowerSupply to Remote Mode. Before calling this function device
                 address must be set by calling selectDevice.
 *************************************************************************/
bool PowerSupply::setRemoteMode()
{
    _qDebug("bool PowerSupply::setRemoteMode(void).");
    QByteArray bytes = nullptr;
    errorLog.clear();

    if(sendCommand("RMT 1")) bytes = readAll();
    else {
        setErrorLog("Unable to set Remote Mode.");
        return false;
    }

    if(bytes.contains(psResponse)) return true;
    else {
        setErrorLog(QString("Invalid response <%1>.").arg(QString(bytes)));
        return false;
    }
}

/*************************************************************************
 Function Name - setLocalLockoutMode
 Parameter(s)  - uint8_t
 Return Type   - bool
 Action        - Sets the PowerSupply to Local Lockout Mode.
 *************************************************************************/
bool PowerSupply::setLocalLockoutMode(uint8_t address)
{
    _qDebug("bool PowerSupply::setLocalLockoutMode(uint8_t address).");
    QByteArray bytes = nullptr;
    errorLog.clear();

    if(selectDevice(address) == false) {
        setErrorLog(QString("PS%1 selection failed.").arg(address));
        return false;
    }
    else {
        if(sendCommand("RMT 2")) bytes = readAll();
        else {
            setErrorLog(QString("Unable to set Lockout Mode for PS%1.").arg(address));
            return false;
        }

        if(bytes.contains(psResponse)) return true;
        else {
            setErrorLog(QString("Invalid response <%1>.").arg(QString(bytes)));
            return false;
        }
    }
}

/*************************************************************************
 Function Name - setLocalLockoutMode
 Parameter(s)  - void
 Return Type   - bool
 Action        - Sets the PowerSupply to Local Lockout Mode. Before calling this function device
                 address must be set by calling selectDevice.
 *************************************************************************/
bool PowerSupply::setLocalLockoutMode()
{
    _qDebug("bool PowerSupply::setLocalLockoutMode().");
    QByteArray bytes = nullptr;
    errorLog.clear();

    if(sendCommand("RMT 2")) bytes = readAll();
    else {
        setErrorLog("Unable to set LocalLockout Mode.");
        return false;
    }

    if(bytes.contains(psResponse)) return true;
    else {
        setErrorLog(QString("Invalid response <%1>.").arg(QString(bytes)));
        return false;
    }
}

/*************************************************************************
 Function Name - repeatLastCommand
 Parameter(s)  - uint8_t
 Return Type   - bool
 Action        - Repeats the Lst Command.
 *************************************************************************/
bool PowerSupply::repeatLastCommand(uint8_t address)
{
    _qDebug("bool PowerSupply::repeatLastCommand(uint8_t address).");
    QByteArray bytes = nullptr;
    errorLog.clear();

    if(selectDevice(address) == false) {
        setErrorLog(QString("PS%1 selection failed.").arg(address));
        return false;
    }
    else {
        if(sendCommand("\\")) bytes = readAll();
        else {
            setErrorLog(QString("Repeat Last command failed for PS%1.").arg(address));
            return false;
        }

        if(bytes.contains(psResponse)) return true;
        else {
            setErrorLog(QString("Invalid response <%1>.").arg(QString(bytes)));
            return false;
        }
    }
}

/*************************************************************************
 Function Name - runSelfTest
 Parameter(s)  - uint8_t
 Return Type   - bool
 Action        - Runs self-test in given PS.
 *************************************************************************/
bool PowerSupply::runSelfTest(uint8_t address)
{
    _qDebug("bool PowerSupply::runSelfTest(uint8_t address).");
    QByteArray bytes = nullptr;
    errorLog.clear();

    if(selectDevice(address) == false) {
        setErrorLog(QString("PS%1 selection failed.").arg(address));
        return false;
    }
    else {
        if(sendCommand("FLT?")) bytes = readAll();
        else {
            setErrorLog(QString("Self-test command failed for PS%1.").arg(address));
            return false;
        }
        Utility obj;
        QString bData = obj.getBinaryString(QString::fromUtf8(bytes),4);

        if(bData.contains('1') && (bData.count() >= 13)) {
            setErrorLog(QString("PS%1 : Error Code [%2].").arg(address).arg(QString(bytes)));

            if(bData.at(1 ) == QChar('1')) setErrorLog("AC Fail");
            if(bData.at(2 ) == QChar('1')) setErrorLog("Over Temperature");
            if(bData.at(3 ) == QChar('1')) setErrorLog("Foldback (tripped)");
            if(bData.at(4 ) == QChar('1')) setErrorLog("Over Voltage Protection");
            if(bData.at(5 ) == QChar('1')) setErrorLog("Shut Off");
            if(bData.at(6 ) == QChar('1')) setErrorLog("Output Off");
            if(bData.at(7 ) == QChar('1')) setErrorLog("Interlock");
            if(bData.at(8 ) == QChar('1')) setErrorLog("Under Voltage Protection");
            if(bData.at(10) == QChar('1')) setErrorLog("Internal Input Overflow");
            if(bData.at(11) == QChar('1')) setErrorLog("Internal Overflow");
            if(bData.at(12) == QChar('1')) setErrorLog("Internal Time Out");
            if(bData.at(13) == QChar('1')) setErrorLog("Internal Comm Error");
        }
    }
    return true;
}
