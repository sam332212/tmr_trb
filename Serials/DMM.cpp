/*************************************  FILE HEADER  *****************************************************************
*
*  File name - DMM.cpp
*  Creation date and time - 28-OCT-2019 , MON 10:00 AM IST
*  Author: - Manoj
*  Purpose of the file - All Digital Multimeter modules will be defined .
*
**********************************************************************************************************************/
/********************************************************
 Inclusion of header file
 ********************************************************/
#include "DMM.h"
#include "Support/debugflag.h"

/********************************************************
 MACRO Definition
 ********************************************************/
#if DMM_DEBUG
    #define #define _qDebug(s) qDebug()<<"\n-------- Inside "<<s<<" --------"
#else
    #define _qDebug(s); {}
#endif

#if DMM_DEBUG_E
    #define _qDebugE(p,q,r,s) qDebug()<<p<<q<<r<<s
#else
    #define _qDebugE(p,q,r,s); {}
#endif

//In Percentage
#define ACCURACY   5
#define ACCURACY_1 20
//Maximum number of times DMM should try to get stable value
#define COUNT    5
#define OPEN_VALUE          20000000.0
#define DMM_READ_DELAY_MS   60

bool DMM::connectionStatus = false;
bool DMM::isreadBusy = false;

/********************************************************
 Function Definition
 ********************************************************/
/*************************************************************************
 Function Name - DMM
 Parameter(s)  - void
 Return Type   - void
 Action        - Creates the object.
 *************************************************************************/
DMM::DMM():timerObj(new Timer)
{
    _qDebug("DMM Constructor.");
    //Ex- Response:"TEKTRONIX,DMM4040,4466110,08/02/10-11:53\r\n"
    IDNresponse.append(0x0D);
    IDNresponse.append(0x0A);

    selfTestRespond.append(0x0D);
    selfTestRespond.append(0x0A);
}

/*************************************************************************
 Function Name - ~DMM
 Parameter(s)  - void
 Return Type   - void
 Action        - Destroys the object.
 *************************************************************************/
DMM::~DMM()
{
    _qDebug("DMM Destructor.");
    clearmem(timerObj);
}

/*************************************************************************
 Function Name - setDB
 Parameter(s)  - Database*
 Return Type   - void
 Action        - sets DMM db.
 *************************************************************************/
void DMM::setDB(Database* database)
{
    _qDebug("void DMM::setDB(Database* database).");

    dmmDB = database;
}

/*************************************************************************
 Function Name - setDBTableName
 Parameter(s)  - QString
 Return Type   - void
 Action        - sets DMM db table name.
 *************************************************************************/
void DMM::setDBTableName(QString tableName)
{
   _qDebug("void DMM::setDBTableName(QString tableName).");

   dbTableName = tableName;
}

/*************************************************************************
 Function Name - setRowId
 Parameter(s)  - int
 Return Type   - void
 Action        - sets DMM info containing row number.
 *************************************************************************/
void DMM::setRowId(int rowId)
{
   _qDebug("void DMM::setRowId(int rowId).");

   rowID = rowId;
}

/*************************************************************************
 Function Name - setPortSettings
 Parameter(s)  - QByteArray, QByteArray, int, int, int, int, int, long, int
 Return Type   - void
 Action        - sets DMM port settings.
 *************************************************************************/
void DMM::setPortSettings(QByteArray txData,QByteArray matchData,int BaudRate,int DataBits,
                          int Parity,int StopBits,int FlowControl,long Timeout_Millisec,int COMPARE_FLAG)
{
    _qDebug("void DMM::setPortSettings(QByteArray txData,QByteArray matchData,int BaudRate,int DataBits,\
            int Parity,int StopBits,int FlowControl,long Timeout_Millisec,int COMPARE_FLAG).");

    txData.append(0x0D);
    txData.append(0x0A);

    setPortData(txData,matchData,BaudRate,DataBits,Parity,StopBits,FlowControl,Timeout_Millisec,COMPARE_FLAG);

    portSet = true;
}

/*************************************************************************
 Function Name - showPortSettings
 Parameter(s)  - void
 Return Type   - void
 Action        - prints DMM port settings.
 *************************************************************************/
void DMM::showPortSettings()
{
    _qDebug("void DMM::showPortSettings().");

    printSettings();
}

/*************************************************************************
 Function Name - connectDMM
 Parameter(s)  - QStringList
 Return Type   - bool
 Action        - searches and connects the appropriate PORT for Digital multimeter.
                 This function returns true if DMM is successfully connected
                 or else returns false.
 *************************************************************************/
bool DMM::connectDMM(QStringList discardPorts)
{
    _qDebug("bool DMM::connectDMM(QStringList discardPorts).");

    bool status = false;

    QStringList rowContents;
    QStringList columnNames;

    QByteArray dataRecieved = "";

    rowContents.clear();
    columnNames.clear();

    if(dmmDB == nullptr)
    {
        QMessageBox::warning(nullptr,"Oops!!\n","DMM DB is not set");
    }
    else if(dbTableName == "")
    {
        QMessageBox::warning(nullptr,"Oops!!\n","DMM Table Name is not set");
    }
    else if(rowID == 0)
    {
        QMessageBox::warning(nullptr,"Oops!!\n","DMM RowID is not set");
    }
    else if(portSet == false)
    {
        QMessageBox::warning(nullptr,"Oops!!\n","DMM PORT is not set");
    }
    else
    {
        /* check portName variable. If contains empty then it indicates application started now.
         * Then get portName from DB. If in DB, port field is empty then it indicates the port
         * has never detected successfully in past. So try to detect the port and if detected
         * successfully then store the port name in DB.
         * If portName is found in DB then directly connect the port using PortName.
         */
        if(portName == "")
        {
            status = dmmDB->getRowContents(dbTableName,rowID,rowContents);
            _qDebugE("rowContents:[",rowContents,"]","");

            if(status) portName = rowContents.at(1);
            status = false;
        }

        if(portName.length() > 0)
        {
            if(isOpen()) closePort();

            _qDebugE("portName:[",portName,"]","");

            //connect port with port name
            if(connect(portName))//connect with port name
            {
                _qDebugE("connectDMM:connect with port name:[",portName,"]"," Success.");
                _qDebugE("IDN:[",IDN,"]","");
                _qDebugE("IDNresponse:[",IDNresponse,"]","");
                if(!sendCommand(IDN))
                {
                    QMessageBox::warning(nullptr,"Error!!\n","DMM writing Error.");
                }
                else
                {
                    dataRecieved = getResponse();
                    _qDebugE("dataRecieved:[",dataRecieved,"]","");

                    if((dataRecieved.length() > 0) && (dataRecieved == IDNresponse))
                    {
                        status = true;
                    }
                    else
                    {
                        //Retry
                        if(!sendCommand(IDN))
                        {
                            QMessageBox::warning(nullptr,"Error!!\n","DMM writing Error.");
                        }
                        else
                        {
                            dataRecieved = getResponse();
                            _qDebugE("Retry dataRecieved:[",dataRecieved,"]","");

                            if((dataRecieved.length() > 0) && (dataRecieved == IDNresponse))
                            {
                                status = true;
                            }
                        }
                    }
                }//else block when sendCommand succeed
            }
            else {
                _qDebugE("connect with port name:[",portName,"]"," failed.");
                portName = "";
            }
        }

        if(!status)
        {
            //It indicates either no Port Name has been stored previously or connection to port failed
            if(isOpen()) closePort();

            //Now connect to DMM with available COM PORTs
            if(connect(discardPorts))
            {
                columnNames.clear();
                if(dmmDB->getColumnNames(dbTableName,columnNames))
                {
                    portName = getPortName();
                    _qDebugE("DMM is connected at ",portName,".","");

                    if(dmmDB->editCell(dbTableName,static_cast<unsigned>(rowID),columnNames.at(1),portName))
                    {
                        status = true;
                    }
                    else
                    {
                        _qDebugE("Editing Cell failed","","","");
                    }
                }
                else
                {
                    _qDebugE("Getting column names failed","","","");
                }
            }
            else
            {
                _qDebugE("DMM is offline.","!!!","!!!","!!!");
                portName = "";
            }
        }
    }
    connectionStatus = status;
    return status;
}

/*************************************************************************
 Function Name - disconnectDMM
 Parameter(s)  - void
 Return Type   - void
 Action        - disconnects the DMM port.
 *************************************************************************/
void DMM::disconnectDMM()
{
    _qDebug("void DMM::disconnectDMM().");

    if(isOpen()) closePort();
}

/*************************************************************************
 Function Name - isConnected
 Parameter(s)  - void
 Return Type   - bool
 Action        - checks the connectivity of the Digital multimeter. This
                 function returns true if DMM is connected or else returns
                 false.
 *************************************************************************/
bool DMM::isConnected()
{
    _qDebug("bool DMM::isConnected()).");

    bool status = false;

    QByteArray dataRecieved;

    _qDebugE("IDN Buffer:",IDN,"","");
    if(sendCommand(IDN) == false)
    {
        _qDebugE("Error!! sendCommand[",IDN,"]"," Failed.");
    }
    else
    {
        dataRecieved = getResponse();
        _qDebugE("dataRecieved:[",dataRecieved,"]","");

        if((dataRecieved.length() > 0) && (dataRecieved == IDNresponse))
        {
            status = true;
        }
        else
        {
            //Retry
            if(sendCommand(IDN) == false)
            {
                _qDebugE("Error!! Retried sendCommand[",IDN,"]"," Failed.");
            }
            else
            {
                dataRecieved = getResponse();
                _qDebugE("Retry dataRecieved:[",dataRecieved,"]","");

                if((dataRecieved.length() > 0) && (dataRecieved == IDNresponse))
                {
                    status = true;
                }
            }
        }
    }
    connectionStatus = status;
    return status;
}

/*************************************************************************
 Function Name - sendCommand
 Parameter(s)  - QByteArray
 Return Type   - void
 Action        - writes/sends command on port and returns true if write succeeded
                 else returns false.
 *************************************************************************/
bool DMM::sendCommand(QByteArray cmd)
{
    _qDebug("void DMM::sendCommand(QByteArray cmd).");
    cmd.append(0x0D);
    cmd.append(0x0A);

    return write(cmd);
}

/*************************************************************************
 Function Name - sendCommand
 Parameter(s)  - void
 Return Type   - QByteArray
 Action        - reads response from port and returns the response buffer.
 *************************************************************************/
QByteArray DMM::getResponse()
{
    _qDebug("QByteArray DMM::getResponse().");

    return readAll();
}

/*************************************************************************
 Function Name - getPort
 Parameter(s)  - void
 Return Type   - QString
 Action        - returns the portName assigned.
 *************************************************************************/
QString DMM::getPort()
{
    _qDebug("QString DMM::getPort().");

    return portName;
}

/*************************************************************************
 Function Name - resetDMM
 Parameter(s)  - void
 Return Type   - void
 Action        - resets DMM to initial position.
 *************************************************************************/
void DMM::resetDMM()
{
    _qDebug("void DMM::resetDMM().");

    sendCommand("*RST");
}

/*************************************************************************
 Function Name - setResistanceMode
 Parameter(s)  - void
 Return Type   - void
 Action        - sets DMM to Resistance Mode.
 *************************************************************************/
bool DMM::setResistanceMode()
{
    _qDebug("Insie bool DMM::setResistanceMode().");
    bool ret = false;

    ret = sendCommand("CONF:RES");
    QThread::msleep(500);
    return ret;
}

/*************************************************************************
 Function Name - setVoltageMode
 Parameter(s)  - void
 Return Type   - void
 Action        - sets DMM to Voltage Mode.
 *************************************************************************/
bool DMM::setVoltageMode()
{
    _qDebug("Insie void DMM::setVoltageMode().");
    bool ret = false;

    ret = sendCommand("CONF:VOLT:DC 100");
    QThread::msleep(500);
    return ret;
}

/*************************************************************************
 Function Name - setCurrentMode
 Parameter(s)  - void
 Return Type   - void
 Action        - sets DMM to Current Mode.
 *************************************************************************/
bool DMM::setCurrentMode()
{
    _qDebug("Insie void DMM::setCurrentMode().");
    bool ret = false;

    ret = sendCommand("CONF:CURR:DC 3");
    QThread::msleep(500);
    qDebug()<<"last error : "<<getLastError();

    return ret;
}

/*************************************************************************
 Function Name - getMeterValue
 Parameter(s)  - void
 Return Type   - double
 Action        - reads value from DMM and returns the value in double.
 *************************************************************************/
double DMM::getMeterValue()
{
    _qDebug("double DMM::getMeterValue().");

    QByteArray dataRecieved;
    double resistanceValue = 0.0;

    dataRecieved.clear();

    if(!sendCommand(":INIT;FETCH?"))
    {
        _qDebugE("getMeterValue():","Command sending Failed","","");
    }
    else
    {
        dataRecieved = readAll();
        _qDebugE("dataRecieved:",dataRecieved,"","");

        if(dataRecieved.isEmpty())
        {
            timerObj->delayInms(50);//delay for Retrying
            //Retrying...
            if(!sendCommand(":INIT;FETCH?"))
            {
                _qDebugE("getMeterValue():","Retried:Command sending Failed","","");
            }
            else
            {
                dataRecieved = readAll();
                _qDebugE("Retried:dataRecieved:",dataRecieved,"","");//Ex- Res:+9.90000000E+37,Vol:+1.14743000E-03,Cur:-1.00000000E-10(open cond)
            }
        }
    }

    if(!dataRecieved.isEmpty())
    {
        dataRecieved.remove(0,dataRecieved.length() - 17);
        //_qDebugE("Modified:dataRecieved:",dataRecieved,"","");//Ex-

        resistanceValue = convertToDouble(dataRecieved);
    }

    return resistanceValue;
}

/*************************************************************************
 Function Name - getStableMeterValue
 Parameter(s)  - long long
 Return Type   - double
 Action        - reads multiple value from DMM and calculates the stable meter
                 value and returns the stable value in float.
 *************************************************************************/
double DMM::getStableMeterValue(long long maxMeasurementTimeMS)
{
    _qDebug("double DMM::getStableMeterValue(long long maxMeasurementTimeMS).");

    QElapsedTimer timer;

    double firstValue;
    double secondValue;
    double upperLimit;
    double lowerLimit;
    double accuracy;
    int count = 0;

    timer.start();

    firstValue = getMeterValue();

    while(true)
    {
        //Do operation here
        timerObj->delayInms(DMM_READ_DELAY_MS);//delay for next reading
        secondValue = getMeterValue();

        if(firstValue < 0.5) accuracy = (firstValue * ACCURACY_1)/100.00;
        else accuracy = (firstValue * ACCURACY)/100.00;
        upperLimit = firstValue + accuracy;
        lowerLimit = firstValue - accuracy;

#if SPECIAL_DEBUG
        qDebug()<<"Read Count:"<<count++;
#endif
        _qDebugE("First Value:",firstValue,", secondValue:",secondValue);
//        _qDebugE("upperLimit:",upperLimit," lowerLimit:",lowerLimit);

        if(((secondValue <= upperLimit) && (secondValue >= lowerLimit))||
            (secondValue >= static_cast<double>(OPEN_VALUE)) || timer.hasExpired(maxMeasurementTimeMS)) break;

         firstValue = secondValue;
    }

    return secondValue;
}


bool DMM::setErrorBeeper(bool enable)
{
    _qDebug("bool DMM::setErrorBeeper(bool enable).");
    bool ret = false;

    if(enable)
    {
        ret = sendCommand("SYSTem:ERRor:BEEPer ON");
    }
    else
    {
        ret = sendCommand("SYSTem:ERRor:BEEPer OFF");
    }
    QThread::msleep(500);

    return ret;
}

bool DMM::getErrorBeeperStatus()
{
    _qDebug("bool DMM::getErrorBeeperStatus().");

    QByteArray dataRecieved;
    bool ret = false;

    dataRecieved.clear();

    if(!sendCommand("SYSTem:ERRor:BEEPer?"))
    {
        _qDebugE("getErrorBeeperStatus():","Command sending Failed","","");
    }
    else
    {
        dataRecieved = readAll();
        _qDebugE("dataRecieved:",dataRecieved,"","");

        if(dataRecieved.isEmpty())
        {
            //Retrying...
            dataRecieved = readAll();
            _qDebugE("Retried:dataRecieved:",dataRecieved,"","");//Ex-

            if(!dataRecieved.isEmpty()) ret = true;
        }
    }

    return ret;
}

bool DMM::setRemoteMode()
{
    _qDebug("bool DMM::setRemoteMode().");
    bool ret = false;

    ret = sendCommand("SYSTem:REMote");

    return ret;
}

QString DMM::getLastError()
{
    _qDebug("QString DMM::getLastError().");

    QByteArray dataRecieved;
    QString str;

    dataRecieved.clear();
    str.clear();
    if(!sendCommand("SYSTem:ERRor?"))
    {
        _qDebugE("getLastError():","Command sending Failed","","");
    }
    else
    {
         dataRecieved = readAll();
         _qDebugE("dataRecieved:",dataRecieved,"","");

         if(dataRecieved.isEmpty())
         {
             //Retrying...
             dataRecieved = readAll();
             _qDebugE("Retried:dataRecieved:",dataRecieved,"","");//Ex-

         }

         if(!dataRecieved.isEmpty())
         {
             str = converthexQByteArrayToQString(dataRecieved);
         }
    }
    return str;
}

/*************************************************************************
 Function Name - getErrorLog
 Parameter(s)  - void
 Return Type   - QString
 Action        - returns the error log.
 *************************************************************************/
QString DMM::getErrorLog() const
{
    return errorLog;
}

/*************************************************************************
 Function Name - setErrorLog
 Parameter(s)  - void
 Return Type   - QString
 Action        - appends the given error log in value.
 *************************************************************************/
void DMM::setErrorLog(const QString &value)
{
    if(errorLog.size() > 0) errorLog.append("\n");
    errorLog.append(value);
}

bool DMM::runSelfTest()
{
    _qDebug("QString DMM::runSelfTest().");

    QByteArray dataRecieved;
    QString str;
    errorLog.clear();

    dataRecieved.clear();
    str.clear();
    if(!sendCommand("*TST?"))
    {
        _qDebugE("runSelfTest():","Command sending Failed","","");
    }
    else
    {
         dataRecieved = readAll();
         _qDebugE("dataRecieved:",dataRecieved,"","");

         if(dataRecieved.isEmpty())
         {
             //Retrying...
             dataRecieved = readAll();
             _qDebugE("Retried:dataRecieved:",dataRecieved,"","");//Ex-

         }

         if(dataRecieved == selfTestRespond) return true;
         else {
             setErrorLog(QString("Error Code from DMM : [%1].").arg(QString(dataRecieved)));
         }
    }
    return false;
}
