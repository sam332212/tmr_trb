/*************************************  FILE HEADER  *****************************************************************
*
*  File name - DevicesHandler.cpp
*  Creation date and time - 26-MAR-2020 , THU 10:00 AM IST
*  Author: - Manoj
*  Purpose of the file - All Measurement Devices' Handling modules will be defined .
*
**********************************************************************************************************************/
/********************************************************
 Inclusion of header file
 ********************************************************/
#include "DevicesHandler.h"
#include "Support/Global.h"

/********************************************************
 MACRO Definition
 ********************************************************/
#define DEFAULT_PS_DISPLAY  "0.00"

#if DEVICESHANDLER_DEBUG
    #define _qDebug(s) qDebug()<<"\n"<<s
#else
    #define _qDebug(s); {}
#endif

#if DEVICESHANDLER_DEBUG_E
    #define _qDebugE(p,q,r,s) qDebug()<<p<<q<<r<<s
#else
    #define _qDebugE(p,q,r,s); {}
#endif

#define ERROR_PERCENTAGE 0.03

/********************************************************
 Function Definition
 ********************************************************/

/*************************************************************************
 Function Name - run
 Parameter(s)  - void
 Return Type   - void
 Action        - calls by the thread start function.
 *************************************************************************/
void DevicesHandler::run()
{
    _qDebug("Inside void DevicesHandler::run().");

    threadStatus = true;
    runStatus = true;
    QStringList usedPorts;
    QString voltage,current;

    while(1)
    {
#if THREAD_RUN
        usedPorts.clear();
        if(*isChecking == false && (!pauseFlag))
        {
            *isChecking = true;

            if((DMM::isreadBusy == false) && (G::dmmObj != nullptr) && (isDmmConnected != nullptr)) {
                isDMMReady = false;
                if(!G::dmmObj->isConnected() && !G::dmmObj->connectDMM(usedPorts))
                {
                    _qDebugE("DevicesHandler:DMM Check Failed.","","","");
                    *isDmmConnected = false;
                }
                else
                {
                    *isDmmConnected = true;
                }
                usedPorts.push_back(G::dmmObj->getPort());
                _qDebugE("DevicesHandler:DMM Check Done."," ================= "," ================= "," ================= ");
            }
            else {
                isDMMReady = true;
            }

            if((IRMeter::isreadBusy == false) && (G::irMeterObj != nullptr) && (isIrMeterConnected != nullptr)) {
                isIRReady = false;
                if(!G::irMeterObj->isConnected() && !G::irMeterObj->connectIRMeter(usedPorts))
                {
                    _qDebugE("DevicesHandler:IR Meter Check Failed.","","","");
                    *isIrMeterConnected = false;
                }
                else
                {
                    *isIrMeterConnected = true;
                }
                usedPorts.push_back(G::irMeterObj->getPort());
               _qDebugE("DevicesHandler:IR Meter Check Done."," ================= "," ================= "," ================= ");
            }
            else {
                isIRReady = true;
            }

            if((PowerSupply::isBusy == false) && (G::psObj != nullptr) && (psAddress != nullptr) && (psStatus != nullptr)) {
                isPSReady = false;
                if(!G::psObj->isConnected(*psAddress,*psStatus) && !G::psObj->connectPS(usedPorts,*psAddress,*psStatus))
                {
                    _qDebugE("DevicesHandler:Power Supply Check Failed.","","","");
                }
                else
                {
                    for(uint8_t i = 0;i < psVoltageLcd.count();i++)
                    {
                        if(G::psObj->selectDevice(static_cast<uint8_t>(psAddress->at(i)))) {
                            G::psObj->getPsData();
                            if(G::psObj->isOutputOn()) {
                                voltage = G::psObj->_psDetails.measuredVoltage;
                                current = G::psObj->_psDetails.measuredCurrent;

                                if(voltage.count() == 0) voltage = DEFAULT_PS_DISPLAY;
                                if(current.count() == 0) current = DEFAULT_PS_DISPLAY;
                                psVoltageLcd[i]->display(voltage);
                                psCurrentLcd[i]->display(current);

                                _qDebugE("measuredVoltage: [",voltage,"]","");
                                _qDebugE("measuredCurrent: [",current,"]","");


                                if(isCurVoltageMatchingWithProgVoltage(static_cast<uint8_t>(psAddress->at(i)),voltage.toFloat())) {
                                    PLAY QMessageBox::warning(nullptr,"Error!!",".");
                                    PLAY if (QMessageBox::No == QMessageBox(QMessageBox::Warning, "Error!!",
                                                                       QString("Voltage difference found at PS%1.\nProgrammed Voltage:%2V but found %3V.\nDo you want to continue the test ?").
                                                                            arg(psAddress->at(i)).arg(psProgrammedVoltage.value(static_cast<uint8_t>(psAddress->at(i)))).arg(voltage),
                                                                       QMessageBox::Yes|QMessageBox::No).exec()) { G::stopTest = true; break; }
                                }
                            }
                            else {
                                voltage = G::psObj->_psDetails.programmedVoltage;
                                current = G::psObj->_psDetails.programmedCurrent;

                                if(voltage.count() == 0) voltage = DEFAULT_PS_DISPLAY;
                                if(current.count() == 0) current = DEFAULT_PS_DISPLAY;
                                psVoltageLcd[i]->display(voltage);
                                psCurrentLcd[i]->display(current);

                                _qDebugE("programmedVoltage: [",voltage,"]","");
                                _qDebugE("programmedCurrent: [",current,"]","");
                            }
                        }
                    }
                }
               _qDebugE("DevicesHandler:Power Supply Check Done."," ================= "," ================= "," ================= ");
            }
            else {
                isPSReady = true;
            }
            *isChecking = false;
            timerObj->delayInms(1000);
        }
#endif
        if(runStatus == false) break;
        if(pauseFlag) paused = true;
    }
    _qDebugE("Loop Break.","","","");

    threadStatus = false;
}

/*************************************************************************
 Function Name - isCurVoltageMatchingWithProgVoltage
 Parameter(s)  - int, float
 Return Type   - bool
 Action        - Returns true if the current voltage is within the PERCENTAGE
                 range of programmed voltage else returns false.
 *************************************************************************/
bool DevicesHandler::isCurVoltageMatchingWithProgVoltage(int curPs,float curVoltage)
{
    _qDebug("Inside bool DevicesHandler::isCurVoltageMatchingWithProgVoltage(int curPs,float curVoltage).");
    float progVoltage = psProgrammedVoltage.value(curPs);
    float upperValue  = progVoltage + (progVoltage * ERROR_PERCENTAGE);
    float lowerValue  = progVoltage - (progVoltage * ERROR_PERCENTAGE);

    if((curVoltage > lowerValue) && (curVoltage < upperValue)) return true;
    return false;
}
