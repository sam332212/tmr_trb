/*************************************  FILE HEADER  *****************************************************************
*
*  File name - IRMeter.h
*  Creation date and time - 03-DEC-2019 , TUE 10:00 AM IST
*  Author: - Manoj
*  Purpose of the file - All IR meter modules will be declared.
*
**********************************************************************************************************************/
#ifndef IRMETER_H
#define IRMETER_H

#define BEEP_SOUND_FOR_DEFAULT      "FAIL"
#define BEEP_SOUND_ONOFF_DEFAULT    "ON"
#define LOWER_VALUE_DEFAULT         1000
#define UPPER_VALUE_DEFAULT         2000
#define MEGGER_TEST_VOLTAGE_DEFAULT 100
#define MEGGER_TEST_CURRENT_DEFAULT 10
#define CHG_TIME_DEFAULT            0.3
#define DCHG_TIME_DEFAULT           0
#define DWELL_TIME_DEFAULT          0.2

/********************************************************
 Inclusion of header file
 ********************************************************/
#include "SerialPort.h"
#include "Support/Database.h"
#include "Support/Utility.h"
#include "Support/Timer.h"

/********************************************************
 Class Declaration
 ********************************************************/
class IRMeter : public SerialPort,Utility
{
public:
    //member functions
    IRMeter();
    ~IRMeter();

    void disconnectIRMeter();
    bool resetIRMeter();
    void setDB(Database* database);
    void setDBTableName(QString tableName);
    void setRowId(int rowId);
    void setPortSettings(QByteArray txData,QByteArray matchData,int BaudRate,int DataBits,
                              int Parity,int StopBits,int FlowControl,long Timeout_Millisec,int COMPARE_FLAG);
    void showPortSettings();

    bool connectIRMeter(QStringList discardPorts);
    bool isConnected();
    bool sendCommand(QByteArray cmd);

    double lowerValue; //Lower Test Resistance
    double upperValue; //Upper Test Resitance
    QString beepSoundStr; //PASS- Beeps if passed; FAIL - Beeps if failed
    QString beepSoundONOFF;
    double testVoltage;
    double CHGTime; //Can be floating point
    int DCHGTime;
    double DWELLTime;

    QByteArray getResponse();
    QString getPort();
    bool setLowerValue(double value = LOWER_VALUE_DEFAULT);
    bool setUpperValue(double value = UPPER_VALUE_DEFAULT);
    bool resetRange();
    bool setLowerMin();
    bool setUpperMax();
    bool setBeepSound(QString str = BEEP_SOUND_FOR_DEFAULT);
    bool setBeepSoundONOFF(QString str = BEEP_SOUND_ONOFF_DEFAULT);
    bool setTestVoltage(double value= MEGGER_TEST_VOLTAGE_DEFAULT);
    bool setTestCurrent(double value= MEGGER_TEST_CURRENT_DEFAULT);
    bool setCHGTime(double CHG_Time = CHG_TIME_DEFAULT);
    bool setDCHGTime(int DCHG_Time = DCHG_TIME_DEFAULT);
    bool setDWELLTime(double DWELL_Time = DWELL_TIME_DEFAULT);
    QString getLowerValue();
    QString getUpperValue();
    QString getBeepSoundStr();
    QString getBeepSoundStatus();
    bool runSelfTest();
    bool checkPassFail(double &value);
    bool getResistance(double &resistance);
    QByteArray checkError();
    QByteArray getStatusRegister();
    double getTestVoltage();
    double getCHGTime();
    int getDCHGTime();
    double getDWELLTime();
    bool setIRMode();

    //member variables
    QByteArray IDN = "*IDN?";
    QByteArray IDNresponse = "Chroma,11200,112001001504,02.41";
    QByteArray selfTestRespond = "16";

    static bool connectionStatus;
    static bool isreadBusy;
    QString getErrorLog() const;

private:
    void setErrorLog(const QString &value);

    Timer* timerObj = nullptr;

    Database *irMeterDB = nullptr;
    QString portName = "";
    QString dbTableName = "";
    int rowID = 0;
    bool portSet = false;
    QString errorLog;
};

#endif // IRMETER_H
