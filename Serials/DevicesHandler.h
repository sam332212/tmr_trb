/*************************************  FILE HEADER  *****************************************************************
*
*  File name - DevicesHandler.h
*  Creation date and time - 26-MAR-2020 , THU 10:00 AM IST
*  Author: - Manoj
*  Purpose of the file - All Measurement Devices' Handling modules will be declared .
*
**********************************************************************************************************************/

#ifndef DEVICESHANDLER_H
#define DEVICESHANDLER_H

/********************************************************
 Inclusion of header file
 ********************************************************/
#include <QThread>
#include <QDebug>
#include <QLCDNumber>
#include <QLabel>

#include "Support/debugflag.h"
#include "SerialPort.h"
#include "Support/Database.h"
#include "Support/Utility.h"
#include "Support/Timer.h"

/********************************************************
 Class Declaration
 ********************************************************/
class DevicesHandler : public QThread,SerialPort
{
public:
    volatile bool *isDmmConnected = nullptr;
    volatile bool *isIrMeterConnected = nullptr;
    volatile bool *isChecking = nullptr;
    QVector<bool> *psStatus = nullptr;
    QVector<int> *psAddress = nullptr;
    QVector<QLCDNumber*>psVoltageLcd;
    QVector<QLCDNumber*>psCurrentLcd;
    bool isDMMReady = true;
    bool isIRReady = true;
    bool isPSReady = true;
    QMap<int, float> psProgrammedVoltage;

    //member functions
    DevicesHandler() { timerObj = new Timer; };
    ~DevicesHandler() { clearmem(timerObj); };

    void run();
    void stop() {this->runStatus = false; };
    void pause() { this->pauseFlag = true; };
    void resume() { this->pauseFlag = this->paused = false; };
    bool isRunning() { return this->threadStatus; };
    bool isPaused() { return this->paused; };
    void setPsProgrammedVoltage(const QMap<int, float> &value) { this->psProgrammedVoltage = value; }

private:
    //member variables
    bool runStatus    = false;//This flag tells about thread running status
    bool threadStatus = false;//This flag tells about thread live
    bool paused       = false;//This flag tells about thread paused

    Timer* timerObj = nullptr;
    bool pauseFlag = false;

    bool isCurVoltageMatchingWithProgVoltage(int curPs,float curVoltage);
};

#endif // DEVICESHANDLER_H
