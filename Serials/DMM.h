/*************************************  FILE HEADER  *****************************************************************
*
*  File name - DMM.h
*  Creation date and time - 28-OCT-2019 , MON 10:00 AM IST
*  Author: - Manoj
*  Purpose of the file - All Digital Multimeter modules will be declared .
*
**********************************************************************************************************************/
#ifndef DMM_H
#define DMM_H

/********************************************************
 Inclusion of header file
 ********************************************************/
#include "SerialPort.h"
#include "Support/Database.h"
#include "Support/Utility.h"
#include "Support/Timer.h"

/********************************************************
 Class Declaration
 ********************************************************/
class DMM : public SerialPort,Utility
{
public:
    //member functions
    DMM();
    ~DMM();

    void disconnectDMM();
    void resetDMM();
    void setDB(Database* database);
    void setDBTableName(QString tableName);
    void setRowId(int rowId);
    void setPortSettings(QByteArray txData,QByteArray matchData,int BaudRate,int DataBits,
                              int Parity,int StopBits,int FlowControl,long Timeout_Millisec,int COMPARE_FLAG);
    void showPortSettings();

    bool connectDMM(QStringList discardPorts);
    bool isConnected();
    bool sendCommand(QByteArray cmd);
    bool setErrorBeeper(bool enable);
    bool getErrorBeeperStatus();
    bool setRemoteMode();
    bool setResistanceMode();
    bool setVoltageMode();
    bool setCurrentMode();
    bool runSelfTest();

    double getMeterValue();
    double getStableMeterValue(long long maxMeasurementTimeMS);

    QByteArray getResponse();
    QString getPort();
    QString getLastError();
    void setIRMode();

    //member variables
    QByteArray IDN = "*IDN?";
    QByteArray IDNresponse = "TEKTRONIX,DMM4040,4466110,08/02/10-11:53";
    QByteArray selfTestRespond = "0";

    static bool connectionStatus;
    static bool isreadBusy;
    QString getErrorLog() const;

private:
    void setErrorLog(const QString &value);

    Timer* timerObj = nullptr;

    Database *dmmDB = nullptr;
    QString portName = "";
    QString dbTableName = "";
    int rowID = 0;
    bool portSet = false;
    QString errorLog;
};

#endif // DMM_H
