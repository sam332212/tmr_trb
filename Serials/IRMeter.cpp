/*************************************  FILE HEADER  *****************************************************************
*
*  File name - DMM.cpp
*  Creation date and time - 28-OCT-2019 , MON 10:00 AM IST
*  Author: - Manoj
*  Purpose of the file - All Digital Multimeter modules will be defined .
*
**********************************************************************************************************************/
/********************************************************
 Inclusion of header file
 ********************************************************/
#include "IRMeter.h"
#include "Support/debugflag.h"

/********************************************************
 MACRO Definition
 ********************************************************/
#if IRMETER_DEBUG
    #define #define _qDebug(s) qDebug()<<"\n-------- Inside "<<s<<" --------"
#else
    #define _qDebug(s); {}
#endif

#if IRMETER_DEBUG_E
    #define _qDebugE(p,q,r,s) qDebug()<<p<<q<<r<<s
#else
    #define _qDebugE(p,q,r,s); {}
#endif

#define ACCURACY 0.5//in percentage
#define COUNT    5  //Maximum number of times IRMETER should try to get stable value

#define DELAY_AFTER_COMMAND 60

bool IRMeter::connectionStatus = false;
bool IRMeter::isreadBusy = false;

/********************************************************
 Function Definition
 ********************************************************/
/*************************************************************************
 Function Name - IRMeter
 Parameter(s)  - void
 Return Type   - void
 Action        - Creates the object.
 *************************************************************************/
IRMeter::IRMeter():timerObj(new Timer)
{
    _qDebug("IRMeter Constructor.");
    //Ex- Response: "Chroma,11200,112001001504,02.41\n"
    IDNresponse.append(0x0A);

    selfTestRespond.append(0x0A);
}

/*************************************************************************
 Function Name - ~IRMeter
 Parameter(s)  - void
 Return Type   - void
 Action        - Destroys the object.
 *************************************************************************/
IRMeter::~IRMeter()
{
    _qDebug("IRMeter Destructor.");
    clearmem(timerObj);
}

/*************************************************************************
 Function Name - setDB
 Parameter(s)  - Database*
 Return Type   - void
 Action        - sets IRMeter db.
 *************************************************************************/
void IRMeter::setDB(Database* database)
{
    _qDebug("void IRMeter::setDB(Database* database).");

    irMeterDB = database;
}

/*************************************************************************
 Function Name - setDBTableName
 Parameter(s)  - QString
 Return Type   - void
 Action        - sets IRMeter db table name.
 *************************************************************************/
void IRMeter::setDBTableName(QString tableName)
{
   _qDebug("void IRMeter::setDBTableName(QString tableName).");

   dbTableName = tableName;
}

/*************************************************************************
 Function Name - setRowId
 Parameter(s)  - int
 Return Type   - void
 Action        - sets IRMeter info containing row number.
 *************************************************************************/
void IRMeter::setRowId(int rowId)
{
   _qDebug("void IRMeter::setRowId(int rowId).");

   rowID = rowId;
}

/*************************************************************************
 Function Name - setPortSettings
 Parameter(s)  - QByteArray, QByteArray, int, int, int, int, int, long, int
 Return Type   - void
 Action        - sets IRMeter port settings.
 *************************************************************************/
void IRMeter::setPortSettings(QByteArray txData,QByteArray matchData,int BaudRate,int DataBits,
                          int Parity,int StopBits,int FlowControl,long Timeout_Millisec,int COMPARE_FLAG)
{
    _qDebug("void IRMeter::setPortSettings(QByteArray txData,QByteArray matchData,int BaudRate,int DataBits,\
            int Parity,int StopBits,int FlowControl,long Timeout_Millisec,int COMPARE_FLAG).");

    txData.append(0x0D);
    txData.append(0x0A);
    setPortData(txData,matchData,BaudRate,DataBits,Parity,StopBits,FlowControl,Timeout_Millisec,COMPARE_FLAG);

    portSet = true;
}

/*************************************************************************
 Function Name - showPortSettings
 Parameter(s)  - void
 Return Type   - void
 Action        - prints IRMeter port settings.
 *************************************************************************/
void IRMeter::showPortSettings()
{
    _qDebug("void IRMeter::showPortSettings().");

    printSettings();
}

/*************************************************************************
 Function Name - connectIRMeter
 Parameter(s)  - QStringList
 Return Type   - bool
 Action        - searches and connects the appropriate PORT for Digital multimeter.
                 This function returns true if IRMeter is successfully connected
                 or else returns false.
 *************************************************************************/
bool IRMeter::connectIRMeter(QStringList discardPorts)
{
    _qDebug("bool IRMeter::connectIRMeter(QStringList discardPorts).");

    bool status = false;
    QString DMMportName;
    QStringList rowContents;
    QStringList columnNames;

    QByteArray dataRecieved = "";

    rowContents.clear();
    columnNames.clear();

    if(irMeterDB == nullptr)
    {
        QMessageBox::warning(nullptr,"Oops!!\n","IRMeter DB is not set");
    }
    else if(dbTableName == "")
    {
        QMessageBox::warning(nullptr,"Oops!!\n","IRMeter Table Name is not set");
    }
    else if(rowID == 0)
    {
        QMessageBox::warning(nullptr,"Oops!!\n","IRMeter RowID is not set");
    }
    else if(portSet == false)
    {
        QMessageBox::warning(nullptr,"Oops!!\n","IRMeter PORT is not set");
    }
    else
    {
        /* check portName variable. If contains empty then it indicates application started now.
         * Then get portName from DB. If in DB, port field is empty then it indicates the port
         * has never detected successfully in past. So try to detect the port and if detected
         * successfully then store the port name in DB.
         * If portName is found in DB then directly connect the port using PortName.
         */
        if(portName == "")
        {
            status = irMeterDB->getRowContents(dbTableName,rowID,rowContents);
            _qDebugE("rowContents:[",rowContents,"]","");

            if(status) portName = rowContents.at(1);
            status = false;
        }

        if(portName.length() > 0)
        {
            if(isOpen()) closePort();

            _qDebugE("portName:[",portName,"]","");

            //connect port with port name
            if(connect(portName))//connect with port name
            {
                _qDebugE("connectIRMeter:connect with port name:[",portName,"]"," Success.");
                _qDebugE("IDN:[",IDN,"]","");
                _qDebugE("IDNresponse:[",IDNresponse,"]","");
                if(!sendCommand(IDN))
                {
                    QMessageBox::warning(nullptr,"Error!!\n","IRMeter writing Error.");
                }
                else
                {
                    dataRecieved = getResponse();
                    _qDebugE("dataRecieved:[",dataRecieved,"]","");

                    if((dataRecieved.length() > 0) && (dataRecieved == IDNresponse))
                    {
                        status = true;
                    }
                    else
                    {
                        //Retry
                        if(!sendCommand(IDN))
                        {
                            QMessageBox::warning(nullptr,"Error!!\n","IRMeter writing Error.");
                        }
                        else
                        {
                            dataRecieved = getResponse();
                            _qDebugE("Retry dataRecieved:[",dataRecieved,"]","");

                            if((dataRecieved.length() > 0) && (dataRecieved == IDNresponse))
                            {
                                status = true;
                            }
                        }
                    }
                }//else block when sendCommand succeed
            }
            else {
                _qDebugE("connectIRMeter:connect with port name:[",portName,"]"," failed.");
                portName = "";
            }
        }

        if(!status)
        {
            //It indicates either no Port Name has been stored previously or connection to port failed
            if(isOpen()) closePort();

            //Now connect to IRMeter with available COM PORTs
            if(connect(discardPorts))
            {
                columnNames.clear();
                if(irMeterDB->getColumnNames(dbTableName,columnNames))
                {
                    portName = getPortName();
                    _qDebugE("IRMeter is connected at ",portName,".","");

                    if(irMeterDB->editCell(dbTableName,static_cast<unsigned>(rowID),columnNames.at(1),portName))
                    {
                        status = true;
                    }
                    else
                    {
                        _qDebugE("Editing Cell failed","","","");
                    }
                }
                else
                {
                    _qDebugE("Getting column names failed","","","");
                }
            }
            else
            {
                _qDebugE("IRMeter is offline.","!!!","!!!","!!!");
                portName = "";
            }
        }
    }
    connectionStatus = status;
    return status;
}

/*************************************************************************
 Function Name - disconnectIRMeter
 Parameter(s)  - void
 Return Type   - void
 Action        - disconnects the IRMeter port.
 *************************************************************************/
void IRMeter::disconnectIRMeter()
{
    _qDebug("void IRMeter::disconnectIRMeter().");

    if(isOpen()) closePort();
}

/*************************************************************************
 Function Name - isConnected
 Parameter(s)  - void
 Return Type   - bool
 Action        - checks the connectivity of the IR meter. This function returns
                 true if IRMeter is connected or else returns false.
 *************************************************************************/
bool IRMeter::isConnected()
{
    _qDebug("bool IRMeter::isConnected()).");

    bool status = false;

    QByteArray dataRecieved;

    _qDebugE("IDN Buffer:",IDN,"","");
    if(sendCommand(IDN) == false)
    {
        _qDebugE("Error!! sendCommand[",IDN,"]"," Failed.");
    }
    else
    {
        dataRecieved = getResponse();
        _qDebugE("dataRecieved:[",dataRecieved,"]","");

        if((dataRecieved.length() > 0) && (dataRecieved == IDNresponse))
        {
            status = true;
        }
        else
        {
            //Retry
            if(sendCommand(IDN) == false)
            {
                _qDebugE("Error!! Retried sendCommand[",IDN,"]"," Failed.");
            }
            else
            {
                dataRecieved = getResponse();
                _qDebugE("Retry dataRecieved:[",dataRecieved,"]","");

                if((dataRecieved.length() > 0) && (dataRecieved == IDNresponse))
                {
                    status = true;
                }
            }
        }
    }
    connectionStatus = status;
    return status;
}

/*************************************************************************
 Function Name - sendCommand
 Parameter(s)  - QByteArray
 Return Type   - void
 Action        - writes/sends command on port and returns true if write succeeded
                 else returns false.
 *************************************************************************/
bool IRMeter::sendCommand(QByteArray cmd)
{
    _qDebug("void IRMeter::sendCommand(QByteArray cmd).");
    cmd.append(0x0D);
    cmd.append(0x0A);

    return write(cmd);
}

/*************************************************************************
 Function Name - sendCommand
 Parameter(s)  - void
 Return Type   - QByteArray
 Action        - reads response from port and returns the response buffer.
 *************************************************************************/
QByteArray IRMeter::getResponse()
{
    _qDebug("QByteArray IRMeter::getResponse().");

    return readAll();
}

/*************************************************************************
 Function Name - getPort
 Parameter(s)  - void
 Return Type   - QString
 Action        - returns the portName assigned.
 *************************************************************************/
QString IRMeter::getPort()
{
    _qDebug("QString IRMeter::getPort().");

    return portName;
}

/*************************************************************************
 Function Name - resetIRMeter
 Parameter(s)  - void
 Return Type   - void
 Action        - resets IRMeter to initial position.
 *************************************************************************/
bool IRMeter::resetIRMeter()
{
    _qDebug("void IRMeter::resetIRMeter().");

    if(!sendCommand("*RST"))return false;
    return true;
}

bool IRMeter::setLowerValue(double value)
{
    _qDebug("void IRMeter::setLowerValue(double value).");

    lowerValue = value;

    QString temp;
    QString command;

    temp.clear();
    if(value<pow(10,6))
    {
        _qDebugE("Value is in kilo","","","");
        temp = kilo(value);
        temp += "OHM";
    }
    else if(value>=pow(10,6) && value<pow(10,9))
    {
        _qDebugE("Value is in mega","","","");
        temp = mega(value);
        temp += "AOHM";
    }
    else if(value>=pow(10,9))
    {
        _qDebugE("Value is in giga","","","");
        temp = giga(value);
        temp += "OHM";
    }
    command = "CALCulate:LIMit:LOWer MIN";
    _qDebugE("command:str:[",command,"]","");

    if(!sendCommand(convertQStringToByteArray(command)))return false;

    timerObj->delayInms(DELAY_AFTER_COMMAND);
    return true;
}

bool IRMeter::setUpperValue(double value)
{
    _qDebug("void IRMeter::setUpperValue(double value).");

    upperValue = value;

    QString temp;
    QString command;

    temp.clear();

    if(value<pow(10,6))
    {
        _qDebugE("Value is in kilo","","","");
        temp = kilo(value);
        temp += "OHM";
    }
    else if(value>=pow(10,6) && value<pow(10,9))
    {
        _qDebugE("Value is in mega","","","");
        temp = mega(value);
        temp += "AOHM";
    }
    else if(value>=pow(10,9))
    {
        _qDebugE("Value is in giga","","","");
        temp = giga(value);
        temp += "OHM";
    }
    temp = "1.00E+8";
    command = "CALCulate:LIMit:UPPer "+temp;
    _qDebugE("command:str:[",command,"]","");

    if(!sendCommand(convertQStringToByteArray(command)))return false;

    timerObj->delayInms(DELAY_AFTER_COMMAND);
    return true;
}

bool IRMeter::resetRange()
{
    _qDebug("void IRMeter::resetRange().");

    if(!setLowerValue(0))return false;
    if(!setUpperValue(1))return false;
    return true;
}

bool IRMeter::setLowerMin()
{
    _qDebug("void IRMeter::setLowerMin().");

    QString command = "CALCulate:LIMit:LOWer MIN";
    _qDebugE("command:str:[",command,"]","");

    if(!sendCommand(convertQStringToByteArray(command)))return false;

    timerObj->delayInms(DELAY_AFTER_COMMAND);
    return true;
}

bool IRMeter::setUpperMax()
{
    _qDebug("void IRMeter::setUpperMax().");

    QString command = "CALCulate:LIMit:UPPer MAX";

 //  QString command = "CALCulate:LIMit:UPPer 750 KOHM MAX"; //hardcoded

    _qDebugE("command:str:[",command,"]","");

    if(!sendCommand(convertQStringToByteArray(command)))return false;

    timerObj->delayInms(DELAY_AFTER_COMMAND);
    return true;
}

bool IRMeter::setBeepSound(QString str)
{
    _qDebug("void IRMeter::setBeepSound(QString str).");

    beepSoundStr = str.trimmed();
    if(!sendCommand("CALCULATE:LIMIT:BEEPER:CONDITION " + convertQStringToByteArray(beepSoundStr)))return false;

    timerObj->delayInms(DELAY_AFTER_COMMAND);
    return true;
}

bool IRMeter::setBeepSoundONOFF(QString str)
{
    _qDebug("void IRMeter::setBeepSoundONOFF(QString str).");

    beepSoundONOFF = str.trimmed();
    if(!sendCommand("CALCulate:LIMIT:BEEPER:STATE " + convertQStringToByteArray(beepSoundONOFF)))return false;

    timerObj->delayInms(DELAY_AFTER_COMMAND);
    return true;
}

bool IRMeter::setTestVoltage(double value)
{
    _qDebug("void IRMeter::setTestVoltage(double value)");

    if(value > 650)
    {
        _qDebugE("Megger: Voltage higher than 650V, unable to set.","voltage:[",QString::number(static_cast<double>(value), 'f', 2),"]");
        return false;
    }
    else
    {
        testVoltage = value;
        if(!sendCommand("LCTest:SOURce:VOLTage "+QByteArray::number(static_cast<double>(value))))return false;

        timerObj->delayInms(DELAY_AFTER_COMMAND);
    }
    return true;
}

bool IRMeter::setTestCurrent(double value)
{
    _qDebug("void IRMeter::setTestCurrent(double value)");

    if(value > 500)
    {
        _qDebugE("Megger: Current higher than 500mA, unable to set.","current:[",QString::number(static_cast<double>(value), 'f', 2),"]");
        return false;
    }
    else
    {
        if(!sendCommand("LCTest:SOURce:CURRent "+QByteArray::number(static_cast<double>(value))))return false;

        timerObj->delayInms(DELAY_AFTER_COMMAND);
    }
    return true;
}

bool IRMeter::setCHGTime(double CHG_Time)
{
    _qDebug("void IRMeter::setCHGTime(double CHG_Time)");

    CHGTime = CHG_Time;
    if(!sendCommand("LCTest:CONFigure:CHGTime " +QByteArray::number(static_cast<double>(CHGTime))))return false;

    //Removal of the below code leads to error: -226 "Query INTERRRUPTED"
    QByteArray data;
    data = getResponse();
    _qDebugE("Response:[",data,"]","");

    timerObj->delayInms(DELAY_AFTER_COMMAND);
    return true;
}

bool IRMeter::setDCHGTime(int DCHG_Time)
{
    _qDebug("void IRMeter::setDCHGTime(int DCHG_Time)");

    DCHGTime = DCHG_Time;
    if(!sendCommand("LCTest:CONFigure:DCHGTime " + QByteArray::number(DCHGTime)))return false;

    timerObj->delayInms(DELAY_AFTER_COMMAND);
    return true;
}

bool IRMeter::setDWELLTime(double DWELL_Time)
{
    _qDebug("In IRMeter::setDWELLTime");

    DWELLTime = DWELL_Time;
    if(!sendCommand("LCTest:CONFigure:DWELL " + QByteArray::number(static_cast<double>(DWELLTime))))return false;

    timerObj->delayInms(DELAY_AFTER_COMMAND);
    return true;
}

QString IRMeter::getLowerValue()
{
    _qDebug("In IRMeter::getLowerValue");

    QByteArray data;
    QString lowerValue;


    data.clear();
    lowerValue.clear();

    sendCommand("CALCulate:LIMit:LOWer?");
    data = getResponse();
    _qDebugE("data:[",data,"]","");

    if(data.length() < 1)
    {
        //Retry
        if(!sendCommand("CALCulate:LIMit:LOWer?"))
        {
            _qDebugE("Error!! ","getLowerValue:","IRMeter writing Error","");
        }
        else
        {
            data = getResponse();
            _qDebugE("Retry data:[",data,"]","");
        }
    }

    if(data.length() > 0)
    {
        lowerValue = converthexQByteArrayToQString(data);

        if(lowerValue.contains("\n"))
            lowerValue.remove("\n");

        lowerValue = lowerValue.trimmed();
    }

    return lowerValue;
}

QString IRMeter::getUpperValue()
{
    _qDebug("QString IRMeter::getUpperValue()");
    QByteArray data;
    QString upperValue;

    data.clear();
    upperValue.clear();

    sendCommand("CALCulate:LIMit:UPPer?");
    data = getResponse();
    _qDebugE("data:[",data,"]","");

    if(data.length() < 1)
    {
        //Retry
        if(!sendCommand("CALCulate:LIMit:UPPer?"))
        {
            _qDebugE("Error!! ","getUpperValue:","IRMeter writing Error","");
        }
        else
        {
            data = getResponse();
            _qDebugE("Retry data:[",data,"]","");
        }
    }

    if(data.length() > 0)
    {
        upperValue = converthexQByteArrayToQString(data);

        if(upperValue.contains("\n"))
            upperValue.remove("\n");

        upperValue = upperValue.trimmed();
    }

    return upperValue;
}

QString IRMeter::getBeepSoundStr()
{
    _qDebug("QString IRMeter::getBeepSoundStr()");

    QByteArray data;
    QString beepSoundString;

    data.clear();
    beepSoundString.clear();

    sendCommand("CALCULATE:LIMIT:BEEPER:CONDITION?");
    data = getResponse();
    _qDebugE("data:[",data,"]","");

    if(data.length() < 1)
    {
        //Retry
        if(!sendCommand("CALCULATE:LIMIT:BEEPER:CONDITION?"))
        {
            _qDebugE("Error!! ","getBeepSoundStr:","IRMeter writing Error","");
        }
        else
        {
            data = getResponse();
            _qDebugE("Retry data:[",data,"]","");
        }
    }

    if(data.length() > 0)
    {
        beepSoundString = converthexQByteArrayToQString(data);

        if(beepSoundString.contains("\n"))
            beepSoundString.remove("\n");

        beepSoundString = beepSoundString.trimmed();
    }

    return beepSoundString;
}

QString IRMeter::getBeepSoundStatus()
{
    _qDebug("QString IRMeter::getBeepSoundStatus");

    QByteArray data;
    QString beepSoundStatus;

    data.clear();
    beepSoundStatus.clear();

    sendCommand("CALCulate:LIMIT:BEEPER:STATE?");
    data = getResponse();
    _qDebugE("data:[",data,"]","");

    if(data.length() < 1)
    {
        //Retry
        if(!sendCommand("CALCulate:LIMIT:BEEPER:STATE?"))
        {
            _qDebugE("Error!! ","getBeepSoundStr:","IRMeter writing Error","");
        }
        else
        {
            data = getResponse();
            _qDebugE("Retry data:[",data,"]","");
        }
    }

    if(data.length() > 0)
    {
        beepSoundStatus = converthexQByteArrayToQString(data);

        if(beepSoundStatus.contains("\n"))
            beepSoundStatus.remove("\n");

        beepSoundStatus = beepSoundStatus.trimmed();
    }

    return beepSoundStatus;
}

bool IRMeter::checkPassFail(double &value)
{
    _qDebug("bool IRMeter::checkPassFail(float &value)");
    QByteArray data;
    QByteArray valueArray;
    bool ret = false;

    data.clear();

    sendCommand("CALCULATE:LIMIT:CLEAR");
    sendCommand("*TRG");

    data = getResponse();
    timerObj->delayInms(4000);

    // handling short condition (charge in progres...)-----------------------
    int i = 0;
    while(i<=2){
        i++;
        sendCommand("LCTest:MEASure:STATe?");
        data.clear();
        data = getResponse();
        qDebug()<<"LCTest:MEASure:STATe?"<<data;
    }

    _qDebugE("after loop LCTest:MEASure:STATe?",data,"","");

    if(data.at(0)=='C')
    {
        sendCommand("ABORt");
        value = 0;
    }
    else
    {
        timerObj->delayInms(4000);

        // till here.... handling short condition (charge in progres...)--------------
        sendCommand("CALCulate:LIMIT:FAIL?");

        data = getResponse();
        _qDebugE("data LIMIT FAIL: ",data,"","");

        sendCommand("LCTest:MEASure:IR?");
        data = getResponse();
        qDebug()<<valueArray;

        value = convertToDouble(valueArray);

        //for 99.99 GOHM, max limit of Megger
        if(static_cast<double>(value) > 99990000.000)  //99.99 GOHM, max limit of Megger
        {
            ret = true;
        }
        else
        {
            //till here...99.99 GOHM, max limit of Megger
            QString result = converthexQByteArrayToQString(data);
            result.remove("\n");
            result = removeSymbolsOtherThanNumbers(result);
            qDebug()<<"Result "<<result;

            if(result.toInt() == 1)
            {
                ret = true;
            }
        }

    }

    return ret;
}

bool IRMeter::getResistance(double &resistance)
{
    _qDebug("bool IRMeter::getResistance(double &resistance)");

    QByteArray data;
    resistance = 0.0;
    int count = 0;
    bool status = false;

    if(!sendCommand("*TRG"))
    {
        _qDebugE("Error!! ","getResistance:","IRMeter writing Error","");
    }
    else
    {
        while(1)
        {
            count++;
            data.clear();

            if(!sendCommand("LCTest:MEASure:STATe?"))
            {
                _qDebugE("Error!! ","getResistance:","IRMeter writing Error","");
            }
            else
            {
                data = getResponse();
                _qDebugE(count,":data:[",data,"]");
            }

            if((data.count() > 0 ) && (data.at(0)=='D')) break;

            if(count > 50)
            {
                sendCommand("ABORt");
                QThread::sleep(2);
                return 0.0;
            }
        }

        if(!sendCommand("LCTest:MEASure:IR?"))
        {
            _qDebugE("Error!! ","getResistance:","IRMeter writing Error","");
        }
        else
        {
            data = getResponse();
            _qDebugE(count,":data:[",data,"]");
            resistance = convertToDouble(data);
            status = true;
        }
    }

    return status;
}

QByteArray IRMeter::checkError()
{
    _qDebug("QByteArray IRMeter::checkError()");

    QByteArray data;
    sendCommand("SYSTem:ERRor?");
    data = getResponse();
    _qDebugE("data:[",data,"]","");

    return data;
}

QByteArray IRMeter::getStatusRegister()
{
    _qDebug("QByteArray IRMeter::getStatusRegister()");

    QByteArray data;
    sendCommand("*STB?");
    data = getResponse();
    _qDebugE("status data:[",data,"]","");

    return data;
}

double IRMeter::getTestVoltage()
{
    _qDebug("double IRMeter::getTestVoltage()");

    QByteArray data;
    QString string;
    double voltage = 0.0;

    sendCommand("LCTest:SOURce:VOLTage?");

    data = getResponse();
    _qDebugE("data:[",data,"]","");

    string = converthexQByteArrayToQString(data);
    if(string.contains("\n"))
        string.remove("\n");

    if(string.count() > 0)
        voltage = string.toDouble();

    return voltage;
}

double IRMeter::getCHGTime()
{
    _qDebug("double IRMeter::getCHGTime");

    QByteArray data;
    QString string;
    double value = 0.0;

    sendCommand("LCTest:CONFigure:CHGTime?");

    data = getResponse();
    _qDebugE("data:[",data,"]","");

    string = converthexQByteArrayToQString(data);
    if(string.contains("\n"))
        string.remove("\n");

    if(string.count() > 0)
        value = string.toDouble();

    return value;
}

int IRMeter::getDCHGTime()
{
    _qDebug("int IRMeter::getDCHGTime()");
    QByteArray data;
    QString string;
    int value = 0;

    sendCommand("LCTest:CONFigure:DCHGTime?");

    data = getResponse();
    _qDebugE("data:[",data,"]","");

    string = converthexQByteArrayToQString(data);
    if(string.contains("\n"))
        string.remove("\n");

    if(string.count() > 0)
        value = string.toInt();

    return value;
}

double IRMeter::getDWELLTime()
{
    _qDebug("double IRMeter::getDWELLTime()");
    QByteArray data;
    QString string;
    double value = 0.0;

    sendCommand("LCTest:CONFigure:DWELL?");

    data = getResponse();
    _qDebugE("data:[",data,"]","");

    string = converthexQByteArrayToQString(data);
    if(string.contains("\n"))
        string.remove("\n");

    if(string.count() > 0)
        value = string.toDouble();

    return value;
}

bool IRMeter::setIRMode()
{
    _qDebug("bool IRMeter::setIRMode()");
    if(!sendCommand("*RST")) return false;
    timerObj->delayInms(DELAY_AFTER_COMMAND);

    if(!sendCommand("DISPlay:LCTest")) return false;
    timerObj->delayInms(DELAY_AFTER_COMMAND);

    if(!sendCommand("CALCulate:LIMIT:FORMAT IR")) return false;
    timerObj->delayInms(DELAY_AFTER_COMMAND);

    if(!sendCommand("TRIGger:SOURce BUS")) return false;
    timerObj->delayInms(DELAY_AFTER_COMMAND);

    if(!sendCommand("TRIGger:DELay MIN")) return false;
    timerObj->delayInms(DELAY_AFTER_COMMAND);

    if(!sendCommand("LCTest:CONFigure:FUNCtion SEQ")) return false;
    timerObj->delayInms(DELAY_AFTER_COMMAND);

    if(!setCHGTime())return false;
    if(!setDCHGTime())return false;
    if(!setTestVoltage())return false;
    if(!setTestCurrent())return false;

    if(!sendCommand("LCTest:CONFigure:SPEed FAST")) return false;
    timerObj->delayInms(DELAY_AFTER_COMMAND);

    if(!sendCommand("LCTest:CONFigure:RANGe:AUTO ON")) return false;
    timerObj->delayInms(DELAY_AFTER_COMMAND);

    if(!setDWELLTime(0.2))return false;

    if(!sendCommand("CALCulate:LIMIT:STATe ON")) return false;
    timerObj->delayInms(DELAY_AFTER_COMMAND);

    if(!sendCommand("CALCulate:LIMIT:ONOFf 0")) return false;
    timerObj->delayInms(DELAY_AFTER_COMMAND);

    if(!setBeepSound())return false;
    if(!setBeepSoundONOFF())return false;
    if(!setLowerValue())return false;
    if(!setUpperValue())return false;

    return true;
}

/*************************************************************************
 Function Name - getErrorLog
 Parameter(s)  - void
 Return Type   - QString
 Action        - returns the error log.
 *************************************************************************/
QString IRMeter::getErrorLog() const
{
    return errorLog;
}

/*************************************************************************
 Function Name - setErrorLog
 Parameter(s)  - void
 Return Type   - QString
 Action        - appends the given error log in value.
 *************************************************************************/
void IRMeter::setErrorLog(const QString &value)
{
    if(errorLog.size() > 0) errorLog.append("\n");
    errorLog.append(value);
}

bool IRMeter::runSelfTest() {
    _qDebug("bool IRMeter::runSelfTest()");
    
    QByteArray data;
    data.clear();
    errorLog.clear();
    
    sendCommand("*TST?");
    data = getResponse();
    _qDebugE("data:[",data,"]","");

    if(data.length() < 1)
    {
        //Retry
        if(!sendCommand("*TST?"))
        {
            _qDebugE("Error!! ","runSelfTest:","IRMeter writing Error","");
            return false;
        }
        else
        {
            data = getResponse();
            _qDebugE("Retry data:[",data,"]","");
        }
    }

    if(data == selfTestRespond) return true;
    else {
        setErrorLog(QString("Error Code from IR Meter : [%1].").arg(QString(data)));
    }

    return false;
}
