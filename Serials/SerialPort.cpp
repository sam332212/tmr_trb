/*************************************  FILE HEADER  *****************************************************************
*
*  File name -SerialPort.cpp
*  Creation date and time - 14-SEP-2019 , SAT 07:00 AM IST
*  Author: - Manoj
*  Purpose of the file - All RS232-communication modules will be defined .
*
**********************************************************************************************************************/
/********************************************************
 Inclusion of header file
 ********************************************************/
#include "SerialPort.h"
#include "Support/debugflag.h"

/********************************************************
 MACRO Definition
 ********************************************************/
#if SERIALPORT_DEBUG
    #define _qDebug(s) qDebug()<<"\n"<<s
#else
    #define _qDebug(s); {}
#endif

#if SERIALPORT_DEBUG_E
    #define _qDebugE(p,q,r,s) qDebug()<<p<<q<<r<<s
#else
    #define _qDebugE(p,q,r,s); {}
#endif

/********************************************************
 Function Definition
 ********************************************************/
/*************************************************************************
 Function Name - SerialPort
 Parameter(s)  - void
 Return Type   - void
 Action        - Creates the object(s).
 *************************************************************************/
SerialPort::SerialPort()
{
    _qDebug("Inside Serialport Constructor.");

    serialPort = new QextSerialPort(QextSerialPort::EventDriven);
    timerObj = new Timer;
}

/*************************************************************************
 Function Name - ~SerialPort
 Parameter(s)  - void
 Return Type   - void
 Action        - Destroys the object.
 *************************************************************************/
SerialPort::~SerialPort()
{
    _qDebug("Inside Serialport Destructor.");

    clearmem(serialPort);
    clearmem(timerObj);
}

/*************************************************************************
 Function Name - getAvailabePorts
 Parameter(s)  - QStringList&
 Return Type   - bool
 Action        - This function searches the available COM ports in the system
                 and stores in given buffer named availablePorts and returns
                 a boolean status.
 *************************************************************************/
bool SerialPort::getAvailabePorts(QStringList &availablePorts)
{
    _qDebug("Inside bool SerialPort::getAvailabePorts(QStringList &availablePorts).");

    bool ret = true;

    availablePorts.clear();

    foreach(const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
    {
        if(info.portName() == "")
        {
            ret = false;
            break;
        }
        else
        {
            availablePorts.push_back(info.portName());
        }
    }

    return  ret;
}

/*************************************************************************
 Function Name - setPortData
 Parameter(s)  - QByteArray, QByteArray, int, int, int, int, int, long, int
 Return Type   - void
 Action        - This function sets port data.
 *************************************************************************/
void SerialPort::setPortData(QByteArray txData,QByteArray matchData,int BaudRate,int DataBits,
                             int Parity,int StopBits,int FlowControl,long Timeout_Millisec,int COMPARE_FLAG)
{
    _qDebug("Inside void SerialPort::setPortData(QByteArray txData,QByteArray matchData,int BaudRate,int DataBits,\
            int Parity,int StopBits,int FlowControl,long Timeout_Millisec,int COMPARE_FLAG).");

    portData.txData = txData;
    portData.matchData = matchData;
    portData.BaudRate = static_cast<BaudRateType>(BaudRate);
    portData.DataBits = static_cast<DataBitsType>(DataBits);
    portData.Parity = static_cast<ParityType>(Parity);
    portData.StopBits = static_cast<StopBitsType>(StopBits);
    portData.FlowControl = static_cast<FlowType>(FlowControl);
    portData.Timeout_Millisec = Timeout_Millisec;
    portData.COMPARE_FLAG = COMPARE_FLAG;
}

/*************************************************************************
 Function Name - setPortData
 Parameter(s)  - QByteArray, QByteArray, int, int, int, int, int, long, int
 Return Type   - void
 Action        - This function sets port data.
 *************************************************************************/
void SerialPort::printSettings()
{
    qDebug()<<"Transmit Data:["<<portData.txData<<"].\n"\
            <<"Match Data:["<<portData.matchData<<"].\n"\
            <<"BaudRate:["<<portData.BaudRate<<"].\n"\
            <<"DataBits:["<<portData.DataBits<<"].\n"\
            <<"Parity:["<<portData.Parity<<"].\n"\
            <<"StopBits:["<<portData.StopBits<<"].\n"\
            <<"FlowControl:["<<portData.FlowControl<<"].\n"\
            <<"Timeout_Millisec:["<<portData.Timeout_Millisec<<"].\n"\
            <<"COMPARE_FLAG:["<<portData.COMPARE_FLAG<<"].\n";
}

/*************************************************************************
 Function Name - connectPort
 Parameter(s)  - QString,BaudRateType,DataBitsType,ParityType,StopBitsType,
                 FlowType,long
 Return Type   - bool
 Action        - This function connects/opens the comport with given portName
                 and with the given settings and returns the status.
 *************************************************************************/
bool SerialPort::connect(QString portName,BaudRateType BaudRate,DataBitsType DataBits,
                             ParityType Parity,StopBitsType StopBits,FlowType FlowControl,
                             long Timeout_Millisec)
{
    _qDebug("Inside bool bool SerialPort::connectPort(QString portName,BaudRateType BaudRate,DataBitsType DataBits,ParityType Parity,StopBitsType StopBits,FlowType FlowControl,long Timeout_Millisec).");

    bool ret = true;

    if(serialPort == nullptr)
    {
        ret = false;
    }
    else
    {
        serialPort->setPortName(portName);
        serialPort->setBaudRate(BaudRate);
        serialPort->setDataBits(DataBits);
        serialPort->setParity(Parity);
        serialPort->setStopBits(StopBits);
        serialPort->setFlowControl(FlowControl);
        serialPort->setTimeout(Timeout_Millisec);
        serialPort->setQueryMode(QextSerialPort::Polling);

        if(serialPort->open(QIODevice::ReadWrite | QIODevice::Unbuffered))
        {
            _qDebugE("Seriar port ",portName," opened successfully .","");

            //discarding garbage bytes
            if(serialPort->bytesAvailable())
            {
                serialPort->readAll();
            }

            serialPort->flush();
        }
        else {
            ret = false;
        }
    }

    return ret;
}

/*************************************************************************
 Function Name - connect
 Parameter(s)  - QByteArray, QByteArray,BaudRateType,DataBitsType,ParityType,
                 StopBitsType,FlowType,long
 Return Type   - bool
 Action        - This function searches all available ports and tries to
                 connect/open each port with given settings. After port open
                 it writes txData and reads response. Then it matches response
                 data with matchData and if successfull then currently opened
                 port is selected as device port. This function returns false
                 if any error occurrs in detecting port or else returns true.
 *************************************************************************/
bool SerialPort::connect(QByteArray txData,QByteArray matchData,BaudRateType BaudRate,DataBitsType DataBits,
                             ParityType Parity,StopBitsType StopBits,FlowType FlowControl,
                             long Timeout_Millisec,int COMPARE_FLAG)
{
    _qDebug("Inside bool bool SerialPort::connectPort(QByteArray txData,QByteArray matchData,BaudRateType BaudRate,\
             DataBitsType DataBits,ParityType Parity,StopBitsType StopBits,FlowType FlowControl,long Timeout_Millisec,\
             int COMPARE_FLAG).");

    bool ret = false;
    bool isFound = false;
    QStringList availablePorts;
    QByteArray rxData;

    //Create and show a message box to show data processing
    QMessageBox* Msg_Buffer_DB = new QMessageBox;

    Msg_Buffer_DB->setWindowTitle("TMR_TRB");
    Msg_Buffer_DB->setText("Please wait!!\nDetecting Port...");
    Msg_Buffer_DB->setWindowFlags(Qt::CustomizeWindowHint);
    Msg_Buffer_DB->setStandardButtons(nullptr);
    Msg_Buffer_DB->show();
    timerObj->delayInms(1);

    availablePorts.clear();
    ret = getAvailabePorts(availablePorts);

    if(!ret || (serialPort == nullptr))
    {
        _qDebugE("Oops No Ports available.","","","");
    }
    else
    {
        serialPort->setBaudRate(BaudRate);
        serialPort->setDataBits(DataBits);
        serialPort->setParity(Parity);
        serialPort->setStopBits(StopBits);
        serialPort->setFlowControl(FlowControl);
        serialPort->setTimeout(Timeout_Millisec);
        serialPort->setQueryMode(QextSerialPort::Polling);

        foreach(const QString &portName, availablePorts)
        {
            serialPort->setPortName(portName);

            if(serialPort->isOpen())
            {
                _qDebugE("Oops! Seriar port ",portName," is already open.","");
            }
            else
            {
                if(serialPort->open(QIODevice::ReadWrite | QIODevice::Unbuffered))
                {
                    _qDebugE("Seriar port ",portName," opened successfully .","");

                    //discarding garbage bytes
                    while(serialPort->bytesAvailable())
                    {
                        serialPort->readAll();
                    }

                    serialPort->flush();

                    write(txData);
                    timerObj->delayInms(800);

                    rxData.clear();
                    rxData = serialPort->readAll();

                    if(rxData.length() < 1)
                    {
                        //Retrying...
                        write(txData);
                        timerObj->delayInms(800);

                        rxData.clear();
                        rxData = readAll();
                    }
                    _qDebugE("rxData:[",rxData,"]","");
                    _qDebugE("matchData:[",matchData,"]","");

                    if(COMPARE_FLAG == WHOLE_WORDS)
                    {
                        if((rxData.length() > 0) && rxData.contains(matchData) && (rxData.length() == matchData.length()))
                        {
                            isFound = true;
                        }
                    }
                    else if(COMPARE_FLAG == ONLY_WORDS)
                    {
                        if((rxData.length() > 0) && rxData.contains(matchData))
                        {
                            isFound = true;
                        }
                    }
                    if(!isFound) serialPort->close();
                }
            }
            if(isFound) break;
        }//foreach loop for number of available PORTs
    }

    clearmem(Msg_Buffer_DB);

    return isFound;
}

/*************************************************************************
 Function Name - connect
 Parameter(s)  - void
 Return Type   - bool
 Action        - This function searches all available ports and tries to
                 connect/open each port with given settings. After port open
                 it writes txData and reads response. Then it matches response
                 data with matchData and if successfull then currently opened
                 port is selected as device port. This function returns false
                 if any error occurrs in detecting port or else returns true.
                 Note: Before calling this function, Port data must be set
                 by calling setPortData Function.
 *************************************************************************/
bool SerialPort::connect()
{
    _qDebug("Inside bool SerialPort::connect().");

    bool ret = false;
    bool isFound = false;
    QStringList availablePorts;
    QByteArray rxData;

    availablePorts.clear();
    ret = getAvailabePorts(availablePorts);
    _qDebugE("availablePorts:[",availablePorts,"]","");

    if(!ret || (serialPort == nullptr))
    {
        _qDebugE("Oops No Ports available.","","","");
    }
    else
    {
        serialPort->setBaudRate(portData.BaudRate);
        serialPort->setDataBits(portData.DataBits);
        serialPort->setParity(portData.Parity);
        serialPort->setStopBits(portData.StopBits);
        serialPort->setFlowControl(portData.FlowControl);
        serialPort->setTimeout(portData.Timeout_Millisec);
        serialPort->setQueryMode(QextSerialPort::Polling);

        foreach(const QString &portName, availablePorts)
        {
            serialPort->setPortName(portName);

            if(serialPort->isOpen())
            {
                _qDebugE("Oops! Seriar port ",portName," is already open.","");
            }
            else
            {
                if(serialPort->open(QIODevice::ReadWrite | QIODevice::Unbuffered))
                {
                    _qDebugE("Seriar port ",portName," opened successfully .","");

                    //discarding garbage bytes
                    while(serialPort->bytesAvailable())
                    {
                        serialPort->readAll();
                    }

                    serialPort->flush();

                    write(portData.txData);
                    timerObj->delayInms(800);

                    rxData.clear();
                    rxData = readAll();

                    if(rxData.length() < 1)
                    {
                        //Retrying...
                        write(portData.txData);
                        timerObj->delayInms(800);

                        rxData.clear();
                        rxData = readAll();
                    }

                    _qDebugE("rxData:[",rxData,"]","");
                    _qDebugE("matchData:[",portData.matchData,"]","");

                    if(portData.COMPARE_FLAG == WHOLE_WORDS)
                    {
                        if((rxData.length() > 0) && rxData.contains(portData.matchData) && (rxData.length() == portData.matchData.length()))
                        {
                            isFound = true;
                        }
                    }
                    else if(portData.COMPARE_FLAG == ONLY_WORDS)
                    {
                        if((rxData.length() > 0) && rxData.contains(portData.matchData))
                        {
                            isFound = true;
                        }
                    }
                    if(!isFound) serialPort->close();
                }
            }
            if(isFound) break;
        }//foreach loop for number of available PORTs
    }

    return isFound;
}

/*************************************************************************
 Function Name - connect
 Parameter(s)  - QStringList
 Return Type   - bool
 Action        - This function searches all available ports and tries to
                 connect/open the port which is not listed in given discardPorts
                 with given settings. After port open it writes txData and reads
                 response. Then it matches response data with matchData and if
                 successfull then currently opened port is selected as device port.
                 This function returns false if any error occurrs in detecting port
                 or else returns true.
                 Note: Before calling this function, Port data must be set
                 by calling setPortData Function.
 *************************************************************************/
bool SerialPort::connect(QStringList discardPortList)
{
    _qDebug("Inside bool SerialPort::connect(QStringList discardPortList).");

    bool ret = false;
    bool isFound = false;
    QStringList availablePorts;
    QByteArray rxData;

    availablePorts.clear();
    ret = getAvailabePorts(availablePorts);
    _qDebugE("availablePorts:[",availablePorts,"]","");
    for(int i = 0;i < discardPortList.count();i++)
    {
        availablePorts.removeOne(discardPortList[i]);
    }
    _qDebugE("availablePorts After Discard:[",availablePorts,"]","");

    if(!ret || (serialPort == nullptr))
    {
        _qDebugE("Oops No Ports available.","","","");
    }
    else
    {
        serialPort->setBaudRate(portData.BaudRate);
        serialPort->setDataBits(portData.DataBits);
        serialPort->setParity(portData.Parity);
        serialPort->setStopBits(portData.StopBits);
        serialPort->setFlowControl(portData.FlowControl);
        serialPort->setTimeout(portData.Timeout_Millisec);
        serialPort->setQueryMode(QextSerialPort::Polling);

        foreach(const QString &portName, availablePorts)
        {
            serialPort->setPortName(portName);

            if(serialPort->isOpen())
            {
                _qDebugE("Oops! Seriar port ",portName," is already open.","");
            }
            else
            {
                if(serialPort->open(QIODevice::ReadWrite | QIODevice::Unbuffered))
                {
                    _qDebugE("Seriar port ",portName," opened successfully .","");

                    //discarding garbage bytes
                    while(serialPort->bytesAvailable())
                    {
                        serialPort->readAll();
                    }

                    serialPort->flush();

                    write(portData.txData);
                    timerObj->delayInms(800);

                    rxData.clear();
                    rxData = readAll();

                    if(rxData.length() < 1)
                    {
                        //Retrying...
                        write(portData.txData);
                        timerObj->delayInms(800);

                        rxData.clear();
                        rxData = readAll();
                    }

                    _qDebugE("rxData:[",rxData,"]","");
                    _qDebugE("matchData:[",portData.matchData,"]","");

                    if(portData.COMPARE_FLAG == WHOLE_WORDS)
                    {
                        if((rxData.length() > 0) && rxData.contains(portData.matchData) && (rxData.length() == portData.matchData.length()))
                        {
                            isFound = true;
                        }
                    }
                    else if(portData.COMPARE_FLAG == ONLY_WORDS)
                    {
                        if((rxData.length() > 0) && rxData.contains(portData.matchData))
                        {
                            isFound = true;
                        }
                    }
                    if(!isFound) serialPort->close();
                }
            }
            if(isFound) break;
        }//foreach loop for number of available PORTs
    }

    return isFound;
}

/*************************************************************************
 Function Name - connect
 Parameter(s)  - QString
 Return Type   - bool
 Action        - This function connects/opens the comport with given portName
                 and with the preset settings and returns the status.
                 Note: Before calling this function, Port data must be set
                 by calling setPortData Function.
 *************************************************************************/
bool SerialPort::connect(QString portName)
{
    _qDebug("Inside bool SerialPort::connect(QString portName).");

    bool ret = true;

    if(serialPort == nullptr)
    {
        ret = false;
    }
    else
    {
        serialPort->setPortName(portName);
        serialPort->setBaudRate(portData.BaudRate);
        serialPort->setDataBits(portData.DataBits);
        serialPort->setParity(portData.Parity);
        serialPort->setStopBits(portData.StopBits);
        serialPort->setFlowControl(portData.FlowControl);
        serialPort->setTimeout(portData.Timeout_Millisec);
        serialPort->setQueryMode(QextSerialPort::Polling);

        if(serialPort->open(QIODevice::ReadWrite | QIODevice::Unbuffered))
        {
            _qDebugE("Seriar port ",portName," opened successfully .","");

            //discarding garbage bytes
            if(serialPort->bytesAvailable())
            {
                serialPort->readAll();
            }

            serialPort->flush();
        }
        else {
            ret = false;
        }
    }

    return ret;
}

/*************************************************************************
 Function Name - isOpen
 Parameter(s)  - void
 Return Type   - bool
 Action        - This function returns true if the port is open or else returns
                 false.
 *************************************************************************/
bool SerialPort::isOpen()
{
    _qDebug("Inside bool SerialPort::isOpen().");

    return serialPort->isOpen();
}

/*************************************************************************
 Function Name - closePort
 Parameter(s)  - void
 Return Type   - void
 Action        - This function closes the comport if connected.
 *************************************************************************/
void SerialPort::closePort()
{
    _qDebug("Inside void SerialPort::closePort().");

    if((serialPort != nullptr) && serialPort->isOpen())
    {
        serialPort->close();
    }
}

/*************************************************************************
 Function Name - read
 Parameter(s)  - uint
 Return Type   - QByteArray
 Action        - This function reads the maximum noOfBytes from the serialport
                 and returns the read buffer.
 *************************************************************************/
QByteArray SerialPort::read(uint noOfBytes)
{
    _qDebug("Inside QByteArray SerialPort::read(uint noOfBytes).");

    QByteArray byte;
    byte.clear();

    ushort count = 0;
    if(serialPort != nullptr)
    {
        for(uint bytes = 1;bytes <= noOfBytes;bytes++)
        {
            while(!serialPort->bytesAvailable() && (count ++ < 500)) timerObj->delayInms(10);//Maximum 5 Second Delay
            byte.append(serialPort->read(1));
        }
    }
    return byte;
}

/*************************************************************************
 Function Name - readAll
 Parameter(s)  - void
 Return Type   - QByteArray
 Action        - This function reads all bytes available in the serialport
                 and returns the read buffer.
 *************************************************************************/
QByteArray SerialPort::readAll()
{
    _qDebug("Inside QByteArray SerialPort::readAll().");
    mutex_1.lock();
    QByteArray byte;
    byte.clear();
    ushort count = 0;

    if(serialPort != nullptr)
    {
        while(!serialPort->bytesAvailable() && (count ++ < 500)) timerObj->delayInms(10);//Maximum 5 Second Delay, checking for only first byte

        while (serialPort->bytesAvailable()) {
            byte.append(serialPort->read(1));

            if(!serialPort->bytesAvailable()) timerObj->delayInms(100);
        }
    }
    mutex_1.unlock();
    return byte;
}

/*************************************************************************
 Function Name - write
 Parameter(s)  - QByteArray&
 Return Type   - bool
 Action        - This function writes the given buffer "data" to the serialport.
 *************************************************************************/
bool SerialPort::write(QByteArray& dataToWrite)
{
    _qDebug("Inside bool SerialPort::write(QByteArray& dataToWrite).");
    mutex.lock();
    qint64 bytesWritten;
    bool ret = true;

    if(serialPort == nullptr)
    {
        ret = false;
    }
    else
    {
        bytesWritten = serialPort->write(dataToWrite);

        if(bytesWritten == -1) {
            ret = false;
        }
    }
    mutex.unlock();
    return ret;
}

/*************************************************************************
 Function Name - write
 Parameter(s)  - QString&
 Return Type   - bool
 Action        - This function writes the given buffer "data" to the serialport.
 *************************************************************************/
bool SerialPort::write(QString& dataToWrite)
{
    _qDebug("Inside bool SerialPort::write(QString& dataToWrite).");
    mutex.lock();
    qint64 bytesWritten;
    bool ret = true;
    QByteArray data;
    data.clear();

    if(serialPort == nullptr)
    {
        ret = false;
    }
    else
    {
        for(int i=0; i<dataToWrite.length(); i++)
        {
            data.append(dataToWrite.at(i));
        }

        bytesWritten = serialPort->write(data);

        if(bytesWritten == -1) {
            ret = false;
        }
    }
    mutex.unlock();
    return ret;
}

/*************************************************************************
 Function Name - getPortName
 Parameter(s)  - void
 Return Type   - QString
 Action        - returns serial port name.
 *************************************************************************/
QString SerialPort::getPortName()
{
    _qDebug("Inside QString SerialPort::getPortName().");

    QString portName;

    portName.clear();
    if(serialPort != nullptr)
    {
        portName = serialPort->portName();
    }
    return  portName;
}
