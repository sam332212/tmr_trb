/*************************************  FILE HEADER  *****************************************************************
*
*  File name -SerialPort.h
*  Creation date and time - 14-SEP-2019 , SAT 07:00 AM IST
*  Author: - Manoj
*  Purpose of the file - All RS232-communication modules will be declared .
*
**********************************************************************************************************************/
#ifndef SERIALPORT_H
#define SERIALPORT_H

/********************************************************
 Inclusion of header file
 ********************************************************/
#include <qextserialport.h>
#include <QSerialPortInfo>
#include <QMessageBox>
#include <QDebug>
#include <QMutex>

#include "Support/Timer.h"

/********************************************************
 Class Declaration
 ********************************************************/
class SerialPort
{
public:
    enum{
        WHOLE_WORDS = 0,ONLY_WORDS
    };
    /*  NOTE: the above enum is used in the "connect" function.
     *  WHOLE_WORDS=> the received data from PORT will me completely matched wih "matchData"
     *  ONLY_WORDS=> the "matchData" will be searched in the received data from PORT
     */
    enum{
        BAUDRATE2400 = 2400,
        BAUDRATE4800 = 4800,
        BAUDRATE9600 = 9600,
        BAUDRATE19200 = 19200,
        BAUDRATE38400 = 38400,
        BAUDRATE57600 = 57600,
        BAUDRATE115200 = 115200
    };
    enum{
        DATABITS_5 = 5,
        DATABITS_6 = 6,
        DATABITS_7 = 7,
        DATABITS_8 = 8
    };
    enum{
        PARITY_NONE,
        PARITY_ODD,
        PARITY_EVEN,
        PARITY_MARK,
        PARITY_SPACE
    };
    enum{
        STOPBIT_1,
        STOPBIT_1_5,
        STOPBIT_2
    };
    enum{
        FLOWCONTROL_OFF,
        FLOWCONTROL_HARDWARE,
        FLOWCONTROL_XONXOFF
    };

protected:
    //member functions
    SerialPort();
    ~SerialPort();

    void closePort();
    void setPortData(QByteArray txData,QByteArray matchData,int BaudRate,int DataBits,int Parity,
                     int StopBits,int FlowControl,long Timeout_Millisec,int COMPARE_FLAG);
    void printSettings();

    bool getAvailabePorts(QStringList &availablePorts);
    bool connect(QString portName,BaudRateType BaudRate,DataBitsType DataBits,ParityType Parity,
                     StopBitsType StopBits,FlowType FlowControl,long Timeout_Millisec);
    bool connect(QByteArray txData,QByteArray matchData,BaudRateType BaudRate,DataBitsType DataBits,
                     ParityType Parity,StopBitsType StopBits,FlowType FlowControl,long Timeout_Millisec,
                     int COMPARE_FLAG);
    bool connect();
    bool connect(QStringList discardPortList);
    bool connect(QString portName);    

    bool write(QByteArray& dataToWrite);
    bool write(QString& dataToWrite);
    bool isOpen();

    QByteArray read(uint noOfBytes);
    QByteArray readAll();
    QString getPortName();

private:
    //member variables
    QMutex mutex;
    QMutex mutex_1;
    QextSerialPort* serialPort = nullptr;
    Timer* timerObj = nullptr;

    typedef struct portData
    {
        QByteArray txData = "";
        QByteArray matchData = "";
        BaudRateType BaudRate;
        DataBitsType DataBits;
        ParityType Parity;
        StopBitsType StopBits;
        FlowType FlowControl;
        long Timeout_Millisec;
        int COMPARE_FLAG;
    }PORT_DATA;
    PORT_DATA portData;
};

#endif // SERIALPORT_H
