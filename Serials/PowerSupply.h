/*************************************  FILE HEADER  *****************************************************************
*
*  File name - PowerSupply.h
*  Creation date and time - 17-MAR-2020 , TUE 10:00 AM IST
*  Author: - Manoj
*  Purpose of the file - All Power Supply Handling modules will be declared .
*
**********************************************************************************************************************/
#ifndef POWERSUPPLY_H
#define POWERSUPPLY_H

/********************************************************
 Inclusion of header file
 ********************************************************/
#include "SerialPort.h"
#include "Support/Database.h"
#include "Support/Utility.h"
#include "Support/Timer.h"

/********************************************************
 Class Declaration
 ********************************************************/
class PowerSupply : public SerialPort,Utility
{
public:
    struct psDetails{
        QString measuredVoltage;
        QString programmedVoltage;
        QString measuredCurrent;
        QString programmedCurrent;
        QString overVoltageSetPoint;
        QString underVoltageSetPoint;
    };
    struct psDetails _psDetails;

    //member functions
    PowerSupply();
    ~PowerSupply();
    void disconnectPS();
    void setDB(Database* database);
    void setDBTableName(QString tableName);
    void setRowId(int rowId);
    void setPortSettings(QByteArray txData,QByteArray matchData,int BaudRate,int DataBits,
                              int Parity,int StopBits,int FlowControl,long Timeout_Millisec,int COMPARE_FLAG);
    void showPortSettings();

    bool resetPS(uint8_t address);
    bool resetAll();
    bool connectPS(QStringList discardPorts,QVector<int> psAddress,QVector<bool>& psStatus);
    bool isConnected(QVector<int> psAddress,QVector<bool>& psStatus);
    bool sendCommand(QByteArray cmd);
    bool selectDevice(uint8_t address);
    bool setVoltage(uint8_t address,double value);
    bool setVoltage(double value);
    bool setCurrent(uint8_t address,double value);
    bool setCurrent(double value);
    bool setOutputOn(uint8_t address);
    bool setOutputOn(QVector<int> address);
    bool setOutputOn();
    bool setOutputOff(uint8_t address);
    bool setOutputOff(QVector<int> address);
    bool setOutputOff();
    bool setAllOutputOn();
    bool setAllOutputOff();
    bool isOutputOn(uint8_t address);
    bool isOutputOn();
    bool getRemoteMode(uint8_t address,uint8_t* mode);
    bool setLocalMode(uint8_t address);
    bool setLocalMode();
    bool setRemoteMode(uint8_t address);
    bool setRemoteMode();
    bool setLocalLockoutMode(uint8_t address);
    bool setLocalLockoutMode();
    bool repeatLastCommand(uint8_t address);
    bool getProgrammedVoltage(uint8_t address,double& value);
    bool getMeasuredVoltage(uint8_t address,double& value);
    bool getProgrammedCurrent(uint8_t address,double& value);
    bool getMeasuredCurrent(uint8_t address,double& value);
    bool runSelfTest(uint8_t address);

    void getPsData(uint8_t address);
    void getPsData();

    QString getErrorLog() const;
    void setErrorLog(const QString &value);

    QString getSerialNumber(uint8_t address);
    QByteArray getResponse();
    QString getPort();

    //member variables
    QByteArray txCommand = "ADR 1";
    QByteArray psCommand = "ADR ";
    QByteArray psResponse = "OK";

    static bool connectionStatus;
    static bool isBusy;

private:
    Timer* timerObj = nullptr;

    Database *psDB = nullptr;
    Utility *utilObj = nullptr;
    QString portName = "";
    QString dbTableName = "";
    int rowID = 0;
    bool portSet = false;
    QString errorLog;
};

#endif // POWERSUPPLY_H
