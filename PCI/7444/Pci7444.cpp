/*************************************  FILE HEADER  *****************************************************************
*
*  File name - Pci7444.cpp
*  Creation date and time - 25-MAY-2020 , MON 8:40 AM IST
*  Author: - Manoj
*  Purpose of the file - All PCI 7444 modules will be implemented .
*
**********************************************************************************************************************/
/********************************************************
 Inclusion of header file
 ********************************************************/
#include "Pci7444.h"
#include "Support/debugflag.h"
#include <QDebug>

/********************************************************
 MACRO Definition
 ********************************************************/
#if PCI7444_DEBUG
    #define _qDebug(s) qDebug()<<"\n"<<s
#else
    #define _qDebug(s)
#endif

#if PCI7444_DEBUG_E
    #define _qDebugE(p,q,r,s) qDebug()<<p<<q<<r<<s
#else
    #define _qDebugE(p,q,r,s);
#endif

/********************************************************
 Function Definition
 ********************************************************/
/*************************************************************************
 Function Name - Pci7444
 Parameter(s)  - void
 Return Type   - void
 Action        - Creates the object.
 *************************************************************************/
Pci7444::Pci7444()
{
    _qDebug("Inside Pci7444 Constructor.");
    boardIdNumbers.clear();
    cardNumbers.clear();
}

/*************************************************************************
 Function Name - ~Pci7444
 Parameter(s)  - void
 Return Type   - void
 Action        - Destroys the object.
 *************************************************************************/
Pci7444::~Pci7444()
{
    _qDebug("Inside Pci7444 Destructor.");
}

/*************************************************************************
 Function Name - init7444Cards
 Parameter(s)  - void
 Return Type   - int
 Action        - Initializes/Registers the PCI DOP and DIP cards with the
                 corresponding cardNumber. If success returns 1 and if fails
                 returns 0.
 *************************************************************************/
int Pci7444::init7444Cards()
{
    qDebug("Inside int int Pci7444::init7444Cards().");
    I16 cardNo;
    int ret = 1;
    QString str;
    errorLog.clear();

    if(boardIdNumbers.count() == 0)
    {
        _qDebugE("7444 Cards are Empty.","","","");
    }
    else
    {
        foreach(U16 bidNo,boardIdNumbers)
        {
            cardNo = Register_Card(PCI_7444, bidNo);
            _qDebugE("Registered Card Number(7444):",cardNo,"","");

            if(cardNo < 0) {
                str = QString("PCI7444: Card BID [%1] initialization failed with Errorcode[%2].").arg(bidNo).arg(cardNo);
                _qDebugE(str,"","","");
                setErrorLog(str);
                ret = 0;//Error
            }
            else {
                cardNumbers.push_back(cardNo);
            }
        }
    }

    return ret;
}

/*************************************************************************
 Function Name - isValidCard
 Parameter(s)  - uint8_t
 Return Type   - bool
 Action        - If cardNo is registered returns true else returns false.
 *************************************************************************/
bool Pci7444::isValidCard(uint8_t cardNo)
{
    for(uint8_t card : cardNumbers) {
        if(card == cardNo) return true;
    }
    return false;
}

/*************************************************************************
 Function Name - readDop
 Parameter(s)  - uint32_t, uint8_t*
 Return Type   - int
 Action        - Reads the given DOP to state. If success returns 1 else returns Error code.
 *************************************************************************/
int Pci7444::readDop(uint32_t dopNo,uint8_t *state)
{
    _qDebug("Inside int Pci7444::readDop(uint32_t dopNo,uint8_t *state).");
    _qDebugE("ReadDop:dopNumber = ",dopNo," ","");
    I16 errorCode = 0;
    U16 portNumber, pinNumber;
    uint8_t cardNumber;
    uint8_t dopNumber;
    QString str;
    errorLog.clear();
    U16 status = 0;

    if(dopNo > totalDops) {
        str = QString("PCI7444:Invalid DOP number [%1], expected <%2.").arg(dopNo).arg(totalDops);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_DOP_NUMBER;
    }

    cardNumber = (dopNo / 128);
    dopNumber  = (dopNo % 128);
    portNumber = (dopNumber / 32);
    pinNumber  = (dopNumber % 32);
    _qDebugE("cardNumber = ",cardNumber,", dopNumber  = ",dopNumber);
    _qDebugE("portNumber  = ",portNumber,", pinNumber = ",pinNumber);

    if(isValidCard(cardNumber) == true) {
        errorCode = DO_ReadLine(static_cast<U16>(cardNumber),portNumber,pinNumber,&status);
        if(errorCode != 0) {
            str = QString("PCI7444: Reading DOP[%1] failed with Errorcode[%2].").arg(dopNo).arg(errorCode);
            _qDebugE(str,"","","");
            setErrorLog(str);
            return errorCode;
         }
        else {
            *state = static_cast<uint8_t>(status);
        }
    }
    else {
        str = QString("PCI7444: Unregistered Card number [%1].").arg(cardNumber);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_CARD_NUMBER;
    }

    return SUCCESS;
}
/*************************************************************************
 Function Name - readDop
 Parameter(s)  - uint8_t, uint8_t, uint8_t*
 Return Type   - int
 Action        - Reads the given DOP. If success returns 1 else returns Error code.
 *************************************************************************/
int Pci7444::readDop(uint8_t cardNumber,uint8_t dopNumber,uint8_t *state)
{
    _qDebug("Inside int Pci7444::readDop(uint8_t cardNo,uint8_t dopNumber,uint8_t *state).");
    _qDebugE("ReadDop:cardNumber = ",cardNumber,", dopNumber =",dopNumber);
    I16 errorCode = 0;
    U16 portNumber, pinNumber;
    QString str;
    errorLog.clear();
    U16 status = 0;

    if(dopNumber > 128) {
        str = QString("PCI7444:Invalid DOP number [%1], expected <128.").arg(dopNumber);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_DOP_NUMBER;
    }

    portNumber = (dopNumber / 32);
    pinNumber  = (dopNumber % 32);
    _qDebugE("dopNumber = ",dopNumber,"","");
    _qDebugE("portNumber  = ",portNumber,", pinNumber = ",pinNumber);

    if(isValidCard(cardNumber) == true) {
        errorCode = DO_ReadLine(static_cast<U16>(cardNumber),portNumber,pinNumber,&status);
        if(errorCode != 0) {
            str = QString("PCI7444: Reading DOP[%1] failed with Errorcode[%2].").arg(dopNumber).arg(errorCode);
            _qDebugE(str,"","","");
            setErrorLog(str);
            return errorCode;
         }
        else {
            *state = static_cast<uint8_t>(status);
        }
    }
    else {
        str = QString("PCI7444: Unregistered Card number [%1].").arg(cardNumber);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_CARD_NUMBER;
    }

    return SUCCESS;
}

/*************************************************************************
 Function Name - writeDop
 Parameter(s)  - int32, uint8*
 Return Type   - int
 Action        - Writes the OnOff value to the given DOP. Reads back the DOP
                 value. If write success or DOP value is same as OnOff then
                 returns 1 else returns Error code.
 *************************************************************************/
int Pci7444::writeDop(uint32_t dopNo,uint8_t OnOff)//OnOff:1=>ON,0=>OFF
{
    _qDebug("Inside int Pci7444::writeDop(uint32_t dopNo,uint8_t OnOff).");
    _qDebugE("write:dopNo = ",dopNo,", OnOff  = ",OnOff);
    I16 errorCode = 0;
    U16 portNumber, pinNumber;
    uint8_t state;
    uint8_t cardNumber;
    uint8_t dopNumber;
    QString str;
    errorLog.clear();

    if(OnOff > 1) {
        str = QString("PCI7444:Invalid OnOff Byte [%1].").arg(OnOff);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_ONOFF_BYTE;
    }

    if(dopNo >= totalDops) {
        str = QString("PCI7444:Invalid DOP number [%1], expected <%2.").arg(dopNo).arg(totalDops);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_DOP_NUMBER;
    }

    cardNumber = (dopNo / 128);
    dopNumber  = (dopNo % 128);
    portNumber = (dopNumber / 32);
    pinNumber  = (dopNumber % 32);
    _qDebugE("cardNumber = ",cardNumber,", dopNumber  = ",dopNumber);
    _qDebugE("portNumber  = ",portNumber,", pinNumber = ",pinNumber);

    if(isValidCard(cardNumber) == true) {
        errorCode = DO_WriteLine(static_cast<U16>(cardNumber),portNumber,pinNumber,static_cast<U16>(OnOff));
        if(errorCode != 0) {
            str = QString("PCI7444: Writing [%1] to DOP[%2] failed with Errorcode[%3].").arg(OnOff).arg(dopNo).arg(errorCode);
            _qDebugE(str,"","","");
            setErrorLog(str);
            return errorCode;
        }
        else {
            readDop(cardNumber,dopNumber,&state);
            if(state != OnOff) return WRONG_DOP_STATUS;
        }
    }
    else {
        str = QString("PCI7444: Unregistered Card number [%1].").arg(cardNumber);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_CARD_NUMBER;
    }
    return SUCCESS;
}

/*************************************************************************
 Function Name - writeDop
 Parameter(s)  - uint8_t, uint8_t, uint8_t
 Return Type   - int
 Action        - Reads the given DOP. If success returns 1 else returns Error code.
 *************************************************************************/
int Pci7444::writeDop(uint8_t cardNumber, uint8_t dopNumber,uint8_t OnOff)//OnOff:1=>ON,0=>OFF
{
    _qDebug("Inside int Pci7444::writeDop(uint8_t cardNumber, uint8_t dopNo,uint8_t OnOff).");
    _qDebugE("writeDop:cardNumber = ",cardNumber,"","");
    _qDebugE("write:dopNumber = ",dopNumber,", OnOff  = ",OnOff);
    I16 errorCode = 0;
    U16 portNumber, pinNumber;
    uint8_t state;
    QString str;
    errorLog.clear();

    if(OnOff > 1) {
        str = QString("PCI7444:Invalid OnOff Byte [%1].").arg(OnOff);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_ONOFF_BYTE;
    }

    if(dopNumber >= 128) {
        str = QString("PCI7444:Invalid DOP number [%1], expected <128.").arg(dopNumber);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_DOP_NUMBER;
    }

    portNumber = (dopNumber / 32);
    pinNumber  = (dopNumber % 32);
    _qDebugE("portNumber  = ",portNumber,", pinNumber = ",pinNumber);

    if(isValidCard(cardNumber) == true) {
        errorCode = DO_WriteLine(static_cast<U16>(cardNumber),portNumber,pinNumber,static_cast<U16>(OnOff));
        if(errorCode != 0) {
            str = QString("PCI7444: Writing [%1] to DOP[%2] failed with Errorcode[%3].").arg(OnOff).arg(dopNumber).arg(errorCode);
            _qDebugE(str,"","","");
            setErrorLog(str);
            return errorCode;
        }
        else {
            readDop(cardNumber,dopNumber,&state);
            if(state != OnOff) return WRONG_DOP_STATUS;
        }
    }
    else {
        str = QString("PCI7444: Unregistered Card number [%1].").arg(cardNumber);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_CARD_NUMBER;
    }
    return SUCCESS;
}

/*************************************************************************
 Function Name - writePort
 Parameter(s)  - uint8_t, U16, U32
 Return Type   - int
 Action        - Writes the data in the port present in cardNumber .
                 If success returns 1 else returns Error code.
 *************************************************************************/
int Pci7444::writePort(uint8_t cardNumber,U16 port, U32 data){
    I16 errorCode = 0;
    QString str;
    errorLog.clear();

    if(isValidCard(cardNumber) == true) {
        errorCode = DO_WritePort(static_cast<U16>(cardNumber),port,data);
        if(errorCode != 0) {
            str = QString("PCI7444: Writing [%1] to PORT[%2] in card[%3] failed with Errorcode[%4].")
                    .arg(data).arg(port).arg(cardNumber).arg(errorCode);
            _qDebugE(str,"","","");
            setErrorLog(str);
            return errorCode;
        }
    }
    else {
        str = QString("PCI7444: Unregistered Card number [%1].").arg(cardNumber);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_CARD_NUMBER;
    }

    return SUCCESS;
}

/*************************************************************************
 Function Name - readPort
 Parameter(s)  - uint8_t, U16, U32*
 Return Type   - int
 Action        - Reads the data from the port present in cardNumber .
                 If success returns 1 else returns Error code.
 *************************************************************************/
int Pci7444::readPort(uint8_t cardNumber,U16 port, U32* data){
    I16 errorCode = 0;
    QString str;
    errorLog.clear();

    if(isValidCard(cardNumber) == true) {
        errorCode = DO_ReadPort(static_cast<U16>(cardNumber),port,data);
        if(errorCode != 0) {
            str = QString("PCI7444: Reading from PORT[%1] in card[%2] failed with Errorcode[%3].")
                    .arg(port).arg(cardNumber).arg(errorCode);
            _qDebugE(str,"","","");
            setErrorLog(str);
            return errorCode;
        }
    }
    else {
        str = QString("PCI7444: Unregistered Card number [%1].").arg(cardNumber);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_CARD_NUMBER;
    }

    return SUCCESS;
}

/*************************************************************************
 Function Name - resetPCI
 Parameter(s)  - void
 Return Type   - int
 Action        - Writes 0x00 in all ports of all cards. If success returns 1
                 else returns Error code.
 *************************************************************************/
int Pci7444::resetPCI() {
    QString str;

    for(uint8_t card : cardNumbers)
    {
        for(U16 port = 0;port < 4;port++) {
            if(writePort(card,port,0x00) != 1) {
                str = QString("PCI7444: Reseting PORT[%1] in Card[%2] failed with ErrorLog[%3]").arg(port).arg(card).arg(errorLog);
                _qDebugE(str,"","","");
                return 0;
            }
        }
    }
    return 1;
}

void Pci7444::setBoardIdNumbers(const QVector<U16> &value)
{
    boardIdNumbers = value;
}

QVector<U16> Pci7444::getCardNumbers() const
{
    return cardNumbers;
}

QString Pci7444::getErrorLog() const
{
    return errorLog;
}

void Pci7444::setTotalDops(uint32_t value)
{
    totalDops = value;
}

void Pci7444::setErrorLog(const QString &value)
{
    if(errorLog.size() > 0) errorLog.append("\n");
    errorLog.append(value);
}



