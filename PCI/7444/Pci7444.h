/*************************************  FILE HEADER  *****************************************************************
*
*  File name - Pci7444.h
*  Creation date and time - 25-MAY-2020 , MON 8:40 AM IST
*  Author: - Manoj
*  Purpose of the file - All PCI 7444 modules will be declared .
*
**********************************************************************************************************************/
#ifndef PCI7444_H
#define PCI7444_H

/********************************************************
 Inclusion of header file
 ********************************************************/
#include "Dask.h"
#include <QVector>

/********************************************************
 Class Declaration
 ********************************************************/
class Pci7444
{
public:
    Pci7444();
    ~Pci7444();
    int init7444Cards();
    bool isValidCard(uint8_t cardNo);
    int readDop(uint32_t dopNo,uint8_t *state);
    int readDop(uint8_t cardNumber,uint8_t dopNumber,uint8_t *state);
    int writeDop(uint32_t dopNo,uint8_t OnOff);
    int writeDop(uint8_t cardNumber, uint8_t dopNumber,uint8_t OnOff);
    void setBoardIdNumbers(const QVector<U16> &value);
    QVector<U16> getCardNumbers() const;
    QString getErrorLog() const;
    void setTotalDops(uint32_t value);
    int writePort(uint8_t cardNumber,U16 port, U32 data);
    int readPort(uint8_t cardNumber,U16 port, U32* data);
    int resetPCI();

private:
    //member variables
    QVector<U16> cardNumbers;
    QVector<U16> boardIdNumbers;
    QString errorLog;
    uint32_t totalDops;

    void setErrorLog(const QString &value);

private:
    enum errorCode {
        SUCCESS             =  1,
        INVALID_CARD_NUMBER = -1,
        INVALID_DOP_NUMBER  = -2,
        INVALID_ONOFF_BYTE  = -3,
        WRONG_DOP_STATUS    = -4
    };
};

#endif // PCI7444_H
