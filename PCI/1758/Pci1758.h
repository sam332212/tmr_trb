/*************************************  FILE HEADER  *****************************************************************
*
*  File name - Pci1758.h
*  Creation date and time - 25-MAY-2020 , MON 8:33 AM IST
*  Author: - Manoj
*  Purpose of the file - All PCI 1758 modules will be declared .
*
**********************************************************************************************************************/
#ifndef PCI1758_H
#define PCI1758_H

#define CLRDOP clearDopInstant();
#define CLRDIP clearDipInstant();

/********************************************************
 Inclusion of header file
 ********************************************************/
#include "bdaqctrl.h"
using namespace Automation::BDaq;

/********************************************************
 Class Declaration
 ********************************************************/
class Pci1758
{
public:
    //member functions
    Pci1758();
    ~Pci1758();
    void getConnected1758Cards(QList<int>& deviceNumber,QList<QString>& description);
    int init1758Cards();
    int writeDop(int32 dopNo, uint8_t OnOff);
    int writeDop(uint8_t cardNumber, uint8_t dopNumber,uint8_t OnOff);
    int readDop(int32 dopNo, uint8_t *state);
    int readDop(uint8_t cardNumber, uint8_t dopNumber,uint8_t *state);
    int readDip(int32 dipNo, uint8_t *state);
    int readDip(uint8_t cardNumber, uint8_t dipNumber, uint8_t *state);
    void setDopCardDesc(const QStringList &value);
    void setDipCardDesc(const QStringList &value);
    QString getErrorLog() const;
    void setTotalDops(int value);
    void setTotalDips(int value);
    int writePort(uint8_t cardNumber,int32 port, uint8_t data);
    int readPort(uint8_t cardNumber,int32 port, uint8_t& data, bool isDop);
    int resetPCI();

private:
    //member variables
    QVector<InstantDoCtrl*> _pci1758Dop;
    QVector<InstantDiCtrl*> _pci1758Dip;
    QVector<int> _1758DopCardNumbers;
    QVector<int> _1758DipCardNumbers;
    QStringList dopCardDesc;
    QStringList dipCardDesc;
    QString errorLog;
    int totalDops;
    int totalDips;

    //member functions
    void setErrorLog(const QString &value);
    void clearDopInstant();
    void clearDipInstant();
    bool is1758DOPCard(int cardNo);
    bool is1758DIPCard(int cardNo);

private:
    enum errorCode {
        SUCCESS             =  1,
        INVALID_CARD_NUMBER = -1,
        INVALID_DIO_NUMBER  = -2,
        INVALID_ONOFF_BYTE  = -3,
        WRONG_DOP_STATUS    = -4
    };
};

#endif // PCI1758_H
