/*************************************  FILE HEADER  *****************************************************************
*
*  File name - Pci1758.cpp
*  Creation date and time - 25-MAY-2020 , MON 8:33 AM IST
*  Author: - Manoj
*  Purpose of the file - All PCI 1758 modules will be implemented .
*
**********************************************************************************************************************/
/********************************************************
 Inclusion of header file
 ********************************************************/
#include "Pci1758.h"
#include "Support/debugflag.h"
#include <QDebug>

/********************************************************
 MACRO Definition
 ********************************************************/
#if PCI1758_DEBUG
    #define _qDebug(s) qDebug()<<"\n"<<s
#else
    #define _qDebug(s) {}
#endif

#if PCI1758_DEBUG_E
    #define _qDebugE(p,q,r,s) qDebug()<<p<<q<<r<<s
#else
    #define _qDebugE(p,q,r,s); {}
#endif

/********************************************************
 Function Definition
 ********************************************************/
/*************************************************************************
 Function Name - Pci1758
 Parameter(s)  - void
 Return Type   - void
 Action        - Creates the object.
 *************************************************************************/
Pci1758::Pci1758()
{
    _qDebug("Inside Pci1758 Constructor.");
    dopCardDesc.clear();
    dipCardDesc.clear();
}

/*************************************************************************
 Function Name - ~Pci1758
 Parameter(s)  - void
 Return Type   - void
 Action        - Destroys the object.
 *************************************************************************/
Pci1758::~Pci1758()
{
    _qDebug("Inside Pci1758 Destructor.");
}

/*************************************************************************
 Function Name - getConnected1758Cards
 Parameter(s)  - QList<int>&, QList<QString>&
 Return Type   - void
 Action        - Reads connected PCI_1758 cards' number and descriptions and
                 stores in given buffer.
 *************************************************************************/
void Pci1758::getConnected1758Cards(QList<int>& deviceNumber,QList<QString>& description)
{
    _qDebug("Inside void Pci1758::getConnected1758Cards(QList<int>& deviceNumber,QList<QString>& description).");
    InstantDoCtrl* instantDoCtrl = InstantDoCtrl::Create();
    InstantDiCtrl* instantDiCtrl = InstantDiCtrl::Create();

    Array<DeviceTreeNode> *supportedDevices = instantDoCtrl->getSupportedDevices();

    deviceNumber.clear();
    description.clear();
    QString deviceDes = nullptr;

    for (int i = 0; i < supportedDevices->getCount(); i++) {
        DeviceTreeNode const &node = supportedDevices->getItem(i);
        qDebug("%d, %ls\n", node.DeviceNumber, node.Description);

        deviceDes = QString::fromWCharArray(node.Description);

        if(deviceDes.contains("PCI-1758")){
            deviceNumber.push_back(node.DeviceNumber);
            description.push_back(deviceDes);
        }
    }

    supportedDevices->Dispose();
    instantDoCtrl->Dispose();
    supportedDevices = nullptr;
    instantDoCtrl = nullptr;

    supportedDevices = instantDiCtrl->getSupportedDevices();
    for (int i = 0; i < supportedDevices->getCount(); i++) {
        DeviceTreeNode const &node = supportedDevices->getItem(i);
        //qDebug("%d, %ls\n", node.DeviceNumber, node.Description);

        deviceDes = QString::fromWCharArray(node.Description);

        if(deviceDes.contains("PCI-1758")){
            deviceNumber.push_back(node.DeviceNumber);
            description.push_back(deviceDes);
        }
    }
    supportedDevices->Dispose();
    instantDoCtrl->Dispose();
    supportedDevices = nullptr;
    instantDoCtrl = nullptr;

    _qDebugE("Device Number:",deviceNumber," , Description  :",description);
}

/*************************************************************************
 Function Name - clearDopInstant
 Parameter(s)  - void
 Return Type   - void
 Action        - Clears DOP instant buffers.
 *************************************************************************/
void Pci1758::clearDopInstant()
{
    for(int i = 0;i < _pci1758Dop.count();i++)
    {
        if(_pci1758Dop[i] != nullptr) _pci1758Dop[i]->Dispose();
        _pci1758Dop[i] = nullptr;
    }
    _pci1758Dop.clear();
}

/*************************************************************************
 Function Name - clearDipInstant
 Parameter(s)  - void
 Return Type   - void
 Action        - Clears DIP instant buffers.
 *************************************************************************/
void Pci1758::clearDipInstant()
{
    for(int i = 0;i < _pci1758Dip.count();i++)
    {
        if(_pci1758Dip[i] != nullptr) _pci1758Dip[i]->Dispose();
        _pci1758Dip[i] = nullptr;
    }
    _pci1758Dip.clear();
}

/*************************************************************************
 Function Name - init1758Cards
 Parameter(s)  - void
 Return Type   - int
 Action        - Initializes/Registers the PCI DOP and DIP cards with the
                 corresponding descriptions. If success returns 1 and if fails
                 returns 0.
 *************************************************************************/
int Pci1758::init1758Cards()
{
    _qDebug("Inside int Pci1758::init1758DopCards().");

    int ret = 1;
    int cardNo = 0;
    ErrorCode errorCode = Success;
    InstantDoCtrl* instantDOP = nullptr;
    InstantDiCtrl* instantDIP = nullptr;
    QString str;

    _1758DopCardNumbers.clear();
    _1758DipCardNumbers.clear();
    errorLog.clear();

    if(dopCardDesc.count() == 0)
    {
        _qDebugE("DOP Card Descriptions are Empty.","","","");
    }
    else
    {
        CLRDOP
        foreach(QString desc,dopCardDesc)
        {
            instantDOP = InstantDoCtrl::Create();

            if(instantDOP == nullptr)
            {
                str = QString("PCI1758: DOP Instant creation failed for [%1].").arg(desc);
                _qDebugE(str,"","","");
                setErrorLog(str);
                ret = 0;//Error
            }
            else
            {
                DeviceInformation selected(desc.toStdWString().c_str());
                errorCode = instantDOP->setSelectedDevice(selected);

                if(errorCode != 0) {
                    str = QString("Device selection failed for [%1] with Errorcode[%2].").arg(desc).arg(errorCode);
                    _qDebugE(str,"","","");
                    setErrorLog(str);
                    if(instantDOP != nullptr) instantDOP->Dispose();
                    instantDOP = nullptr;
                    ret = 0;//Error
                }
                else {
                    _pci1758Dop.push_back(instantDOP);
                    _1758DopCardNumbers.push_back(cardNo++);
                }
            }
        }

        cardNo = 0;

        if(dipCardDesc.count() == 0)
        {
            _qDebugE("DIP Card Descriptions are Empty.","","","");
        }
        else
        {
            CLRDIP
            foreach(QString desc,dipCardDesc)
            {
                instantDIP = InstantDiCtrl::Create();

                if(instantDIP == nullptr)
                {
                    str = QString("PCI1758: DIP Instant creation failed for [%1].").arg(desc);
                    _qDebugE(str,"","","");
                    setErrorLog(str);
                    ret = 0;//Error
                }
                else
                {
                    DeviceInformation selected(desc.toStdWString().c_str());
                    errorCode = instantDIP->setSelectedDevice(selected);

                    if(errorCode != 0) {
                        str = QString("Device selection failed for [%1] with Errorcode[%2].").arg(desc).arg(errorCode);
                        _qDebugE(str,"","","");
                        setErrorLog(str);
                        if(instantDIP != nullptr) instantDIP->Dispose();
                        instantDIP = nullptr;
                        ret = 0;//Error
                    }
                    else {
                        _pci1758Dip.push_back(instantDIP);
                        _1758DipCardNumbers.push_back(cardNo++);
                    }
                }
            }
        }
    }
    return ret;
}

/*************************************************************************
 Function Name - is1758DOPCard
 Parameter(s)  - int
 Return Type   - bool
 Action        - Checks the presence of given cardNo in the DOP buffer. If
                 found returns true else returns false.
 *************************************************************************/
bool Pci1758::is1758DOPCard(int cardNo)
{
    int i;

    for(i = 0;i < _1758DopCardNumbers.count();i++)
    {
        if(_1758DopCardNumbers[i] == cardNo) return true;
    }
    return false;
}

/*************************************************************************
 Function Name - is1758DIPCard
 Parameter(s)  - int
 Return Type   - bool
 Action        - Checks the presence of given cardNo in the DIP buffer. If
                 found returns true else returns false.
 *************************************************************************/
bool Pci1758::is1758DIPCard(int cardNo)
{
    int i;

    for(i = 0;i < _1758DipCardNumbers.count();i++)
    {
        if(_1758DipCardNumbers[i] == cardNo) return true;
    }
    return false;
}

/*************************************************************************
 Function Name - writeDop
 Parameter(s)  - int32, uint8_t
 Return Type   - int
 Action        - Writes the OnOff value to the given DOP. Reads back the DOP
                 value. If write success or DOP value is same as OnOff then
                 returns 1 else returns Error code.
 *************************************************************************/
int Pci1758::writeDop(int32 dopNo,uint8_t OnOff)
{
    _qDebug("Inside int Pci1758::writeDop(int32 dopNo, uint8_t OnOff).");
    _qDebugE("writeDop:dopNumber = ",dopNo,", OnOff  = ",OnOff);

    uint8_t cardNumber, portNumber, pinNumber;
    ErrorCode errorCode = Success;
    uint8_t state;
    uint8_t dopNumber;
    QString str;
    errorLog.clear();

    if(OnOff > 1) {
        str = QString("PCI1758:Invalid OnOff Byte [%1].").arg(OnOff);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_ONOFF_BYTE;
    }

    if(dopNo >= totalDops) {
        str = QString("PCI1758:Invalid DOP number [%1], expected <%2.").arg(dopNo).arg(totalDops);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_DIO_NUMBER;
    }

    cardNumber = (dopNo / 128);
    dopNumber  = (dopNo % 128);
    portNumber = (dopNumber / 8);
    pinNumber  = (dopNumber % 8);
    _qDebugE("cardNumber = ",cardNumber,", dopNumber  = ",dopNumber);
    _qDebugE("portNumber  = ",portNumber,", pinNumber = ",pinNumber);

    if(is1758DOPCard(cardNumber) == true) {
        errorCode = _pci1758Dop[cardNumber]->WriteBit(portNumber,pinNumber, OnOff);
        if(errorCode != 0) {
            str = QString("PCI1758: Writing [%1] to DOP[%2] failed with Errorcode[%3].").arg(OnOff).arg(dopNo).arg(errorCode);
            _qDebugE(str,"","","");
            setErrorLog(str);
            return static_cast<int>(errorCode);//Error
        }
        else {
            readDop(cardNumber,dopNumber,&state);
            if(state != OnOff) {
                str = QString("PCI1758: Expected DOP value [%1], Actual[%2].").arg(OnOff).arg(state);
                _qDebugE(str,"","","");
                setErrorLog(str);
                return WRONG_DOP_STATUS;
            }
        }
    }
    else {
        str = QString("PCI1758: Unregistered Card number [%1].").arg(cardNumber);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_CARD_NUMBER;
    }
    return SUCCESS;
}

/*************************************************************************
 Function Name - writeDop
 Parameter(s)  - uint8_t, uint8_t, uint8_t
 Return Type   - int
 Action        - Writes the OnOff value to the given DOP. Reads back the DOP
                 value. If write success or DOP value is same as OnOff then
                 returns 1 else returns Error code.
 *************************************************************************/
int Pci1758::writeDop(uint8_t cardNumber, uint8_t dopNumber,uint8_t OnOff)
{
    _qDebug("Inside int Pci1758::writeDop(uint8_t cardNumber, uint8_t dopNumber,uint8_t OnOff).");
    _qDebugE("writeDop:cardNumber = ",cardNumber,"","");
    _qDebugE("writeDop:dopNumber  = ",dopNumber,", OnOff  = ",OnOff);

    uint8_t portNumber, pinNumber;
    ErrorCode errorCode = Success;
    uint8_t state;
    QString str;
    errorLog.clear();

    if(OnOff > 1) {
        str = QString("PCI1758:Invalid OnOff Byte [%1].").arg(OnOff);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_ONOFF_BYTE;
    }

    if(dopNumber >= 128) {
        str = QString("PCI1758:Invalid DOP number [%1], expected <128.").arg(dopNumber);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_DIO_NUMBER;
    }

    portNumber = (dopNumber / 8);
    pinNumber  = (dopNumber % 8);
    _qDebugE("portNumber  = ",portNumber,", pinNumber = ",pinNumber);

    if(is1758DOPCard(cardNumber) == true) {
        errorCode = _pci1758Dop[cardNumber]->WriteBit(portNumber,pinNumber, OnOff);
        if(errorCode != 0) {
            str = QString("PCI1758: Writing [%1] to DOP[%2] failed with Errorcode[%3].").arg(OnOff).arg(dopNumber).arg(errorCode);
            _qDebugE(str,"","","");
            setErrorLog(str);
            return static_cast<int>(errorCode);//Error
        }
        else {
            readDop(cardNumber,dopNumber,&state);
            if(state != OnOff) {
                str = QString("PCI1758: Expected DOP value [%1], Actual[%2].").arg(OnOff).arg(state);
                _qDebugE(str,"","","");
                setErrorLog(str);
                return WRONG_DOP_STATUS;
            }
        }
    }
    else {
        str = QString("PCI1758: Unregistered Card number [%1].").arg(cardNumber);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_CARD_NUMBER;
    }
    return SUCCESS;
}

/*************************************************************************
 Function Name - readDop
 Parameter(s)  - int32, uint8_t*
 Return Type   - int
 Action        - Reads the given DOP to state. If success returns 1 else returns Error code.
 *************************************************************************/
int Pci1758::readDop(int32 dopNo,uint8_t *state)
{
    _qDebug("Inside int Pci1758::readDop(int32 dopNumber,uint8_t *state).");
    _qDebugE("ReadDop:dopNumber = ",dopNo,"","");

    uint8_t cardNumber, portNumber, pinNumber;
    uint8_t dopNumber;
    ErrorCode errorCode = Success;
    QString str;
    errorLog.clear();

    if(dopNo >= totalDops) {
        str = QString("PCI1758:Invalid DOP number [%1], expected <%2.").arg(dopNo).arg(totalDops);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_DIO_NUMBER;
    }

    cardNumber = (dopNo / 128);
    dopNumber  = (dopNo % 128);
    portNumber = (dopNumber / 8);
    pinNumber  = (dopNumber % 8);
    _qDebugE("cardNumber = ",cardNumber,", dopNumber  = ",dopNumber);
    _qDebugE("portNumber  = ",portNumber,", pinNumber = ",pinNumber);

    if(is1758DOPCard(cardNumber) == true) {
        errorCode = _pci1758Dop[cardNumber]->ReadBit(portNumber,pinNumber, state);
        if(errorCode != 0) {
            str = QString("PCI1758: Reading DOP[%1] failed with Errorcode[%2]").arg(dopNo).arg(errorCode);
            _qDebugE(str,"","","");
            setErrorLog(str);
            return static_cast<int>(errorCode);//Error
        }
    }
    else {
        str = QString("PCI1758: Unregistered Card number [%1].").arg(cardNumber);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_CARD_NUMBER;
    }
    return SUCCESS;
}

/*************************************************************************
 Function Name - readDop
 Parameter(s)  - uint8_t, uint8_t, uint8_t*
 Return Type   - int
 Action        - Reads the given DOP to state. If success returns 1 else returns Error code.
 *************************************************************************/
int Pci1758::readDop(uint8_t cardNumber, uint8_t dopNumber,uint8_t *state)
{
    _qDebug("Inside int Pci1758::readDop(uint8_t cardNumber, uint8_t dopNumber,uint8_t *state).");
    _qDebugE("ReadDop:cardNumber = ",cardNumber,", dopNumber =",dopNumber);

    uint8_t portNumber, pinNumber;
    ErrorCode errorCode = Success;
    QString str;
    errorLog.clear();

    if(dopNumber >= 128) {
        str = QString("PCI1758:Invalid DOP number [%1], expected <128.").arg(dopNumber);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_DIO_NUMBER;
    }

    portNumber = (dopNumber / 8);
    pinNumber  = (dopNumber % 8);
    _qDebugE("portNumber  = ",portNumber,", pinNumber = ",pinNumber);

    if(is1758DOPCard(cardNumber) == true) {
        errorCode = _pci1758Dop[cardNumber]->ReadBit(portNumber,pinNumber, state);
        if(errorCode != 0) {
            str = QString("PCI1758: Reading DOP[%1] failed with Errorcode[%2]").arg(dopNumber).arg(errorCode);
            _qDebugE(str,"","","");
            setErrorLog(str);
            return static_cast<int>(errorCode);//Error
        }
    }
    else {
        str = QString("PCI1758: Unregistered Card number [%1].").arg(cardNumber);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_CARD_NUMBER;
    }
    return SUCCESS;
}

/*************************************************************************
 Function Name - readDip
 Parameter(s)  - int32, uint8_t*
 Return Type   - int
 Action        - Reads the given DIP to state. If success returns 1 else returns Error code.
 *************************************************************************/
int Pci1758::readDip(int32 dipNo, uint8_t *state)
{
    _qDebug("Inside int Pci1758::readDip(int32 dipNumber, uint8_t *state).");
    _qDebugE("ReadDip:dipNumber = ",dipNo,"","");
    uint8_t cardNumber,portNumber, pinNumber;
    uint8_t dipNumber;
    ErrorCode errorCode = Success;
    QString str;
    errorLog.clear();

    if(dipNo >= totalDips) {
        str = QString("PCI1758:Invalid DIP number [%1], expected <%2.").arg(dipNo).arg(totalDips);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_DIO_NUMBER;
    }

    cardNumber = (dipNo / 128);
    dipNumber  = (dipNo % 128);
    portNumber = (dipNumber / 8);
    pinNumber  = (dipNumber % 8);
    _qDebugE("cardNumber = ",cardNumber,", dipNumber  = ",dipNumber);
    _qDebugE("portNumber  = ",portNumber,", pinNumber = ",pinNumber);

    if(is1758DIPCard(cardNumber) == true) {
        errorCode = _pci1758Dip[cardNumber]->ReadBit(portNumber,pinNumber, state);
        if(errorCode != 0) {
            str = QString("PCI1758: Reading DIP[%1] failed with Errorcode[%2]").arg(dipNo).arg(errorCode);
            _qDebugE(str,"","","");
            setErrorLog(str);
            return static_cast<int>(errorCode);//Error
        }
        _qDebugE("dipNumber = ",dipNo,", dip value:",*state);
    }
    else {
        str = QString("PCI1758: Unregistered Card number [%1].").arg(cardNumber);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_CARD_NUMBER;
    }
    return SUCCESS;
}

/*************************************************************************
 Function Name - readDip
 Parameter(s)  - int32, uint8_t*
 Return Type   - int
 Action        - Reads the given DIP to state. If success returns 1 else returns Error code.
 *************************************************************************/
int Pci1758::readDip(uint8_t cardNumber, uint8_t dipNumber, uint8_t *state)
{
    _qDebug("Inside int Pci1758::readDip(uint8_t cardNumber, uint8_t dipNumber, uint8_t *state).");
    _qDebugE("ReadDip:cardNumber = ",cardNumber,", dipNumber =",dipNumber);
    uint8_t portNumber, pinNumber;
    ErrorCode errorCode = Success;
    QString str;
    errorLog.clear();

    if(dipNumber >= 128) {
        str = QString("PCI1758:Invalid DIP number [%1], expected <128.").arg(dipNumber);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_DIO_NUMBER;
    }

    portNumber = (dipNumber / 8);
    pinNumber  = (dipNumber % 8);
    _qDebugE("portNumber  = ",portNumber,", pinNumber = ",pinNumber);

    if(is1758DIPCard(cardNumber) == true) {
        errorCode = _pci1758Dip[cardNumber]->ReadBit(portNumber,pinNumber, state);
        if(errorCode != 0) {
            str = QString("PCI1758: Reading DIP[%1] failed with Errorcode[%2]").arg(dipNumber).arg(errorCode);
            _qDebugE(str,"","","");
            setErrorLog(str);
            return static_cast<int>(errorCode);//Error
        }
    }
    else {
        str = QString("PCI1758: Unregistered Card number [%1].").arg(cardNumber);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_CARD_NUMBER;
    }
    return SUCCESS;
}

/*************************************************************************
 Function Name - writePort
 Parameter(s)  - uint8_t, int32, uint8_t
 Return Type   - int
 Action        - Writes the data in the port present in cardNumber .
                 If success returns 1 else returns Error code.
 *************************************************************************/
int Pci1758::writePort(uint8_t cardNumber,int32 port, uint8_t data){
    _qDebug("Inside int Pci1758::writePort(uint8_t cardNumber,int32 port, uint8_t data).");
    ErrorCode errorCode = Success;
    QString str;
    errorLog.clear();

    if(is1758DOPCard(cardNumber) == true) {
        errorCode = _pci1758Dop[cardNumber]->Write(port,data);
        if(errorCode != 0) {
            str = QString("PCI1758: Writing [%1] to PORT[%2] in card[%3] failed with Errorcode[%4].")
                    .arg(data).arg(port).arg(cardNumber).arg(errorCode);
            _qDebugE(str,"","","");
            setErrorLog(str);
            return static_cast<int>(errorCode);//Error
        }
    }
    else {
        str = QString("PCI1758: Unregistered Card number [%1].").arg(cardNumber);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_CARD_NUMBER;
    }

    return SUCCESS;
}

/*************************************************************************
 Function Name - readPort
 Parameter(s)  - uint8_t, int32, uint8_t&, , bool
 Return Type   - int
 Action        - Reads the data from the port present in cardNumber .
                 If success returns 1 else returns Error code.
                 If isDop is true reads DOP card else reads DIP card.
 *************************************************************************/
int Pci1758::readPort(uint8_t cardNumber,int32 port, uint8_t& data, bool isDop){
    _qDebug("Inside int Pci1758::readPort(uint8_t cardNumber,int32 port, uint8_t& data, bool isDop).");
    ErrorCode errorCode = Success;
    QString str;
    errorLog.clear();

    if(isDop && (is1758DOPCard(cardNumber) == true)) {
        errorCode = _pci1758Dop[cardNumber]->Read(port,data);
        if(errorCode != 0) {
            str = QString("PCI1758: Reading DOP from PORT[%1] in card[%2] failed with Errorcode[%3].")
                    .arg(port).arg(cardNumber).arg(errorCode);
            _qDebugE(str,"","","");
            setErrorLog(str);
            return static_cast<int>(errorCode);//Error
        }
    }
    else if(!isDop && (is1758DIPCard(cardNumber) == true)) {
        errorCode = _pci1758Dip[cardNumber]->Read(port,data);
        if(errorCode != 0) {
            str = QString("PCI1758: Reading DIP from PORT[%1] in card[%2] failed with Errorcode[%3].")
                    .arg(port).arg(cardNumber).arg(errorCode);
            _qDebugE(str,"","","");
            setErrorLog(str);
            return static_cast<int>(errorCode);//Error
        }
    }
    else {
        str = QString("PCI1758: Unregistered Card number [%1].").arg(cardNumber);
        _qDebugE(str,"","","");
        setErrorLog(str);
        return INVALID_CARD_NUMBER;
    }

    return SUCCESS;
}

/*************************************************************************
 Function Name - resetPCI
 Parameter(s)  - void
 Return Type   - int
 Action        - Writes 0x00 in all ports of all cards. If success returns 1
                 else returns Error code.
 *************************************************************************/
int Pci1758::resetPCI() {
    ErrorCode errorCode = Success;
    uint8_t cardNumber;
    QString str;
    int ret = 1;
    errorLog.clear();

    for(int i = 0;i < _1758DopCardNumbers.count();i++)
    {
        cardNumber = _1758DopCardNumbers[i];
        for(int32 port = 0;port < 16;port++) {
            errorCode = _pci1758Dop[cardNumber]->Write(port,0x0);
            if(errorCode != 0) {
                str = QString("PCI1758: Reseting PORT[%1] in Card[%2] failed with Errorcode[%3]").arg(port).arg(cardNumber).arg(errorCode);
                _qDebugE(str,"","","");
                setErrorLog(str);
                ret = static_cast<int>(errorCode);//Error
            }
        }
    }
    return ret;
}

void Pci1758::setDopCardDesc(const QStringList &value)
{
    dopCardDesc = value;
}

void Pci1758::setDipCardDesc(const QStringList &value)
{
    dipCardDesc = value;
}

QString Pci1758::getErrorLog() const
{
    return errorLog;
}

void Pci1758::setTotalDops(int value)
{
    totalDops = value;
}

void Pci1758::setTotalDips(int value)
{
    totalDips = value;
}

void Pci1758::setErrorLog(const QString &value)
{
    if(errorLog.size() > 0) errorLog.append("\n");
    errorLog.append(value);
}
