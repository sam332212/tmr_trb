#ifndef REPORT_GENERATOR_H
#define REPORT_GENERATOR_H

#include <QObject>
#include <QTableWidget>
#include <QThread>


class Report_Generator : public QThread
{
    Q_OBJECT
    QString title;
    QString path;
    QString data;
    QVector<QVector<QStringList>> tables;
    QVector<QStringList> selfTestTable;
    QString user = "";
    QString UUT = "";
    QString UnitNumber = "";
    QString testName = "";
    QString testType = "";
    QString startDate;
    QString startTime = "";
    QString endTime;
    QString testDuration = "";
    QStringList status;
    QString userRep = "";
    QString qaRep = "";
    QString insulationTestVoltage = "";
    QString softwareCheckSum;
    QString softwareVersion;
    QString softwareBuildDate;
    QVector<QStringList> powerSupplies;
    bool thread_running;
    bool stop;
public:
    explicit Report_Generator(const QString title = "", const QString path = "");

    bool is_thread_running(){return thread_running;}
    void print_pdf(int orientation = 0);
    void setTitle(QString title) {this->title = title;}
    void setUUT(QString UUT_Name) {UUT = UUT_Name;}
    void setUnitNumber(QString unit_number) {UnitNumber = unit_number;}
    void setUser(QString user_name) {user = user_name;}
    void setTestName(QString test_name) {testName = test_name;}
    void setTestType(QString type) {testType = type;}
    void setStartDate(QString date) {startDate = date;}
    void setStartTime(QString start) {startTime = start;}
    void setEndTime(QString end) {endTime = end;}
    void setTestDuration(QString time) {testDuration = time;}
    void setTestResult(QStringList &result) {status = result;}
    void setUserRep(QString repName) {userRep = repName;}
    void setQARep(QString repName) {qaRep = repName;}
    void setInsulationTestVoltage(QString voltage) {insulationTestVoltage = voltage;}
    void setPowerSupplies(QVector<QStringList> &supplies) {powerSupplies = supplies;}
    void setTables(QVector<QVector<QStringList>> &tables) {this->tables = tables;}
    void stop_thread(){stop=true;}
    void setSoftwareCheckSum(QString checkSum) {softwareCheckSum = checkSum;}
    void setSoftwareVersion(QString version) {softwareVersion = version;}
    void setSoftwareBuildDate(QString buildDate) {softwareBuildDate = buildDate;}
    void setSelfTestTable(QVector<QStringList> &table) {this->selfTestTable = table;}

    QString getUUT()        {return UUT;}
    QString getTitle()      {return title;}
    QString getUnitNumber() {return UnitNumber;}
    QString getUser()       {return user;}
    QString getTestName()   {return testName;}
    QString getTestType()   {return testType;}
    QString getStartTime()  {return startTime;}
    QString getTestDuration()   {return testDuration;}
    QStringList getTestResult() {return status;}
    QString getUserRep()     {return userRep;}
    QString getQARep()      {return qaRep;}
    QString getInsulationTestVoltage() {return insulationTestVoltage;}
    QVector<QStringList>& getPowerSupplies() {return powerSupplies;}
    QVector<QVector<QStringList>> & getTables() {return tables;}

    void printParameters();
    void printSelfTestPdf(int orientation);
    QString get_data(){return data;}
    QString get_path(){return path;}

};

#endif // REPORT_GENERATOR_H
