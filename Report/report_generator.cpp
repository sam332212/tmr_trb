#include "report_generator.h"
#include "PDF_API.h"
#include <QDateTime>
#include <QFileDialog>




Report_Generator::Report_Generator(const QString title, QString path)
                : title(title) , path(path)
{
    qDebug()<<"Given Path:"<<path;
}

void Report_Generator::printParameters()
{
    qDebug()<<"Title = "<<title;
    qDebug()<<"UUT = "<<UUT;
    qDebug()<<"Unit Number = "<<UnitNumber;
    qDebug()<<"Test Name = "<<testName;
    qDebug()<<"Test Type = "<<testType;
    qDebug()<<"Test Start Time = "<<startTime;
    qDebug()<<"Test Duration = "<<testDuration;
    qDebug()<<"Insulation Test Voltage = "<<insulationTestVoltage;
    qDebug()<<"Result = "<<status;
    qDebug()<<"User Rep. = "<<userRep;
    qDebug()<<"QA Rep. = "<<qaRep;
    for(int i = 0; i < powerSupplies.count(); i++) {
        qDebug()<<"PS"<<i+1<<" = "<<powerSupplies.at(i);
    }
}


void Report_Generator::print_pdf(int orientation)
{
    qDebug()<<"Generating report  for "<<title;
    QString Reports_Path = "D:/Reports";
    QString result,fileName;
    QString unitName = UUT;
    QString test = testName.replace('/','_');
    int noOfTables = tables.count();
	QString currentDate = QDateTime::currentDateTime().toString("dd.MM.yyyy").replace(".","_");

    if(status.contains("FAIL",Qt::CaseInsensitive)) result = "FAIL";
    else result = "PASS";

    PDF_API pdf_generator;

    // Font
    QFont HeadingFont;
    HeadingFont.setFamily("Times New Roman");
    HeadingFont.setPointSize(16);
    HeadingFont.setUnderline(true);
    QFont font;
    font.setFamily("Times New Roman");
    font.setPointSize(12);

    //  Insert title
    pdf_generator.insertText(title, HeadingFont, QColor(Qt::black), Qt::AlignCenter);
    QString text = "";
    if(softwareVersion != "") {
        text = "<b>Software Version: </b>" + softwareVersion;
		pdf_generator.insertText(text,font,QColor(Qt::black),Qt::AlignLeft);
	}
    if(softwareBuildDate != "") {
		text = "<b>Software Build Date : </b>" + softwareBuildDate;
        pdf_generator.insertText(text,font,QColor(Qt::black),Qt::AlignLeft);
	}
    if(softwareCheckSum != "") {
		text = "<b>Software Checksum: </b>" + softwareCheckSum;
		pdf_generator.insertText(text,font,QColor(Qt::black),Qt::AlignLeft);
	}
    pdf_generator.insertNewLine();
    text = "<b>Unit Name: </b>" + UUT;
    pdf_generator.insertText(text,font,QColor(Qt::black),Qt::AlignLeft);
    if(UnitNumber != ""){
        text = QString("<b>Unit Serial Number:</b> %1").arg(UnitNumber);
        pdf_generator.insertText(text,font,QColor(Qt::black),Qt::AlignLeft);
    }
    text = "<b>Test Name: </b>" + testName;
    pdf_generator.insertText(text,font,QColor(Qt::black),Qt::AlignLeft);
    if(testType != "") {
        text = "<b>Type of Test: </b>" + testType;
        pdf_generator.insertText(text,font,QColor(Qt::black),Qt::AlignLeft);
    }
    if(insulationTestVoltage != "") {
        pdf_generator.insertText("<b>Test Voltage: </b>" + insulationTestVoltage,font,QColor(Qt::black),Qt::AlignLeft);
    }
    if(startDate == "") {
        QString date = QDateTime::currentDateTime().toString("dd.MM.yyyy").replace(".","/");
        QString time = QDateTime::currentDateTime().toString("hh:mm:ss");

        startTime = date+pdf_generator.getSpacesHTMLCode(5)+
               time;
        text = "<b>Date & Time: </b>" + startTime;
    }
    else
        text = "<b>Date & Time: </b>" + startDate + "   "+ startTime;
    pdf_generator.insertText(text,font,QColor(Qt::black),Qt::AlignLeft);
    if(endTime != "") {
        text = "<b>End Time: </b>" + endTime;
        pdf_generator.insertText(text,font,QColor(Qt::black),Qt::AlignLeft);
    }

    text = "<b>Test Duration: </b>" + testDuration;
    pdf_generator.insertText(text,font,QColor(Qt::black),Qt::AlignLeft);
    text = "<b>Result: </b>" + result;
    pdf_generator.insertText(text,font,QColor(Qt::black),Qt::AlignLeft);

    qDebug()<<"Number of Cycles = "<<noOfTables;
    for(int i = 0 ; i < noOfTables; i++) {
        if(noOfTables > 1) {
			pdf_generator.insertNewLine();
            text = QString("<b>Cycle:</b> %1/%2").arg(i+1).arg(noOfTables);
            pdf_generator.insertText(text, font, QColor(Qt::black), Qt::AlignLeft);
            text = "<b>Result: </b>" + status.at(i);
            pdf_generator.insertText(text, font, QColor(Qt::black), Qt::AlignLeft);
        }
		pdf_generator.insertTable(tables.at(i)); 
	}
    pdf_generator.insertNewLine();
    pdf_generator.insertNewLine();
    pdf_generator.insertNewLine();
    pdf_generator.insertText("<b>User Rep</b>"
                             + pdf_generator.getSpacesHTMLCode(100- 7)
                             + "<b>QA/QC Rep<b>",font,QColor(Qt::black),Qt::AlignCenter);
    pdf_generator.insertText(userRep
                             + pdf_generator.getSpacesHTMLCode(100-userRep.count())
                             + qaRep , font,QColor(Qt::black),Qt::AlignCenter);

    qDebug()<<"Saving..";
    if(path == "") {
        Reports_Path = Reports_Path + "/" + currentDate + "/" + unitName.replace(" ","_") + "/";
        if(UnitNumber.size() > 0) Reports_Path += UnitNumber + "/";
        fileName = test.replace(" ","_") + "_";
        fileName += QDateTime::currentDateTime().toString("dd.MM.yyyy").replace(".","_") +  "_" + QDateTime::currentDateTime().toString("hh:mm:ss").replace(":","_");;

        qDebug()<<"Report Path = "<<Reports_Path;

        QDir dir(Reports_Path);
        if(!dir.exists())
        {
            dir.mkpath(".");
        }
        path = Reports_Path;
        path = QFileDialog :: getSaveFileName(nullptr, tr("Save As"), path + fileName, "Files(*.pdf)");

        if(!fileName.contains(".pdf",Qt::CaseInsensitive)) fileName += ".pdf";
    }

    qDebug()<<"Path = "<<path;
    qDebug()<<"FileName: "<<fileName;

    if(path == "") {
        qDebug()<<"User Cancelled the Result Saving or Invalid path ["<<path<<"]";
        return;
    }
    if(orientation)
        pdf_generator.save(path,QPrinter::Orientation::Landscape);
    else
        pdf_generator.save(path,QPrinter::Orientation::Portrait);

    qDebug()<<"Report generated..";
}

void Report_Generator::printSelfTestPdf(int orientation)
{
    qDebug()<<"Generating Self Test report ";
    qDebug()<<"-----------\n"<<tables.at(0)<<"\n-----------";
    QString Reports_Path = "D:/Reports";
    QString fileName;
    QString currentDate = QDateTime::currentDateTime().toString("dd.MM.yyyy").replace(".","_");

    PDF_API pdf_generator;

    // Font
    QFont HeadingFont;
    HeadingFont.setFamily("Times New Roman");
    HeadingFont.setPointSize(16);
    HeadingFont.setUnderline(true);
    QFont font;
    font.setFamily("Times New Roman");
    font.setPointSize(12);

    //  Insert title
    pdf_generator.insertText("SELF TEST", HeadingFont, QColor(Qt::black), Qt::AlignCenter);
    pdf_generator.insertNewLine();

    QString text = "";
    if(softwareVersion != "") {
        text = "<b>Software Version: </b>" + softwareVersion;
        pdf_generator.insertText(text,font,QColor(Qt::black),Qt::AlignLeft);
    }
    if(softwareBuildDate != "") {
        text = "<b>Software Build Date : </b>" + softwareBuildDate;
        pdf_generator.insertText(text,font,QColor(Qt::black),Qt::AlignLeft);
    }
    if(softwareCheckSum != "") {
        text = "<b>Software Checksum: </b>" + softwareCheckSum;
        pdf_generator.insertText(text,font,QColor(Qt::black),Qt::AlignLeft);
    }
    pdf_generator.insertNewLine();

    if(startDate == "") {
        QString date = QDateTime::currentDateTime().toString("dd.MM.yyyy").replace(".","/");
        QString time = QDateTime::currentDateTime().toString("hh:mm:ss");

        startTime = date+pdf_generator.getSpacesHTMLCode(5)+time;
        text = "<b>Date & Time: </b>" + startTime;
    }
    else
        text = "<b>Date & Time: </b>" + startDate + "   "+ startTime;

    pdf_generator.insertText(text,font,QColor(Qt::black),Qt::AlignLeft);

    if(endTime != "") {
        text = "<b>End Time: </b>" + endTime;
        pdf_generator.insertText(text,font,QColor(Qt::black),Qt::AlignLeft);
    }

    text = "<b>Test Duration: </b>" + testDuration;
    pdf_generator.insertText(text,font,QColor(Qt::black),Qt::AlignLeft);
    pdf_generator.insertHorizontalLine();

    pdf_generator.insertNewLine();
    pdf_generator.insertTable(tables.at(0));
    pdf_generator.insertNewLine();
    pdf_generator.insertHorizontalLine();
    pdf_generator.insertNewLine();
    pdf_generator.insertNewLine();
    pdf_generator.insertNewLine();
    pdf_generator.insertText("<b>User Rep</b>"
                             + pdf_generator.getSpacesHTMLCode(100- 7)
                             + "<b>QA/QC Rep<b>",font,QColor(Qt::black),Qt::AlignCenter);
    pdf_generator.insertText(userRep
                             + pdf_generator.getSpacesHTMLCode(100-userRep.count())
                             + qaRep , font,QColor(Qt::black),Qt::AlignCenter);

    qDebug()<<"Saving..";
    if(path == "") {
        Reports_Path = Reports_Path + "/" + currentDate + "/";
        fileName = "self_Test_report";
        fileName += QDateTime::currentDateTime().toString("dd.MM.yyyy").replace(".","_") +  "_" + QDateTime::currentDateTime().toString("hh:mm:ss").replace(":","_");;

        qDebug()<<"Report Path = "<<Reports_Path;

        QDir dir(Reports_Path);
        if(!dir.exists())
        {
            dir.mkpath(".");
        }
        path = Reports_Path;
        path = QFileDialog :: getSaveFileName(nullptr, tr("Save As"), path + fileName, "Files(*.pdf)");

        if(!fileName.contains(".pdf",Qt::CaseInsensitive)) fileName += ".pdf";
    }

    qDebug()<<"Path = "<<path;
    qDebug()<<"FileName: "<<fileName;

    if(path == "") {
        qDebug()<<"User Cancelled the Result Saving or Invalid path ["<<path<<"]";
        return;
    }
    if(orientation)
        pdf_generator.save(path,QPrinter::Orientation::Landscape);
    else
        pdf_generator.save(path,QPrinter::Orientation::Portrait);

    qDebug()<<"Report generated..";
}
