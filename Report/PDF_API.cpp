#include "pdf_api.h"
#include "Support/debugflag.h"
#include <QMessageBox>


/********************************************************
 MACRO Definition
 ********************************************************/
#if PDF_API_DEBUG
    #define _qDebug(s) qDebug()<<s
#else
    #define _qDebug(s);
#endif

#if PDF_API_DEBUG_E
    #define _qDebugE(p,q,r,s) qDebug()<<p<<q<<r<<s
#else
    #define _qDebugE(p,q,r,s);
#endif

/********************************************************/

PDF_API::PDF_API(QObject *parent) : QObject(parent)
{
    _qDebug("Inside PDF_API:Constructor");

    this->setObjectName("PDF_API_Object");

    HTMLCode += "<!DOCTYPE html>"
                "\n<html>"
                "\n<body>";

    HTMLCode += "\n<style>"
                "\n@media print"
                "\n{"
                "\ntable { page-break-after:auto }"
                "\ntr    { page-break-inside:avoid; page-break-after:auto }"
                "\ntd    { page-break-inside:avoid; page-break-after:auto }"
                "\nthead { display:table-header-group }"
                "\ntfoot { display:table-footer-group }"
                "\n}"
                "\n</style>";
    _qDebug("Exiting PDF_API:Constructor");
}

void PDF_API::save(QString filePath, QPrinter::Orientation orient)
{
    _qDebug("Inside PDF_API:save");

    qDebug()<<"HTMLCode.size(:"<<HTMLCode.size();
    QTextEdit document;
    document.setHtml(HTMLCode);

    QPrinter printer(QPrinter::PrinterResolution);
    printer.setOrientation(orient);
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setPaperSize(QPrinter::A4);
    printer.setOutputFileName(filePath);
    printer.setPageMargins(QMarginsF(0.1, 0.01, 0.1, 0.1));

    document.print(&printer);

    _qDebug("Exiting PDF_API:save");
}

void PDF_API::insertNewLine()
{
    _qDebug("Inside PDF_API:insertNewLine");

    HTMLCode += "\n<p>&nbsp;</p>";

}

// Tarun Created
void PDF_API::insertTwoText(QString text1, QString text2, QFont font, QColor color)
{
    _qDebug("Inside PDF_API:insertTwoText");
    QString fontFamily = font.family();
    int fontSize = font.pointSize();
    bool italic = font.italic();
    bool bold = font.bold();
    bool underline = font.underline();

    QString style;
    style += tr("color: %1; ").arg(color.name());
    style += tr("font-family: %1; ").arg(fontFamily);
    style += tr("font-size: %1pt; ").arg(fontSize);
    style += tr("line-height: 100%;");
    QString style1 = style + tr("text-align: left");
    QString style2 = style + tr("text-align: right");


    QString code1 = tr("\n <div class=\"row\"><div class=\"col\" style=\"%1\"> ").arg(style1);
    QString code2 = tr("\n <div class=\"col\" style=\"%1\"> ").arg(style2);
    if(bold) {
        code1 += " <b>";
        code2 += " <b>";
    }
    if(italic) {
        code1 += " <i>";
        code2 += " <i>";
    }
    if(underline) {
        code1 += " <u>";
        code2 += " <u>";
    }
    code1 += text1;
    code2 += text2;

    if(bold) {
        code1 += " </b>";
        code2 += " </b>";
    }
    if(italic) {
        code1 += " </i>";
        code2 += " </i>";
    }
    if(underline) {
        code1 += " </u>";
        code2 += " </u>";
    }
//    QString text = code1  + "</div>" + code2 + "</div></div>";

//    HTMLCode += text;


    QString text = "";
      text += tr("\n <table style=\"width: 90%;\">");
      text += tr("\n<tr><td style=\"text-align:left\">%1</td>").arg(text1);
      text += tr("\n<td style=\"text-align:right\">%1</td></tr>").arg(text2);
    text +=  tr("\n</table>");
    HTMLCode += text;
    qDebug()<<"TWO lines = "<<text;
        _qDebug("Exiting PDF_API:insertTwoText");
//    <span style=”float: left”>Item 1</span> <span style=”float: right;”>Item 2</span>

}


void PDF_API::insertText(QString text,
                            QFont font,
                            QColor color,
                            Qt::AlignmentFlag flag)
{
    _qDebug("Inside PDF_API:insertText");

    QString fontFamily = font.family();
    int fontSize = font.pointSize();
    bool italic = font.italic();
    bool bold = font.bold();
    bool underline = font.underline();

    QString style;
    style += tr("color: %1; ").arg(color.name());
    style += tr("font-family: %1; ").arg(fontFamily);
    style += tr("font-size: %1pt; ").arg(fontSize);
    style += tr("line-height: 100%;");

    QString align;
    if(flag == Qt::AlignLeft)
        align = "left";
    else if(flag == Qt::AlignRight)
        align = "right";
    else if(flag == Qt::AlignCenter)
        align = "center";

    QString code = tr("\n <p align=\"%2\" style=\"%1\"> ").arg(style).arg(align);
    if(bold)
        code += " <b>";

    if(italic)
        code += " <i>";

    if(underline)
        code += " <u>";

    code += text;

    if(underline)
        code += " </u>";

    if(italic)
        code += " </i>";

    if(bold)
        code += " </b>";

    code+= " </p>";
    HTMLCode += code;
    _qDebug("Exiting PDF_API:insertText");
}
void PDF_API::insertHorizontalLine()
{
    _qDebug("Inside PDF_API:insertHorizontalLine");

    HTMLCode += "<hr>";
}

void PDF_API::insertTable(const QTableWidget *Table,
                          const QFont HeadingFont,
                          const QFont DataFont,
                          const QString BorderColor,
                          int WidthPercentage,
                          int CellPadding, int CellSpacing)
{
    _qDebug("Inside PDF_API:insertTable");

    int RowCount = Table->rowCount();
    int ColCount = Table->columnCount();


    HTMLCode += tr("\n\t <table border=\"1\" "
                "width=\"%1%\" "
                "cellpadding=\"%2\" "
                "cellspacing=\"%3\" "
                "bordercolor=\"%4\" >")
            .arg(WidthPercentage).arg(CellPadding).arg(CellSpacing).arg(BorderColor);

    int Total_Column_Width = Table->width();

    //Inserting heading
    HTMLCode += "\n\t\t <tr>";
    HTMLCode += tr("\n\t\t\t <th> Row No. </th>"); //row no
    for(int j=0; j<ColCount; j++)
    {
        QFont font = HeadingFont;
        QString style;
        style += tr("color: %1; ").arg(Table->horizontalHeaderItem(j)->textColor().name());
        style += tr("font-size: %1pt; ").arg(font.pointSize());

        // Width
        style += tr("width:%1%; ").arg((Table->columnWidth(j)/Total_Column_Width)*100);

        // Italic
        if(font.italic())
            style +=("font-style: italic; ");

        // Family
        style += tr("font-family: %1").arg(font.family());

        // Bold
        if(font.bold())
            style += "font-style: bold; ";

        // Underline
        if(font.underline())
            style += "font-style: underline; ";

        HTMLCode += tr("\n\t\t\t <th style=\"%2\"> %1 </th>")
                .arg(Table->horizontalHeaderItem(j)->text())
                                .arg(style);
    }
    HTMLCode += "\n\t\t </tr>";


    //Insert Data
    for(int i=0; i<RowCount; i++)
    {

        HTMLCode +="\n\t\t <tr>";

        HTMLCode += tr("\n\t\t\t <td> %1 </th>")
                .arg(i+1); //  add row number

        for(int j=0; j<ColCount; j++)
        {
            QFont font = DataFont;
            QString style;
            style += tr("color: %1; ").arg(Table->item(i,j)->textColor().name());
            style += tr("font-size: %1pt; ").arg(font.pointSize());

            // Width
            style += tr("width:%1%; ").arg((Table->columnWidth(j)/Total_Column_Width)*100);

            // Italic
            if(font.italic())
                style +=("font-style: italic; ");

            // Family
            style += tr("font-family: %1; ").arg(font.family());

            // Bold
            if(font.bold())
                style += "font-style: bold; ";

            // Underline
            if(font.underline())
                style += "font-style: underline; ";

            // To avoid page break in pages
            style += "page-break-inside: avoid; ";

            HTMLCode += tr("\n\t\t\t <td style=\"%2\"> %1 </th>")
                    .arg(Table->item(i,j)->text())
                                    .arg(style);
        }
        HTMLCode += "\n\t\t </tr>";
    }

    HTMLCode +="\n\t </table>";

    _qDebug("Exiting PDF_API:insertTable");

}



void PDF_API::insertTable(const QSqlTableModel *model,
                          const QFont HeadingFont,
                          const QFont DataFont,
                          const QString BorderColor,
                          int WidthPercentage,
                          int CellPadding,
                          int CellSpacing)
{
    _qDebug("Inside PDF_API:insertTable");
    int RowCount = model->rowCount();
    int ColCount = model->columnCount();

    HTMLCode += tr("\n\t <table border=\"1\" "
                "width=\"%1%\" "
                "cellpadding=\"%2\" "
                "cellspacing=\"%3\" "
                "bordercolor=\"%4\" >")
            .arg(WidthPercentage).arg(CellPadding).arg(CellSpacing).arg(BorderColor);

//    int Total_Column_Width = Table->width();

    //Inserting heading
    HTMLCode += "\n\t\t <tr>";
    for(int j=0; j<ColCount; j++)
    {
        QFont font = HeadingFont;
        QString style;
        style += tr("color: %1; ").arg("black");
        style += tr("font-size: %1pt; ").arg(font.pointSize());

//        // Width
//        style += tr("width:%1%; ").arg((Table->columnWidth(j)/Total_Column_Width)*100);

        // Italic
        if(font.italic())
            style +=("font-style: italic; ");

        // Family
        style += tr("font-family: %1").arg(font.family());

        // Bold
        if(font.bold())
            style += "font-style: bold; ";

        // Underline
        if(font.underline())
            style += "font-style: underline; ";

        HTMLCode += tr("\n\t\t\t <th style=\"%2\"> %1 </th>").arg(model->headerData(j, Qt::Horizontal).toString().arg(style));

    }
    HTMLCode += "\n\t\t </tr>";


    //Insert Data
    for(int i=0; i<RowCount; i++)
    {

        HTMLCode +="\n\t\t <tr>";
        for(int j=0; j<ColCount; j++)
        {
            QFont font = DataFont;
            QString style;
            style += tr("color: %1; ").arg("black");
            style += tr("font-size: %1pt; ").arg(font.pointSize());

//            // Width
//            style += tr("width:%1%; ").arg((Table->columnWidth(j)/Total_Column_Width)*100);

            // Italic
            if(font.italic())
                style +=("font-style: italic; ");

            // Family
            style += tr("font-family: %1").arg(font.family());

            // Bold
            if(font.bold())
                style += "font-style: bold; ";

            // Underline
            if(font.underline())
                style += "font-style: underline; ";

            QModelIndex index = model->index(i, j);
            HTMLCode += tr("\n\t\t\t <td style=\"%2\"> %1 </th>")
                    .arg(model->data(index).toString())
                                    .arg(style);

        }
        HTMLCode += "\n\t\t </tr>";
    }

    HTMLCode +="\n\t </table>";

    _qDebug("Exiting PDF_API:insertTable");

}

void PDF_API::insertTable(const QVector<QStringList> RowVector,
                          const QStringList ColumnNames,
                          const QVector<int> ColumnWidth,
                          const QFont HeadingFont,
                          const QFont DataFont,
                          const QString BorderColor,
                          int WidthPercentage,
                          int CellPadding,
                          int CellSpacing)
{
    _qDebug("Inside PDF_API:insertTable - Generic Table");

    int RowCount = RowVector.count();
    int ColCount = ColumnNames.count();


    HTMLCode += tr("\n\t <table border=\"1\" "
                "width=\"%1%\" "
                "cellpadding=\"%2\" "
                "cellspacing=\"%3\" "
                "bordercolor=\"%4\" >")
            .arg(WidthPercentage).arg(CellPadding).arg(CellSpacing).arg(BorderColor);

//    int Total_Column_Width = Table->width();

    //Inserting heading
    HTMLCode += "\n\t\t <tr>";
    for(int j=0; j<ColCount; j++)
    {
        QFont font = HeadingFont;
        QString style;
        style += tr("color: %1; ").arg("black");
        style += tr("font-size: %1pt; ").arg(font.pointSize());

//        // Width
        style += tr("width:%1%; ").arg(ColumnWidth.at(j));

        // Italic
        if(font.italic())
            style +=("font-style: italic; ");

        // Family
        style += tr("font-family: %1").arg(font.family());

        // Bold
        if(font.bold())
            style += "font-style: bold; ";

        // Underline
        if(font.underline())
            style += "font-style: underline; ";

        HTMLCode += tr("\n\t\t\t <th style=\"%2\"> %1 </th>").arg(ColumnNames.at(j)).arg(style);
    }
    HTMLCode += "\n\t\t </tr>";


    //Insert Data
    for(int i=0; i<RowCount; i++)
    {
        QStringList RowData = RowVector.at(i);
        HTMLCode +="\n\t\t <tr>";
        for(int j=0; j<ColCount; j++)
        {
            QFont font = DataFont;
            QString style;
            style += tr("color: %1; ").arg("black");
            style += tr("font-size: %1pt; ").arg(font.pointSize());

//            // Width
            style += tr("width:%1%; ").arg(ColumnWidth.at(j));

            // Italic
            if(font.italic())
                style +=("font-style: italic; ");

            // Family
            style += tr("font-family: %1").arg(font.family());

            // Bold
            if(font.bold())
                style += "font-style: bold; ";

            // Underline
            if(font.underline())
                style += "font-style: underline; ";

            HTMLCode += tr("\n\t\t\t <td style=\"%2\"> %1 </th>")
                    .arg(RowData.at(j))
                                    .arg(style);

        }
        HTMLCode += "\n\t\t </tr>";
    }

    HTMLCode +="\n\t </table>";
    _qDebug("Exiting PDF_API:insertTable - Generic Table");
}
/// Tarun kumar creation
void PDF_API::insertTable(const QVector<QStringList> &table,
//                          const QVector<int> ColumnWidth,
                          const QFont HeadingFont,
                          const QFont DataFont,
                          const QString BorderColor,
                          int WidthPercentage,
                          int CellPadding,
                          int CellSpacing)
{
    _qDebug("Inside PDF_API:insertTable - Generic Table");

    int RowCount = table.count();
    int ColCount = table.at(0).count() + 1;

    qDebug()<<"Row x Col = "<<RowCount<<" x "<<ColCount;
    HTMLCode += tr("\n\t <table border=\"1\" "
                "width=\"%1%\" "
                "cellpadding=\"%2\" "
                "cellspacing=\"%3\" "
                "bordercolor=\"%4\" >")
            .arg(WidthPercentage).arg(CellPadding).arg(CellSpacing).arg(BorderColor);

//    int Total_Column_Width = Table->width();

    //Inserting heading
    HTMLCode += "\n\t\t <tr>";
	QStringList ColumnNames = table.at(0);
    ColumnNames.insert(0,"S. No.");
	QVector<int> ColumnWidth;
    for(int j = 0; j < ColCount; j++)
    {
//        QFont font = HeadingFont;
        QString style;
        style += tr("color: %1; ").arg("black");
        style += tr("font-size: %1pt; ").arg(HeadingFont.pointSize());

//        // Width
		ColumnWidth.append(ColumnNames.at(j).count());
        style += tr("width:%1%; ").arg(ColumnWidth.at(j));

        // Italic
        if(HeadingFont.italic())
            style +=("font-style: italic; ");

        // Family
        style += tr("font-family: %1").arg(HeadingFont.family());

        // Bold
        if(HeadingFont.bold())
            style += "font-style: bold; ";

        // Underline
        if(HeadingFont.underline())
            style += "font-style: underline; ";

        HTMLCode += tr("\n\t\t\t <th style=\"%2\"> %1 </th>").arg(ColumnNames.at(j)).arg(style);
    }
    HTMLCode += "\n\t\t </tr>";


    //Insert Data
    for(int i = 1; i < RowCount; i++)
    {
        QStringList RowData = table.at(i);
        HTMLCode +="\n\t\t <tr>";
        for(int j = 0; j < ColCount; j++)
        {
            QFont font = DataFont;
            QString style;
            style += tr("color: %1; ").arg("black");
            style += tr("font-size: %1pt; ").arg(font.pointSize());

//            // Width
            style += tr("width:%1%; ").arg(ColumnWidth.at(j));

            // Italic
            if(font.italic())
                style +=("font-style: italic; ");

            // Family
            style += tr("font-family: %1").arg(font.family());

            // Bold
            if(font.bold())
                style += "font-style: bold; ";

            // Underline
            if(font.underline())
                style += "font-style: underline; ";

            QString header = ColumnNames[j].remove("_");
            header = header.remove(" ");
            if(header.contains("connectornumber",Qt::CaseInsensitive) || header.contains("pinnumber",Qt::CaseInsensitive) ||
               header.contains("expected",Qt::CaseInsensitive)        || header.contains("measured",Qt::CaseInsensitive)  ||
               header.contains("result",Qt::CaseInsensitive)           || header.contains("signal",Qt::CaseInsensitive)      ) {
                style += "; text-align:center; ";
            }

            if(j > 0) {
                QString str = RowData.at(j-1);
                str.replace(">","&gt;");
                str.replace("<","&lt;");
                HTMLCode += tr("\n\t\t\t <td style=\"%2\"> %1 </th>")
                    .arg(str).arg(style);
            }
            else {
                style += "; text-align:center; ";
                HTMLCode += tr("\n\t\t\t <td style=\"%2\"> %1 </th>")
                    .arg(i).arg(style);
            }

        }
        HTMLCode += "\n\t\t </tr>";
    }

    HTMLCode +="\n\t </table>";
    _qDebug("Exiting PDF_API:insertTable - Generic Table");
}



QString PDF_API::getSpacesHTMLCode(int noOfSpaces)
{
    QString Spaces;
    for(int i=0; i<noOfSpaces; i++)
    {
        Spaces += "&nbsp;";
    }
    return Spaces;
}

void PDF_API::insertImage(QString filePath, int WidthPixel, int HeightPixel)
{
    _qDebug("Inside PDF_API:insertImage");

    QString code = tr("\n<img src=\"%1\" alt=\"Image\" style=\"width:%2px; height:%3px;\">")
                    .arg(filePath).arg(WidthPixel).arg(HeightPixel);
    HTMLCode += code;
}

void PDF_API::insertHTML(QString htmlCode)
{
    _qDebug("Inside PDF_API:insertHTML");

    HTMLCode += htmlCode;
}

void PDF_API::insertNewPage()
{
    _qDebug("Inside PDF_API:insertNewPage");

    HTMLCode += "\n<p style=\"page-break-after: always;\"> &nbsp; </p>";
}
