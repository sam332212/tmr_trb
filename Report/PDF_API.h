#ifndef PDF_API_H
#define PDF_API_H

#include <QDebug>
#include <QObject>
#include <QTextDocument>
#include <QPrinter>
#include <QTableWidget>
#include <QSqlTableModel>
#include <QFile>
#include <QTextBrowser>
#include <QDir>
#include <QProcess>
#include <QProgressDialog>
#include <QCoreApplication>

class PDF_API : public QObject
{
    Q_OBJECT
private:
    QString HTMLCode;
    QProgressDialog *progressDialog;

public:
    explicit PDF_API(QObject *parent = nullptr);

    template <class myType>
    inline static void debug (myType info, bool EnableDiable = true)
    {
        if(EnableDiable)
            qDebug()<<info;
    }
    void insertNewLine();

    void insertTab();

    void insertText(QString text,
                    QFont font,
                    QColor color,
                    Qt::AlignmentFlag flag);

    void insertTwoText(QString text1,
                       QString text2,
                       QFont font,
                       QColor color);


    void insertHorizontalLine();

    //Table Widget
    /*
     * const QTableWidget *Table,
                          const QFont HeadingFont,
                          const QFont DataFont,
                          const QString BorderColor,
                          int WidthPercentage,
                          int CellPadding, int CellSpacing*/

    void insertTable(const QTableWidget *Table,
                     const QFont HeadingFont = QFont("Times New Roman", 12, QFont::Bold, false),
                     const QFont DataFont = QFont("Times New Roman", 12),
                     const QString BorderColor = "black",
                     int WidthPercentage = 100,
                     int CellPadding = 2,
                     int CellSpacing = 2);

    //Sql Table
    void insertTable(const QSqlTableModel *model,
                     const QFont HeadingFont = QFont("Times New Roman", 12, QFont::Bold, false),
                     const QFont DataFont = QFont("Times New Roman", 12),
                     const QString BorderColor = "black",
                     int WidthPercentage = 100,
                     int CellPadding = 2,
                     int CellSpacing = 2);

    //Generic Table
    void insertTable(const QVector<QStringList> RowVector,
                     const QStringList ColumnNames,
                     const QVector<int> ColumnWidth,
                     const QFont HeadingFont = QFont("Times New Roman", 12, QFont::Bold, false),
                     const QFont DataFont = QFont("Times New Roman", 12),
                     const QString BorderColor = "black",
                     int WidthPercentage = 100,
                     int CellPadding = 2,
                     int CellSpacing = 2);

    void insertTable(const QVector<QStringList> &table,
//                     const QVector<int> ColumnWidth,
                     const QFont HeadingFont = QFont("Times New Roman", 12, QFont::Bold, false),
                     const QFont DataFont = QFont("Times New Roman", 12),
                     const QString BorderColor = "black",
                     int WidthPercentage = 100,
                     int CellPadding = 1,
                     int CellSpacing = 0);
	
    QString getSpacesHTMLCode(int noOfSpaces);

    void insertImage(QString filePath, int WidthPixel, int HeightPixel);

    void insertHTML(QString htmlCode);

    void insertNewPage();

    void save(QString filePath,QPrinter::Orientation orient = QPrinter::Orientation::Portrait);

    QString get_data(){return HTMLCode;}

public slots:

signals:
    void infoSignal(QString Message, QString title);
    void errorSignal(QString Message, QString title);



};


#endif // PDF_API_H
