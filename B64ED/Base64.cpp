/*************************************  FILE HEADER  *****************************************************************
*
*  File name - Base64.cpp
*  Creation date and time - 16-AUG-2021 , MON 11:00 AM IST
*  Author: - Manoj
*  Purpose of the file - All Base64 encode-decode modules will be defined.
*
**********************************************************************************************************************/
/********************************************************
 Inclusion of header file
 ********************************************************/
#include "Base64.h"
#include "Support/debugflag.h"
#include <QDebug>

/********************************************************
 MACRO Definition
 ********************************************************/
#if BASE64_DEBUG
    #define _qDebug(s) qDebug()<<"\n-------- Inside "<<s<<" --------"
#else
    #define _qDebug(s); {}
#endif

#if BASE64_DEBUG_E
    #define _qDebugE(p,q,r,s) qDebug()<<p<<q<<r<<s
#else
    #define _qDebugE(p,q,r,s); {}
#endif

Base64::Base64()
{

}

QString Base64::encode(const QString data,bool noTail) {
    _qDebug("QString Base64::encode(const QString data,bool noTail)");
    _qDebugE("Given data:[",data,"],noTail:",noTail);
    Base64 base64;
    if(base64.isValid(data) && (!isBase64(data))) {
        if(!noTail) return data.toLatin1().toBase64();
        else {
            QString enc = data.toLatin1().toBase64();
            int pos;
            do {
                pos = enc.lastIndexOf(QChar('='));
                if(pos >= 0) enc = enc.left(pos);
            }while(pos >= 0);
            return enc;
        }
    }
    return data;
}

QVector<QStringList> Base64::encode(const QVector<QStringList> data,bool noTail) {
    _qDebug("QVector<QStringList> Base64::encode(const QVector<QStringList> data,bool noTail)");
    _qDebugE("Given data:[",data,"],noTail:",noTail);
    QVector<QStringList> encodeBuff;
    encodeBuff.clear();

    for(QStringList list : data) {
        encodeBuff.push_back(encode(list,noTail));
    }

    return encodeBuff;
}

QStringList Base64::encode(const QStringList data,bool noTail) {
    _qDebug("QStringList Base64::encode(const QStringList data,bool noTail)");
    _qDebugE("Given data:[",data,"],noTail:",noTail);
    QStringList encodeBuff;
    encodeBuff.clear();

    for(QString str : data) {
        encodeBuff.push_back(encode(str,noTail));
    }

    return encodeBuff;
}

QString Base64::decode(const QString data) {
  _qDebug("QString Base64::decode(const QString data)");
  _qDebugE("Given data:[",data,"]","");
  if(isBase64(data)) return QByteArray::fromBase64(data.toLatin1()).data();
  return data;
}

QVector<QStringList> Base64::decode(const QVector<QStringList> data) {
    _qDebug("QVector<QStringList> Base64::decode(const QVector<QStringList> data)");
    _qDebugE("Given data:[",data,"]","");
    QVector<QStringList> decodeBuff;
    decodeBuff.clear();

    for(QStringList list : data) {
        decodeBuff.push_back(decode(list));
    }

    return decodeBuff;
}

QStringList Base64::decode(const QStringList& data) {
    _qDebug("QStringList Base64::decode(const QStringList& data)");
    _qDebugE("Given data:[",data,"]","");
    QStringList decodeBuff;
    decodeBuff.clear();

    for(QString str : data) {
        decodeBuff.push_back(decode(str));
    }

    return decodeBuff;
}

bool Base64::isBase64(const QString data){
    _qDebug("bool Base64::isBase64(QString data)");

    QString fData = data;
    int mod = (fData.length() % 4);
    if (mod != 0) {
        for(int i = 0;i < (4-mod);i++) fData += "=";
        if (fData.length() % 4 != 0) return false;
    }

    QString decode = QByteArray::fromBase64(data.toLatin1());
    QString encode = decode.toLatin1().toBase64();
    //qDebug()<<"data:"<<data<<", decode:"<<decode<<", encode:"<<encode;

    if(encode != fData) return false;
    return true;
}

bool Base64::isValid(QString data) {
    for(QString str : specialCharsToAvoidEncode) {
        if(data.contains(str)) return false;
    }

    return true;
}
