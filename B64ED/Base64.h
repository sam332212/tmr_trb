/*************************************  FILE HEADER  *****************************************************************
*
*  File name - Base64.h
*  Creation date and time - 16-AUG-2021 , MON 11:00 AM IST
*  Author: - Manoj
*  Purpose of the file - All Base64 encode-decode modules will be declared .
*
**********************************************************************************************************************/
#ifndef BASE64_H
#define BASE64_H

/********************************************************
 Inclusion of header file
 ********************************************************/
#include <QString>
#include <QStringList>

/********************************************************
 Class Declaration
 ********************************************************/
class Base64
{
public:
    Base64();

    static QString encode(const QString data,bool noTail=true);
    static QVector<QStringList> encode(const QVector<QStringList> data,bool noTail=true);
    static QStringList encode(const QStringList data,bool noTail=true);
    static QString decode(const QString data);
    static QVector<QStringList> decode(const QVector<QStringList> data);
    static QStringList decode(const QStringList& data);
    static bool isBase64(const QString stringBase64);

private:
    const QStringList specialCharsToAvoidEncode = {"±","Ω","Ω"};

    bool isValid(QString data);
};

#endif // BASE64_H
