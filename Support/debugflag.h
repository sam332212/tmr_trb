#ifndef DEBUGFLAG_H
#define DEBUGFLAG_H

#define G   Global
#define clearmem(x) delete(x);x=nullptr

#undef  ENABLE
#define ENABLE  1
#if ENABLE
    #define PCI_7444_EN    1
    #define PCI_1758_EN    1
    #define THREAD_RUN     1
    #define DMM_PRESENT    1
    #define PS_PRESENT     1
    #define IR_PRESENT     1
#else
    #define PCI_7444_EN    0
    #define PCI_1758_EN    0
    #define THREAD_RUN     0
    #define DMM_PRESENT    0
    #define PS_PRESENT     0
    #define IR_PRESENT     0
#endif

#undef  GLOBAL_DEBUG
#define GLOBAL_DEBUG  0// Change 1 to 0 to remove all General Debug msg's.
#if GLOBAL_DEBUG
    #define MAIN_DEBUG              1
    #define SPECIAL_DEBUG           1

    #define DATABASE_DEBUG          0
    #define DATABASE_DEBUG_E        0

    #define DMM_DEBUG               0
    #define DMM_DEBUG_E             0

    #define IRMETER_DEBUG           0
    #define IRMETER_DEBUG_E         0

    #define PS_DEBUG                0
    #define PS_DEBUG_E              0

    #define EDITUSER_DEBUG          0
    #define EDITUSER_DEBUG_E        0

    #define HOME_DEBUG              1
    #define HOME_DEBUG_E            1

    #define LOGIN_DEBUG             0
    #define LOGIN_DEBUG_E           0

    #define SERIALPORT_DEBUG        0
    #define SERIALPORT_DEBUG_E      0

    #define SIGNUP_DEBUG            0
    #define SIGNUP_DEBUG_E          0

    #define UTILITY_DEBUG           0
    #define UTILITY_DEBUG_E         0

    #define TIMER_DEBUG             0

    #define DEVICESHANDLER_DEBUG    0
    #define DEVICESHANDLER_DEBUG_E  0

    #define PCI1758_DEBUG           0
    #define PCI1758_DEBUG_E         1

    #define PCI7444_DEBUG           0
    #define PCI7444_DEBUG_E         1

    #define ISOLATIONP2C_DEBUG      1
    #define ISOLATIONP2C_DEBUG_E    1

    #define ISOLATIONP2P_DEBUG      1
    #define ISOLATIONP2P_DEBUG_E    1

    #define INSULATIONP2C_DEBUG     1
    #define INSULATIONP2C_DEBUG_E   1

    #define LEVELSHIFTER_DEBUG      1
    #define LEVELSHIFTER_DEBUG_E    1

    #define GLOBAL_C_DEBUG          0
    #define GLOBAL_C_DEBUG_E        0

    #define POLARITY_DEBUG          1
    #define POLARITY_DEBUG_E        1

    #define LOAD_DEBUG              1
    #define LOAD_DEBUG_E            1

    #define RELAYTEST_DEBUG         1
    #define RELAYTEST_DEBUG_E       1

    #define SELFTEST_DEBUG          1
    #define SELFTEST_DEBUG_E        1

    #define JIG_SELFTEST_DEBUG      1
    #define JIG_SELFTEST_DEBUG_E    1

    #define BASE64_DEBUG            0
    #define BASE64_DEBUG_E          0

    #define EMIEMC_DEBUG            1
    #define EMIEMC_DEBUG_E          1

    #define ISOLATIONP2P_V1_DEBUG   1
    #define ISOLATIONP2P_V1_DEBUG_E 1

    #define MANUALISOLATION_DEBUG   1
    #define MANUALISOLATION_DEBUG_E 1

    #define MANUAL_POLARITY_DEBUG   1
    #define MANUAL_POLARITY_DEBUG_E 1
#else
    #define MAIN_DEBUG              0
    #define SPECIAL_DEBUG           1

    #define DATABASE_DEBUG          0
    #define DATABASE_DEBUG_E        0

    #define DMM_DEBUG               0
    #define DMM_DEBUG_E             0

    #define IRMETER_DEBUG           0
    #define IRMETER_DEBUG_E         0

    #define PS_DEBUG                0
    #define PS_DEBUG_E              0

    #define EDITUSER_DEBUG          0
    #define EDITUSER_DEBUG_E        0

    #define HOME_DEBUG              0
    #define HOME_DEBUG_E            0

    #define LOGIN_DEBUG             0
    #define LOGIN_DEBUG_E           0

    #define SERIALPORT_DEBUG        0
    #define SERIALPORT_DEBUG_E      0

    #define SIGNUP_DEBUG            0
    #define SIGNUP_DEBUG_E          0

    #define TIMER_DEBUG             0

    #define DEVICESHANDLER_DEBUG    0
    #define DEVICESHANDLER_DEBUG_E  0

    #define ISOLATIONP2C_DEBUG      0
    #define ISOLATIONP2C_DEBUG_E    0

    #define ISOLATIONP2P_DEBUG      0
    #define ISOLATIONP2P_DEBUG_E    0

    #define INSULATIONP2C_DEBUG     0
    #define INSULATIONP2C_DEBUG_E   0

    #define PCI1758_DEBUG           0
    #define PCI1758_DEBUG_E         0

    #define PCI7444_DEBUG           0
    #define PCI7444_DEBUG_E         0

    #define LEVELSHIFTER_DEBUG      0
    #define LEVELSHIFTER_DEBUG_E    0

    #define GLOBAL_C_DEBUG          0
    #define GLOBAL_C_DEBUG_E        0

    #define POLARITY_DEBUG          0
    #define POLARITY_DEBUG_E        0

    #define LOAD_DEBUG              0
    #define LOAD_DEBUG_E            0

    #define RELAYTEST_DEBUG         0
    #define RELAYTEST_DEBUG_E       0

    #define SELFTEST_DEBUG          0
    #define SELFTEST_DEBUG_E        0

    #define JIG_SELFTEST_DEBUG      0
    #define JIG_SELFTEST_DEBUG_E    0

    #define BASE64_DEBUG            0
    #define BASE64_DEBUG_E          0

    #define EMIEMC_DEBUG            0
    #define EMIEMC_DEBUG_E          0

    #define ISOLATIONP2P_V1_DEBUG   0
    #define ISOLATIONP2P_V1_DEBUG_E 0

    #define MANUALISOLATION_DEBUG   0
    #define MANUALISOLATION_DEBUG_E 0

    #define MANUAL_POLARITY_DEBUG   0
    #define MANUAL_POLARITY_DEBUG_E 0
#endif
#endif // DEBUGFLAG_H
