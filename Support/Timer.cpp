/*************************************  FILE HEADER  *****************************************************************
*
*  File name - Timer.cpp
*  Creation date and time - 31-AUG-2019 , SAT 4:56 PM IST
*  Author: - Manoj
*  Purpose of the file - All timer handling modules will be implemented .
*
**********************************************************************************************************************/
/********************************************************
 Inclusion of header file
 ********************************************************/
#include "Timer.h"
#include "debugflag.h"

/********************************************************
 MACRO Definition
 ********************************************************/
#if TIMER_DEBUG
    #define _qDebug(s) qDebug()<<"\n"<<s
#else
    #define _qDebug(s); {}
#endif

/********************************************************
 Function Definition
 ********************************************************/
/*************************************************************************
 Function Name - Timer
 Parameter(s)  - void
 Return Type   - void
 Action        - Creates the object.
 *************************************************************************/
Timer::Timer()
{
    _qDebug("inside Timer Constructor.");
}

/*************************************************************************
 Function Name - ~Timer
 Parameter(s)  - void
 Return Type   - void
 Action        - Destroys the object.
 *************************************************************************/
Timer::~Timer()
{
    _qDebug("inside Timer Destructor.");
}

/*************************************************************************
 Function Name - delayInms
 Parameter(s)  - int
 Return Type   - void
 Action        - Creates delay in milliseconds as given in timeInms.
 *************************************************************************/
void Timer::delayInms(int timeInms)
{
    _qDebug("inside void Timer::delayInms(int timeInms).");

    QTime dieTime= QTime::currentTime().addMSecs(timeInms);
    while( QTime::currentTime() < dieTime )
    QCoreApplication::processEvents(QEventLoop::AllEvents, 100);
}
