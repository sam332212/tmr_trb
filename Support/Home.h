/*************************************  FILE HEADER  *****************************************************************
*
*  File name - Home.h
*  Creation date and time - 21-AUG-2019 , SAT 03:56 PM IST
*  Author: - Manoj
*  Purpose of the file - All home page modules will be declared .
*
**********************************************************************************************************************/
#ifndef HOME_H
#define HOME_H

/********************************************************
 Inclusion of header file
 ********************************************************/
#include <QMainWindow>
#include <QDebug>
#include <QCloseEvent>
#include <QMessageBox>
#include <QDir>
#include <QFileDialog>
#include <QSpinBox>
#include <QIcon>
#include <QCheckBox>
#include <QLCDNumber>
#include <QShortcut>
#include <QHBoxLayout>

#include "Utility.h"
#include "UserControl/Login.h"
#include "Serials/DMM.h"
#include "Serials/IRMeter.h"
#include "Serials/PowerSupply.h"
#include "Serials/DevicesHandler.h"
#include "PCI/7444/Pci7444.h"
#include "TestCases/IsolationP2C.h"
#include "TestCases/IsolationP2P.h"
#include "TestCases/IsolationP2P_V1.h"
#include "TestCases/InsulationP2C.h"
#include "TestCases/LevelShifter.h"
#include "TestCases/Polarity.h"
#include "TestCases/Load.h"
#include "Report/report_generator.h"
#include "TestCases/RelayTest.h"
#include "TestCases/SelfTest.h"
#include "TestCases/JigSelfTest.h"
#include "TestCases/EmiEmc.h"
#include "TestCases/ManualIsolation.h"
#include "TestCases/ManualPolarity.h"

/********************************************************
 Class Declaration
 ********************************************************/
namespace Ui {
class Home;
}

class Home : public QMainWindow
{
    Q_OBJECT

public:
    //member functions
    explicit Home(QWidget *parent = nullptr);
    ~Home();
    bool initHome();
    void test();
private slots:
    //member functions
    void on_pushButton_logout_clicked();
    void show_ui();
    void close_ui();
    void close_App();
    void selectAll();
    void unselectAll();
    void displayExtDevicesStatus();
    void updateTestTimer();
    void updateCount();
    void closeRelayTest();
    void closeSelfTest();
    void closeManualIsolationTest();
    void on_okPushButton_clicked();
    void on_cancelPushButton_clicked();
    void on_exportPushButton_clicked();
    void on_deletePushButton_clicked();
    void on_actionImport_triggered();
    void on_actionExit_triggered();
    void on_pushButton_run_stop_clicked();
    void on_action_triggered(QString menuTitle,QString action_str);
    void on_actionExport_triggered();
    void on_actionDelete_triggered();
    void on_actionSelect_All_triggered();
    void on_actionUnselect_All_triggered();
    void on_actionSave_Result_triggered();
    void on_actionSave_Pass_Result_triggered();
    void on_actionSave_Fail_Result_triggered();
    void on_actionAuto_Save_Report_after_Test_triggered();
    void on_actionAuto_Generate_Self_Check_Report_triggered();
    void on_actionEnable_Keyboard_Action_triggered();
    void on_actionAbout_triggered();
    void on_actionRelay_Test_triggered();
    void on_actionSelf_Test_triggered();
    void on_actionEnable_Report_LandScape_Mode_triggered();
    void on_actionENC_DB_triggered();
    void on_actionDEC_DB_triggered();
    void indexChanged(int index);
    void edPushButtonClicked();
    void edCancelPushButtonClicked();
    void on_comboBox_manualPolarity_currentIndexChanged(const QString &arg1);
    void on_actionSave_Table_triggered();

private:
    struct EncDec {
        QLabel* label_db = nullptr;
        QComboBox* comboBox_db = nullptr;
        QCheckBox* checkbox_dbSelectAll = nullptr;
        QLabel* label_table = nullptr;
        QComboBox* comboBox_table = nullptr;
        QCheckBox* checkbox_tableSelectAll = nullptr;
        QPushButton* pushButton_ed = nullptr;
        QPushButton* pushButton_cancel = nullptr;
        QLabel* label_blank = nullptr;

        QHBoxLayout* hBoxLayout1 = nullptr;
        QHBoxLayout* hBoxLayout2 = nullptr;
        QHBoxLayout* hBoxLayout3 = nullptr;
        QVBoxLayout* vBoxLayout = nullptr;

        QWidget* widget = nullptr;

        QVector<QStringList> dbTables;
        QVector<Database*> dbObj;
    };

private:
    //member variables
    Ui::Home *ui;
    Login* loginObj = nullptr;
    RelayTest* relayTest = nullptr;
    ManualIsolation* manualIsolationObj = nullptr;
    SelfTest* selfTest = nullptr;
    JigSelfTest* jigSelfTest = nullptr;
    Database* homeDbObj = nullptr;
    Database* meterDbOdj = nullptr;
    Timer* timerObj = nullptr;
    QTimer* timerMeterCheckObj = nullptr;
    QTimer* testTimerObj = nullptr;
    QWidget* myWidgetObj = nullptr;
    QLabel* label_cycleObj = nullptr;
    QSpinBox* spinBox_cycleObj = nullptr;
    QComboBox* comboBox_tablesObj = nullptr;
    QLabel* blankLabel = nullptr;
    QPushButton* pushButton_okObj = nullptr;
    QPushButton* pushButton_cancelObj = nullptr;
    QPushButton* pushButton_deleteObj = nullptr;
    QPushButton* pushButton_exportObj = nullptr;
    QShortcut* saveResultKey = nullptr;
    QShortcut* savePassResultKey = nullptr;
    QShortcut* saveFailResultKey = nullptr;
    QShortcut* selectAllKey = nullptr;
    QShortcut* unSelectAllKey = nullptr;
    QShortcut* exitKey = nullptr;
    QMessageBox* stopMsgBox = nullptr;
    Utility utilObj;
    QFont _font;
    QVector<QCheckBox*> checkBoxLists;
    QVector<int32> psAddress;
    bool pauseFlag = false;

    QVector<QMenu*>uutQmenuLists;
    QVector<QAction*>testQactionLists;
    QVector<QStringList>unitTestDataTableLists;
    QVector<QStringList>unitTestLists;
    QVector<QStringList>testStartMessageLists;
    QVector<QStringList>testIntermediateMessageLists;
    QVector<QStringList>tableColumnWidthLists;
    QVector<Utility::ResetData>unitResetTableLists;
    QVector<QStringList>psTableData;
    QVector<QMap<QString, uint32>> pinMapData;
    QVector<QMap<QString, uint32>> dipMapData;
    QVector<QMap<QString, uint32>> dopMapData;
    QString homeDbConnection;
    QString meterDbConnection;
    QString uutTableName;
    QString psTableName;
    QString columnWidth;
    QStringList uutTestTableNameLists;
    QStringList unitMapTableNameLists;
    QString lastFilePath;
    QStringList hiddenDataHeaders;
    QVector<QStringList> hiddenData;
    QVector<QVector<QStringList>> testData;
    QStringList statusData;
    QMap<QString, QVector<QStringList>> manualData_Main;
    QMap<QString, QVector<QStringList>> manualDataHidden_Main;
    QMap<QString, QVector<QStringList>> manualData_StandBy;
    QMap<QString, QVector<QStringList>> manualDataHidden_StandBy;

    EncDec* encDec = nullptr;

    bool isAutoSaveReport      = true;
    bool isAutoSaveSCReport    = true;
    bool isReportLandscapeMode = true;
    bool isDmmSelfTestDone     = false;
    bool isIRMeterSelfTestDone = false;
    bool isPsSelfTestDone      = false;
    bool isFreshStart = true;//This flag set:true tells that the home UI started newly or after refresh
    bool isUpdatedOnce = false;//This flag says Home UI is updated once after log In
    bool isEdit = false;//This flag says the table in db has been edited(import or delete)
    volatile bool isDmmConnected = true;//This flag says the previous status of dmm connectivity, default is true
    volatile bool isIrMeterConnected = true;//This flag says the previous status of dmm connectivity, default is true
    volatile bool isChecking = false;//This flag is used to synchonize between this to DeviceHandler

    QVector<bool> psStatus;//This value says the status of PS connectivity, default is true

    int uutIndex = 0;
    uint timeElapSec;
    uint timeElapMin;
    uint timeElapHour;

    //member functions
    void closeEvent (QCloseEvent *event);
    void initGlobalParameters();
    void initDeviceHandler();
    void startUpdateTimerLabel();
    void stopUpdateTimerLabel();
    QStringList toStringList(const QList<QByteArray> list);
    void update_home_ui();
    QString get_file_name(QString file_path);
    void disconnectKeys();
    void connectKeys();
    void runTest(int cycleCount);
    void cleanMyWidget();
    void resetUIComponents();
    void initMeters();
    void initPS();
    void generateReport(QVector<QVector<QStringList>> reportData,QStringList testStatus,QString path);
    void saveTable();
    void getFailData(QVector<QVector<QStringList>>& failData, QStringList& testStatus);
    void getPassData(QVector<QVector<QStringList>>& passData, QStringList& testStatus);
    void enableComponents(bool status);
    void enableResultSaving(bool status);
    void updateUserSelections();
    void handleSelection(QAction* action,int rowNumber);
    void runDmmSelfTest(bool isConnected);
    void runIRMeterSelfTest(bool isConnected);
    void runPsSelfTest(QVector<bool> status);
    void setHeader();
    void updateManualTable(QTableWidget* table,QVector<QStringList> data,QString columnWidth);

    bool initDBs();
    bool updateSystemParameters();
    bool isValidChecksum();
    bool loadTableFromDb(QString tableNamee,QString columnWidth);
    bool loadManualTestTableFromDb(QString tableNamee,QString columnWidth);
    bool loadManualIsolationTest();
    bool onLoadPolarityPS();
    bool offLoadPolarityPS();
    bool onLevelShifterPs();
    bool offLevelShifterPs();
    bool isCredentialsEmpty(QString& warningText);
    bool encodeData(Database* db,QStringList tableNames);
    bool encodeData(Database* db,QString tableName);
    bool decodeData(Database* db,QStringList tableNames);
    bool decodeData(Database* db,QString tableName);
    bool isAtleastOneRowSelected(QTableWidget* table);

    QString extractValueFromLabel(QString str);
    Utility::ResetData getUnitResetData(QStringList headers, QVector<QStringList> resetTableData);
};

#endif // HOME_H
