/*************************************  FILE HEADER  *****************************************************************
*
*  File name - Global.h
*  Creation date and time - 07-JUN-2020 , SUN 07:00 AM IST
*  Author: - Manoj
*  Purpose of the file - All Global components will be declared .
*
**********************************************************************************************************************/
#ifndef GLOBALL_H
#define GLOBALL_H

/********************************************************
 MACRO Definition
 ********************************************************/
#define INPUT_POWER_SUPPLY_PS   1
#define JIG_POWER_SUPPLY_PS     2
#define LEVEL_SHIFTER_PS        3
#define COIL_SUPPLY_PS          4
#define STATUS_SUPPLY_PS        5
#define PS1                     INPUT_POWER_SUPPLY_PS
#define PS2                     JIG_POWER_SUPPLY_PS
#define PS3                     LEVEL_SHIFTER_PS
#define PS4                     COIL_SUPPLY_PS
#define PS5                     STATUS_SUPPLY_PS
#define TEST_DELAY_MS           20//It will be used before measurement, after relays On. Relay Bounce Stability delay.
#define LATCH_RELAY_DELAY_MS    20//It will be used during Latch Relay On & Off in Load Test and Polarity Test
#define LOAD_DELAY_MS           100
#define ETH_DELAY_MS            20 //It will be used as earthing delay after each test in Insulation Test
#define DMM_READ_MAX_DELAY_MS   3000
#define DWELL_TIME              0.2 //15Nov2021 changed to 0.2 from 0.5
#define ON                      1
#define OFF                     0
#define PAUSE_POINT_STR         "@"
#define PASS                    "PASS"
#define FAIL                    "FAIL"
#define AVERAGE_ENABLE          1
#define PCI1758DOPS             512
#define PCI1758DIPS             256
#define PCI7444DOPS             256
#define UUT_TABLE_COLUMNS       6
#define UUT_TEST_TABLE_COLUMNS  5
#define SYSTEMPARAMS_TABLE_ROWS 7
#define CHASSIS                 "Chassis"
#define SELECTION_ROWS          4
#define TEST_CYCLE_PRESENT      0

#define COIL_SUPPLY_PS_CURRENT_EMIEMC_TEST  7 //IN Amp
#define COIL_SUPPLY_PS_DEFAULT_CURRENT      3 //IN Amp
#define COIL_SUPPLY_PS_DEVICE_NUMBER        4

#define METER_TABLE_NAME "MeterSettings"
#define SELECTION_TABLE  "UserSelection"
#define MESSAGE_TITLE    "TMR_TRB"
#define SYSTEM_PARAMETER_TABLE "SYSTEM_PARAMETERS"

#define DEFAULT_GUI_VERSION "V1.0"
#define DEFAULT_BUILD_DATE  "22/11/2022"

#define DEFAULT_SOUND_FILE  ":/Sounds/Error.wav"
#define SOUND_FILE          ":/Sounds/Exclamation.wav"

#define PLAY G::playSound();
#define INFO G::playSound(SOUND_FILE);

/********************************************************
 Inclusion of header file
 ********************************************************/
#include <QTableWidget>
#include <QLabel>
#include <QProgressBar>
#include <QScrollBar>
#include <QFont>
#include <QCheckBox>
#include <QSoundEffect>
#include "Support/debugflag.h"
#include "Serials/DMM.h"
#include "Serials/IRMeter.h"
#include "Serials/PowerSupply.h"
#include "Serials/DevicesHandler.h"
#include "PCI/1758/Pci1758.h"
#include "PCI/7444/Pci7444.h"

/********************************************************
 Class Declaration
 ********************************************************/
class Global
{
public:
    enum PCI{
        P1758 = 0,
        P7444
    };

    Global();
    ~Global();

    static DMM* dmmObj;
    static IRMeter* irMeterObj;
    static PowerSupply* psObj;
    static DevicesHandler* handlerObj;
    static Pci1758* pci1758Obj;
    static Pci7444* pci7444Obj;
    static QTableWidget* testTable;
    static QLabel* testResultStatus;
    static QLabel* testRunStatus;
    static QLabel* cycleDisplay;
    static QPushButton* runStopButton;
    static QVector<QLabel*> psStatus;
    static QLabel* testVoltage;
    static QProgressBar* progressBar;
    static QTimer* testTimer;
    static QTimer* cycleTimer;
    static bool stopTest;
    static QFont myFont;
    static QString testStartMessage;
    static QString testInterMediateMessage;
    static double calibResistance;
    static double calibVoltage;
    static long   measCount;
    static long   testCount;
    static QString passSSLabel;
    static QString failSSLabel;
    static QMessageBox* msgBox;
    static QMessageBox* testMsgBox;
    static QString appVersion;
    static QString builtDate;
    static QString expectedChecksum;
    static QString softwareChecksum;
    static QString startDate;
    static QString endDate;
    static QString startTime;
    static QString endTime;
    static bool checksumValidate;

    static void cleanUi();
    static void setScrollToZero();
    static void manageScrolling(int index,int rowNo);
    static void manageScrolling(QTableWidget *table,int index,int rowNo);
    static void enableTableHeaderUnderline(QTableWidget *table);
    static void updateProgressBar(QProgressBar* progressBar,int curCount,int maxCount);
    static int writeDOP(uint32_t dopNo , uint8_t onOff , int pci=PCI::P1758);
    static int writeDOP(QVector<uint32_t> dopNos , uint8_t onOff , int pci=PCI::P1758);
    static int writeDOP(QStringList dopNos , uint8_t onOff,int pci);
    static int readDIP(uint32_t dipNo , uint8_t& state ,int pci=PCI::P1758);
    static int readDIP(QStringList dipNos , QVector<uint8_t>& states, int pci=PCI::P1758);
    static int readDIP(QVector<uint32_t> dipNos , QString& states, int pci=PCI::P1758);
    static int readDIP(QVector<uint32_t> dipNos , QVector<uint8_t>& states, int pci=PCI::P1758);
    static bool getDop(const QVector<QStringList> dataMap,QString key,uint32* dop);
    static bool getDops(const QVector<QStringList> dataMap,QString keys,QVector<uint32_t>& dops,QString splitter);
    static bool getDops(const QVector<QStringList>& dataMap,QStringList connPins,QStringList& dops);
    static void getConnPin(QString str,QStringList& data);
    static void getConnPin(QString str,QString& conn, QString& pin, QString splitter);
    static bool onDops (const QMap<QString, uint32>& dopMap,QString connPins,int pci=PCI::P1758);
    static bool offDops(const QMap<QString, uint32>& dopMap,QString connPins,int pci=PCI::P1758);
    static bool updateResultStatus(bool status,bool& isPassUpdated,bool& isFailUpdated);
    static bool updateResult(bool status, int row, int column, bool testStatus);
    static bool updateResultStatus(QLabel* label,bool status, bool &isPassUpdated, bool &isFailUpdated);
    static void showInfoMessage(QString imagePath);
    static void showInfoMessage(QString title,QString message);
    static void closeTestMessage();
    static void showInfoMessage(QMessageBox* msgBox,QString title,QString message);
    static void playSound(QString file = DEFAULT_SOUND_FILE);
    static void enableRunButton(bool status);

private:
    static int prevSlidebarPos;
    static int curSlidebarPos;
    static QSoundEffect effect;

};

#endif // GLOBALL_H
