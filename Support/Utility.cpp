/*************************************  FILE HEADER  *****************************************************************
*
*  File name - Utility.cpp
*  Creation date and time - 16-NOV-2019 , SAT 9:53 AM IST
*  Author: - Manoj
*  Purpose of the file - All utility modules i.e usable to others, will be implemented .
*                        This file will be independent of all source code files.
*
**********************************************************************************************************************/
/********************************************************
 Inclusion of header file
 ********************************************************/
#include "Utility.h"
#include "debugflag.h"
#include <QFile>
#include <QCryptographicHash>
#include <QCoreApplication>
#include <QMessageBox>
#include <QHeaderView>
#include <QDate>

/********************************************************
 MACRO Definition
 ********************************************************/
#if UTILITY_DEBUG
    #define _qDebug(s) qDebug()<<"\n"<<s
#else
    #define _qDebug(s) {}
#endif

#if UTILITY_DEBUG_E
    #define _qDebugE(p,q,r,s) qDebug()<<p<<q<<r<<s
#else
    #define _qDebugE(p,q,r,s); {}
#endif

#define OPEN_RES_LIM     20000000.0
#define HIGHEST_RES_LIM  1000000000.0
#define OPEN_RES_DISPLAY "> 20 M"
#define HIGHEST_RES_DISPLAY  "> 1 G"
#define PASS  true
#define FAIL  false

/********************************************************
 Function Definition
 ********************************************************/
/*************************************************************************
 Function Name - Utility
 Parameter(s)  - void
 Return Type   - void
 Action        - Creates the object.
 *************************************************************************/
Utility::Utility()
{
    _qDebug("Inside Utility Constructor.");

    _headers1.measuredValue    = "Measured_Value";
    _headers1.result           = "Result";
    _headers1.remark           = "Remarks";

    _hidden1.dopHigh1         = "DOP_HIGH_1";
    _hidden1.dopLow1          = "DOP_LOW_1";
    _hidden1.dopHigh2         = "DOP_HIGH_2";
    _hidden1.dopLow2          = "DOP_LOW_2";
    _hidden1.INsToHigh        = "Outputs_High";
    _hidden1.INsToLow         = "Outputs_Low";
    _hidden1.expectedValue    = "Expected_Value";
    _hidden1.dips             = "DIPS";
    _hidden1.dipsExpValue     = "Expected_DIPS_Value";
    _hidden1.dipsValueAfter   = "After_DIPS_Value";
    _hidden1.outputValueAfter = "Off_Output_Value";

    _hidden2.            dopHigh = "Outputs_High";
    _hidden2.             dopLow = "Outputs_Low";
    _hidden2.                dop = "DOP_Conn_Pin";
    _hidden2.      expectedValue = "Expected_Value";
    _hidden2.expectedValueBefore = "Expected_Value_Before_DOP";
    _hidden2. expectedValueAfter = "Expected_Value_After_DOP";
}

/*************************************************************************
 Function Name - ~Utility
 Parameter(s)  - void
 Return Type   - void
 Action        - Destroys the object.
 *************************************************************************/
Utility::~Utility()
{
    _qDebug("Inside Utility Destructor.");
}

/*************************************************************************
 Function Name - convertToDouble
 Parameter(s)  - QByteArray
 Return Type   - double
 Action        - Converts given data to double type and returns double type.
 *************************************************************************/
double Utility::convertToDouble(QByteArray data)
{
    _qDebug("Inside double Utility::convertToDouble(QByteArray data).");
    //Ex- Res:+9.90000000E+37,Vol:+1.14743000E-03,Cur:-1.00000000E-10 (open condition)
    double value = 0.0;
    int dataLength;

    dataLength = data.length();

    _qDebugE("data:[",data,"]","");
    _qDebugE("Data length:",dataLength,"","");

    if(dataLength == 10)
    {
        if(data.at(0) == '0' && ((data.at(1) == 'x') || data.at(1) == 'X'))
        {
            data.remove(0,2);
            hexToFloat.data[3] = static_cast<char>(parse_char(data.at(0)) * 0x10 + parse_char(data.at(1)));
            hexToFloat.data[2] = static_cast<char>(parse_char(data.at(2)) * 0x10 + parse_char(data.at(3)));
            hexToFloat.data[1] = static_cast<char>(parse_char(data.at(4)) * 0x10 + parse_char(data.at(5)));
            hexToFloat.data[0] = static_cast<char>(parse_char(data.at(6)) * 0x10 + parse_char(data.at(7)));
        }
        else
        {
            _qDebugE("Invalid Hex Data Input.","Input Data:[",data,"]");

            hexToFloat.data[3] = 0;
            hexToFloat.data[2] = 0;
            hexToFloat.data[1] = 0;
            hexToFloat.data[0] = 0;
        }
        value = hexToFloat.doubleValue;
    }
    else if(dataLength == 8)
    {
        hexToFloat.data[3] = static_cast<char>(parse_char(data.at(0)) * 0x10 + parse_char(data.at(1)));
        hexToFloat.data[2] = static_cast<char>(parse_char(data.at(2)) * 0x10 + parse_char(data.at(3)));
        hexToFloat.data[1] = static_cast<char>(parse_char(data.at(4)) * 0x10 + parse_char(data.at(5)));
        hexToFloat.data[0] = static_cast<char>(parse_char(data.at(6)) * 0x10 + parse_char(data.at(7)));
        value = hexToFloat.doubleValue;
    }
    else if(dataLength == 4)
    {
        hexToFloat.data[3] = data.at(0);
        hexToFloat.data[2] = data.at(1);
        hexToFloat.data[1] = data.at(2);
        hexToFloat.data[0] = data.at(3);   
        value = hexToFloat.doubleValue;
    }
    else
    {
        QString string = converthexQByteArrayToQString(data);
        QString exp;
        exp.clear();
        _qDebugE("string:[",string,"]","");

        string.remove("\n");
        string.remove("\r");
        _qDebugE("Formatted string:[",string,"]","");

        if(string.contains("e") || string.contains("E"))
        {
            if(string.contains("e"))
            {
                _qDebugE("String contains e","","","");
                int index = string.indexOf("e");
                int len = string.count() - index;
                for(int i=index; i<string.count(); i++)
                {
                    exp += string.at(i);
                }
                string.remove(index, len);
                _qDebugE("Exp is:",exp," and String is:",string);
            }
            else
            {
                _qDebugE("String contains E","","","");
                int index = string.indexOf("E");
                int len = string.count() - index;
                for(int i=index; i<string.count(); i++)
                {
                    exp += string.at(i);
                }
                string.remove(index, len);
                _qDebugE("Exp is:",exp," and String is:",string);
            }
        }
        value = string.toDouble();

        //exp
        if(!exp.isEmpty())
        {
            //exp is in form E+02 or E-02
            int expNumber;
            exp.remove(0,1);
            expNumber = exp.toInt();
            value = value * (pow(10, expNumber));
        }
    }

    _qDebugE("value: [",QString::number(static_cast<double>(value), 'f', 2),"]","");//or-1
    //_qDebugE("value: [",QString("%1").arg(static_cast<double>(value)),"]","");//or-2
    //_qDebugE("value: [",QString::number(static_cast<double>(value),'g', 8),"]","");//or-3

    /* Test input:
     *  QByteArray array;
        array.clear();
        array.push_back(static_cast<char>(0x42));
        array.push_back(static_cast<char>(0x36));
        array.push_back(static_cast<char>(0xAE));
        array.push_back(static_cast<char>(0x14));
        or/
        array.push_back("4236ae14");
        or/
        array.push_back("0x4236ae14");

        Test output: or-1:"45.67" , or-2:"45.67" , or-3:"45.669998"
     */
    /* Test input: +9.90000000E+37
     * OP:
     * value: [ "98999992879758482692073965491767476224.00" ]
     * value: [ "9.9e+37" ]
     * value: [ "9.8999993e+37" ]
     */

    return value;
}

/*************************************************************************
 Function Name - toStringList
 Parameter(s)  - const QList<QByteArray>
 Return Type   - QStringList
 Action        - This function converts QList to QstringList.
 *************************************************************************/
QStringList Utility::toStringList(const QList<QByteArray> list)
{
    _qDebug("inside void Utility::toStringList(const QList<QByteArray> list)");

    QStringList strings;
    foreach (const QByteArray &item, list) {
        strings.append(QString::fromLocal8Bit(item)); // Assuming local 8-bit.
    }
    return strings;
}

/*************************************************************************
 Function Name - convertToQstring
 Parameter(s)  - double, int8_t
 Return Type   - QString
 Action        - Converts given double type data to Qstring till the precision
                 given.
 *************************************************************************/
QString Utility::convertToQstring(double value,int8_t precision)
{
    //return QString::number(value,'f',4); //or-
    return QString("%1").arg(value,0,'f', precision);
}

/*************************************************************************
 Function Name - convertToQstring
 Parameter(s)  - int
 Return Type   - QString
 Action        - Converts given int type data to Qstring.
 *************************************************************************/
QString Utility::convertToQstring(int value)
{
    return QString::number(value);
}

/*************************************************************************
 Function Name - parse_char
 Parameter(s)  - char
 Return Type   - int
 Action        - Parses each textual character as a number.
 *************************************************************************/
int Utility:: parse_char(char c)
{
    if ('0' <= c && c <= '9') return c - '0';
    if ('a' <= c && c <= 'f') return 10 + c - 'a';
    if ('A' <= c && c <= 'F') return 10 + c - 'A';

    return 0;
}

/*************************************************************************
 Function Name - convertQByteArrayToHexQString
 Parameter(s)  - QByteArray
 Return Type   - QString
 Action        - Converts given data to QString type and returns QString type.
 *************************************************************************/
QString Utility::convertQByteArrayToHexQString(QByteArray byteArray)
{
    _qDebug("Inside QString Utility::convertQByteArrayToHexQString(QByteArray byteArray).");

    unsigned char byteCH;
    QString hexData;
    QString tempStr;

    hexData.clear();
    hexData += "0x";
    //Ex- Input:"adcd1234", Output:"0X6164636431323334"
    for(int i=0; i<byteArray.length(); i++)
    {
        byteCH = static_cast<unsigned char>(byteArray.at(i));

        tempStr = QString::number(byteCH, 16);
        if(tempStr.length()==1)
        {
            tempStr.insert(0,'0');
        }
        hexData += tempStr;
    }
    hexData = hexData.toUpper();
    return hexData;
}

/*************************************************************************
 Function Name - converthexQByteArrayToQString
 Parameter(s)  - QByteArray
 Return Type   - QString
 Action        - Converts given hex data to QString type and returns QString type.
 *************************************************************************/
QString Utility::converthexQByteArrayToQString(QByteArray hexData)
{
    _qDebug("Inside QString Utility::converthexQByteArrayToQString(QByteArray hexData).");
    //Ex-Input:"ed8788dc",Output"ed8788dc"
    QString textValue;

    //textValue = QString(hexData.toHex()); //or-
    textValue = QString::fromLatin1(hexData.toStdString().c_str());
    return textValue;
}

/*************************************************************************
 Function Name - convertQStringToByteArray
 Parameter(s)  - QString
 Return Type   - QByteArray
 Action        - Converts given QString to QByteArray type and returns QByteArray type.
 *************************************************************************/
QByteArray Utility::convertQStringToByteArray(QString str)
{
    _qDebug("Inside QByteArray Utility::convertQStringToByteArray(QString str).");

    QByteArray byteArray;

    byteArray = str.toUtf8();

    _qDebugE("byteArray:[",byteArray,"]","");

    return byteArray;
}

/*************************************************************************
 Function Name - convertToQString
 Parameter(s)  - QByteArray
 Return Type   - int
 Action        - Converts given byteArray to int type and returns int type.
 *************************************************************************/
int Utility::convertToInt(QByteArray byteArray)
{
    _qDebug("Inside int Utility::convertToInt(QByteArray byteArray).");
    //Ex-Input:"",Output""

    bool ok;
    int intValue;

    intValue = byteArray.toHex().toInt(&ok,16);

    return intValue;
}


/*************************************************************************
 Function Name - converthexQStringToQByteArray
 Parameter(s)  - QString
 Return Type   - QByteArray
 Action        - Converts given hex data to QByteArray type and returns
                 QByteArray type.
 *************************************************************************/
QByteArray Utility::converthexQStringToQByteArray(QString hexData)
{
    _qDebug("Inside QByteArray Utility::converthexQStringToQByteArray(QString hexData).");

    QByteArray hexArray;
    QString tempStr;

    char ch;
    bool ok = true;
    hexArray.clear();

    //Ex-Input:"0X6164636431323334",Output:"adcd1234"
    if((hexData != "") && (hexData.length() > 3))//Minimum expected 0X00
    {
        //hexData.remove(0,2);//Removing first two characters(0X) or/
        hexData =  hexData.mid(2);//remove 0X

        hexData.replace(" ","");

        if((hexData.length() % 2) == 1)
        {
           _qDebugE("Invalid HexData length.","hexData:[",hexData,"].");
        }
        else
        {
            //validate the hex data
            for(int i = 0;i < hexData.length();i++)
            {
                ch = static_cast<char>(hexData.at(i).toLatin1());

                if(!(((ch >= '0') && (ch <= '9')) || ((ch >= 'A') && (ch <= 'F')) || ((ch >= 'a') && (ch <= 'f'))))
                {
                    _qDebugE("Invalid Hex character at ",i,".","");
                    ok = false;
                    break;
                }
            }

            if(ok)
            {
                //Now convert Hex_string to QByteArray
                hexArray = QByteArray::fromHex(hexData.toLatin1());
                _qDebugE("hexArray:[",hexArray,"].","");
            }
        }
    }

    return hexArray;
}

/*************************************************************************
 Function Name - convertHexToDecimal
 Parameter(s)  - QString
 Return Type   - uint32_t
 Action        - Converts given hex value to decimal.
 *************************************************************************/
uint32_t Utility::convertHexToDecimal(QString hexData)
{
    _qDebug("Inside uint32_t Utility::convertHexToDecimal(QString hexData).");

    //Ex-Input:"0xED8788DC",Output:3985082588
    bool ok;
    uint32_t decimalValue = hexData.toUInt(&ok,16);

    return decimalValue;
}

/*************************************************************************
 Function Name - convertDecimalToHex
 Parameter(s)  - uint32_t
 Return Type   - QString
 Action        - Converts given decimal value to hexadecimal .
 *************************************************************************/
QString Utility::convertDecimalToHex(uint32_t decimalValue)
{
    _qDebug("Inside QString Utility::convertDecimalToHex(uint32_t decimalValue).");

    //Ex-Input:3985082588,Output:"0xED8788DC"
    QString hexadecimal;
    hexadecimal.setNum(decimalValue,16);
    hexadecimal = hexadecimal.toUpper();
    hexadecimal.push_front("0x");

    return hexadecimal;
}

/*************************************************************************
 Function Name - setPrecision
 Parameter(s)  - QString , int
 Return Type   - void
 Action        - Sets precision in valueString on the basis of given precision .
 *************************************************************************/
void Utility::setPrecision(QString &valueString, int precision)
{
    _qDebug("Inside void Utility::setPrecision(QString &valueString, unsigned precision).");

    int indexOfDecimal = 0;
    int digitsAfterDecimal;
    int indexOfRemoveBegins;
    int lengthToRemove;

    if(valueString.contains("."))
    {
        indexOfDecimal = valueString.indexOf(".");
    }

    digitsAfterDecimal = valueString.length() - indexOfDecimal - 1;

    if((indexOfDecimal>0) && (digitsAfterDecimal>precision))
    {
        indexOfRemoveBegins = indexOfDecimal+precision+1;
        lengthToRemove = valueString.count() - indexOfRemoveBegins;
        if((indexOfRemoveBegins < valueString.length()))
            valueString.remove(indexOfRemoveBegins, lengthToRemove);
    }
}

/*************************************************************************
 Function Name - setPrecision
 Parameter(s)  - QString , int, int
 Return Type   - void
 Action        - Sets precision in valueString on the basis of given precision .
 *************************************************************************/
void Utility::setPrecision(QString &valueString,int fixedDec,int precision)
{
    _qDebug("Inside void Utility::setPrecision(QString &valueString,int fixedDec,int precision).");

    int indexOfDecimal = 0;
    int digitsAfterDecimal;
    int digitsBeforeDecimal = 0;
    int indexOfRemoveBegins;
    int lengthToRemove;

    if(valueString.contains("."))
    {
        indexOfDecimal = valueString.indexOf(".");
        digitsBeforeDecimal = indexOfDecimal;
    }

    digitsAfterDecimal = valueString.length() - indexOfDecimal - 1;

    if((indexOfDecimal>0) && (digitsAfterDecimal>precision))
    {
        indexOfRemoveBegins = indexOfDecimal+precision+1;
        lengthToRemove = valueString.count() - indexOfRemoveBegins;
        if((indexOfRemoveBegins < valueString.length()))
            valueString.remove(indexOfRemoveBegins, lengthToRemove);
    }

    if(fixedDec > digitsBeforeDecimal)
    {
        for(int i = 0;i < (fixedDec - digitsBeforeDecimal);i++) valueString.push_front(QString::number(0));
    }
}

QString Utility::nano(double value)
{
    _qDebug("Inside QString Utility::nano(double value).");

    QString valueString = QString::number(value * pow(10,9));

    if(QString::compare(valueString,"0") == 0)
    {
        _qDebugE("valueString:[",valueString,"]"," value string data is zero.");
        valueString.clear();
        valueString = "--";
    }
    else
    {
        setPrecision(valueString);
        valueString += " n";
    }

    return valueString;
}

QString Utility::micro(double value)
{
    _qDebug("Inside QString Utility::micro(double value).");

    QString valueString = QString::number(value * pow(10,6));
    setPrecision(valueString);
    valueString += " u";

    return valueString;
}

QString Utility::milli(double value)
{
    _qDebug("Inside QString Utility::milli(double value).");

    QString valueString = QString::number(value * pow(10,3));
    setPrecision(valueString);
    valueString += " m";

    return valueString;
}

QString Utility::kilo(double value)
{
    _qDebug("Inside QString Utility::kilo(double value).");

    QString valueString = QString::number(value / pow(10,3));
    setPrecision(valueString);
    valueString += " K";

    return valueString;
}

QString Utility::mega(double value)
{
    _qDebug("Inside QString Utility::mega(double value).");

    QString valueString = QString::number(value / pow(10,6));
    setPrecision(valueString);
    valueString += " M";

    return valueString;
}

QString Utility::giga(double value)
{
    _qDebug("Inside QString Utility::giga(double value).");

    QString valueString = QString::number(value / pow(10,9));
    qDebug()<<"valueString:"<<valueString;
    if(!valueString.contains("e+")) {
        setPrecision(valueString);
        valueString += " G";
    }
    else valueString += " ";
    return valueString;
}

QString Utility::getChecksumExe()
{
    QFile theFile(QCoreApplication::applicationFilePath());
    QByteArray thisFile;
    if (theFile.open(QIODevice::ReadOnly))
    {
        thisFile = theFile.readAll();
    }
    else
    {
        qDebug() << "Can't open file..."<<thisFile;
        QMessageBox::warning(nullptr,"Error!!","Can't open file...");
    }

    QString fileMd5 = QString(QCryptographicHash::hash((thisFile), QCryptographicHash::Md5).toHex());
    fileMd5 = "0x" + fileMd5.toUpper();
    //_qDebugE("The exe checksum value :",fileMd5,"","");
    return fileMd5;
}

QString Utility::convertToStd(double value)
{
    _qDebug("Inside QString Utility::convertToStd(double value).");

    if(value == 0.0) return QString::number(0);

    if(value<=(pow(10,-9)))
    {
        return nano(value);
    }
    else if(value<(pow(10,-3)) && value>=pow(10,-6))
    {
        return micro(value);
    }
    else if(value<(pow(10,0)) && value>=pow(10, -3))
    {
        return milli(value);
    }
    else if(value>=pow(10,3) && value<pow(10,6))
    {
        return kilo(value);
    }
    else if(value>=pow(10,6) && value<pow(10,9))
    {
        return mega(value);
    }
    else if(value>=pow(10,9))
    {
        return giga(value);
    }
    else
    {
        QString temp = QString::number(value);
        setPrecision(temp);
        temp += " ";
        return temp;
    }
}

QString Utility::convertToStd(double value, UNIT unit)
{
    _qDebug("Inside QString Utility::convertToStd(double value, UNIT unit).");
    QString temp;

    if(value<=(pow(10,-9)))
    {
        return nano(value);
    }
    else if(value<(pow(10,-3)) && value>pow(10,-6))
    {
        return micro(value);
    }
    else if(value<(pow(10,0)) && value>=pow(10, -3))
    {
        return milli(value);
    }
    else if(value>=pow(10,3) && value<pow(10,6))
    {
        return kilo(value);
    }
    else if(value>=pow(10,6) && value<pow(10,9))
    {
        return mega(value);
    }
    else if(value>=pow(10,9))
    {
        return giga(value);
    }
    else
    {
        temp = QString::number(value);
        setPrecision(temp);
        temp += " ";
    }

    if(unit == UNIT::OHM)
    {
        temp.append("Ω");
    }
    else
    {
        temp.append("V");
    }
    return temp;
}

QString Utility::removeSymbolsOtherThanNumbers(QString text)
{
    _qDebug("Inside QString Utility::removeSymbolsOtherThanNumbers(QString text).");
//    QVector<QChar> integers;
//    for(int i=48; i<=57; i++)
//    {
//        integers.append(i);
//    }
//    QVector<QChar> charToRemove;
//    for(int i=0; i<text.count(); i++)
//    {
//        if(!integers.contains(text.at(i)))
//        {
//            charToRemove.append(text.at(i));
//        }
//    }
//    for(int i=0; i<charToRemove.count(); i++)
//    {
//        text.remove(charToRemove.at(i));
//    }
    QString d = "Ω";
    if(text.contains("Ω") == true) qDebug()<<"Found["<<d<<"]";
    for(int i = 0;i < text.count();i++)
    {
        //qDebug()<<text[i];
        if(text[i] == '%')
        {
            qDebug()<<text[i];
        }
        else {
            qDebug("Not Found");
        }
    }
    return text;
}

/*************************************************************************
 Function Name - getLineDops
 Parameter(s)  - void
 Return Type   - void
 Action        - calculates dopToHigh,dopToLow,highDop and lowDop on the
                 basis of given dopToHigh,dopToLow.
 *************************************************************************/
void Utility::getLineDops(uint32_t* dopToHigh,uint32_t* dopToLow,uint32_t* highDop,uint32_t* lowDop)
{
    _qDebug("Inside void Utility::getLineDops(uint32_t* dopToHigh,uint32_t* dopToLow,uint32_t* highDop,uint32_t* lowDop).");
    if((*dopToHigh % 2) == 1) *highDop = RLC_P2A;
    else if((*dopToHigh % 2) == 0) {
        *dopToHigh = (*dopToHigh - 1);
        *highDop = RLC_Q2A;
    }

    if((*dopToLow % 2) == 0) *lowDop = RLC_S2B;
    else if((*dopToLow % 2) == 1) {
        *dopToLow = (*dopToLow + 1);
        *lowDop = RLC_R2B;
    }
    _qDebugE("dopToHigh:",*dopToHigh,", dopToLow:",*dopToLow);
    _qDebugE("highDop(P/Q):",*highDop,", lowDop(R/S):",*lowDop);
}

/*************************************************************************
 Function Name - getHighDops
 Parameter(s)  - void
 Return Type   - void
 Action        - calculates dopToHigh and highDop on thebasis of given dopToHigh.
 *************************************************************************/
void Utility::getHighDops(uint32_t* dopToHigh,uint32_t* highDop)
{
    _qDebug("Inside void Utility::getHighDops(uint32_t* dopToHigh,uint32_t* highDop).");
    if((*dopToHigh % 2) == 1) *highDop = RLC_P2A;
    else if((*dopToHigh % 2) == 0) {
        *dopToHigh = (*dopToHigh - 1);
        *highDop = RLC_Q2A;
    }

    _qDebugE("dopToHigh:",*dopToHigh,", highDop(P/Q):",*highDop);
}

/*************************************************************************
 Function Name - getLowDops
 Parameter(s)  - void
 Return Type   - void
 Action        - calculates dopToLow and lowDop on the basis of given dopToLow.
 *************************************************************************/
void Utility::getLowDops(uint32_t* dopToLow,uint32_t* lowDop)
{
    _qDebug("Inside void Utility::getLowDops(uint32_t* dopToLow,uint32_t* lowDop).");

    if((*dopToLow % 2) == 0) *lowDop = RLC_S2B;
    else if((*dopToLow % 2) == 1) {
        *dopToLow = (*dopToLow + 1);
        *lowDop = RLC_R2B;
    }

    _qDebugE("dopToLow:",*dopToLow,", lowDop(R/S):",*lowDop);
}

/*************************************************************************
 Function Name - getResult
 Parameter(s)  - QString, double
 Return Type   - bool
 Action        - Retrieves the expected value from given expValueStr and
                 compares with the given msrdValue. It returns true if the
                 comparision pass or else returns false.
 *************************************************************************/
bool Utility::getResult(QString expValueStr,double msrdValue)
{
    _qDebug("Inside bool Utility::getResult(QString expValueStr,double msrdValue).");
    QStringList str;
    QString prefix = nullptr;
    QString plusMinus = "±";
    double value = 0.00;

    bool status = FAIL;
    bool isGreater = false;
    bool isLess = false;

    int count = 0;
    _qDebugE("expValueStr:",expValueStr,", msrdValue:",msrdValue);
    if(expValueStr.count() > 0)
    {
        str = expValueStr.split(' ');
        count = str.count();
        if(expValueStr.contains(">") || expValueStr.contains("<"))
        {
            //Format EX- "> 20 MΩ" or "< 1 Ω" or "> 10 KΩ"
            if(count > 1) value = str.at(1).toDouble();
            if(count > 2) prefix = str[2].remove(str[2].count()-1,1);//Remove unit(Ex-Ω,V,A etc) and keep Prefix(Ex-G/M/K/n/u/m)

            if(str.at(0).compare(">") == 0) isGreater = true;
            else if(str.at(0).compare("<") == 0) isLess = true;

            value = getValueForPrefix(prefix,value);
            _qDebugE("getValueForPrefix:",value,"","");
            if(isGreater) {
                if(msrdValue > value) status = PASS;
            }
            else if(isLess) {
                if(msrdValue < value) status = PASS;
            }
        }
        else if(expValueStr.contains(plusMinus) && expValueStr.contains("%"))
        {
            //Format EX- "200 ± 5% V" or "2 ± 5% KΩ"
            double percentValue = 0;
            double lowValue,highValue;
            value = str.at(0).toDouble();
            if(count > 2) percentValue = str[2].remove(str[2].count()-1,1).toDouble();//Remove '%'
            if(count > 3) prefix = str[3].remove(str[3].count()-1,1);//Remove unit(Ex-Ω,V,A etc) and keep Prefix(Ex-G/M/K/n/u/m)

            value = getValueForPrefix(prefix,value);
            _qDebugE("getValueForPrefix:",value,"","");
            lowValue  = (value - (value * (percentValue/100)));
            highValue = (value + (value * (percentValue/100)));
            _qDebugE("lowValue:",lowValue,", highValue:",highValue);

            if((msrdValue >= lowValue) && (msrdValue <= highValue)) status = PASS;
        }
        else if(expValueStr.contains(plusMinus))
        {
            //Format EX- "32 ± 1 V" or "2 ± 5 KΩ"
            double offset = 0.00;
            double lowValue,highValue;
            value = str.at(0).toDouble();
            if(count > 2) offset = str[2].toDouble();//Remove '%'
            if(count > 3) prefix = str[3].remove(str[3].count()-1,1);//Remove unit(Ex-Ω,V,A etc) and keep Prefix(Ex-G/M/K/n/u/m)

            value = getValueForPrefix(prefix,value);
            _qDebugE("getValueForPrefix:",value,"","");
            lowValue  = (value - offset);
            highValue = (value + offset);
            _qDebugE("lowValue:",lowValue,", highValue:",highValue);

            if((msrdValue >= lowValue) && (msrdValue <= highValue)) status = PASS;
        }
    }

    return status;
}

/*************************************************************************
 Function Name - getLineDops
 Parameter(s)  - void
 Return Type   - void
 Action        - calculates dopToHigh,dopToLow,highDop and lowDop on the
                 basis of given dopToHigh,dopToLow.
 *************************************************************************/
double Utility::getValueForPrefix(QString prefix,double value)
{
    if(prefix.count() == 0) return value;
    else if(prefix == "n") return (value*0.000000001);
    else if(prefix == "u") return (value*0.000001);
    else if(prefix == "m") return (value*0.001);
    else if(prefix == "K") return (value*1000);
    else if(prefix == "M") return (value*1000000);
    else if(prefix == "G") return (value*1000000000);
    else return value;
}

/*************************************************************************
 Function Name - getDisplayValue
 Parameter(s)  - QString, double
 Return Type   - QString
 Action        - Returns display Value from given expValueStr.
 *************************************************************************/
QString Utility::getDisplayValue(QString expValueStr,double msrdValue)
{
    _qDebug("Inside bool QString::getDisplayValue(QString expValueStr,double msrdValue).");
    QStringList str;
    QString displayStr = "";
    QString ohmStr = "Ω";
    QString ohmStr1 = "Ω";
    QString plusMinusStr = "±";
    int count = 0;

    qDebug()<<"Utility::getDisplayValue:: expValueStr:"<<expValueStr<<", msrdValue:"<<QString::number(msrdValue, 'g', 8);
    if(expValueStr.count() > 0)
    {
        str = expValueStr.split(' ');
        count = str.count();

        if(str[0] == ">" || str[0] == "<")
        {
            if(msrdValue > OPEN_RES_LIM)
            {
                displayStr = OPEN_RES_DISPLAY;
                if(count > 2) displayStr = (displayStr + str[2].at(str[2].count() -1));
            }
            else if(msrdValue <= 0.0 && (!expValueStr.contains(ohmStr)) && (!expValueStr.contains(ohmStr1)))
            {
                if(str[0] == "<") displayStr = expValueStr;
                else {
                    displayStr = "0.00 m";
                    if(count > 2) displayStr = (displayStr + str[2].at(str[2].count() -1));
                }
            }
            else if(msrdValue < 1)
            {
                displayStr = "< 1 ";
                if(count > 2) displayStr = (displayStr + str[2].at(str[2].count() -1));
            }
        }
        else if(expValueStr.contains(plusMinusStr))
        {
            if(msrdValue > OPEN_RES_LIM)
            {
                displayStr = OPEN_RES_DISPLAY;
                if(count > 3) displayStr = (displayStr + str[3].at(str[3].count() -1));
            }
            else if(msrdValue <= 0.0)
            {
                displayStr = "0.00 m";
                if(count > 3) displayStr = (displayStr + str[3].at(str[3].count() -1));
            }
        }

        if(displayStr.size() == 0)
        {
            QString unit = expValueStr.remove(0,expValueStr.count()-1);
            displayStr = (convertToStd(msrdValue)+unit);
        }
    }
    return displayStr;
}

/*************************************************************************
 Function Name - getResistanceDisplayValue
 Parameter(s)  - double
 Return Type   - QString
 Action        - Returns display Value from given msrdValue.
 *************************************************************************/
QString Utility::getResistanceDisplayValue(double msrdValue)
{
    _qDebug("Inside bool QString::getResistanceDisplayValue(double msrdValue).");
    QStringList str;
    QString displayStr = "";
    QString ohmStr = "Ω";

    if(msrdValue > HIGHEST_RES_LIM) displayStr = (HIGHEST_RES_DISPLAY + ohmStr);
    else if(msrdValue < 1) displayStr = "< 1 " + ohmStr;

    if(displayStr.size() == 0) displayStr = (convertToStd(msrdValue)+ohmStr);

    return displayStr;
}

/*************************************************************************
 Function Name - getBinaryString
 Parameter(s)  - QString, int
 Return Type   - QString
 Action        - Converts each character(if matches among '0' & '9' or
                 'A/a' & 'F/f) in given str to digit number of binary digits.
                 Ex- str="123",digit=4 => op: 000100100011
                     str="1g3",digit=4 => op: 00010011
 *************************************************************************/
QString Utility::getBinaryString(QString str,int digit){
    QString binaryStr = "";
    int number;
    for(int i = 0;i < str.length();i++) {
        if((str.at(i) >= '0' && str.at(i) <= '9') ||
           (str.at(i).toUpper() >= 'A' && str.at(i).toUpper() <= 'F')) {
            switch(str.at(i).toUpper().toLatin1()) {
                case 'A' : number = 10; break;
                case 'B' : number = 11; break;
                case 'C' : number = 12; break;
                case 'D' : number = 13; break;
                case 'E' : number = 14; break;
                case 'F' : number = 15; break;
                default  : number = str.at(i).digitValue();
            }
            binaryStr.push_back(getBinaryString(number,digit));
        }
    }
    return binaryStr;
}

/*************************************************************************
 Function Name - getBinaryString
 Parameter(s)  - int, int
 Return Type   - QString
 Action        - Converts given value to digit number of binary digits.
                 Ex- value=1 ,digit=4 => op: 0001
                     value=10,digit=8 => op: 00001010
 *************************************************************************/
QString Utility::getBinaryString(int value,int digit){
    QString binaryStr = "";
    for (int i = 0; i < digit; i++) binaryStr.push_front(QString::number((value>>i) & 0x01));
    return binaryStr;
}

/*************************************************************************
 Function Name - autofitTable
 Parameter(s)  - QTableWidget*, QMap<int,int>
 Return Type   - void
 Action        - fits the given table to window.
 *************************************************************************/
 void Utility::autofitTable(QTableWidget* table,const QStringList columnWidths)
 {
     int totalColumns = table->horizontalHeader()->count();
     int totalWidths = columnWidths.count();

     for(int colNo = 0;colNo < totalColumns;colNo++) {
        if(colNo < totalWidths) table->setColumnWidth(colNo,columnWidths.at(colNo).toInt());
     }
     table->resizeRowsToContents();
     table->setSizeAdjustPolicy(QTableWidget::QAbstractScrollArea::AdjustToContents);
 }

/*************************************************************************
 Function Name - autofitTable
 Parameter(s)  - QTableWidget*, QMap<int,int>
 Return Type   - void
 Action        - fits the given table to window.
 *************************************************************************/
//QMap<column,columnwidth>customSize
 void Utility::autofitTable(QTableWidget* table,QMap<int,int>customSize)
 {
     int totalColumns = table->horizontalHeader()->count();
     int customColumns = customSize.count();
     int remainingColumns = (totalColumns - customColumns);
     int totalSize = (table->width()-20);//20 is offset error
     int remainingColumnsSize = 0;
     int individualExtraWidth = 0;
     int customColumnSize = 0;

     QString header;
     QFontMetrics fm(table->font());

     //qDebug()<<"customSize:"<<customSize;
     //qDebug()<<"totalSize:"<<totalSize;

     //set custom column width
     for (auto iter = customSize.constBegin(); iter != customSize.constEnd(); ++iter) {
         customColumnSize += iter.value();
         table->setColumnWidth(iter.key(),iter.value());
     }
     //qDebug()<<"customColumnSize:"<<customColumnSize;

     //Now remove customColumns' Width from totalSize
     totalSize -= customColumnSize;
     //qDebug()<<"Now totalSize:"<<totalSize;

     for(int i = 0;i < totalColumns;i++) {
         if(customSize.contains(i)) continue;
         header = table->horizontalHeaderItem(i)->text();
         remainingColumnsSize += fm.horizontalAdvance(header);
     }
     //qDebug()<<"remainingColumnsSize:"<<remainingColumnsSize;

     remainingColumnsSize = (totalSize - remainingColumnsSize);
     //qDebug()<<"Now remainingColumnsSize:"<<remainingColumnsSize;
     individualExtraWidth = (remainingColumnsSize/remainingColumns);

     //qDebug()<<"remainingColumns:"<<remainingColumns<<"\nindividualExtraWidth:"<<individualExtraWidth;

     for(int colNo = 0;colNo < totalColumns;colNo++) {
         if(customSize.contains(colNo)) continue;
         int headerWidth = fm.horizontalAdvance(table->horizontalHeaderItem(colNo)->text());
         //qDebug()<<"headerName:"<<table->horizontalHeaderItem(colNo)->text()<<", headerWidth:"<<headerWidth;
         if(individualExtraWidth <= 100) {
             if(headerWidth < 100) table->setColumnWidth(colNo,headerWidth + (individualExtraWidth *3));
             else if(headerWidth < 110) table->setColumnWidth(colNo,headerWidth + (individualExtraWidth));
             else table->setColumnWidth(colNo,headerWidth + (individualExtraWidth/3));
         }
         else if((individualExtraWidth > 100) && (individualExtraWidth <= 250)) {
             int maxWidth = 0;
             for(int rowNo = 0;rowNo < table->rowCount();rowNo++) {
                 QTableWidgetItem* item = table->item(rowNo,colNo);
                 if(item == nullptr) continue;
                 int dataSize = fm.horizontalAdvance(item->text());
                 if(dataSize > maxWidth) maxWidth = dataSize;
             }
             //qDebug()<<"maxWidth:"<<maxWidth;
             if(headerWidth > maxWidth) table->setColumnWidth(colNo,headerWidth + (individualExtraWidth/4));
             else table->setColumnWidth(colNo,maxWidth+ (individualExtraWidth/2));
         }
         else if(individualExtraWidth > 250) {
             int maxWidth = 0;
             for(int rowNo = 0;rowNo < table->rowCount();rowNo++) {
                 QTableWidgetItem* item = table->item(rowNo,colNo);
                 if(item == nullptr) continue;
                 int dataSize = fm.horizontalAdvance(item->text());
                 if(dataSize > maxWidth) maxWidth = dataSize;
             }
             //qDebug()<<"maxWidth:"<<maxWidth;
             if(headerWidth > maxWidth) table->setColumnWidth(colNo,headerWidth + (individualExtraWidth/4));
             else table->setColumnWidth(colNo,maxWidth+ (individualExtraWidth/6));
         }
     }

     table->horizontalHeader()->setStretchLastSection(true);
     table->resizeRowsToContents();
 }

 /*************************************************************************
  Function Name - getCurrentDate
  Parameter(s)  - void
  Return Type   - QString
  Action        - returns current Date.
  *************************************************************************/
  QString Utility::getCurrentDate()
  {
      return QDate::currentDate().toString("dd/MM/yyyy");
  }

  /*************************************************************************
   Function Name - getCurrentTime
   Parameter(s)  - void
   Return Type   - QString
   Action        - returns current Time.
   *************************************************************************/
  QString Utility::getCurrentTime()
  {
      return QTime::currentTime().toString();
  }

  /*************************************************************************
   Function Name - getCurrentDateTime
   Parameter(s)  - void
   Return Type   - QString
   Action        - returns current Date and time.
   *************************************************************************/
   QString Utility::getCurrentDateTime()
   {
       return (getCurrentDate() + " " + getCurrentTime());
   }

  /*************************************************************************
   Function Name - setDateTime
   Parameter(s)  - QString&, QString&
   Return Type   - void
   Action        - sets current date and time in given variable.
   *************************************************************************/
  void Utility::setDateTime(QString& date, QString& time)
  {
      date = getCurrentDate();
      time = getCurrentTime();
  }
