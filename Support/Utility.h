/*************************************  FILE HEADER  *****************************************************************
*
*  File name - Utility.h
*  Creation date and time - 16-NOV-2019 , SAT 9:53 AM IST
*  Author: - Manoj
*  Purpose of the file - All utility modules i.e usable to others, will be declared .
*
**********************************************************************************************************************/
#ifndef UTILITY_H
#define UTILITY_H

/********************************************************
 Inclusion of header file
 ********************************************************/
#include <QDebug>
#include <math.h>
#include <QChar>
#include <QTableWidget>

//    Signal_Name	DOP_Number
#define RLC_P2A		501
#define RLC_Q2A		502
#define RLC_R2B		503
#define RLC_S2B		504
#define RLC_DMM		505
#define RLC_MEG		506
#define RLC_ERH		507
#define RLC_LT1		508
#define RLC_LT2		509
#define RLC_LT3		510
#define RLC_RES		511

#define DEFAULT_PREC 3

/********************************************************
 Class Declaration
 ********************************************************/
class Utility
{
public:
    enum UNIT{
        OHM,
        VOLT
    };

    struct componentsType1
    {
        QString measuredValue;
        QString result;
        QString remark;
    };
    struct componentsType1 _headers1;

    struct hiddenComponentsType1
    {
        QString dopHigh1;
        QString dopLow1;
        QString dopHigh2;
        QString dopLow2;
        QString INsToHigh;
        QString INsToLow;
        QString expectedValue;
        QString dips;
        QString dipsExpValue;
        QString dipsValueAfter;
        QString outputValueAfter;
    };
    struct hiddenComponentsType1 _hidden1;

    struct hiddenComponentsType2
    {
        QString dopHigh;
        QString dopLow;
        QString dop;
        QString expectedValue;
        QString expectedValueBefore;
        QString expectedValueAfter;
    };
    struct hiddenComponentsType2 _hidden2;

    struct ResetData {
        QStringList dipsList;
        QStringList dipsExpectedValueList;
        QStringList dopsHigh2List;
        QStringList dopsLow2List;
        int count;
        void clear() {
            dipsList.clear();
            dipsExpectedValueList.clear();
            dopsHigh2List.clear();
            dopsLow2List.clear();
            count = 0;
        }
    };

    //member functions
    Utility();
    ~Utility();
    double convertToDouble(QByteArray data);
    QStringList toStringList(const QList<QByteArray> list);
    QString convertToQstring(double value,int8_t precision);
    QString convertToQstring(int value);
    QString convertQByteArrayToHexQString(QByteArray byteArray);
    QString converthexQByteArrayToQString(QByteArray hexData);
    QByteArray convertQStringToByteArray(QString str);
    QByteArray converthexQStringToQByteArray(QString HexData);
    uint32_t convertHexToDecimal(QString hexData);
    QString convertDecimalToHex(uint32_t decimalValue);
    int convertToInt(QByteArray byteArray);
    void setPrecision(QString &valueString, int precision = DEFAULT_PREC);
    void setPrecision(QString &valueString,int fixedDec,int precision);
    QString nano(double value);
    QString micro(double value);
    QString milli(double value);
    QString kilo(double value);
    QString mega(double value);
    QString giga(double value);
    QString convertToStd(double value);
    QString convertToStd(double value, UNIT unit);
    QString removeSymbolsOtherThanNumbers(QString text);
    QString getChecksumExe();
    void getLineDops(uint32_t* dopToHigh,uint32_t* dopToLow,uint32_t* highDop,uint32_t* lowDop);
    void getHighDops(uint32_t* dopToHigh,uint32_t* highDop);
    void getLowDops(uint32_t* dopToLow,uint32_t* lowDop);
    bool getResult(QString expValueStr,double msrdValue);
    QString getDisplayValue(QString expValueStr,double msrdValue);
    QString getResistanceDisplayValue(double msrdValue);
    QString getBinaryString(QString str,int digit);
    QString getBinaryString(int value,int digit);
    QString getCurrentDate();
    QString getCurrentTime();
    QString getCurrentDateTime();
    void setDateTime(QString& date, QString& time);
    void autofitTable(QTableWidget* table,QMap<int,int>customSize);
    void autofitTable(QTableWidget* table,const QStringList columnWidths);
    double mod(double value){return ((value < 0) ? 0.0 : value);}

    //member variables
    uint8_t processStopFlag;

private:
    union IEEE_754
    {
        char data[4];
        float floatValue;
        double doubleValue;
    }hexToFloat;
    union CHAR_INT
    {
        char data[4];
        int intValue;
    }charToInt;

    int parse_char(char c);
    double getValueForPrefix(QString prefix,double value);
};

#endif // UTILITY_H
