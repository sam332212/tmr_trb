/*************************************  FILE HEADER  *****************************************************************
*
*  File name - Timer.h
*  Creation date and time - 31-AUG-2019 , SAT 4:56 PM IST
*  Author: - Manoj
*  Purpose of the file - All timer handling modules will be declared .
*
**********************************************************************************************************************/
#ifndef TIMER_H
#define TIMER_H

/********************************************************
 Inclusion of header file
 ********************************************************/
#include <QTime>
#include <QCoreApplication>
#include <QDebug>

/********************************************************
 Class Declaration
 ********************************************************/
class Timer
{
public:
    //member functions
    Timer();
    ~Timer();
    void delayInms(int timeInms);
};

#endif // TIMER_H
