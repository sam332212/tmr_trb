/*************************************  FILE HEADER  *****************************************************************
*
*  File name - Home.cpp
*  Creation date and time - 21-AUG-2019 , SAT 03:56 PM IST
*  Author: - Manoj
*  Purpose of the file - All Login modules will be defined .
*
**********************************************************************************************************************/
/********************************************************
 Inclusion of header file
 ********************************************************/
#include "Home.h"
#include "Global.h"
#include "ui_Home.h"
#include "debugflag.h"
#include "xlsxdocument.h"

#include "B64ED/Base64.h"

/********************************************************
 MACRO Definition
 ********************************************************/
#if HOME_DEBUG
    #define _qDebug(s) qDebug()<<"\n======== Inside "<<s<<" ========"
#else
    #define _qDebug(s); {}
#endif

#if HOME_DEBUG_E
    #define _qDebugE(p,q,r,s) qDebug()<<p<<q<<r<<s
#else
    #define _qDebugE(p,q,r,s); {}
#endif

#define dqDebug qDebug
#define EB enableComponents(true);
#define DB enableComponents(false);
#define MULTIMETER_PORT_CHECK_DELAY 2000//in ms

/********************************************************
 Function Definition
 ********************************************************/

/*************************************************************************
 Function Name - Home
 Parameter(s)  - QWidget*
 Return Type   - void
 Action        - Creates the object.
 *************************************************************************/
Home::Home(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Home),loginObj(new Login),homeDbObj(new Database),meterDbOdj(new Database),timerObj(new Timer),
    timerMeterCheckObj(new QTimer),testTimerObj(new QTimer)
{
    _qDebug("Home Constructor=>>>>>>>>>>>>>>");

    ui->setupUi(this);
    ui->actionCalibration->setVisible(false);
 }

/*************************************************************************
 Function Name - ~Home
 Parameter(s)  - void
 Return Type   - void
 Action        - Destroys the object.
 *************************************************************************/
 Home::~Home()
 {
     _qDebug("Home Destructor.");
     disconnect(timerMeterCheckObj,SIGNAL(timeout()),this,SLOT(checkAndUpdateMeterPort()));
     timerMeterCheckObj->stop();

     //Reset External Devices
     G::dmmObj->resetDMM();
     G::irMeterObj->resetIRMeter();
     G::psObj->resetAll();

     //clear Memories
     if(loginObj != nullptr) loginObj->deleteLater();
     clearmem(timerObj);
     clearmem(testTimerObj);
     clearmem(G::cycleTimer);
     clearmem(label_cycleObj);
     clearmem(spinBox_cycleObj);
     clearmem(blankLabel);
     clearmem(pushButton_okObj);
     clearmem(pushButton_cancelObj);
     clearmem(comboBox_tablesObj);
     clearmem(pushButton_deleteObj);
     clearmem(pushButton_cancelObj);
     clearmem(myWidgetObj);
     clearmem(timerMeterCheckObj);
     clearmem(G::dmmObj);
     clearmem(G::irMeterObj);
     clearmem(G::psObj);
     clearmem(G::handlerObj);
     clearmem(G::pci1758Obj);
     clearmem(G::pci7444Obj);

     if(G::handlerObj != nullptr)
     {
         G::handlerObj->stop();
         while(G::handlerObj->isRunning()) timerObj->delayInms(10);
         G::handlerObj->quit();
         clearmem(G::handlerObj);
     }
     if(homeDbObj != nullptr){homeDbObj->closeDB();clearmem(homeDbObj);}
     if(meterDbOdj != nullptr){meterDbOdj->closeDB();clearmem(meterDbOdj);}

     //Delete Actions
     for(int i = 0;i < testQactionLists.size();i++){clearmem(testQactionLists[i]);}
     //Delete Menus
     for(int i = 0;i < uutQmenuLists.size();i++){clearmem(uutQmenuLists[i]);}

     unitTestDataTableLists.clear();
     unitTestLists.clear();
     uutTestTableNameLists.clear();

     disconnectKeys();

     delete ui;
 }

/*************************************************************************
 Function Name - closeEvent
 Parameter(s)  - QCloseEvent*
 Return Type   - void
 Action        - This function handles this windows close action.
 *************************************************************************/
 void Home::closeEvent (QCloseEvent *event)
 {
     _qDebug("void Home::closeEvent (QCloseEvent *event).");

     if(ui->pushButton_run_stop->text() == "STOP") {
         event->ignore();
         PLAY QMessageBox::warning(nullptr,"Error!!","Please stop the current test.");
         return;
     }

     QMessageBox::StandardButton resBtn = QMessageBox::question( this, MESSAGE_TITLE,tr("Do you Want to Close the App?\n"),QMessageBox::No | QMessageBox::Yes);
     if (resBtn != QMessageBox::Yes)
     {
         event->ignore();
     }
     else
     {
         QMessageBox* msgBox = new QMessageBox;
         msgBox->setWindowTitle(MESSAGE_TITLE);
         msgBox->setText("Please wait!! The App is being closed...  ");
         msgBox->setWindowFlags(Qt::CustomizeWindowHint);
         msgBox->setStandardButtons(nullptr);
         msgBox->show();
         timerObj->delayInms(1);

         timerMeterCheckObj->stop();

         G::handlerObj->stop();
         while(G::handlerObj->isRunning() == true) timerObj->delayInms(10);
         G::handlerObj->quit();

         clearmem(msgBox);

         event->accept();

         delete this;
    }
 }

/*************************************************************************
 Function Name - initDeviceHandler
 Parameter(s)  - void
 Return Type   - void
 Action        - This function initializes the device handler PS LCDs.
 *************************************************************************/
 void Home::initDeviceHandler()
 {
     G::handlerObj->psVoltageLcd.clear();
     G::handlerObj->psVoltageLcd.push_back(ui->lcd_PS1Voltage);
     G::handlerObj->psVoltageLcd.push_back(ui->lcd_PS2Voltage);
     G::handlerObj->psVoltageLcd.push_back(ui->lcd_PS3Voltage);
     G::handlerObj->psVoltageLcd.push_back(ui->lcd_PS4Voltage);
     G::handlerObj->psVoltageLcd.push_back(ui->lcd_PS5Voltage);
     G::handlerObj->psCurrentLcd.clear();
     G::handlerObj->psCurrentLcd.push_back(ui->lcd_PS1Current);
     G::handlerObj->psCurrentLcd.push_back(ui->lcd_PS2Current);
     G::handlerObj->psCurrentLcd.push_back(ui->lcd_PS3Current);
     G::handlerObj->psCurrentLcd.push_back(ui->lcd_PS4Current);
     G::handlerObj->psCurrentLcd.push_back(ui->lcd_PS5Current);

     for(int i = 0;i < G::handlerObj->psVoltageLcd.count();i++) G::handlerObj->psVoltageLcd.at(i)->display("0.00");
     for(int i = 0;i < G::handlerObj->psCurrentLcd.count();i++) G::handlerObj->psCurrentLcd.at(i)->display("0.00");
 }

/*************************************************************************
 Function Name - initGlobalParameters
 Parameter(s)  - void
 Return Type   - void
 Action        - This function initializes the global parameter.
 *************************************************************************/
 void Home::initGlobalParameters()
 {
     G::dmmObj     = new DMM;
     G::irMeterObj = new IRMeter;
     G::psObj      = new PowerSupply;
     G::handlerObj = new DevicesHandler;
     G::pci1758Obj = new Pci1758;
     G::pci7444Obj = new Pci7444;

     G::testTable        = ui->tableWidget;
     G::testResultStatus = ui->label_TestStatus;
     G::testRunStatus    = ui->label_status;
     G::testVoltage      = ui->label_TestVoltage;
     G::progressBar      = ui->progressBar;
     G::testTimer        = testTimerObj;
     G::cycleTimer       = new QTimer;
     G::cycleDisplay     = ui->label_cycle;
     G::runStopButton    = ui->pushButton_run_stop;

     G::psStatus.clear();
     G::psStatus.push_back(ui->label_PS1OutputStatus);
     G::psStatus.push_back(ui->label_PS2OutputStatus);
     G::psStatus.push_back(ui->label_PS3OutputStatus);
     G::psStatus.push_back(ui->label_PS4OutputStatus);
     G::psStatus.push_back(ui->label_PS5OutputStatus);

     G::myFont.setPointSize(12);
     G::myFont.setBold(true);
     G::myFont.setFamily("Times New Roman");

     G::softwareChecksum = utilObj.getChecksumExe().mid(0,8);
 }

/*************************************************************************
 Function Name - initHome
 Parameter(s)  - void
 Return Type   - bool
 Action        - This is the initialization function which should be called
                 immediate after object creation and made necessary configurations.
 *************************************************************************/
 bool Home::initHome()
 {
     _qDebug("void Home::initHome().");

     initGlobalParameters();
     qDebug()<<"Calculated Checksum:["<<G::softwareChecksum<<"]";

     if(!initDBs()) return false;

     ui->menuHelp->menuAction()->setVisible(false);
     initDeviceHandler();
     setHeader();

     //set an icon for window
     this->setWindowIcon(QIcon(":/Images/atl.ico"));

     //connect timer to check ExternalDeviceConnectivity in each specified timeInterval(Meter and Power Supply)
     connect(timerMeterCheckObj,SIGNAL(timeout()),this,SLOT(displayExtDevicesStatus()));
     //connect timer to update test run time
     connect(testTimerObj,SIGNAL(timeout()),this,SLOT(updateTestTimer()));

     ui->progressBar->setValue(0);
     ui->pushButton_run_stop->setDisabled(true);

     //set Font for table_Header
     _font.setPointSize(14);
     _font.setBold(true);
     _font.setFamily("Times New Roman");

     //table settings
     ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);//Make table widget non editable
     ui->tableWidget->verticalHeader()->setVisible(false);

     ui->label_TestVoltage->setText("");
     ui->label_TestVoltage->setHidden(true);
     ui->label_cycle->setHidden(true);
     ui->comboBox_manualPolarity->setHidden(true);
     ui->actionSave_Table->setVisible(false);

     G::pci1758Obj->setDopCardDesc(QStringList{"PCI-1758UDO,BID#1","PCI-1758UDO,BID#2","PCI-1758UDO,BID#3","PCI-1758UDO,BID#4"});
     G::pci1758Obj->setDipCardDesc(QStringList{"PCI-1758UDI,BID#6","PCI-1758UDI,BID#7"});
     G::pci1758Obj->setTotalDops(PCI1758DOPS);
     G::pci1758Obj->setTotalDips(PCI1758DIPS);
#if PCI_1758_EN
     //Init PCI Card 1758
     if(!G::pci1758Obj->init1758Cards()){
         PLAY QMessageBox::warning(nullptr,"Error!!","PCI1758 Card Initialization Failed.");
         return false;
     }
     if(!G::pci1758Obj->resetPCI()) {
        PLAY QMessageBox::warning(nullptr,"Error!!","PCI1758 Card Reset Failed.");
        return false;
     }
#endif

     G::pci7444Obj->setBoardIdNumbers(QVector<U16>({8,9}));
     G::pci7444Obj->setTotalDops(PCI7444DOPS);
#if PCI_7444_EN
     //Init PCI Card 7444
     if(!G::pci7444Obj->init7444Cards()){
         PLAY QMessageBox::warning(nullptr,"Error!!","PCI7444 Card Initialization Failed.");
         return false;
     }
     if(!G::pci7444Obj->resetPCI()) {
         PLAY QMessageBox::warning(nullptr,"Error!!","PCI7444 Card Reset Failed.");
         return false;
     }
#endif

     return true;
 }

 /*************************************************************************
  Function Name - initDBs
  Parameter(s)  - void
  Return Type   - void
  Action        - initializes DBs.
  *************************************************************************/
 bool Home::initDBs()
 {
     _qDebug("bool Home::initDBs().");
     QStringList meterColumnNames;
     QStringList selectionColumnNames;
     QStringList sysParColumnNames;
     QString expectedChecksum;
     meterColumnNames.clear();
     meterColumnNames.push_back("MeterName");
     meterColumnNames.push_back("PortName");
     selectionColumnNames.clear();
     selectionColumnNames.push_back("Selection");
     selectionColumnNames.push_back("Value");
     sysParColumnNames.clear();
     sysParColumnNames.push_back("Parameter");
     sysParColumnNames.push_back("Value");

     psAddress.clear();

     uutQmenuLists.clear();
     testQactionLists.clear();
     unitTestDataTableLists.clear();
     unitTestLists.clear();
     uutTableName.clear();
     lastFilePath.clear();

     //initialize member variables
     homeDbConnection  = "home";
     meterDbConnection = "meter";
     uutTableName      = "UUT";
     psTableName       = "POWER_SUPPLIES";

     //initialze database table names
     if(homeDbObj->openDB("TMR_TRB.db","localHost",MESSAGE_TITLE,"tmr_trb_pw",homeDbConnection))
     {
         _qDebugE("Home Database opened successfully.","","","");
         if(!homeDbObj->createTable(SYSTEM_PARAMETER_TABLE,sysParColumnNames))
         {
             _qDebugE(SYSTEM_PARAMETER_TABLE," Creation Failed.","","");
             PLAY QMessageBox::warning(nullptr,"Error!!",QString("%1 Creation Failed.").arg(SYSTEM_PARAMETER_TABLE));
             return false;
         }
         else
         {
             if(homeDbObj->getRowCount(SYSTEM_PARAMETER_TABLE) == 0)
             {
                 if(homeDbObj->addBlankRows(SYSTEM_PARAMETER_TABLE,SYSTEMPARAMS_TABLE_ROWS))
                 {
                     homeDbObj->editCell(SYSTEM_PARAMETER_TABLE,1,sysParColumnNames.at(0),"CalibratedResistance");
                     homeDbObj->editCell(SYSTEM_PARAMETER_TABLE,2,sysParColumnNames.at(0),"CalibratedVoltage");
                     homeDbObj->editCell(SYSTEM_PARAMETER_TABLE,3,sysParColumnNames.at(0),"Version");
                     homeDbObj->editCell(SYSTEM_PARAMETER_TABLE,4,sysParColumnNames.at(0),"BuiltDate");
                     homeDbObj->editCell(SYSTEM_PARAMETER_TABLE,5,sysParColumnNames.at(0),"Checksum");
                     homeDbObj->editCell(SYSTEM_PARAMETER_TABLE,6,sysParColumnNames.at(0),"ChecksumValidate");
                     homeDbObj->editCell(SYSTEM_PARAMETER_TABLE,7,sysParColumnNames.at(0),"id");

                     homeDbObj->editCell(SYSTEM_PARAMETER_TABLE,1,sysParColumnNames.at(1),"2.3");
                     homeDbObj->editCell(SYSTEM_PARAMETER_TABLE,2,sysParColumnNames.at(1),"0.0");
                     homeDbObj->editCell(SYSTEM_PARAMETER_TABLE,3,sysParColumnNames.at(1),DEFAULT_GUI_VERSION);
                     homeDbObj->editCell(SYSTEM_PARAMETER_TABLE,4,sysParColumnNames.at(1),DEFAULT_BUILD_DATE);
                     homeDbObj->editCell(SYSTEM_PARAMETER_TABLE,5,sysParColumnNames.at(1),G::softwareChecksum);
                     homeDbObj->editCell(SYSTEM_PARAMETER_TABLE,6,sysParColumnNames.at(1),QString::number(1));
                     homeDbObj->editCell(SYSTEM_PARAMETER_TABLE,7,sysParColumnNames.at(1),"");
                 }
             }

             if(!updateSystemParameters()) return false;
             if(!isValidChecksum()) return false;
         }

         if(homeDbObj->getTableData(psTableName,psTableData))
         {
             _qDebugE("psTableData:[",psTableData,"]","");

             QMap<int, float> psProgrammedVoltage;
             //Initialize PS Address
             for(int i = 0;i < psTableData.count();i++)
             {
                 int count = psTableData.at(i).count();
                 if(count > 1) {
                    psAddress.push_back(psTableData.at(i).at(0).toInt());
                    psProgrammedVoltage.insert(psTableData.at(i).at(0).toInt(),psTableData.at(i).at(1).toFloat());
                 }
             }
             G::handlerObj->setPsProgrammedVoltage(psProgrammedVoltage);
         }

         if(!homeDbObj->createTable(SELECTION_TABLE,selectionColumnNames))
         {
             _qDebugE(SELECTION_TABLE," Creation Failed.","","");
             PLAY QMessageBox::warning(nullptr,"Error!!",QString("%1 Creation Failed.").arg(SELECTION_TABLE));
             return false;
         }
         else
         {
             if(homeDbObj->getRowCount(SELECTION_TABLE) == 0)
             {
                 if(homeDbObj->addBlankRows(SELECTION_TABLE,4))
                 {
                     homeDbObj->editCell(SELECTION_TABLE,1,selectionColumnNames.at(0),"AutoSaveReport");
                     homeDbObj->editCell(SELECTION_TABLE,2,selectionColumnNames.at(0),"AutoSaveSelfCheckReport");
                     homeDbObj->editCell(SELECTION_TABLE,3,selectionColumnNames.at(0),"EnableReportLandscape");
                     homeDbObj->editCell(SELECTION_TABLE,4,selectionColumnNames.at(0),"EnableKeyBoard");

                     homeDbObj->editCell(SELECTION_TABLE,1,selectionColumnNames.at(1),"1");
                     homeDbObj->editCell(SELECTION_TABLE,2,selectionColumnNames.at(1),"1");
                     homeDbObj->editCell(SELECTION_TABLE,3,selectionColumnNames.at(1),"1");
                     homeDbObj->editCell(SELECTION_TABLE,4,selectionColumnNames.at(1),"1");
                 }
             }
             updateUserSelections();
         }

         if(meterDbOdj->openDB("METER_SETTINGS.db","localHost","METER","meter_pw",meterDbConnection))
         {
             _qDebugE("Meter Database opened successfully.","","","");

             if(!meterDbOdj->createTable(METER_TABLE_NAME,meterColumnNames))
             {
                 _qDebugE("Meter Database creation failed.","","","");
                 PLAY QMessageBox::warning(nullptr,"Error!!",QString("%1 Creation Failed.").arg(METER_TABLE_NAME));
                 return false;
             }
             else
             {
                 if(meterDbOdj->getRowCount(METER_TABLE_NAME) == 0)
                 {
                     if(meterDbOdj->addBlankRows(METER_TABLE_NAME,3))
                     {
                         meterDbOdj->editCell(METER_TABLE_NAME,1,meterColumnNames.at(0),"DMM PORT");
                         meterDbOdj->editCell(METER_TABLE_NAME,2,meterColumnNames.at(0),"IR METER PORT");
                         meterDbOdj->editCell(METER_TABLE_NAME,3,meterColumnNames.at(0),"PS PORT");
                     }
                 }

                 ui->actionSave_Result->setDisabled(true);
                 ui->actionSave_Pass_Result->setDisabled(true);
                 ui->actionSave_Fail_Result->setDisabled(true);
                 ui->actionSelect_All->setDisabled(true);
                 ui->actionUnselect_All->setDisabled(true);

                 if(loginObj->initLogin() == false)
                 {
                     clearmem(loginObj);
                     PLAY QMessageBox::warning(nullptr,"Error!!\n","Login Initialization Failed.");
                     return false;
                 }
                 else
                 {
                    connect(loginObj,SIGNAL(goToHome()), this, SLOT(show_ui()));
                    connect(loginObj,SIGNAL(close()), this, SLOT(close_ui()));
                    loginObj->show();
                 }
             }//else block when MeterDB creation passes
         }//if block opens the Meter DB
         else {
             _qDebugE("Oops! Main Database open Failed.","","","");
             PLAY QMessageBox::warning(nullptr,"Error!!","Meter Database open Failed.");
             return false;
          }
     }//if block opens the Home DB
     else {
         _qDebugE("Oops! Main Database open Failed.","","","");
         PLAY QMessageBox::warning(nullptr,"Error!!","Main Database open Failed.");
         return false;
      }

     return true;
 }

 /*************************************************************************
  Function Name - isValidChecksum
  Parameter(s)  - void
  Return Type   - bool
  Action        - returns true if expected checksum matches with actual checksum
                  else returns false.
  *************************************************************************/
 bool Home::isValidChecksum()
 {
    if(G::checksumValidate) {
     if(G::expectedChecksum != G::softwareChecksum) {
         PLAY QMessageBox::warning(nullptr,"Error!!",QString("Checksum Mismatched!!\nExpected: %1\nActual     : %2")
                              .arg(G::expectedChecksum).arg(G::softwareChecksum));
         return false;
     }
    }
    return true;
 }

/*************************************************************************
 Function Name - updateUserSelections
 Parameter(s)  - void
 Return Type   - void
 Action        - updates user selection UI.
 *************************************************************************/
 void Home::updateUserSelections()
 {
     QVector<QStringList> selData;
     int value;
     selData.clear();
     homeDbObj->getTableData(SELECTION_TABLE,selData);

     for(int i = 0;i < selData.count();i++)
     {
         if(selData.at(i).count() > 1) {
             value = selData.at(i).at(1).toInt();
             switch (i) {
                 case 0 :
                            if(value) {
                                ui->actionAuto_Save_Report_after_Test->setIconVisibleInMenu(true);
                                isAutoSaveReport = true;
                            }
                            else {
                                ui->actionAuto_Save_Report_after_Test->setIconVisibleInMenu(false);
                                isAutoSaveReport = false;
                            }
                            break;
                 case 1 :
                            if(value) {
                                ui->actionAuto_Generate_Self_Check_Report->setIconVisibleInMenu(true);
                                isAutoSaveSCReport = true;
                            }
                            else {
                                ui->actionAuto_Generate_Self_Check_Report->setIconVisibleInMenu(false);
                                isAutoSaveSCReport = false;
                            }
                            break;
                 case 2 :
                            if(value) {
                                ui->actionEnable_Report_LandScape_Mode->setIconVisibleInMenu(true);
                                isReportLandscapeMode = true;
                            }
                            else {
                                ui->actionEnable_Report_LandScape_Mode->setIconVisibleInMenu(false);
                                isReportLandscapeMode = false;
                            }
                            break;
                 case 3 :
                            if(value) {
                                ui->actionEnable_Keyboard_Action->setIconVisibleInMenu(true);
                                connectKeys();
                            }
                            else {
                                ui->actionEnable_Keyboard_Action->setIconVisibleInMenu(false);
                                disconnectKeys();
                            }
                            break;
             }//switch block
         }//if block when each row-data count is minimum 2
     }//for loop runs for number of selections
 }

/*************************************************************************
 Function Name - updateSystemParameters
 Parameter(s)  - void
 Return Type   - bool
 Action        - updates calibration parameters.
 *************************************************************************/
 bool Home::updateSystemParameters() {
     QVector<QStringList> selData;
     selData.clear();
     if(!homeDbObj->getTableData(SYSTEM_PARAMETER_TABLE,selData))
     {
         PLAY QMessageBox::warning(nullptr,"Error!!","Unable to read system parameters from Database.");
         return false;
     }

     if(selData.count() < SYSTEMPARAMS_TABLE_ROWS) {
         PLAY QMessageBox::warning(nullptr,"Error!!",QString("The system parameter databse rows can't be <%1.")
                                   .arg(SYSTEMPARAMS_TABLE_ROWS));
         return false;
     }

     for(int i = 0;i < selData.count();i++)
     {
         if(selData.at(i).count() > 1) {
             switch (i) {
                case 0 :
                          G::calibResistance = selData.at(i).at(1).toDouble();
                          break;
                case 1 :
                          G::calibVoltage = selData.at(i).at(1).toDouble();
                          break;
                case 2 :
                          G::appVersion = selData.at(i).at(1);
                          break;
                case 3 :
                          G::builtDate = selData.at(i).at(1);
                          break;
                case 4 :
                          G::expectedChecksum = selData.at(i).at(1);
                          break;
                case 5 :
                          G::checksumValidate = ((selData.at(i).at(1) == "0") ? false : true);
                          break;
                case 6 :
                          loginObj->pidE = selData.at(i).at(1);
                          break;
             }
         }
     }
     return true;
 }

/*************************************************************************
 Function Name - disconnectKeys
 Parameter(s)  - void
 Return Type   - void
 Action        - disConnects the slots from Home Shortcut Keys.
 *************************************************************************/
 void Home::disconnectKeys()
 {
    QObject::disconnect(exitKey, SIGNAL(activated()), this, SLOT(close_App()));
    QObject::disconnect(selectAllKey, SIGNAL(activated()), this, SLOT(selectAll()));
    QObject::disconnect(unSelectAllKey, SIGNAL(activated()), this, SLOT(unselectAll()));
    QObject::disconnect(saveResultKey, SIGNAL(activated()), this, SLOT(on_actionSave_Result_triggered()));
    QObject::disconnect(savePassResultKey, SIGNAL(activated()), this, SLOT(on_actionSave_Pass_Result_triggered()));
    QObject::disconnect(saveFailResultKey, SIGNAL(activated()), this, SLOT(on_actionSave_Fail_Result_triggered()));

    clearmem(saveResultKey);
    clearmem(savePassResultKey);
    clearmem(saveFailResultKey);
    clearmem(selectAllKey);
    clearmem(unSelectAllKey);
    clearmem(exitKey);
 }

/*************************************************************************
 Function Name - connectKeys
 Parameter(s)  - void
 Return Type   - void
 Action        - Connects the slots to Home Shortcut Keys.
 *************************************************************************/
 void Home::connectKeys()
 {
     saveResultKey = new QShortcut(QKeySequence("Ctrl+S"), this);
     savePassResultKey = new QShortcut(QKeySequence("Ctrl+Shift+P"), this);
     saveFailResultKey = new QShortcut(QKeySequence("Ctrl+Shift+F"), this);
     selectAllKey = new QShortcut(QKeySequence("Ctrl+A"), this);
     unSelectAllKey = new QShortcut(QKeySequence("Ctrl+U"), this);
     exitKey = new QShortcut(QKeySequence("Ctrl+E"), this);

     QObject::connect(exitKey, SIGNAL(activated()), this, SLOT(close_App()));
     QObject::connect(selectAllKey, SIGNAL(activated()), this, SLOT(selectAll()));
     QObject::connect(unSelectAllKey, SIGNAL(activated()), this, SLOT(unselectAll()));
     QObject::connect(saveResultKey, SIGNAL(activated()), this, SLOT(on_actionSave_Result_triggered()));
     QObject::connect(savePassResultKey, SIGNAL(activated()), this, SLOT(on_actionSave_Pass_Result_triggered()));
     QObject::connect(saveFailResultKey, SIGNAL(activated()), this, SLOT(on_actionSave_Fail_Result_triggered()));
 }

/*************************************************************************
 Function Name - enableComponents
 Parameter(s)  - bool
 Return Type   - void
 Action        - If status is true, then the components will be enabled.
                 If status is false, then the components will be disabled.
 *************************************************************************/
 void Home::enableComponents(bool status)
 {
     ui->pushButton_logout->setEnabled(status);
     ui->lineEdit_uutNumber->setEnabled(status);
     ui->lineEdit_typeOfTest->setEnabled(status);
     ui->lineEdit_UserPerson->setEnabled(status);
     ui->lineEdit_QAperson->setEnabled(status);
     ui->comboBox_manualPolarity->setEnabled(status);
     ui->menuBar->setEnabled(status);

     for(int rowNo = 0;rowNo < ui->tableWidget->rowCount();rowNo++) {
         if(ui->tableWidget->cellWidget(rowNo,0) != nullptr) {
             if(rowNo == 0 && ui->label_testName->text().contains("manual",Qt::CaseInsensitive)) continue;
             static_cast<QCheckBox*>(ui->tableWidget->cellWidget(rowNo,0))->setEnabled(status);
         }
     }
 }

 /*************************************************************************
  Function Name - isAtleastOneRowSelected
  Parameter(s)  - QTableWidget*
  Return Type   - bool
  Action        - It returns true if atleast one row is selected else returns false.
  *************************************************************************/
 bool Home::isAtleastOneRowSelected(QTableWidget* table) {
     for(int rowNo = 0;rowNo < table->rowCount();rowNo++) {
         if(table->cellWidget(rowNo,0) != nullptr) {
            if(static_cast<QCheckBox*>(table->cellWidget(rowNo,0))->isChecked()) return true;
         }
     }
     return false;
 }

/*************************************************************************
 Function Name - enableResultSaving
 Parameter(s)  - bool
 Return Type   - void
 Action        - If status is true, then the components will be enabled.
                  If status is false, then the components will be disabled.
 *************************************************************************/
 void Home::enableResultSaving(bool status)
 {
     ui->actionSave_Result->setEnabled(status);
     ui->actionSave_Pass_Result->setEnabled(status);
     ui->actionSave_Fail_Result->setEnabled(status);
 }

 /*************************************************************************
  Function Name - selectAll
  Parameter(s)  - void
  Return Type   - void
  Action        - selects(checkbox checked) all test pairs in table.
  *************************************************************************/
 void Home::selectAll()
 {
     if(ui->actionSelect_All->isEnabled()) on_actionSelect_All_triggered();
 }

 /*************************************************************************
  Function Name - selectAll
  Parameter(s)  - void
  Return Type   - void
  Action        - selects(checkbox checked) all test pairs in table.
  *************************************************************************/
 void Home::unselectAll()
 {
     if(ui->actionUnselect_All->isEnabled()) on_actionUnselect_All_triggered();
 }

 /*************************************************************************
  Function Name - initPS
  Parameter(s)  - void
  Return Type   - void
  Action        - Initializes the Power Supplies connected.
  *************************************************************************/
 void Home::initPS()
 {
     _qDebug("void Home::initPS().");
     int volCount,curCount;

     if(psTableData.count() > 0)
     {
         G::psObj->resetAll();
         volCount = G::handlerObj->psVoltageLcd.count();
         curCount = G::handlerObj->psCurrentLcd.count();

         ui->label_PS1OutputStatus->setStyleSheet("QLabel { background-color : red;}");
         ui->label_PS2OutputStatus->setStyleSheet("QLabel { background-color : red;}");
         ui->label_PS3OutputStatus->setStyleSheet("QLabel { background-color : red;}");
         ui->label_PS4OutputStatus->setStyleSheet("QLabel { background-color : red;}");
         ui->label_PS5OutputStatus->setStyleSheet("QLabel { background-color : red;}");

         for(int i = 0;i < psTableData.count();i++)
         {
             int count = psTableData.at(i).count();
             if(count > 0)
             {
                if(G::psObj->selectDevice(static_cast<uint8_t>(psTableData.at(i).at(0).toInt())))
                {
                    G::psObj->setLocalLockoutMode();
                    if(count > 1) G::psObj->setVoltage(psTableData.at(i).at(1).toDouble());
                    if(count > 2) G::psObj->setCurrent(psTableData.at(i).at(2).toDouble());

                    if((count > 3) && (psTableData.at(i).at(3).toInt() == 1)) {
                        if(G::psObj->setOutputOn()) {
                            switch (i) {
                                case 0 :
                                            ui->label_PS1OutputStatus->setStyleSheet("QLabel { background-color : #3AEA02;}");
                                            break;
                                case 1 :
                                            ui->label_PS2OutputStatus->setStyleSheet("QLabel { background-color : #3AEA02;}");
                                            break;
                                case 2 :
                                            ui->label_PS3OutputStatus->setStyleSheet("QLabel { background-color : #3AEA02;}");
                                            break;
                                case 3 :
                                            ui->label_PS4OutputStatus->setStyleSheet("QLabel { background-color : #3AEA02;}");
                                            break;
                                case 4 :
                                            ui->label_PS5OutputStatus->setStyleSheet("QLabel { background-color : #3AEA02;}");
                                            break;
                                default :
                                            break;
                            }
                        }
                    }

                    G::psObj->getPsData();
                    //Display Voltage and current in PS
                    if(volCount > i) {
                        if(G::psObj->isOutputOn()) {
                            G::handlerObj->psVoltageLcd.at(i)->display(G::psObj->_psDetails.measuredVoltage);
                        }
                        else {
                            G::handlerObj->psVoltageLcd.at(i)->display(G::psObj->_psDetails.programmedVoltage);
                        }
                    }
                    if(curCount > i) {
                        if(G::psObj->isOutputOn()) {
                            G::handlerObj->psCurrentLcd.at(i)->display(G::psObj->_psDetails.measuredCurrent);
                        }
                        else {
                            G::handlerObj->psCurrentLcd.at(i)->display(G::psObj->_psDetails.programmedCurrent);
                        }
                    }
                }
             }
         }//for loop
     }//if block when PS data read from db success
 }

/*************************************************************************
 Function Name - show_ui
 Parameter(s)  - void
 Return Type   - void
 Action        - Shows this UI.
 *************************************************************************/
 void Home::show_ui()
 {
     _qDebug("Home::show_ui().");

     QString userNameText = "  User: ";

     //clean table
     ui->tableWidget->horizontalHeader()->hide();
     ui->tableWidget->setRowCount(0);
     timerObj->delayInms(10);//table stable time

     userNameText.push_back(loginObj->get_user_name()+"  ");

     ui->label_userName->setText(userNameText);
     ui->label_unitName->setText("   No Unit Selected   ");
     ui->label_testName->setText("   No test Selected   ");
     ui->label_TestStatus->clear();
     ui->label_TestStatus->setStyleSheet("");
     ui->progressBar->setValue(0);
     ui->pushButton_run_stop->setDisabled(true);
     ui->label_timer->setText("00 H : 00 M : 00 S");
     ui->label_cycle->setText("Cycle No: 1");
     ui->label_status->setStyleSheet("QLabel { background-color : red;}");

     if(loginObj->isSuperUser)
     {
         ui->menuExtra->menuAction()->setVisible(true);
         QMessageBox mb(QMessageBox::NoIcon, "TMR_TRB\n",QString("Calucated Checksum: %1\nid: %2")
                        .arg(G::softwareChecksum).arg(loginObj->pid), QMessageBox::Ok, this);
         mb.setTextInteractionFlags(Qt::TextSelectableByMouse);
         mb.exec();
     }
     else
     {
        ui->menuExtra->menuAction()->setVisible(false);
     }

     update_home_ui();

     this->showMaximized();

     initMeters();

     timerMeterCheckObj->start(MULTIMETER_PORT_CHECK_DELAY);

     //Giving access to HandlerObject to access the remote devices
     G::handlerObj->isDmmConnected = &isDmmConnected;
     G::handlerObj->isIrMeterConnected = &isIrMeterConnected;
     G::handlerObj->isChecking = &isChecking;
     G::handlerObj->psAddress = &psAddress;
     G::handlerObj->psStatus = &psStatus;

     G::handlerObj->stop();
     while(G::handlerObj->isRunning() == true) timerObj->delayInms(10);
     G::handlerObj->start();
 }

 /*************************************************************************
  Function Name - resetUIComponents
  Parameter(s)  - void
  Return Type   - void
  Action        - resets this UI components to default.
  *************************************************************************/
 void Home::resetUIComponents()
 {
     ui->tableWidget->horizontalHeader()->hide();
     ui->tableWidget->setRowCount(0);
     timerObj->delayInms(10);//table stable time
     ui->label_TestStatus->clear();
     ui->label_TestStatus->setStyleSheet("");
     ui->progressBar->setValue(0);
     ui->label_timer->setText("00 H : 00 M : 00 S");
     ui->label_cycle->setHidden(true);
     ui->comboBox_manualPolarity->setHidden(true);
     ui->actionSave_Table->setVisible(false);
     ui->label_status->setStyleSheet("QLabel { background-color : red;}");
     ui->label_TestVoltage->setHidden(true);
     ui->label_TestVoltage->clear();
     ui->label_unitName->setText("  Unit: ");
     ui->label_testName->setText("  Test: ");
     enableResultSaving(false);
     ui->actionSelect_All->setDisabled(true);
     ui->actionUnselect_All->setDisabled(true);
 }

/*************************************************************************
 Function Name - close_ui
 Parameter(s)  - void
 Return Type   - void
 Action        - Closes and clears this UI.
 *************************************************************************/
 void Home::close_ui()
 {
     _qDebug("void Home::close_ui().");
     delete this;
 }

 /*************************************************************************
  Function Name - close_Home
  Parameter(s)  - void
  Return Type   - void
  Action        -  Clears and closes this APP.
  *************************************************************************/
 void Home::close_App()
 {
     _qDebug("void Home::close_App().");

     QMessageBox::StandardButton resBtn = QMessageBox::question( this, MESSAGE_TITLE,tr("Do you Want to Close the App?\n"),QMessageBox::No | QMessageBox::Yes);
     if (resBtn == QMessageBox::Yes)
     {
         QMessageBox* msgBox = new QMessageBox;
         msgBox->setWindowTitle(MESSAGE_TITLE);
         msgBox->setText("Please wait!! The App is being closed...  ");
         msgBox->setWindowFlags(Qt::CustomizeWindowHint);
         msgBox->setStandardButtons(nullptr);
         msgBox->show();
         timerObj->delayInms(1);

         timerMeterCheckObj->stop();

         G::handlerObj->stop();
         while(G::handlerObj->isRunning() == true) timerObj->delayInms(10);
         G::handlerObj->quit();

         clearmem(msgBox);

         delete this;
    }
 }

 /*************************************************************************
  Function Name - initMeters
  Parameter(s)  - void
  Return Type   - void
  Action        - This function initializes the meters.
  *************************************************************************/
 void Home::initMeters()
 {
     _qDebug("void Home::initMeters()");
     static bool isBegin = true;//This flag set true says first time display of Home UI
     if(isBegin)
     {
         QStringList meterTable;
         meterTable = meterDbOdj->getTableNames();

         if(meterTable.length() == 0)
         {
             PLAY QMessageBox::warning(nullptr,"Error!!\n","Meter DB couldn't be found.");
         }
         else
         {
             //DATABITS_8->8,PARITY_NONE->0
             G::dmmObj->setDB(meterDbOdj);
             G::dmmObj->setDBTableName(meterTable.at(0));
             G::dmmObj->setRowId(1);
             G::dmmObj->setPortSettings(G::dmmObj->IDN,G::dmmObj->IDNresponse,G::dmmObj->BAUDRATE115200,8,
                                           0,G::dmmObj->STOPBIT_1,G::dmmObj->FLOWCONTROL_OFF,
                                           5000,G::dmmObj->WHOLE_WORDS);
             //txData,matchData,BaudRate,DataBits,Parity,StopBits,FlowControl,Timeout_Millisec,COMPARE_FLAG
             G::dmmObj->showPortSettings();

             G::irMeterObj->setDB(meterDbOdj);
             G::irMeterObj->setDBTableName(meterTable.at(0));
             G::irMeterObj->setRowId(2);
             G::irMeterObj->setPortSettings( G::irMeterObj->IDN, G::irMeterObj->IDNresponse, G::irMeterObj->BAUDRATE19200,8,
                                           0, G::irMeterObj->STOPBIT_1, G::irMeterObj->FLOWCONTROL_OFF,
                                           5000, G::irMeterObj->WHOLE_WORDS);
             //txData,matchData,BaudRate,DataBits,Parity,StopBits,FlowControl,Timeout_Millisec,COMPARE_FLAG
              G::irMeterObj->showPortSettings();

             G::psObj->setDB(meterDbOdj);
             G::psObj->setDBTableName(meterTable.at(0));
             G::psObj->setRowId(3);
             G::psObj->setPortSettings(G::psObj->txCommand,G::psObj->psResponse,G::psObj->BAUDRATE57600,8,
                                           0,G::psObj->STOPBIT_1,G::psObj->FLOWCONTROL_OFF,
                                           5000,G::psObj->ONLY_WORDS);
             //txData,matchData,BaudRate,DataBits,Parity,StopBits,FlowControl,Timeout_Millisec,COMPARE_FLAG
             G::psObj->showPortSettings();

             isBegin = false;
         }
     }

     displayExtDevicesStatus();
 }

 /*************************************************************************
  Function Name - displayExtDevicesStatus
  Parameter(s)  - void
  Return Type   - void
  Action        - This function displays the error status of the external devices.
  *************************************************************************/
 void Home::displayExtDevicesStatus()
 {
     //_qDebug("void Home::displayExtDevicesStatus().");
     static bool prevDmmStatus;
     static bool prevIrStatus;
     static QVector<bool> prevPsStatus;
     static bool isPsStatusChanged;

     static QStringList usedPorts;
     static QMessageBox* msgBox; //Create and show a message box to show data processing

     msgBox = nullptr;
     usedPorts.clear();
     isPsStatusChanged = false;
     if(isFreshStart == true) {
         //set Default Values
         prevDmmStatus = true;
         prevIrStatus = true;
         prevPsStatus.clear();
         for(int i = 0;i < psAddress.count();i++) prevPsStatus.push_back(true);

         msgBox = new QMessageBox;
         msgBox->setWindowTitle(MESSAGE_TITLE);
         msgBox->setText("Please wait!!\nResources are being checked...   ");
         msgBox->setWindowFlags(Qt::CustomizeWindowHint);
         msgBox->setStandardButtons(nullptr);
         msgBox->show();
         timerObj->delayInms(1);

         isDmmConnected = true;
         isIrMeterConnected = true;
         psStatus.clear();
         for(int i = 0;i < psAddress.count();i++) psStatus.push_back(true);

#if DMM_PRESENT
         if(!G::dmmObj->isConnected())
         {
             if(!G::dmmObj->connectDMM(usedPorts))
             {
                 isDmmConnected = false;
             }
         }
         usedPorts.push_back(G::dmmObj->getPort());
#endif
         _qDebugE("displayExtDevicesStatus:DMM Check Done."," ================= "," ================= "," ================= ");
#if IR_PRESENT
         if(! G::irMeterObj->isConnected())
         {
             if(! G::irMeterObj->connectIRMeter(usedPorts))
             {
                 isIrMeterConnected = false;
             }
         }
         usedPorts.push_back( G::irMeterObj->getPort());
#endif
        _qDebugE("displayExtDevicesStatus:IR Meter Check Done."," ================= "," ================= "," ================= ");
#if PS_PRESENT
         boolean status = false;
         if(!G::psObj->isConnected(psAddress,psStatus))
         {
             status = G::psObj->connectPS(usedPorts,psAddress,psStatus);
         }
         if(status == true) initPS();
#endif
        _qDebugE("displayExtDevicesStatus:Power Supply Check Done."," ================= "," ================= "," ================= ");
     }//if block when isFreshStart == true


     if(!isChecking)
     {
         isChecking = true;

         if((prevDmmStatus != isDmmConnected) || isFreshStart)
         {
             if(isDmmConnected)
             {
                 QIcon icon(":/Images/connect.png");
                 ui->label_dmmStatus->setPixmap(icon.pixmap(30,30));
             }
             else
             {
                 QIcon icon(":/Images/disconnect.png");
                 ui->label_dmmStatus->setPixmap(icon.pixmap(30,30));
             }
         }

         if((prevIrStatus != isIrMeterConnected) || isFreshStart)
         {
             if(isIrMeterConnected)
             {
                 QIcon icon(":/Images/connect.png");
                 ui->label_irMeterStatus->setPixmap(icon.pixmap(30,30));
             }
             else
             {
                 QIcon icon(":/Images/disconnect.png");
                 ui->label_irMeterStatus->setPixmap(icon.pixmap(30,30));
             }
         }

         for(int i = 0;i < psStatus.count();i++) {
             QIcon icon1(":/Images/connect.png");
             QIcon icon2(":/Images/disconnect.png");
             if((prevPsStatus.at(i) != psStatus.at(i)) || isFreshStart)
             {
                 isPsStatusChanged = true;
                 if(psStatus.at(i))
                 {
                     switch (i) {
                         case 0 :
                                     ui->label_PSStatus_1->setPixmap(icon1.pixmap(30,30));
                                     break;
                         case 1 :
                                     ui->label_PSStatus_2->setPixmap(icon1.pixmap(30,30));
                                     break;
                         case 2 :
                                     ui->label_PSStatus_3->setPixmap(icon1.pixmap(30,30));
                                     break;
                         case 3 :
                                     ui->label_PSStatus_4->setPixmap(icon1.pixmap(30,30));
                                     break;
                         case 4 :
                                     ui->label_PSStatus_5->setPixmap(icon1.pixmap(30,30));
                                     break;
                     }
                 }
                 else
                 {
                     switch (i) {
                         case 0 :
                                     ui->label_PSStatus_1->setPixmap(icon2.pixmap(30,30));
                                     break;
                         case 1 :
                                     ui->label_PSStatus_2->setPixmap(icon2.pixmap(30,30));
                                     break;
                         case 2 :
                                     ui->label_PSStatus_3->setPixmap(icon2.pixmap(30,30));
                                     break;
                         case 3 :
                                     ui->label_PSStatus_4->setPixmap(icon2.pixmap(30,30));
                                     break;
                         case 4 :
                                     ui->label_PSStatus_5->setPixmap(icon2.pixmap(30,30));
                                     break;
                     }
                 }
             }
         }//for loop

         for(int i = 0;i < psStatus.count();i++) {
             if(prevPsStatus.at(i) != psStatus.at(i))
             {
                 isPsStatusChanged = true;
                 break;
             }
         }

         if((prevDmmStatus != isDmmConnected) || (prevIrStatus != isIrMeterConnected) || (isPsStatusChanged == true)) {
             prevDmmStatus = isDmmConnected;
             prevIrStatus = isIrMeterConnected;
             prevPsStatus.clear();
             foreach(bool stat,psStatus) prevPsStatus.push_back(stat);
             QString message = nullptr;

             if(!isDmmConnected && !isIrMeterConnected)
             {
                 message.push_back("DMM is offline.\nIR Meter is offline.\n");
             }
             else if(!isDmmConnected)
             {
                 message.push_back("DMM is offline.\n");
             }
             else if(!isIrMeterConnected)
             {
                 message.push_back("IR Meter is offline.\n");
             }

             for(int i = 0;i < psStatus.count();i++)
             {
                 if(!psStatus.at(i)) message.push_back(QString ("PS%1 is offline.\n").arg(i+1));
             }

             if(message.count() > 0) {
                 PLAY QMessageBox::warning(nullptr,"Error!!",message);
             }
         }
         isChecking = false;
     }

     if(isFreshStart) {
         msgBox->setText("Please wait!!\nRunning Self-Test...   ");
#if DMM_PRESENT
        runDmmSelfTest(isDmmConnected);
#endif
#if IR_PRESENT
        runIRMeterSelfTest(isIrMeterConnected);
#endif
#if PS_PRESENT
        runPsSelfTest(psStatus);
#endif
        clearmem(msgBox);
     }
     isFreshStart = false;
 }

/*************************************************************************
 Function Name - runDmmSelfTest
 Parameter(s)  - bool
 Return Type   - void
 Action        - This function runs selftest in DMM.
 *************************************************************************/
 void Home::runDmmSelfTest(bool isConnected) {
#if DMM_PRESENT
     if(!isDmmSelfTestDone && isConnected) {
         isDmmSelfTestDone = true;
         if(G::dmmObj->runSelfTest()) ui->label_dmm->setStyleSheet("QLabel { background-color :   #5df456  ; }");
         else {
             ui->label_dmm->setStyleSheet("QLabel { background-color :  red ; }");
             PLAY QMessageBox::warning(nullptr,"Error!!",G::dmmObj->getErrorLog());
         }
     }
#endif
 }

/*************************************************************************
 Function Name - runIRMeterSelfTest
 Parameter(s)  - bool
 Return Type   - void
 Action        - This function runs selftest in IRMeter.
 *************************************************************************/
 void Home::runIRMeterSelfTest(bool isConnected){
#if IR_PRESENT
     if(!isIRMeterSelfTestDone && isConnected) {
         isIRMeterSelfTestDone = true;
         if(G::irMeterObj->runSelfTest()) ui->label_irMeter->setStyleSheet("QLabel { background-color :   #5df456  ; }");
         else {
            ui->label_irMeter->setStyleSheet("QLabel { background-color :  red ; }");
            PLAY QMessageBox::warning(nullptr,"Error!!",G::irMeterObj->getErrorLog());
         }
     }
#endif
 }

/*************************************************************************
 Function Name - runPsSelfTest
 Parameter(s)  - QVector<bool>
 Return Type   - void
 Action        - This function runs selftest in PS.
 *************************************************************************/
 void Home::runPsSelfTest(QVector<bool> status){
#if PS_PRESENT
     if(!isPsSelfTestDone) {
         isPsSelfTestDone = true;
         QString failMessage = "";

         if(status.at(0)) {
             G::psObj->runSelfTest(PS1);
             if(G::psObj->getErrorLog().size() > 0) {
                 failMessage += G::psObj->getErrorLog() + "\n";
             }
         }
         if(status.at(1)) {
             G::psObj->runSelfTest(PS2);
             if(G::psObj->getErrorLog().size() > 0) {
                 failMessage += G::psObj->getErrorLog() + "\n";
             }
         }
         if(status.at(2)) {
             G::psObj->runSelfTest(PS3);
             if(G::psObj->getErrorLog().size() > 0) {
                 failMessage += G::psObj->getErrorLog() + "\n";
             }
         }
         if(status.at(3)) {
             G::psObj->runSelfTest(PS4);
             if(G::psObj->getErrorLog().size() > 0) {
                 failMessage += G::psObj->getErrorLog() + "\n";
             }
         }
         if(status.at(4)) {
             G::psObj->runSelfTest(PS5);
             if(G::psObj->getErrorLog().size() > 0) {
                 failMessage += G::psObj->getErrorLog() + "\n";
             }
         }

         if(failMessage.size() > 0) {
             PLAY QMessageBox::warning(nullptr,"Error!!",failMessage);
         }
     }
#endif
 }

 /*************************************************************************
  Function Name - updateTestTimer
  Parameter(s)  - void
  Return Type   - void
  Action        - This function updates the test_timer_label when test runs.
  *************************************************************************/
 void Home::updateTestTimer()
 {
     QString text;
     timeElapSec++;
     if(timeElapSec > 59)
     {
         timeElapMin++;
         timeElapSec = 0;
     }
     if(timeElapMin > 59)
     {
         timeElapHour++;
         timeElapMin = 0;
         timeElapSec = 0;
     }
     text.sprintf("%02d H : %02d M : %02d S",timeElapHour,timeElapMin,timeElapSec);
     ui->label_timer->setText(text);
 }

 /*************************************************************************
  Function Name - updateCount
  Parameter(s)  - void
  Return Type   - void
  Action        - Updates testCount.
  *************************************************************************/
 void Home::updateCount()
 {
     G::testCount++;
 }

 /*************************************************************************
  Function Name - startUpdateTimerLabel
  Parameter(s)  - void
  Return Type   - void
  Action        - This function starts timer to update test_timer_label.
  *************************************************************************/
 void Home::startUpdateTimerLabel()
 {
    timeElapSec = 0;
    timeElapMin = 0;
    timeElapHour = 0;
 }

 /*************************************************************************
  Function Name - stopUpdateTimerLabel
  Parameter(s)  - void
  Return Type   - void
  Action        - This function stops the test timer.
  *************************************************************************/
 void Home::stopUpdateTimerLabel()
 {
    if(testTimerObj->isActive()) testTimerObj->stop();
 }

 /*************************************************************************
  Function Name - update_home_ui
  Parameter(s)  - void
  Return Type   - void
  Action        - This function updates the home ui and returns the status.
  *************************************************************************/
 void Home::update_home_ui()
 {
     _qDebug("void Home::update_home_ui()");

     QVector<QStringList> uutTableData;
     QVector<QStringList> uutTestTableData;
     QVector<QStringList> dopTableData;
     QVector<QStringList> dipTableData;
     QVector<QStringList> resetTableData;
     QString uutTestTableName;
     QString uutPinMapTableName;
     QString uutDopMapTableName;
     QString uutDipMapTableName;
     QString uutResetTableName;
     QStringList testTableLists;
     QStringList testLists;
     QStringList testStartMessage;
     QStringList testIntermediateMessage;
     QStringList tableColumnWidths;
     QStringList tableHeaders;
     QMenu* menu = nullptr;
     QAction* action = nullptr;
     QString str;
     QMap<QString, uint32> data;

     if((!isUpdatedOnce) || isEdit)
     {
         uutTableData.clear();

         //Find the parent table "UUT" under uutTableName and get uut_names
         if(!homeDbObj->getTableData(uutTableName,uutTableData))
         {
             PLAY QMessageBox::warning(nullptr,"Error!!",QString("Fetching table:[%1] failed at Home::update_home_ui().").arg(uutTableName));
             return;
         }

         _qDebugE("uutTableData:",uutTableData,"","");

         //Delete Actions
         for(int i = 0;i < testQactionLists.size();i++){clearmem(testQactionLists[i]);}
         //Delete Menus
         for(int i = 0;i < uutQmenuLists.size();i++)      {clearmem(uutQmenuLists[i]);}

         uutTestTableNameLists.       clear();
         uutQmenuLists.               clear();
         testQactionLists.            clear();
         unitTestDataTableLists.      clear();
         unitTestLists.               clear();
         testStartMessageLists.       clear();
         testIntermediateMessageLists.clear();
         tableColumnWidthLists       .clear();
         unitResetTableLists         .clear();
         pinMapData.                  clear();
         dopMapData.                  clear();
         dipMapData.                  clear();

         //Run loop to get table_name of uut
         foreach (const QStringList &uut, uutTableData)
         {
             if(uut.size() != UUT_TABLE_COLUMNS) {
                 PLAY QMessageBox::warning(nullptr,"Error!!",QString("The number of data columns in table:[%1] is not %2.")
                                      .arg(uutTableName).arg(UUT_TABLE_COLUMNS));
                 return;
             }

             menu = ui->menuUnit_Selection->addMenu(uut.at(0));
             if(menu == nullptr) {
                 PLAY QMessageBox::warning(nullptr,"Error!!",QString("Failed to add menu:[%1] in UI.").arg(uut.at(0)));
                 return;
             }
             uutQmenuLists.push_back(menu);

             uutTestTableName = uut.at(1);
             uutTestTableNameLists.push_back(uutTestTableName);

             uutPinMapTableName = uut.at(2);
             uutDopMapTableName = uut.at(3);
             uutDipMapTableName = uut.at(4);
             uutResetTableName  = uut.at(5);

             uutTestTableData.clear();
             testLists.clear();
             testTableLists.clear();
             testStartMessage.clear();
             testIntermediateMessage.clear();
             tableColumnWidths.clear();

             if(!homeDbObj->getTableData(uutTestTableName,uutTestTableData))
             {
                 PLAY QMessageBox::warning(nullptr,"Error!!",QString("Fetching table:[%1] failed at Home::update_home_ui().").arg(uutTestTableName));
                 return;
             }

             _qDebugE("uutTestTableData:[",uutTestTableData,"]","");

             foreach (const QStringList &testTable, uutTestTableData)
             {
                 if(testTable.size() != UUT_TEST_TABLE_COLUMNS) {
                     PLAY QMessageBox::warning(nullptr,"Error!!",QString("The number of data columns in table:[%1] is not %2.")
                                          .arg(uutTestTableName).arg(UUT_TEST_TABLE_COLUMNS));
                     return;
                 }

                 action = menu->addAction(testTable.at(0));
                 if(action == nullptr) {
                     PLAY QMessageBox::warning(nullptr,"Error!!",QString("Failed to add menu action:[%1] in UI.").arg(testTable.at(0)));
                     return;
                 }
                 testQactionLists.push_back(action);

                 testLists.push_back(testTable.at(0));
                 testTableLists.push_back(testTable.at(1));
                 testStartMessage.push_back(testTable.at(2));
                 testIntermediateMessage.push_back(testTable.at(3));
                 tableColumnWidths.push_back(testTable.at(4));

                 //connect(action, &QAction::triggered, [=]{on_action_triggered(global_count);});
                 connect(action, &QAction::triggered,  [this, action, menu](){
                     on_action_triggered(menu->title(),action->text());});
             }

             unitTestLists.push_back(testLists);
             unitTestDataTableLists.push_back(testTableLists);
             testStartMessageLists.push_back(testStartMessage);
             testIntermediateMessageLists.push_back(testIntermediateMessage);
             tableColumnWidthLists.push_back(tableColumnWidths);

             _qDebugE("unitTestLists:\n",unitTestLists,"\nunitTestDataTableLists:\n",unitTestDataTableLists);
             _qDebugE("testStartMessageLists:\n",testStartMessageLists,"\ntestIntermediateMessageLists:\n",testIntermediateMessageLists);
             _qDebugE("tableColumnWidthLists:\n",tableColumnWidthLists,"","");

             //Get Pin Map Table Data for uut
             uutTestTableData.clear();
             if(!homeDbObj->getTableData(uutPinMapTableName,uutTestTableData))
             {
                 PLAY QMessageBox::warning(nullptr,"Error!!",QString("Fetching table:[%1] failed at Home::update_home_ui().").arg(uutPinMapTableName));
                 return;
             }
             data.clear();
             for(const QStringList& s : uutTestTableData) {
                 if(s.size() != 2) {
                     PLAY QMessageBox::warning(nullptr,"Error!!",QString("The number of data columns in table:[%1] is not 2.").arg(uutPinMapTableName));
                     return;
                 }
                 data.insert(s.at(0),s.at(1).toUInt());
             }
             pinMapData.push_back(data);

             //Get DOP Map Table Data for uut
             dopTableData.clear();
             if(!homeDbObj->getTableData(uutDopMapTableName,dopTableData))
             {
                 PLAY QMessageBox::warning(nullptr,"Error!!",QString("Fetching table:[%1] failed at Home::update_home_ui().").arg(uutDopMapTableName));
                 return;
             }
             data.clear();
             for(const QStringList& s : dopTableData) {
                 if(s.size() != 2) {
                     PLAY QMessageBox::warning(nullptr,"Error!!",QString("The number of data columns in table:[%1] is not 2.").arg(uutDopMapTableName));
                     return;
                 }
                 data.insert(s.at(0),s.at(1).toUInt());
             }
             dopMapData.push_back(data);

             //Get DIP Map Table Data for uut
             dipTableData.clear();
             if(!homeDbObj->getTableData(uutDipMapTableName,dipTableData))
             {
                 PLAY QMessageBox::warning(nullptr,"Error!!",QString("Fetching table:[%1] failed at Home::update_home_ui().").arg(uutDipMapTableName));
                 return;
             }
             data.clear();
             for(const QStringList& s : dipTableData) {
                 if(s.size() != 2) {
                     PLAY QMessageBox::warning(nullptr,"Error!!",QString("The number of data columns in table:[%1] is not 2.").arg(uutDipMapTableName));
                     return;
                 }
                 data.insert(s.at(0),s.at(1).toUInt());
             }
             dipMapData.push_back(data);

             resetTableData.clear();
             if(!homeDbObj->getTableData(uutResetTableName,resetTableData))
             {
                 PLAY QMessageBox::warning(nullptr,"Error!!",QString("Fetching table:[%1] failed at Home::update_home_ui().").arg(uutResetTableName));
                 return;
             }
             if(!homeDbObj->getColumnNames(uutResetTableName,tableHeaders))
             {
                 PLAY QMessageBox::warning(nullptr,"Error!!",QString("Fetching column names for table:[%1] failed at Home::update_home_ui().").arg(uutResetTableName));
                 return;
             }
             unitResetTableLists.push_back(getUnitResetData(tableHeaders,resetTableData));
         }//foreach loop runs for number of rows in units table

         if(!isEdit) isEdit = false;
         if(!isUpdatedOnce) isUpdatedOnce = true;
     }//main if block
     else
     {
         _qDebugE("Home UI is not updated. ","isUpdatedOnce:",isUpdatedOnce,"");
         _qDebugE("isEdit:",isEdit,"","");
     }
 }

 /*************************************************************************
  Function Name - getUnitResetData
  Parameter(s)  - QStringList, QVector<QStringList>
  Return Type   - Utility::ResetData
  Action        - This function extracts the reset data from table and returns
                  the reset data structure.
  *************************************************************************/
 Utility::ResetData Home:: getUnitResetData(QStringList headers, QVector<QStringList> resetTableData)
 {
     Utility::ResetData resetData;
     resetData.clear();

     if(resetTableData.count() > 0) {
         uint8_t         dipsIndex = UINT8_MAX;
         uint8_t dipsExpValueIndex = UINT8_MAX;
         uint8_t     dopHigh2Index = UINT8_MAX;
         uint8_t      dopLow2Index = UINT8_MAX;

         for(uint8_t i = 0;i < headers.count();i++)
         {
             if(     headers.at(i).compare(utilObj._hidden1.dips        )== 0) dipsIndex         = i;
             else if(headers.at(i).compare(utilObj._hidden1.dipsExpValue)== 0) dipsExpValueIndex = i;
             else if(headers.at(i).compare(utilObj._hidden1.dopHigh2    )== 0) dopHigh2Index     = i;
             else if(headers.at(i).compare(utilObj._hidden1.dopLow2     )== 0) dopLow2Index      = i;
         }

         if(dipsIndex  == UINT8_MAX) {
             PLAY QMessageBox::warning(nullptr,"Error!!",QString("DIPS column is not present in Table Header."));
             return resetData;
         }
         if(dipsExpValueIndex == UINT8_MAX) {
             PLAY QMessageBox::warning(nullptr,"Error!!",QString("Expected_DIPS_Value column is not present in Table Header."));
             return resetData;
         }
         if(dopHigh2Index == UINT8_MAX) {
             PLAY QMessageBox::warning(nullptr,"Error!!",QString("DOP_HIGH_2 column is not present in Table Header."));
             return resetData;
         }
         if(dopLow2Index == UINT8_MAX) {
             PLAY QMessageBox::warning(nullptr,"Error!!",QString("DOP_LOW_2  column is not present in Table Header."));
             return resetData;
         }

         for(uint8_t i = 0;i < resetTableData.count();i++)
         {
             resetData.dipsList.push_back(resetTableData.at(i).at(dipsIndex));
             resetData.dipsExpectedValueList.push_back(resetTableData.at(i).at(dipsExpValueIndex));
             resetData.dopsHigh2List.push_back(resetTableData.at(i).at(dopHigh2Index));
             resetData.dopsLow2List.push_back(resetTableData.at(i).at(dopLow2Index));

             if(resetData.dipsList.at(i).split(",").count() != resetData.dipsExpectedValueList.at(i).split(",").count()) {
                 PLAY QMessageBox::warning(nullptr,"Error!!",QString("No_Of_DIPS and No_Of_Expected_DIPValues mismatching.\n"
                                                                     "DIPS:[%1], ExpectedValues[%2].").arg(resetData.dipsList.at(i))
                                                                     .arg(resetData.dipsExpectedValueList.at(i)));
                 resetData.clear();
                 break;
             }
         }
         if(resetData.dipsList.count() != resetData.dipsExpectedValueList.count()) {
             PLAY QMessageBox::warning(nullptr,"Error!!",QString("Total rows of No_Of_DIPS and No_Of_Expected_DIPValues mismatching.\n"
                                                                 "Total rows having DIPS:[%1], Total rows having DIPS' ExpectedValues[%2].")
                                                                 .arg(resetData.dipsList.count())
                                                                 .arg(resetData.dipsExpectedValueList.count()));
             resetData.clear();
         }
         resetData.count = resetData.dipsList.count();
     }

     return resetData;
 }

 /*************************************************************************
  Function Name - get_file_name
  Parameter(s)  - QString
  Return Type   - QString
  Action        - This function extracts the file name without the extension
                  from the file path.
  *************************************************************************/
 QString Home::get_file_name(QString file_path)
 {
     _qDebug("QString Home::get_file_name(QString file_path).");

     QStringList tokens;

     QString file_name;

     tokens = file_path.split("/");

     //_qDebugE("tokens:",tokens,"","");

     file_name.clear();

     if(tokens.size() > 0)
     {
         file_name =  tokens.at(tokens.size() - 1);
     }

     //remove extension
     tokens = file_name.split(".");

     if(tokens.size() > 0)
     {
         file_name =  tokens.at(0);
     }
     else
     {
         file_name = "";
     }
     return file_name;
 }

/*************************************************************************
 Function Name - runTest
 Parameter(s)  - void, int
 Return Type   - void
 Action        - This function runs the required test for given number of cycles.
 *************************************************************************/
 void Home::runTest(int cycleCount)
 {
    _qDebug("void Home::runTest(int cycleCount).");

    QString uutTestName = ui->label_testName->text().remove(' ');
    _qDebugE("uutTestName:[",uutTestName,"]","");
    QString text;
    QMessageBox::StandardButton resBtn = QMessageBox::Yes;
    bool isManualTest = false;

    testData.clear();
    statusData.clear();
    startUpdateTimerLabel();
    DB

    G::startDate = G::startTime = G::endDate= G::endTime = "";
    if(isCredentialsEmpty(text))
    {
        INFO resBtn = QMessageBox::question( this, MESSAGE_TITLE,text,QMessageBox::No | QMessageBox::Yes);
    }

    if (resBtn == QMessageBox::Yes)
    {
        if(uutTestName.contains("isolationpintochassis",Qt::CaseInsensitive))
        {
            IsolationP2C::runTest(pinMapData.at(uutIndex),hiddenDataHeaders,hiddenData);
            saveTable();
            ui->actionSave_Table->setVisible(true);
        }
        else if(uutTestName.contains("insulationpintochassis",Qt::CaseInsensitive))
        {
            InsulationP2C::runTest(pinMapData.at(uutIndex),hiddenDataHeaders,hiddenData);
            saveTable();
            ui->actionSave_Table->setVisible(true);
        }
        else if(uutTestName.contains("isolationpintopin",Qt::CaseInsensitive))
        {
            IsolationP2P_V1::runTest(pinMapData.at(uutIndex),hiddenDataHeaders,hiddenData);
            saveTable();
            ui->actionSave_Table->setVisible(true);
        }
        else if(uutTestName.contains("levelshifter",Qt::CaseInsensitive))
        {
            //Set Power Supply ON
            if(onLevelShifterPs())
            {
                for(int cycleNo = 1;cycleNo <= cycleCount;cycleNo++)
                {
                    if(G::stopTest) break;

                    QString cycleDisplay = "Cycle: "+ QString::number(cycleNo) + "/" + QString::number(cycleCount);
                    ui->label_cycle->setText(cycleDisplay);
                    LevelShifter::runTest(pinMapData.at(uutIndex),hiddenDataHeaders,hiddenData);
                    saveTable();
                }
                offLevelShifterPs();
            }
        }
        else if(uutTestName.contains("manualpolarity",Qt::CaseInsensitive))
        {
            isManualTest = true;
            if(onLoadPolarityPS())
            {
                if(uutTestName.contains("standby",Qt::CaseInsensitive) || uutTestName.contains("umb",Qt::CaseInsensitive))
                    ManualPolarity::runTest(pinMapData.at(uutIndex),dopMapData.at(uutIndex),dipMapData.at(uutIndex),
                                         hiddenDataHeaders,manualDataHidden_StandBy.value(ui->comboBox_manualPolarity->currentText()));
                else
                    ManualPolarity::runTest(pinMapData.at(uutIndex),dopMapData.at(uutIndex),dipMapData.at(uutIndex),
                                         hiddenDataHeaders,manualDataHidden_Main.value(ui->comboBox_manualPolarity->currentText()));

                saveTable();
                offLoadPolarityPS();
            }
        }
        else if(uutTestName.contains("polarity",Qt::CaseInsensitive))
        {
            if(onLoadPolarityPS())
            {
                for(int cycleNo = 1;cycleNo <= cycleCount;cycleNo++)
                {
                    if(G::stopTest) break;

                    QString cycleDisplay = "Cycle: "+ QString::number(cycleNo) + "/" + QString::number(cycleCount);
                    ui->label_cycle->setText(cycleDisplay);
                    Polarity::runTest(pinMapData.at(uutIndex),dopMapData.at(uutIndex),dipMapData.at(uutIndex),
                                      hiddenDataHeaders,hiddenData,unitResetTableLists.at(uutIndex));
                    saveTable();
                }
                offLoadPolarityPS();
            }
        }
        else if(uutTestName.contains("load",Qt::CaseInsensitive))
        {
            if(onLoadPolarityPS())
            {
                for(int cycleNo = 1;cycleNo <= cycleCount;cycleNo++)
                {
                    if(G::stopTest) break;

                    QString cycleDisplay = "Cycle: "+ QString::number(cycleNo) + "/" + QString::number(cycleCount);
                    ui->label_cycle->setText(cycleDisplay);
                    Load::runTest(pinMapData.at(uutIndex),dopMapData.at(uutIndex),dipMapData.at(uutIndex),
                                  hiddenDataHeaders,hiddenData,unitResetTableLists.at(uutIndex));
                    saveTable();
                }
                offLoadPolarityPS();
            }
        }
        else if(uutTestName.contains("emi",Qt::CaseInsensitive))
        {
            if(onLoadPolarityPS())
            {
                ui->label_cycle->setText("Cycle No: 1");
                connect(G::cycleTimer,SIGNAL(timeout()),this,SLOT(updateCount()));
                EmiEmc::runTest(pinMapData.at(uutIndex),dopMapData.at(uutIndex),dipMapData.at(uutIndex),hiddenDataHeaders,
                             hiddenData,testData,statusData);
                offLoadPolarityPS();
                disconnect(G::cycleTimer,SIGNAL(timeout()),this,SLOT(updateCount()));
            }
        }
    }
    stopUpdateTimerLabel();
    DMM::isreadBusy     = false;
    IRMeter::isreadBusy = false;
    clearmem(stopMsgBox);

    _qDebugE("No of Test Tables:",testData.count(),", isAutoSaveReport:",isAutoSaveReport);
    //There should be at least one table and 2 rows(1st row is table header) to generate report
    if(testData.count() > 0) {
        enableResultSaving(true);
        if(isAutoSaveReport && (!isManualTest)) generateReport(testData,statusData,"");
    }

    ui->pushButton_run_stop->setText("RUN");
    ui->pushButton_run_stop->setToolTip("Click to Run Test");
    ui->pushButton_run_stop->setEnabled(true);
    EB
 }

/*************************************************************************
 Function Name - isCredentialsEmpty
 Parameter(s)  - QString&
 Return Type   - bool
 Action        - Checks the user entry credential and keeps the message in
                 the given buffer warningText. If any field is missing returns
                 true, else returns false.
 *************************************************************************/
 bool Home::isCredentialsEmpty(QString& warningText)
 {
     bool status = false;
     int uutNo,tot,userRep,qaRep;
     uutNo = ui->lineEdit_uutNumber->text().count();
     tot = ui->lineEdit_typeOfTest->text().count();
     userRep = ui->lineEdit_UserPerson->text().count();
     qaRep = ui->lineEdit_QAperson->text().count();
     warningText.clear();
     if(!uutNo && !tot && !userRep && !qaRep)
     {
         warningText.push_back("UUT No,Type Of Test,User Rep and QA Rep fields are empty.\nDo you want to Continue?");
     }
     else if(!uutNo && !tot && !userRep)
     {
         warningText.push_back("UUT No,Type Of Test and User Rep fields are empty.\nDo you want to Continue?");
     }
     else if(!uutNo && !tot && !qaRep)
     {
         warningText.push_back("UUT No,Type Of Test and QA Rep fields are empty.\nDo you want to Continue?");
     }
     else if(!uutNo && !tot)
     {
         warningText.push_back("UUT No and Type Of Test fields are empty.\nDo you want to Continue?");
     }
     else if(!uutNo && !userRep && !qaRep)
     {
         warningText.push_back("UUT No,User Rep and QA Rep fields are empty.\nDo you want to Continue?");
     }
     else if(!uutNo && !userRep)
     {
         warningText.push_back("UUT No and User Rep fields are empty.\nDo you want to Continue?");
     }
     else if(!uutNo && !qaRep)
     {
         warningText.push_back("UUT No and QA Rep fields are empty.\nDo you want to Continue?");
     }
     else if(!uutNo)
     {
         warningText.push_back("UUT No field is empty.\nDo you want to Continue?");
     }
     else if(!tot)
     {
         warningText.push_back("Type Of Test field is empty.\nDo you want to Continue?");
     }
     else if(!userRep)
     {
         warningText.push_back("User Rep field is empty.\nDo you want to Continue?");
     }
     else if(!qaRep)
     {
         warningText.push_back("QA Rep field is empty.\nDo you want to Continue?");
     }

     if(warningText.count() > 0) status = true;//Indicates user field(s) is/are empty
     return status;
 }

/*************************************************************************
 Function Name - saveTable
 Parameter(s)  - void
 Return Type   - void
 Action        - Saves table's data.
 *************************************************************************/
 void Home::saveTable()
 {
     //Note: Don't clear testData and  buffer here. Because in next cycle the data will be appended.
     //In testData the SerialNo is not added.
     _qDebug("void Home::saveTable()");
     QVector<QStringList> tableData;
     QStringList data;
     QString resultStr, remarkStr;
     int columnCount;
     int resultIndex = 0;
     int remarkIndex = 0;

     tableData.clear();
     data.clear();
     columnCount = G::testTable->columnCount();

     //Storing Table Header
     for(int colNo = 1;colNo < columnCount;colNo++)
     {
         data.push_back(G::testTable->horizontalHeaderItem(colNo)->text());

         resultStr = remarkStr = G::testTable->horizontalHeaderItem(colNo)->text();
         if(resultStr.compare(utilObj._headers1.result) == 0) resultIndex = colNo;
         if(remarkStr.compare(utilObj._headers1.remark) == 0) remarkIndex = colNo;
     }
     tableData.push_back(data);

     for(int rowNo = 0;rowNo < G::testTable->rowCount();rowNo++)
     {
         resultStr = G::testTable->item(rowNo,resultIndex)->text();
         remarkStr = G::testTable->item(rowNo,remarkIndex)->text();

         if(resultStr.size() > 0 || (remarkStr.size() > 0))
         {
             data.clear();
             for(int colNo = 1;colNo < columnCount;colNo++) {
                 data.push_back(G::testTable->item(rowNo,colNo)->text());
             }
             tableData.push_back(data);
         }
     }
     if(tableData.count() > 1) testData.push_back(tableData);
     statusData.push_back(ui->label_TestStatus->text());

     _qDebugE("testData:",testData,"\n statusData:",statusData);
 }

/*************************************************************************
 Function Name - savePassTable
 Parameter(s)  - QVector<QVector<QStringList>>&, QStringList&
 Return Type   - void
 Action        - Saves table's pass data only.
 *************************************************************************/
 void Home::getPassData(QVector<QVector<QStringList>>& passData, QStringList& testStatus)
 {
    QVector<QStringList> tableData;
    QStringList rowData;

    passData.clear();

    for(QVector<QStringList> data : testData) {
        testStatus.push_back("Pass");
        tableData.clear();
        if(data.count() > 0) tableData.push_back(data.at(0));
        else continue;

        for(int rowNo = 1;rowNo < data.count();rowNo++) {
            rowData = data.at(rowNo);
            if(rowData.contains("pass",Qt::CaseInsensitive) == true){
                tableData.push_back(rowData);
            }
        }
        if(tableData.count() > 1) passData.push_back(tableData);
    }

    _qDebugE("passData:",passData,"\n.........................","");
 }

/*************************************************************************
 Function Name - savePassTable
 Parameter(s)  - QVector<QVector<QStringList>>&, QStringList&
 Return Type   - void
 Action        - Saves table's fail data only.
 *************************************************************************/
 void Home::getFailData(QVector<QVector<QStringList>>& failData, QStringList& testStatus)
 {
    QVector<QStringList> tableData;
    QStringList data;
    QString str;

    failData.clear();
    data.clear();

    for(int tableNo = 0;tableNo < statusData.count();tableNo++)
    {
        testStatus.push_back("Fail");
        tableData.clear();
        str = statusData.at(tableNo);

        if(str.contains("fail",Qt::CaseInsensitive) == true)
        {
            tableData.push_back(testData.at(tableNo).at(0));//Table Header

            for(int rowNo = 1;rowNo < testData.at(tableNo).count();rowNo++)
            {
                data = testData.at(tableNo).at(rowNo);
                if(data.contains("fail",Qt::CaseInsensitive) == true){
                    tableData.push_back(data);
                }
            }
            if(tableData.count() > 1) failData.push_back(tableData);
        }
    }
    _qDebugE("failData:",failData,"\n.........................","");
 }

/*************************************************************************
 Function Name - onLevelShifterPs
 Parameter(s)  - void
 Return Type   - bool
 Action        - Makes PS output ON for Level Shifter test.
 *************************************************************************/
 bool Home::onLevelShifterPs()
 {
    bool ret = true;
    PowerSupply::isBusy = true;

    G::showInfoMessage("PS","Please wait!! Preparing PS O/P ON...  ");

    while(!G::handlerObj->isPSReady) QApplication::processEvents();

#if(PS_PRESENT)
    if(G::psObj->setOutputOn(LEVEL_SHIFTER_PS)) {
        G::psStatus.at(LEVEL_SHIFTER_PS - 1)->setStyleSheet("QLabel { background-color : #3AEA02;}");
    }
    else {
         G::closeTestMessage();
         PLAY QMessageBox::warning(nullptr,"Error!!",G::psObj->getErrorLog());
         ret = false;
    }
#endif
    PowerSupply::isBusy = false;
    G::closeTestMessage();
    return ret;
 }

/*************************************************************************
 Function Name - offLevelShifterPs
 Parameter(s)  - void
 Return Type   - bool
 Action        - Makes PS output OFF for Level Shifter test.
 *************************************************************************/
 bool Home::offLevelShifterPs()
 {
    bool ret = true;
    PowerSupply::isBusy = true;

    G::showInfoMessage("PS","Please wait!! Preparing PS O/P OFF...  ");

    while(!G::handlerObj->isPSReady) QApplication::processEvents();

#if(PS_PRESENT)
    if(G::psObj->setOutputOff(LEVEL_SHIFTER_PS)) {
        G::psStatus.at(LEVEL_SHIFTER_PS - 1)->setStyleSheet("QLabel {  background-color : red;}");
    }
    else {
         G::closeTestMessage();
         PLAY QMessageBox::warning(nullptr,"Error!!",G::psObj->getErrorLog());
         ret = false;
    }
#endif
    PowerSupply::isBusy = false;
    G::closeTestMessage();
    return ret;
 }

/*************************************************************************
 Function Name - onLoadPolarityPS
 Parameter(s)  - void
 Return Type   - bool
 Action        - Makes PS output ON for Load & Polarity test.
 *************************************************************************/
 bool Home::onLoadPolarityPS()
 {
    _qDebug("bool Home::onLoadPolarityPS().");
    bool ret = true;
    PowerSupply::isBusy = true;

    G::showInfoMessage("PS","Please wait!! Preparing PS O/P ON...  ");

    while(!G::handlerObj->isPSReady) QApplication::processEvents();

#if(PS_PRESENT)
    if(G::psObj->setOutputOn({INPUT_POWER_SUPPLY_PS, COIL_SUPPLY_PS, STATUS_SUPPLY_PS}) == true) {
        G::psStatus.at(INPUT_POWER_SUPPLY_PS - 1)->setStyleSheet("QLabel { background-color : #3AEA02;}");
        G::psStatus.at(COIL_SUPPLY_PS        - 1)->setStyleSheet("QLabel { background-color : #3AEA02;}");
        G::psStatus.at(STATUS_SUPPLY_PS      - 1)->setStyleSheet("QLabel { background-color : #3AEA02;}");
    }
    else {
         G::closeTestMessage();
         PLAY QMessageBox::warning(nullptr,"Error!!",G::psObj->getErrorLog());
         ret = false;
    }
#endif
    PowerSupply::isBusy = false;
    G::closeTestMessage();
    return ret;
}

 /*************************************************************************
  Function Name - offLoadPolarityPS
  Parameter(s)  - void
  Return Type   - bool
  Action        - Makes PS output OFF for Load & Polarity test.
  *************************************************************************/
 bool Home::offLoadPolarityPS()
 {
     _qDebug("bool Home::offLoadPolarityPS().");
     bool ret = true;
     PowerSupply::isBusy = true;

     G::showInfoMessage("PS","Please wait!! Preparing PS O/P OFF...  ");

     while(!G::handlerObj->isPSReady) QApplication::processEvents();

#if(PS_PRESENT)
     if(G::psObj->setOutputOff({INPUT_POWER_SUPPLY_PS, COIL_SUPPLY_PS, STATUS_SUPPLY_PS})) {
         G::psStatus.at(INPUT_POWER_SUPPLY_PS - 1)->setStyleSheet("QLabel { background-color : red;}");
         G::psStatus.at(COIL_SUPPLY_PS - 1       )->setStyleSheet("QLabel { background-color : red;}");
         G::psStatus.at(STATUS_SUPPLY_PS - 1     )->setStyleSheet("QLabel { background-color : red;}");
     }
     else {
         PLAY QMessageBox::warning(nullptr,"Error!!",G::psObj->getErrorLog());
          ret = false;
     }
#endif
     PowerSupply::isBusy = false;
     G::closeTestMessage();
     return ret;
  }

/*************************************************************************
 Function Name - cleanMyWidget
 Parameter(s)  - void
 Return Type   - void
 Action        - clears the custom widget
 *************************************************************************/
 void Home::cleanMyWidget()
 {
     _qDebug("void Home::cleanMyWidget().");

     clearmem(label_cycleObj);
     clearmem(spinBox_cycleObj);
     clearmem(blankLabel);
     clearmem(pushButton_okObj);
     clearmem(pushButton_cancelObj);
     clearmem(comboBox_tablesObj);
     clearmem(pushButton_exportObj);
     clearmem(pushButton_deleteObj);
     clearmem(myWidgetObj);

     this->setEnabled(true);
 }

 /*************************************************************************
  Function Name - loadManualIsolationTest
  Parameter(s)  - void
  Return Type   - bool
  Action        - Opens Manual Isolation Test Window.
  *************************************************************************/
  bool Home::loadManualIsolationTest()
  {
      _qDebug("bool Home::loadManualIsolationTest().");
      QString title = "ManualIsolationTest";
      manualIsolationObj = new ManualIsolation;

      if(manualIsolationObj == nullptr) {
          PLAY QMessageBox::warning(nullptr,"Error!!","ManualTestObj object creation failed.");
          return false;
      }
      else {
          QMessageBox* msgBox = new QMessageBox;
          msgBox->setWindowTitle(title);
          msgBox->setText("Please wait!! "+title+" Window is being loaded...  ");
          msgBox->setWindowFlags(Qt::CustomizeWindowHint);
          msgBox->setStandardButtons(nullptr);
          msgBox->show();
          timerObj->delayInms(1);

          timerMeterCheckObj->stop();

          manualIsolationObj->setMsgTitle(title);
          manualIsolationObj->setWindowIcon(QIcon(":/Images/atl.ico"));

          DMM::isreadBusy = true;
          while(!G::handlerObj->isDMMReady) timerObj->delayInms(1);

          connect(manualIsolationObj,SIGNAL(closeUI()), this, SLOT(closeManualIsolationTest()));
          manualIsolationObj->setWindowTitle(title);
          manualIsolationObj->initialize(extractValueFromLabel(ui->label_userName->text()),
                                    extractValueFromLabel(ui->label_unitName->text()),
                                    ui->lineEdit_uutNumber->text(),
                                    ui->lineEdit_typeOfTest->text(),
                                    ui->lineEdit_UserPerson->text(),
                                    ui->lineEdit_QAperson->text(),
                                    pinMapData.at(uutIndex));
          hide();
          manualIsolationObj->show();

          clearmem(msgBox);
      }
      return true;
  }

/*************************************************************************
 Function Name - loadTableFromDb
 Parameter(s)  - QString,QString
 Return Type   - bool
 Action        - loads table from database.
 *************************************************************************/
 bool Home::loadTableFromDb(QString tableName,QString columnWidth)
 {
     _qDebug("bool Home::loadTableFromDb(QString tableNamee,QString columnWidth).");
     bool ret = true;
     QVector<QStringList> tableData;
     QTableWidgetItem* item = nullptr;
     QStringList tableHeaders;
     QMessageBox* messageBox = nullptr;

     int count = 0;
     int rowNo;
     int columnNo;
     int rowCount;
     int columnCount;
     int dispColumnCount;

     //clean table
     ui->tableWidget->horizontalHeader()->hide();
     ui->tableWidget->setRowCount(0);
     timerObj->delayInms(10);//table stable time

     tableData.clear();
     checkBoxLists.clear();
     hiddenDataHeaders.clear();
     hiddenData.clear();

     ui->actionSelect_All->setDisabled(true);
     ui->actionUnselect_All->setDisabled(true);

     if(tableName.size() == 0)
     {
         PLAY QMessageBox::warning(nullptr,"Error!!\n","Table name is Blank.");
         ret = false;
     }
     else
     {
         //Get Table Header
         tableHeaders.clear();
         ret = homeDbObj->getColumnNames(tableName,tableHeaders);
         dispColumnCount = (tableHeaders.indexOf("Remarks") + 1);

         if(!ret)
         {
             _qDebugE("Fetching table headers: ",tableName," failed.","");
             PLAY QMessageBox::warning(nullptr,"Error!!\n","No Data Found");
         }
         else
         {
             _qDebugE("tableHeaders:",tableHeaders," , dispColumnCount:",dispColumnCount);

             ret = homeDbObj->getTableData(tableName,tableData);

             if(!ret)
             {
                 _qDebugE("Fetching table: ",tableName," failed.","");
                 PLAY QMessageBox::warning(nullptr,"Error!!\n","No Data Found");
             }
             else
             {
                 _qDebugE("tableData:",tableData,"","");

                 rowCount = tableData.count();
                 columnCount = tableHeaders.count();

                 _qDebugE("Rows:",rowCount," Columns:",columnCount);

                 if(rowCount == 0 || columnCount == 0)
                 {
                     PLAY QMessageBox::warning(nullptr,"Error!!\n","No Data Found");
                     ret = false;
                 }
                 else
                 {
                     ui->tableWidget->horizontalHeader()->show();

                     //Create and show a message box to show data processing
                     messageBox = new QMessageBox;
                     messageBox->setText("Please wait!! Data is being loaded...");
                     messageBox->setWindowFlags(Qt::CustomizeWindowHint);
                     messageBox->setStandardButtons(nullptr);
                     messageBox->show();
                     timerObj->delayInms(1);

                     //load tableData to table=====================
                     //load table header
                     ui->tableWidget->setColumnCount(dispColumnCount);

                     _qDebugE("Setting Column Names ...","","","");
                     for(int i = 0; i < dispColumnCount;i++)
                     {
                         item = new QTableWidgetItem(tableHeaders.at(i));
                         item->setFont(_font);
                         item->setForeground(Qt::blue);
                         item->setBackground(QColor("lightblue"));
                         ui->tableWidget->setHorizontalHeaderItem(i, item);
                     }
                     G::enableTableHeaderUnderline(ui->tableWidget);

                     for(int i = dispColumnCount;i < columnCount;i++) hiddenDataHeaders.push_back(tableHeaders.at(i));

                     _qDebugE("Setting Rows ...","","","");
                     //load table body
                     for(count = 0,rowNo = 0;rowNo < rowCount;rowNo++)
                     {
                         ui->tableWidget->insertRow(rowNo);

                         if(tableData.at(rowNo).at(0) == PAUSE_POINT_STR) {
                             for(columnNo = 0;columnNo < dispColumnCount;columnNo++)
                             {
                                 item = new QTableWidgetItem("");
                                 ui->tableWidget->setItem(rowNo,columnNo,item);
                                 ui->tableWidget->item(rowNo,columnNo)->setBackground(Qt::gray);
                             }
                         }
                         else {
                             if(tableName.contains("emi",Qt::CaseInsensitive)) {
                                  item = new QTableWidgetItem(QString::number(count + 1));
                                  //item->setFont(G::myFont);
                                  ui->tableWidget->setItem(rowNo,0,item);
                             }
                             else {
                                 QCheckBox* checkBox = new QCheckBox();
                                 checkBox->setText(QString::number(count + 1));
                                 checkBox->setChecked(true);
                                 //checkBox->setFont(G::myFont);
                                 ui->tableWidget->setCellWidget(rowNo,0,checkBox);

                                 checkBoxLists.append(checkBox);
                             }

                             for(columnNo = 1;columnNo < dispColumnCount;columnNo++)
                             {
                                 item = new QTableWidgetItem(tableData.at(rowNo).at(columnNo));
                                 item->setTextAlignment(Qt::TextWordWrap);
                                 //item->setFont(G::myFont);
                                 ui->tableWidget->setItem(rowNo,columnNo,item);
                             }
                             count++;
                         }
                         if((rowNo % 500 == 0) && (rowNo != 0)) timerObj->delayInms(10);
                     }

                     for(columnNo = dispColumnCount;columnNo < columnCount;columnNo++)
                     {
                         QStringList data;
                         for(rowNo = 0;rowNo < rowCount;rowNo++)
                         {
                             data.push_back(tableData.at(rowNo).at(columnNo));
                         }
                         hiddenData.push_back(data);
                     }

                     //QMap<column,columnwidth>customSize [ SL No,     Expected Value,          Measured Value,          Result,               Remark]
                     //QMap<int,int> customSize = {{0,60}, {dispColumnCount-4,180}, {dispColumnCount-3,150}, {dispColumnCount-2,70}, {dispColumnCount-1,100}};
                     //utilObj.autofitTable(ui->tableWidget,customSize);

                     utilObj.autofitTable(ui->tableWidget,columnWidth.split(","));

                     clearmem(messageBox);

                     if(!tableName.contains("emi",Qt::CaseInsensitive)) {
                         ui->actionSelect_All->setEnabled(true);
                         ui->actionUnselect_All->setEnabled(true);
                     }

                     _qDebugE("hiddenTableNames:",hiddenDataHeaders,"\n hiddenData:",hiddenData);
                 }
             }
         }//else lock when fetching table is success
     }//else block when table name is not empty

     return  ret;
 }

 /*************************************************************************
  Function Name - loadManualTestTableFromDb
  Parameter(s)  - QString,QString
  Return Type   - bool
  Action        - loads ManualTest table from database.
  *************************************************************************/
  bool Home::loadManualTestTableFromDb(QString tableName,QString columnWidth)
  {
      _qDebug("bool Home::loadManualTestTableFromDb(QString tableNamee,QString columnWidth).");
      /*Specific Table Format ---------------
        Signal_Name
         Signal1   K1  K2  K3 ...
                   ON  ON  ON ...
                   ONN OFF OFF ..
         Signal2   K1  K2  K3 ...
                   ON  ON  ON ...
                   ONN OFF OFF ..
      */;
      QVector<QStringList> tableData;
      QTableWidgetItem* item = nullptr;
      QStringList tableHeaders;

      int rowCount;
      int columnCount;
      int dispColumnCount;
      int sigCount = 0;

      bool isMain = true;

      manualData_Main.clear();
      manualDataHidden_Main.clear();
      manualData_StandBy.clear();
      manualDataHidden_StandBy.clear();

      //clean table
      ui->tableWidget->horizontalHeader()->hide();
      ui->tableWidget->setRowCount(0);
      timerObj->delayInms(10);//table stable time

      tableData.clear();
      checkBoxLists.clear();
      hiddenDataHeaders.clear();
      hiddenData.clear();

      ui->actionSelect_All->setDisabled(true);
      ui->actionUnselect_All->setDisabled(true);

      if(tableName.size() == 0)
      {
          PLAY QMessageBox::warning(nullptr,"Error!!\n","Table name is Blank.");
          return false;
      }

      //Get Table Header
      tableHeaders.clear();

      if(!homeDbObj->getColumnNames(tableName,tableHeaders))
      {
          _qDebugE("Fetching table headers: ",tableName," failed.","");
          PLAY QMessageBox::warning(nullptr,"Error!!\n","No Data Found");
          return false;
      }

      dispColumnCount = (tableHeaders.indexOf("Remarks") + 1);
      if(dispColumnCount < 2) {
          _qDebugE("Fetching table headers: ",tableName," failed.","");
          PLAY QMessageBox::warning(nullptr,"Error!!\n",
                                    QString("Minimum expected display columns is 2 but total display columns found %1")
                                    .arg(dispColumnCount));
          return false;
      }

      _qDebugE("tableHeaders:",tableHeaders," , dispColumnCount:",dispColumnCount);


      if(!homeDbObj->getTableData(tableName,tableData))
      {
          _qDebugE("Fetching table: ",tableName," failed.","");
          PLAY QMessageBox::warning(nullptr,"Error!!\n","No Data Found");
          return false;
      }

      //_qDebugE("tableData:",tableData,"","");

      rowCount = tableData.count();
      columnCount = tableHeaders.count();

      _qDebugE("Rows:",rowCount," Columns:",columnCount);

      if(rowCount == 0 || columnCount == 0)
      {
          PLAY QMessageBox::warning(nullptr,"Error!!\n","No Data Found");
          return false;
      }

      ui->tableWidget->horizontalHeader()->show();

      //load tableData to table=====================
      //load table header
      ui->tableWidget->setColumnCount(dispColumnCount);

      _qDebugE("Setting Column Names ...","","","");
      for(int i = 0; i < dispColumnCount;i++)
      {
          item = new QTableWidgetItem(tableHeaders.at(i));
          item->setFont(_font);
          item->setForeground(Qt::blue);
          item->setBackground(QColor("lightblue"));
          ui->tableWidget->setHorizontalHeaderItem(i, item);
      }
      G::enableTableHeaderUnderline(ui->tableWidget);

      for(int i = dispColumnCount;i < columnCount;i++) hiddenDataHeaders.push_back(tableHeaders.at(i));

      QStringList mainSignalNames, standbySignalNames;
      for(int rowNo = 0;rowNo < rowCount;)
      {
          QString signalName;
          QStringList rowContent;
          QVector<QStringList> signalData;
          QVector<QStringList> signalDataHidden;

          signalName = tableData.at(rowNo).at(1);
          _qDebugE("signalName:",signalName,"","");

          if(tableData.at(rowNo).at(0) == PAUSE_POINT_STR) { isMain = false; rowNo++; continue; }
          if(signalName.size() == 0) { rowNo++; continue; }

          do {
              if(tableData.at(rowNo).at(0) == PAUSE_POINT_STR) break;

              rowContent.clear();
              for(int columnNo = 0;columnNo < dispColumnCount;columnNo++) rowContent.push_back(tableData.at(rowNo).at(columnNo));
              signalData.push_back(rowContent);

              rowContent.clear();
              for(int columnNo = dispColumnCount;columnNo < columnCount;columnNo++) rowContent.push_back(tableData.at(rowNo).at(columnNo));
              signalDataHidden.push_back(rowContent);
          }while((++rowNo < rowCount) && (tableData.at(rowNo).at(1).size() == 0));

          _qDebugE("signalData:",signalData,"\n\nsignalDataHidden:",signalDataHidden);
          _qDebugE(++sigCount,":================================================================================","isMain:",isMain);

          if(isMain) {
              manualData_Main.insert(signalName,signalData);
              manualDataHidden_Main.insert(signalName,signalDataHidden);
              mainSignalNames.push_back(signalName);
          }
          else {
              manualData_StandBy.insert(signalName,signalData);
              manualDataHidden_StandBy.insert(signalName,signalDataHidden);
              standbySignalNames.push_back(signalName);
          }
      }

      if(ui->label_testName->text().contains("standby",Qt::CaseInsensitive) ||
              ui->label_testName->text().contains("umb",Qt::CaseInsensitive)) {
          if(manualData_StandBy.count() <= 0) {
              PLAY QMessageBox::warning(nullptr,"Error!!\n","No Standby Data Found");
              return false;
          }
          ui->comboBox_manualPolarity->clear();
          ui->comboBox_manualPolarity->addItems(standbySignalNames);

          //Add first signal row content
          updateManualTable(ui->tableWidget,manualData_StandBy.value(standbySignalNames.at(0)),columnWidth);
          G::showInfoMessage(G::testInterMediateMessage);
      }
      else {
          if(manualData_Main.count() <= 0) {
              PLAY QMessageBox::warning(nullptr,"Error!!\n","No Main Data Found");
              return false;
          }
          ui->comboBox_manualPolarity->clear();
          ui->comboBox_manualPolarity->addItems(mainSignalNames);

          //Add first signal row content
          updateManualTable(ui->tableWidget,manualData_Main.value(mainSignalNames.at(0)),columnWidth);
          G::showInfoMessage(G::testStartMessage);
      }

      ui->comboBox_manualPolarity->setVisible(true);

      _qDebugE("manualData_Main:",manualData_Main.count(),"\n\nmanualDataHidden_Main:",manualDataHidden_Main.count());
      _qDebugE("manualData_StandBy:",manualData_StandBy.count(),"\n\nmanualDataHidden_StandBy:",manualDataHidden_StandBy.count());

      return  true;
  }

  /*************************************************************************
   Function Name - updateManualTable
   Parameter(s)  - QTableWidget*, QVector<QStringList>, QString
   Return Type   - void
   Action        - updates given table with data and the first checkbox of
                   the table will be disabled.
   *************************************************************************/
  void Home::updateManualTable(QTableWidget* table,QVector<QStringList> data,QString columnWidth)
  {
      QTableWidgetItem* item = nullptr;
      int columnCount = 0;
      checkBoxLists.clear();
      table->setRowCount(0);

      if(data.count() > 0) columnCount = data.at(0).count();

      for(int rowNo = 0,count = 0;rowNo < data.count();rowNo++) {
          table->insertRow(rowNo);

          QCheckBox* checkBox = new QCheckBox();
          checkBox->setText(QString::number(++count));
          checkBox->setChecked(true);
          if(rowNo == 0) checkBox->setDisabled(true);
          table->setCellWidget(rowNo,0,checkBox);
          checkBoxLists.append(checkBox);

          for(int columnNo = 1;columnNo < columnCount;columnNo++)
          {
              item = new QTableWidgetItem(data.at(rowNo).at(columnNo));
              item->setTextAlignment(Qt::TextWordWrap);
              table->setItem(rowNo,columnNo,item);
          }
      }
      utilObj.autofitTable(ui->tableWidget,columnWidth.split(","));
  }

/*************************************************************************
 Function Name - generateReport
 Parameter(s)  - QVector<QVector<QStringList>>,QStringList, QString
 Return Type   - void
 Action        - calls Report_Generator API to generate report.
 *************************************************************************/
 void Home::generateReport(QVector<QVector<QStringList>> reportData,QStringList testStatus,QString path)
 {
    _qDebug("void Home::generateReport(QVector<QVector<QStringList>> reportData,QStringList testStatus,QString path).");
    QString str;
    QStringList data;
    //QStringList results = {"PASS", "PASS", "PASS", "PASS"};
    QString result = extractValueFromLabel(ui->label_TestStatus->text());

    QMessageBox* messageBox = new QMessageBox;
    messageBox->setText("Please wait!! Report is being generated...");
    messageBox->setWindowFlags(Qt::CustomizeWindowHint);
    messageBox->setStandardButtons(nullptr);
    messageBox->show();
    timerObj->delayInms(1);

    Report_Generator report(ui->label_testName->text().trimmed(),path);

    report.setSoftwareVersion(G::appVersion);
    report.setSoftwareBuildDate(G::builtDate);
    report.setSoftwareCheckSum(G::softwareChecksum);
    report.setStartDate(G::startDate);
    report.setEndTime(G::endDate);
    report.setStartTime(G::startTime);
    report.setEndTime(G::endTime);
    report.setTestDuration(ui->label_timer->text().trimmed());
    report.setUser(extractValueFromLabel(ui->label_userName->text()));
    report.setUUT(extractValueFromLabel(ui->label_unitName->text()));
    report.setUnitNumber(extractValueFromLabel(ui->lineEdit_uutNumber->text()));
    report.setTestName(extractValueFromLabel(ui->label_testName->text()));
    report.setTestType(extractValueFromLabel(ui->lineEdit_typeOfTest->text()));
    report.setInsulationTestVoltage(extractValueFromLabel(ui->label_TestVoltage->text()));
    report.setUserRep(extractValueFromLabel(ui->lineEdit_UserPerson->text()));
    report.setQARep(extractValueFromLabel(ui->lineEdit_QAperson->text()));
    report.setTestResult(testStatus);
    report.setTestDuration(ui->label_timer->text().trimmed());

    report.printParameters();
    report.setTables(reportData);
    report.print_pdf(isReportLandscapeMode?1:0);
    timerObj->delayInms(500);

    clearmem(messageBox);
 }

/*************************************************************************
 Function Name - extractValueFromLabel
 Parameter(s)  - QString
 Return Type   - QString
 Action        - Extracts the required value from label for Report generation.
 *************************************************************************/
 QString Home::extractValueFromLabel(QString str)
 {
     QStringList data;

     str = str.trimmed();
     data = str.split(":");
     if(data.count() > 1) str = data.at(1).trimmed();

     return str;
 }

 /*************************************************************************
  Function Name - closeRelayTest
  Parameter(s)  - void
  Return Type   - void
  Action        - closes RelayTest windows and initializes the required properties.
  *************************************************************************/
 void Home::closeRelayTest()
 {
     _qDebug("void Home::closeRelayTest().");
     G::dmmObj->resetDMM();
     relayTest->resetPS();
     relayTest->hide();
     resetUIComponents();
     show();
     clearmem(G::msgBox);
     G::handlerObj->resume();
     timerMeterCheckObj->start(MULTIMETER_PORT_CHECK_DELAY);
     clearmem(relayTest);
 }

 /*************************************************************************
  Function Name - closeManualIsolationTest
  Parameter(s)  - void
  Return Type   - void
  Action        - closes ManualIsolationTest windows and initializes the required properties.
  *************************************************************************/
 void Home::closeManualIsolationTest()
 {
     _qDebug("void Home::closeManualIsolationTest().");
     G::dmmObj->resetDMM();
     DMM::isreadBusy = false;
     manualIsolationObj->hide();
     resetUIComponents();
     show();
     timerMeterCheckObj->start(MULTIMETER_PORT_CHECK_DELAY);
     clearmem(G::msgBox);
     clearmem(manualIsolationObj);
 }

/*************************************************************************
 Function Name - closeSelfTest
 Parameter(s)  - void
 Return Type   - void
 Action        - closes SelfTest windows and initializes the required points.
 *************************************************************************/
 void Home::closeSelfTest()
 {
     _qDebug("void Home::closeSelfTest()");
     G::dmmObj->resetDMM();
     jigSelfTest->resetPS();
     jigSelfTest->hide();
     resetUIComponents();
     show();
     clearmem(G::msgBox);
     G::handlerObj->resume();
     timerMeterCheckObj->start(MULTIMETER_PORT_CHECK_DELAY);
     clearmem(jigSelfTest);
 }

/*************************************************************************
 Function Name - handleSelection
 Parameter(s)  - QAction*, int
 Return Type   - void
 Action        - handles user selections for GUI.
  *************************************************************************/
 void Home::handleSelection(QAction* action,int rowNumber) {
     int colCount = homeDbObj->getColumnCount(SELECTION_TABLE);
     int rowCount = homeDbObj->getRowCount(SELECTION_TABLE);
     _qDebugE("column Count:",colCount,", Row Count:",rowCount);

     if(colCount != 2) {
         PLAY QMessageBox::warning(nullptr,"Error!!",QString("Expected columns in table [%1] is 2 but found %2.").
                              arg(SELECTION_TABLE).arg(colCount));
         return;
     }
     if(rowCount != SELECTION_ROWS) {
         PLAY QMessageBox::warning(nullptr,"Error!!",QString("Expected rows in table [%1] is %2 but found %3.").
                              arg(SELECTION_TABLE).arg(SELECTION_ROWS).arg(rowCount));
         return;
     }

     QStringList columnNames;
     columnNames.clear();
     if(!homeDbObj->getColumnNames(SELECTION_TABLE,columnNames)){
         PLAY QMessageBox::warning(nullptr,"Error!!",QString("Unable to fetch column names from table [%1].").
                              arg(SELECTION_TABLE));
         return;
     }
     if(columnNames.count() != colCount) {
         PLAY QMessageBox::warning(nullptr,"Error!!",QString("Column Headers' length mismatched in table [%1].\nExpected columns is %2 but found %3.").
                              arg(SELECTION_TABLE).arg(colCount).arg(columnNames.count()));
         return;
     }

     if(action->isIconVisibleInMenu()) {
         if(homeDbObj->editCell(SELECTION_TABLE,rowNumber,columnNames.at(1),"0")) {

             _qDebugE(QString("Writing 0 at Row %1 in table [%2] successfull.").arg(rowNumber).arg(SELECTION_TABLE)
                      ,"","","");

             switch(rowNumber) {
                case 1 :    isAutoSaveReport      = false;
                            break;
                case 2 :    isAutoSaveSCReport    = false;
                            break;
                case 3 :    isReportLandscapeMode = false;
                            break;
                case 4 :    disconnectKeys();
                            break;
                default:
                            PLAY QMessageBox::warning(nullptr,"Error!!",
                                      QString("Invalid Row entry %1 for table [%2].").
                                      arg(rowNumber).arg(SELECTION_TABLE));
                            return;
             }
             action->setIconVisibleInMenu(false);
         }
         else {
             _qDebugE(QString("Writing 0 at Row %1 in table [%2] failed.").arg(rowNumber).arg(SELECTION_TABLE)
                      ,"","","");
             PLAY QMessageBox::warning(nullptr,"Error!!",QString("Writing 0 at Row %1 in table [%2] failed.").
                                  arg(rowNumber).arg(SELECTION_TABLE));
         }
     }
     else {
         if(homeDbObj->editCell(SELECTION_TABLE,rowNumber,columnNames.at(1),"1")) {
             _qDebugE(QString("Writing 1 at Row %1 in table [%2] successfull.").arg(rowNumber).arg(SELECTION_TABLE)
                      ,"","","");

             switch(rowNumber) {
                case 1 :    isAutoSaveReport      = true;
                            break;
                case 2 :    isAutoSaveSCReport    = true;
                            break;
                case 3 :    isReportLandscapeMode = true;
                            break;
                case 4 :    connectKeys();
                            break;
                default:
                            PLAY QMessageBox::warning(nullptr,"Error!!",
                                      QString("Invalid Row entry %1 for table [%2].").
                                      arg(rowNumber).arg(SELECTION_TABLE));
                            return;
             }
             action->setIconVisibleInMenu(true);
         }
         else {
             _qDebugE(QString("Writing 1 at Row %1 in table [%2] failed.").arg(rowNumber).arg(SELECTION_TABLE)
                      ,"","","");
             PLAY QMessageBox::warning(nullptr,"Error!!",QString("Writing 1 at Row %1 in table [%2] failed.").
                                  arg(rowNumber).arg(SELECTION_TABLE));
         }
     }
 }

 /*************************************************************************
  Function Name - setHeader
  Parameter(s)  - void
  Return Type   - void
  Action        - sets GUI header with version,built date and checksum.
  *************************************************************************/
 void Home::setHeader()
 {
     QString header = ui->label_header->text();
     QString headerInfo = header.left(header.indexOf("Version"));
     QString guiInfo = header.right(header.size() - header.indexOf("Version"));
     guiInfo = QString (guiInfo).arg(G::appVersion).arg(G::builtDate).arg(G::softwareChecksum);

     ui->label_header->setText(QString ("<span style='font-size:20pt; font-weight:600; color:#ededf1;'>%1</span><span style='font-size:14pt; font-weight:bold; color:#cad1f9 ;'>%2</span>")
     .arg(headerInfo).arg(guiInfo).replace("\n","<br />"));
 }

/*************************************************************************
 Function Name - test
 Parameter(s)  - void
 Return Type   - void
 Action        - Enter test code here to test and call it from main().
 *************************************************************************/
 void Home::test()
 {
    _qDebug("void Home::test()");
    int noOfRows;
    int noOfColumns;
    int blankCellCount = 0;

    QVector<QStringList> dataBuff;

    QStringList rowData;
    QString filePath = QDir::homePath() + "/Desktop/MY_FILE.xlsx";
    QString newFilePath = QDir::homePath() + "/Desktop/NewF/MY_FILE1.xlsx";
    QString tableName;
    QString data;

    filePath = QFileDialog :: getOpenFileName(this, tr("Open file"), QDir::homePath() + "/Desktop",
                                              "Microsoft Excel(*.xlsx)");
    _qDebugE("The Excel file path : ",filePath,"","");

    if(filePath == "") return;

    QXlsx::Document xlsx(filePath);
    QXlsx::CellRange cellRange = xlsx.dimension();

    noOfRows = cellRange.rowCount();
    noOfColumns = cellRange.columnCount();
    _qDebugE("Total Rows: ",noOfRows," , Total Columns: ",noOfColumns);

    dataBuff.clear();
    //verifying input File Data
    for(int rowNo = 1;rowNo <= noOfRows;rowNo++)
    {
         blankCellCount = 0;
         rowData.clear();
         for(int columnNo = 1;columnNo <= noOfColumns;columnNo++)
         {
             data =  xlsx.read(rowNo,columnNo).toString();
             rowData.push_back(data.trimmed());
             if(data == "") blankCellCount++;
         }

         if(blankCellCount == noOfColumns) {
             noOfRows = (rowNo - 1);
             break;
         }
         else {
             dataBuff.push_back(rowData);
         }
    }

    //_qDebugE("dataBuff:","{",dataBuff,"}");
    _qDebugE("dataBuff count:",dataBuff.count(),"","");

    //Manipulate data for expected format
    QString OPEN  = "> 20 MΩ";
    QString SHORT = "< 1 Ω";
    QStringList openBuff,shortBuff;
    QString con,pin,signal,pauseChar;
    QString curCon,curPin,curSignal;
    int slNo = 1;
    int connCol = 1;
    int pinCol  = 2;
    int sigCol  = 3;
    QVector<QStringList>exelData;
    exelData.clear();

    for(int i = 1;i < dataBuff.count()-1;i++) {
        pauseChar = dataBuff.at(i).at(0);
        con    = dataBuff.at(i).at(connCol);
        pin    = dataBuff.at(i).at(pinCol);
        signal = dataBuff.at(i).at(sigCol);

        openBuff.clear();
        shortBuff.clear();

        if(pauseChar == "@") {
            openBuff.append(pauseChar);
            for(int i = 1;i<=9;i++) openBuff.append("");
            exelData.push_back(openBuff);
            continue;
        }

        for(int j = i+1;j < dataBuff.count();j++) {
            pauseChar = dataBuff.at(j).at(0);
            curCon    = dataBuff.at(j).at(connCol);
            curPin    = dataBuff.at(j).at(pinCol);
            curSignal = dataBuff.at(j).at(sigCol);

            if(pauseChar == "@") continue;

            if(curCon == con) {
                if(curSignal == signal) {
                    if(shortBuff.count() == 0) {
                        shortBuff.push_back(signal);
                        shortBuff.push_back(con+"/"+pin);
                        shortBuff.push_back(con+"("+curPin);
                        shortBuff.push_back(SHORT);
                        shortBuff.push_back("");//Meas_Value
                        shortBuff.push_back("");//Result
                        shortBuff.push_back("");//Remark
                        shortBuff.push_back(con+"/"+pin);
                        shortBuff.push_back(con+"/("+curPin);
                        shortBuff.push_back(SHORT);
                    }
                    else {
                        shortBuff[2] = shortBuff[2] + "," + curPin;
                        shortBuff[8] = shortBuff[8] + "," + curPin;
                    }
                }
                else {
                    if(openBuff.count() == 0) {
                        openBuff.push_back(signal);
                        openBuff.push_back(con+"/"+pin);
                        openBuff.push_back(con+"("+curPin);
                        openBuff.push_back(OPEN);
                        openBuff.push_back("");//Meas_Value
                        openBuff.push_back("");//Result
                        openBuff.push_back("");//Remark
                        openBuff.push_back(con+"/"+pin);
                        openBuff.push_back(con+"/("+curPin);
                        openBuff.push_back(OPEN);
                    }
                    else {
                        openBuff[2] = openBuff[2] + "," + curPin;
                        openBuff[8] = openBuff[8] + "," + curPin;
                    }
                }
            }
        }

        if(openBuff.count() > 0) {
            openBuff[2] = openBuff[2] + ")";
            openBuff[8] = openBuff[8] + ")";
            exelData.push_back(openBuff);
        }
        if(shortBuff.count() > 0) {
            shortBuff[2] = shortBuff[2] + ")";
            shortBuff[8] = shortBuff[8] + ")";
            exelData.push_back(shortBuff);
        }
    }
    _qDebugE("exelData:","{",exelData,"}");

    QXlsx::Document xlsx1(newFilePath);
    QXlsx::CellRange cellRange1 = xlsx1.dimension();

    //clear the sheet if exist
    for(int i = 1;i <= cellRange1.rowCount();i++) {
        for(int j = 1;j <= cellRange1.columnCount();j++) {
            xlsx1.write(i,j,QVariant());
        }
    }

    //Fill first Row(Table Header) --------------
    xlsx1.write(1,1,"S_No");
    xlsx1.write(1,2,"Signal_Name");
    xlsx1.write(1,3,"Input Pins");
    xlsx1.write(1,4,"Output Pins");
    xlsx1.write(1,5,"Expected Resistance");
    xlsx1.write(1,6,"Measured_Value");
    xlsx1.write(1,7,"Result");
    xlsx1.write(1,8,"Remarks");
    xlsx1.write(1,9,"Outputs_High");
    xlsx1.write(1,10,"Outputs_Low");
    xlsx1.write(1,11,"Expected_Value");

    int row = 2;
    for(QStringList data : exelData) {
        _qDebugE("data:",data,"","");
        int col = 2;
        if(data.at(0) == "@") {
            xlsx1.write(row,1,"@");

            for(int i = 0;i < data.count();i++,col++) {
                xlsx1.write(row,col,"");
            }
        }
        else {
            xlsx1.write(row,1,QString::number(slNo++));
            for(int i = 0;i < data.count();i++,col++) {
                xlsx1.write(row,col,data.at(i));
            }
        }
        row++;
    }
    xlsx1.save();
 }

/*************************************************************************
 Function Name - encodeData
 Parameter(s)  - Database*, QStringList
 Return Type   - bool
 Action        - Encodes the given tableNames in the given Database. Returns
                 true if encoding in db is successfull, else returns false.
 *************************************************************************/
 bool Home::encodeData(Database* db,QStringList tableNames) {
     _qDebug("bool Home::encodeData(Database* db,QStringList tableNames)");

    for(QString table : tableNames) {
        if(!encodeData(db,table)) return false;
    }

    return true;
 }

/*************************************************************************
 Function Name - encodeData
 Parameter(s)  - Database*, QString
 Return Type   - bool
 Action        - Encodes the given tableName in the given Database. Returns
                 true if encoding in db is successfull, else returns false.
 *************************************************************************/
 bool Home::encodeData(Database* db,QString tableName) {
     _qDebug("bool Home::encodeData(Database* db,QString tableName)");
     _qDebugE("Given tableName:",tableName,"","");

     if(db == nullptr) { PLAY QMessageBox::warning(nullptr,"Error!!\n","Invalid Database Object."); return false; }
     if(tableName.count() == 0) { PLAY QMessageBox::warning(nullptr,"Error!!\n","Invalid Table Name.");return false; }

     QVector<QStringList> tableData;
     if(!db->getTableData(tableName,tableData)) {
         PLAY QMessageBox::warning(nullptr,"Error!!\n",QString("Getting data from table %1 failed").arg(tableName));
         return false;
     }

     if(!db->cleanTable(tableName)) {
         PLAY QMessageBox::warning(nullptr,"Error!!\n",QString("Clearing table %1 failed").arg(tableName));
         return false;
     }

     if(db->addMultipleRowsData(tableName,tableData) == false){
         PLAY QMessageBox::warning(this,"Error!!\n","Unable to update Database!!");
         return false;
     }

     return true;
 }

 /*************************************************************************
  Function Name - decodeData
  Parameter(s)  - Database*, QStringList
  Return Type   - bool
  Action        - Decodes the given tableNames in the given Database. Returns
                  true if encoding in db is successfull, else returns false.
  *************************************************************************/
  bool Home::decodeData(Database* db,QStringList tableNames) {
      _qDebug("bool Home::decodeData(Database* db,QStringList tableNames)");

     for(QString table : tableNames) {
         if(!decodeData(db,table)) return false;
     }

     return true;
  }

 /*************************************************************************
  Function Name - decodeData
  Parameter(s)  - Database*, QString
  Return Type   - bool
  Action        - Decodes the given tableName in the given Database. Returns
                  true if encoding in db is successfull, else returns false.
  *************************************************************************/
  bool Home::decodeData(Database* db,QString tableName) {
      _qDebug("bool Home::decodeData(Database* db,QString tableName)");
      _qDebugE("Given tableName:",tableName,"","");

      if(db == nullptr) { PLAY QMessageBox::warning(nullptr,"Error!!\n","Invalid Database Object."); return false; }
      if(tableName.count() == 0) { PLAY QMessageBox::warning(nullptr,"Error!!\n","Invalid Table Name.");return false; }

      QVector<QStringList> tableData;
      if(!db->getTableData(tableName,tableData)) {
          PLAY QMessageBox::warning(nullptr,"Error!!\n",QString("Getting data from table %1 failed").arg(tableName));
          return false;
      }

      if(!db->cleanTable(tableName)) {
          PLAY QMessageBox::warning(nullptr,"Error!!\n",QString("Clearing table %1 failed").arg(tableName));
          return false;
      }

      if(db->addMultipleRowsData(tableName,tableData) == false){
          PLAY QMessageBox::warning(this,"Error!!\n","Unable to update Database!!");
          return false;
      }

      return true;
  }

 /*=====================================================================================================
 **************************************** ACTION EVENTS ************************************************
 ======================================================================================================*/
/*************************************************************************
 Function Name - on_pushButton_logout_clicked
 Parameter(s)  - void
 Return Type   - void
 Action        - This function handles the pushButton clicked signal.
 *************************************************************************/
 void Home::on_pushButton_logout_clicked()
 {
     _qDebug("void Home::on_pushButton_logout_clicked().");
     QMessageBox::StandardButton resBtn = QMessageBox::question( this, MESSAGE_TITLE,tr("Do you Want to LogOut?\n"),QMessageBox::No | QMessageBox::Yes);
     if (resBtn == QMessageBox::Yes)
     {
         QMessageBox* msgBox = new QMessageBox;
         msgBox->setWindowTitle(MESSAGE_TITLE);
         msgBox->setText("Please wait!! Logging Out...  ");
         msgBox->setWindowFlags(Qt::CustomizeWindowHint);
         msgBox->setStandardButtons(nullptr);
         msgBox->show();
         timerObj->delayInms(1);

         isFreshStart = true;
         timerMeterCheckObj->stop();

         G::handlerObj->stop();
         while(G::handlerObj->isRunning() == true) timerObj->delayInms(10);
         G::handlerObj->quit();

         clearmem(msgBox);

         this->hide();
         loginObj->show();
     }
 }

/*************************************************************************
 Function Name - on_actionImport_triggered
 Parameter(s)  - void
 Return Type   - void
 Action        - This function handles the import menu triggered signal.
 *************************************************************************/
 void Home::on_actionImport_triggered()
 {
     _qDebug("void Home::on_actionImport_triggered().");

     QVector<QStringList> dataBuff;

     QStringList rowData;
     QStringList columnNames;

     QByteArray line;

     QString HomePath;
     QString filePath;
     QString tableName;
     QString data;

     int rowNo;
     int columnNo;
     int noOfRows;
     int noOfColumns;
     int blankCellCount = 0;

     QMessageBox* Msg_Buffer_DB = nullptr;

     if(lastFilePath != "")
     {
         HomePath = lastFilePath;
     }
     else
     {
         HomePath = QDir::homePath() + "/Desktop";
     }

     filePath = QFileDialog :: getOpenFileName(this, tr("Open file"), HomePath, "Microsoft Excel(*.xlsx)");
     _qDebugE("The Excel file path : ",filePath,"","");

     if(filePath != "")
     {
         lastFilePath = filePath;

         //Create and show a message box to show data processing
         Msg_Buffer_DB = new QMessageBox;

         Msg_Buffer_DB->setWindowTitle("TMR_TRB");
         Msg_Buffer_DB->setText("Please wait!! Data is being loaded...");
         Msg_Buffer_DB->setWindowFlags(Qt::CustomizeWindowHint);
         Msg_Buffer_DB->setStandardButtons(nullptr);
         Msg_Buffer_DB->show();
         timerObj->delayInms(1);

         QXlsx::Document xlsx(filePath);
         QXlsx::CellRange cellRange = xlsx.dimension();

         noOfRows = cellRange.rowCount();
         noOfColumns = cellRange.columnCount();
         _qDebugE("Total Rows: ",noOfRows," , Total Columns: ",noOfColumns);

         //Getting Table Header
         columnNames.clear();
         for(columnNo = 1;columnNo <= noOfColumns;columnNo++)
         {
             data =  xlsx.read(1,columnNo).toString();
             if(data == "") break;
             columnNames.push_back(data.trimmed());
         }
         noOfColumns = columnNames.count();

         dataBuff.clear();
         //verifying input File Data
         for(rowNo = 2;rowNo <= noOfRows;rowNo++)
         {
              blankCellCount = 0;
              rowData.clear();
              for(columnNo = 1;columnNo <= noOfColumns;columnNo++)
              {
                  data =  xlsx.read(rowNo,columnNo).toString();
                  rowData.push_back(data.trimmed());
                  if(data == "") blankCellCount++;
              }

              if(blankCellCount == noOfColumns) {
                  noOfRows = (rowNo - 1);
                  break;
              }
              else {
                  dataBuff.push_back(rowData);
              }
         }

         _qDebugE("tableName:",tableName," , columnNames:",columnNames);
         _qDebugE("dataBuff:","{",dataBuff,"}");
         if(dataBuff.count() > 0)
         {
             //Now store data to the DB table
             tableName = get_file_name(filePath);
             _qDebugE("tableName:",tableName," , columnNames:",columnNames);

             homeDbObj->deleteTable(tableName);

             if(!homeDbObj->createTable(tableName,columnNames))
             {
                 _qDebugE("Oops! Table_db creation Failed.","","","");

                 PLAY QMessageBox::warning(nullptr,"Error!!\n","Oops Database couldn't be created");
             }
             else
             {
                 //load data to db
                 if(homeDbObj->addMultipleRowsData(tableName,dataBuff) == false)
                 {
                     PLAY QMessageBox::warning(this,"Error!!\n","Unable to update Database!!");
                 }
                 else
                 {
                     INFO QMessageBox::information(this,"TMR_TRB\n","Data Imported Successfully");
                     isEdit = true;
                     update_home_ui();
                 }
             }
         }

         clearmem(Msg_Buffer_DB);
     }
 }

/*************************************************************************
 Function Name - on_actionExport_triggered
 arameter(s)  - void
 Return Type   - void
 Action        - This function handles the export menu triggered signal.
 *************************************************************************/
 void Home::on_actionExport_triggered()
 {
     _qDebug("void Home::on_actionExport_triggered().");

     QHBoxLayout* hBoxLayout1 = new QHBoxLayout;
     QHBoxLayout* hBoxLayout2 = new QHBoxLayout;
     QVBoxLayout* vBoxLayout = new QVBoxLayout;
     QStringList dbTableNames;

     dbTableNames.clear();
     dbTableNames = homeDbObj->getTableNames();

     if(dbTableNames.size() == 0)
     {
         PLAY QMessageBox::warning(nullptr,"Error!!\n","No Table Found");
     }
     else
     {
         myWidgetObj = new QWidget;
         label_cycleObj = new QLabel("Select Table: ");
         comboBox_tablesObj = new QComboBox;
         pushButton_exportObj = new QPushButton("Export");
         pushButton_cancelObj = new QPushButton("Cancel");

         myWidgetObj->setWindowTitle("Export Table");
         myWidgetObj->setMaximumSize(250,100);//width,height
         myWidgetObj->setMinimumSize(250,100);//width,height

         comboBox_tablesObj->addItems(dbTableNames);

         comboBox_tablesObj->setCurrentIndex(0);

         connect(pushButton_exportObj,SIGNAL(clicked()), this, SLOT(on_exportPushButton_clicked()));
         connect(pushButton_cancelObj,SIGNAL(clicked()), this, SLOT(on_cancelPushButton_clicked()));

         hBoxLayout1->addWidget(label_cycleObj);
         hBoxLayout1->addWidget(comboBox_tablesObj);

         hBoxLayout2->addWidget(pushButton_exportObj);
         hBoxLayout2->addWidget(pushButton_cancelObj);

         vBoxLayout->addLayout(hBoxLayout1);
         vBoxLayout->addLayout(hBoxLayout2);

         myWidgetObj->setLayout(vBoxLayout);
         myWidgetObj->setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowMinMaxButtonsHint);//restore
         myWidgetObj->setWindowModality(Qt::ApplicationModal);//Block other windows from receiving input events
         myWidgetObj->show();
     }
 }

/*************************************************************************
 Function Name - on_actionDelete_triggered
 Parameter(s)  - void
 Return Type   - void
 Action        - This function handles the delete menu triggered signal.
 *************************************************************************/
 void Home::on_actionDelete_triggered()
 {
     _qDebug("void Home::on_actionDelete_triggered().");

     QHBoxLayout* hBoxLayout1 = new QHBoxLayout;
     QHBoxLayout* hBoxLayout2 = new QHBoxLayout;
     QVBoxLayout* vBoxLayout = new QVBoxLayout;
     QStringList dbTableNames;

     dbTableNames.clear();
     dbTableNames = homeDbObj->getTableNames();

     if(dbTableNames.size() == 0)
     {
         PLAY QMessageBox::warning(nullptr,"Error!!\n","No Table Found");
     }
     else
     {
         myWidgetObj = new QWidget;
         label_cycleObj = new QLabel("Select Table: ");
         comboBox_tablesObj = new QComboBox;
         pushButton_deleteObj = new QPushButton("Delete");
         pushButton_cancelObj = new QPushButton("Cancel");

         myWidgetObj->setWindowTitle("Delete Table");
         myWidgetObj->setMaximumSize(250,100);//width,height
         myWidgetObj->setMinimumSize(250,100);//width,height

         comboBox_tablesObj->addItems(dbTableNames);

         comboBox_tablesObj->setCurrentIndex(0);

         connect(pushButton_deleteObj,SIGNAL(clicked()), this, SLOT(on_deletePushButton_clicked()));
         connect(pushButton_cancelObj,SIGNAL(clicked()), this, SLOT(on_cancelPushButton_clicked()));

         hBoxLayout1->addWidget(label_cycleObj);
         hBoxLayout1->addWidget(comboBox_tablesObj);

         hBoxLayout2->addWidget(pushButton_deleteObj);
         hBoxLayout2->addWidget(pushButton_cancelObj);

         vBoxLayout->addLayout(hBoxLayout1);
         vBoxLayout->addLayout(hBoxLayout2);

         myWidgetObj->setLayout(vBoxLayout);
         myWidgetObj->setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowMinMaxButtonsHint);//restore
         myWidgetObj->setWindowModality(Qt::ApplicationModal);//Block other windows from receiving input events
         myWidgetObj->show();
     }
 }

/*************************************************************************
 Function Name - on_actionExit_triggered
 Parameter(s)  - void
 Return Type   - void
 Action        - This function handles the Exit menu triggered signal.
 *************************************************************************/
 void Home::on_actionExit_triggered()
 {
     _qDebug("void Home::on_actionExit_triggered().");
     close();
 }

/*************************************************************************
 Function Name - on_pushButton_run_stop_clicked
 Parameter(s)  - void
 Return Type   - void
 Action        - This function handles the run_stop pushbutton clicked signal.
 *************************************************************************/
 void Home::on_pushButton_run_stop_clicked()
 {
     _qDebug("void Home::on_pushButton_run_stop_clicked().");
     if(!isAtleastOneRowSelected(ui->tableWidget) && !ui->label_testName->text().contains("emi",Qt::CaseInsensitive)) {
         PLAY QMessageBox::warning(nullptr,"Oops!!","Please select atleast one row.");
         return;
     }

     ui->pushButton_run_stop->setDisabled(true);

     if(ui->pushButton_run_stop->text() == "RUN") {
         ui->pushButton_run_stop->setText("STOP");
         ui->pushButton_run_stop->setToolTip("Click to Stop Test");
         G::stopTest = false;
         testData.clear();//testData holds all test table data

#if TEST_CYCLE_PRESENT
         ui->label_cycle->setHidden(false);
         if(ui->label_testName->text().contains("load",Qt::CaseInsensitive) || ui->label_testName->text().contains("polarity",Qt::CaseInsensitive) || ui->label_testName->text().contains("shifter",Qt::CaseInsensitive)) {
             QHBoxLayout* hBoxLayout1 = new QHBoxLayout;
             QHBoxLayout* hBoxLayout2 = new QHBoxLayout;
             QVBoxLayout* vBoxLayout = new QVBoxLayout;
             QFont font;
             font.setPointSize(15);
             font.setBold(true);
             font.setFamily("Times New Roman");

             myWidgetObj = new QWidget;
             label_cycleObj = new QLabel("Enter Number Of Cycles to Run: ");
             spinBox_cycleObj = new QSpinBox;
             blankLabel = new QLabel("");
             pushButton_okObj = new QPushButton("Ok");
             pushButton_cancelObj = new QPushButton("Cancel");

             myWidgetObj->setWindowTitle("TMR_TRB cycle count");
             myWidgetObj->setMaximumSize(360,150);//width,height
             myWidgetObj->setMinimumSize(360,150);//width,height

             label_cycleObj->setMaximumSize(200,30);
             label_cycleObj->setMinimumSize(200,30);
             label_cycleObj->setFont(font);

             spinBox_cycleObj->setMinimum(1);
             spinBox_cycleObj->setMaximum(100);
             spinBox_cycleObj->setSingleStep(1);
             spinBox_cycleObj->setDisplayIntegerBase(10);
             spinBox_cycleObj->setValue(1);
             spinBox_cycleObj->setMaximumSize(120,30);
             spinBox_cycleObj->setMinimumSize(120,30);
             spinBox_cycleObj->setFont(font);

             blankLabel->setMaximumSize(100,30);
             blankLabel->setMinimumSize(100,30);
             pushButton_okObj->setFont(font);
             pushButton_cancelObj->setFont(font);

             connect(pushButton_okObj,SIGNAL(clicked()), this, SLOT(on_okPushButton_clicked()));
             connect(pushButton_cancelObj,SIGNAL(clicked()), this, SLOT(on_cancelPushButton_clicked()));

             hBoxLayout1->addWidget(label_cycleObj);
             hBoxLayout1->addWidget(spinBox_cycleObj);

             hBoxLayout2->addWidget(blankLabel);
             hBoxLayout2->addWidget(pushButton_okObj);
             hBoxLayout2->addWidget(pushButton_cancelObj);

             //vBoxLayout->addWidget(label_cycleObj);
             //vBoxLayout->addWidget(spinBox_cycleObj);
             vBoxLayout->addLayout(hBoxLayout1);
             vBoxLayout->addLayout(hBoxLayout2);

             myWidgetObj->setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowMinMaxButtonsHint);//restore
             myWidgetObj->setLayout(vBoxLayout);
             myWidgetObj->setWindowModality(Qt::ApplicationModal);//Block other windows from receiving input events
             myWidgetObj->show();
         }
         else {
             runTest(0);
         }
#else
         runTest(1);
#endif
     }
     else if(ui->pushButton_run_stop->text() == "STOP") {
         QMessageBox::StandardButton resBtn = QMessageBox::question( this, MESSAGE_TITLE,tr("Do you Want to Stop the Test?\n"),QMessageBox::No | QMessageBox::Yes);
         if (resBtn == QMessageBox::Yes)
         {
             stopMsgBox = new QMessageBox;
             stopMsgBox->setWindowTitle(MESSAGE_TITLE);
             stopMsgBox->setText("Please wait!! Test is being Stopped...  ");
             stopMsgBox->setWindowFlags(Qt::CustomizeWindowHint);
             stopMsgBox->setStandardButtons(nullptr);
             stopMsgBox->show();
             timerObj->delayInms(1);

             G::stopTest = true;
         }
         else
         {
             ui->pushButton_run_stop->setEnabled(true);
         }
     }
 }

/*************************************************************************
 Function Name - on_okPushButton_clicked
 Parameter(s)  - void
 Return Type   - void
 Action        - This function handles the ok pushbutton clicked signal.
 *************************************************************************/
 void Home::on_okPushButton_clicked()
 {
     _qDebug("void Home::on_okPushButton_clicked().");

     int cycle_count = spinBox_cycleObj->value();

     _qDebugE("Cycle count : ",cycle_count,"","");

     cleanMyWidget();

     runTest(cycle_count);
 }

/*************************************************************************
 Function Name - on_cancelPushButton_clicked
 Parameter(s)  - void
 Return Type   - void
 Action        - This function handles the cancel pushbutton clicked signal.
 *************************************************************************/
 void Home::on_cancelPushButton_clicked()
 {
     _qDebug("void Home::on_cancelPushButton_clicked().");

     ui->pushButton_run_stop->setText("RUN");
     ui->pushButton_run_stop->setEnabled(true);
     cleanMyWidget();
 }

/*************************************************************************
 Function Name - on_exportPushButton_clicked
 Parameter(s)  - void
 Return Type   - void
 Action        - This function handles the export pushbutton clicked signal.
 *************************************************************************/
 void Home::on_exportPushButton_clicked()
 {
     _qDebug("void Home::on_exportPushButton_clicked().");

     QString selectedTable;
     QStringList columnNames;
     QVector<QStringList> tableData;
     QString data;

     QDir myDir;
     QString dirPath;

     int index;

     tableData.clear();

     if(comboBox_tablesObj != nullptr)
     {
         comboBox_tablesObj->setDisabled(true);

         selectedTable = comboBox_tablesObj->currentText();

         if(!homeDbObj->getTableData(selectedTable,tableData))
         {
             PLAY QMessageBox::warning(nullptr,"Error!!\n",QString("Fetching data from %1 failed").arg(selectedTable));
         }
         else
         {
             //get table column names
             if(!homeDbObj->getColumnNames(selectedTable,columnNames))
             {
                 PLAY QMessageBox::warning(nullptr,"Error!!\n",QString("Fetching columnNames from %1 failed").arg(selectedTable));
             }
             else
             {
                 dirPath = QDir::homePath() + "/Desktop";

                 if(!myDir.exists(dirPath)) myDir.mkpath(dirPath);

                 dirPath = dirPath + "/" + selectedTable + ".xlsx";

                 dirPath = QFileDialog :: getSaveFileName(this, tr("Save As"), dirPath, "Files(*.xlsx)");

                 if(dirPath == "")
                 {
                     PLAY QMessageBox::warning(nullptr,"Error!!\n","Saving path is NULL");
                 }
                 else
                 {
                     //Create and show a message box to show data processing
                     timerObj->delayInms(1);

                     QStringList names = dirPath.split("/");

                     for(index = 0;index < names.size();index++) {
                         if(names[index].contains(".xlsx",Qt::CaseInsensitive)) break;
                     }

                     if(index == names.size()) dirPath += ".xlsx";

                     _qDebugE("dirPath:",dirPath,"","");

                     QXlsx::Document xlsx(dirPath);

                     for(int colNo = 1;colNo <= columnNames.count();colNo++)
                     {
                         data = columnNames.at(colNo-1);
                         xlsx.write(1,colNo,data);
                     }

                     for(int rowNo = 2,i = 0;i < tableData.count();rowNo++,i++)
                     {
                         for(int colNo = 1;colNo <= tableData.at(i).count();colNo++)
                         {
                             data = tableData.at(i).at(colNo-1);
                             xlsx.write(rowNo,colNo,data);
                         }
                     }
                     xlsx.save();
                     INFO QMessageBox::information(nullptr,"Export","File Exported Successfully.");
                 }//else block when dirPath is not empty
             }//else block when fetching column names is success
         }//else block when fetching table data is success

         comboBox_tablesObj->setEnabled(true);
     }
 }

/*************************************************************************
 Function Name - on_deletePushButton_clicked
 Parameter(s)  - void
 Return Type   - void
 Action        - This function handles the delete pushbutton clicked signal.
 *************************************************************************/
 void Home::on_deletePushButton_clicked()
 {
     _qDebug("void Home::on_deletePushButton_clicked().");

     QString selectedTable;
     QVector<QStringList> tableData;
     QStringList tableNames;

     QDir myDir;
     QString dirPath;

     tableData.clear();
     tableNames.clear();

     QMessageBox::StandardButton resBtn = QMessageBox::question( this, MESSAGE_TITLE,tr("Do you Want to Delete the selected Table?\n"),QMessageBox::No | QMessageBox::Yes);
     if (resBtn == QMessageBox::Yes)
     {
         if(comboBox_tablesObj != nullptr)
         {
             comboBox_tablesObj->setDisabled(true);

             selectedTable = comboBox_tablesObj->currentText();

             if(!homeDbObj->deleteTable(selectedTable))
             {
                 PLAY QMessageBox::warning(nullptr,"Error!!\n",QString("Deleting %1 failed").arg(selectedTable));
             }
             else
             {
                 INFO QMessageBox::information(nullptr,"Delete","Table deleted Successfully.");

                 //get remaining table names
                 tableNames = homeDbObj->getTableNames();

                 if(tableNames.size() == 0)
                 {
                     _qDebugE("No table in DB Left.","","","");
                 }
                 else
                 {
                     comboBox_tablesObj->clear();
                     comboBox_tablesObj->addItems(tableNames);
                 }

                 //update Home menu
                 isEdit = true;
                 update_home_ui();
             }//else block when deleting table is success

             comboBox_tablesObj->setEnabled(true);
         }
     }
 }

/*************************************************************************
 Function Name - on_action_triggered
 Parameter(s)  - QString,QString
 Return Type   - void
 Action        - This function handles the action of menu item. Opens the
                  corresponding table.
 *************************************************************************/
 void Home::on_action_triggered(QString menuTitle,QString action_str)
 {
     _qDebug("void Home::on_action_triggered(QMenu* menu,QString action_str)");

     _qDebugE("menuTitle:",menuTitle,", action_str:",action_str);

     QString testTableName;
     QString tableColumnWidths;
     testTableName.clear();

     enableResultSaving(false);
     testData.clear();
     G::testStartMessage.clear();
     G::testInterMediateMessage.clear();

     ui->label_TestVoltage->setHidden(true);
     ui->label_TestVoltage->clear();
     ui->label_unitName->clear();
     ui->label_testName->clear();
     ui->label_TestStatus->clear();
     ui->label_TestStatus->setStyleSheet("");
     ui->progressBar->setValue(0);
     ui->pushButton_run_stop->setDisabled(true);
     ui->label_timer->setText("00 H : 00 M : 00 S");
     ui->label_cycle->setHidden(true);
     ui->comboBox_manualPolarity->setHidden(true);
     ui->actionSave_Table->setVisible(false);
     ui->label_status->setStyleSheet("QLabel { background-color : red;}");
     uutIndex = 0;

     for(int i = 0;i < uutQmenuLists.size();i++)
     {
         if(menuTitle.compare(uutQmenuLists.at(i)->title(),Qt::CaseSensitive) == 0)
         {
             for(int j = 0;j < unitTestLists.at(i).size();j++)
             {
                 if(action_str.compare(unitTestLists.at(i).at(j),Qt::CaseSensitive) == 0)
                 {
                    testTableName =  unitTestDataTableLists.at(i).at(j);
                    tableColumnWidths = tableColumnWidthLists.at(i).at(j);
                    G::testStartMessage = testStartMessageLists.at(i).at(j);
                    G::testInterMediateMessage = testIntermediateMessageLists.at(i).at(j);

                    ui->label_unitName->setText("  Unit: " + menuTitle + "  ");
                    ui->label_testName->setText("  Test: " +action_str + "  ");

//                    if(action_str.contains("insulation",Qt::CaseInsensitive)) {
//                        ui->label_TestVoltage->setHidden(false);
//                        ui->label_TestVoltage->setText("  Test Voltage: "+
//                                                      QString::number(MEGGER_TEST_VOLTAGE_DEFAULT) +"  V  ");
//                    }

                    uutIndex = i;
                    break;
                 }
             }

             break;
         }
     }

     if(action_str.contains("manual",Qt::CaseInsensitive))
     {
         columnWidth = tableColumnWidths;
         if(action_str.contains("isolation",Qt::CaseInsensitive))     loadManualIsolationTest();
         else if(action_str.contains("polarity",Qt::CaseInsensitive)) {
             if(loadManualTestTableFromDb(testTableName,tableColumnWidths)) ui->pushButton_run_stop->setEnabled(true);
             else ui->pushButton_run_stop->setDisabled(true);
         }
     }
     else if(testTableName.size() == 0)
     {
         _qDebugE("Oops!! Test Table Name could not be Found.","","","");
         PLAY QMessageBox::warning(nullptr,"Error!!",QString("Test Table Name [%1]could not be Found.").arg(testTableName));
         //make Run Button Disable
         ui->pushButton_run_stop->setDisabled(true);
     }
     else
     {
         _qDebugE("Test Table Name:",testTableName,"","");

         //upload table here
         if(loadTableFromDb(testTableName,tableColumnWidths))
         {
             //make Run Button Enable
             ui->pushButton_run_stop->setEnabled(true);
         }
         else
         {
             //make Run Button Disable
             ui->pushButton_run_stop->setDisabled(true);
         }
     }
 }

/*************************************************************************
 Function Name - on_actionSelect_All_triggered
 Parameter(s)  - void
 Return Type   - void
 Action        - This function selects/checks all checkboxes in the table.
 *************************************************************************/
 void Home::on_actionSelect_All_triggered()
 {
     _qDebug("void Home::on_actionSelect_All_triggered()");
     for(int i = 0;i < checkBoxLists.count();i++)
     {
         checkBoxLists.at(i)->setChecked(true);
     }
 }

/*************************************************************************
 Function Name - on_actionUnselect_All_triggered
 Parameter(s)  - void
 Return Type   - void
 Action        - This function unselects/unchecks all checkboxes in the table.
 *************************************************************************/
 void Home::on_actionUnselect_All_triggered()
 {
     _qDebug("void Home::on_actionUnselect_All_triggered()");
     for(int i = 0;i < checkBoxLists.count();i++)
     {
         if(checkBoxLists.at(i)->isEnabled())checkBoxLists.at(i)->setChecked(false);
     }
 }

/*************************************************************************
 Function Name - on_actionSave_Result_triggered
 Parameter(s)  - void
 Return Type   - void
 Action        - This function creates test report.
 *************************************************************************/
 void Home::on_actionSave_Result_triggered()
 {
    _qDebug("void Home::on_actionSave_Result_triggered()");
    if(ui->actionSave_Result->isEnabled() == false) return;

    if(testData.count() == 0)
    {
        PLAY QMessageBox::warning(nullptr,MESSAGE_TITLE,"No Data Found to save.\nPlease Run the Test and Save Again.");
        return;
    }

    QDir myDir;
    QString dirPath;
    QString currentDate = QDateTime::currentDateTime().toString("dd.MM.yyyy").replace(".","_");

    dirPath = QDir::homePath() + "/Desktop";

    if(!myDir.exists(dirPath)) myDir.mkpath(dirPath);

    dirPath = dirPath + "/" + extractValueFromLabel(ui->label_testName->text()).replace(' ','_') +"_" +
            QDateTime::currentDateTime().toString("dd_MM_yyyy") +  "_" +
            QDateTime::currentDateTime().toString("hh_mm_ss") + ".pdf";

    dirPath = QFileDialog :: getSaveFileName(this, tr("Save As"), dirPath, "Files(*.pdf)");
    _qDebugE("Saved File Path :[",dirPath,"]","");

    if(dirPath == "")
    {
        //PLAY QMessageBox::warning(nullptr,"Error!!\n","Report couldn't be saved due to empty file name.");
    }
    else
    {
        //Create and show a message box to show data processing
        QMessageBox* msgBox = new QMessageBox;

        msgBox->setWindowTitle(MESSAGE_TITLE);
        msgBox->setText("Please wait!! Report is being Generated ...");
        msgBox->setWindowFlags(Qt::CustomizeWindowHint);
        msgBox->setStandardButtons(nullptr);
        msgBox->show();
        timerObj->delayInms(1);

        generateReport(testData,statusData,dirPath);

        clearmem(msgBox);
    }
 }

/*************************************************************************
 Function Name - on_actionSave_Pass_Result_triggered
 Parameter(s)  - void
 Return Type   - void
 Action        - This function creates Pass test report.
 *************************************************************************/
 void Home::on_actionSave_Pass_Result_triggered()
 {
    _qDebug("void Home::on_actionSave_Pass_Result_triggered()");
    if(ui->actionSave_Pass_Result->isEnabled() == false) return;

    QVector<QVector<QStringList>> data;
    QStringList testStatus;
    QDir myDir;
    QString dirPath;

    data.clear();
    getPassData(data,testStatus);
    if(data.count() == 0)
    {
        PLAY QMessageBox::warning(nullptr,MESSAGE_TITLE,"No Pass Data Found to save.\nPlease Run the Test and Save Again.");
        return;
    }

    dirPath = QDir::homePath() + "/Desktop";

    if(!myDir.exists(dirPath)) myDir.mkpath(dirPath);

    dirPath = dirPath + "/" + extractValueFromLabel(ui->label_testName->text()).replace(' ','_') +"_PASS_" +
            QDateTime::currentDateTime().toString("dd_MM_yyyy") +  "_" +
            QDateTime::currentDateTime().toString("hh_mm_ss") + ".pdf";

    dirPath = QFileDialog :: getSaveFileName(this, tr("Save As"), dirPath, "Files(*.pdf)");
    _qDebugE("Saved File Path :[",dirPath,"]","");

    if(dirPath == "")
    {
        //PLAY QMessageBox::warning(nullptr,"Error!!\n","Report couldn't be saved due to empty file name.");
    }
    else
    {
        //Create and show a message box to show data processing
        QMessageBox* msgBox = new QMessageBox;

        msgBox->setWindowTitle(MESSAGE_TITLE);
        msgBox->setText("Please wait!! Report is being Generated ...");
        msgBox->setWindowFlags(Qt::CustomizeWindowHint);
        msgBox->setStandardButtons(nullptr);
        msgBox->show();
        timerObj->delayInms(1);

        generateReport(data,testStatus,dirPath);

        clearmem(msgBox);
    }
 }

/*************************************************************************
 Function Name - on_actionSave_Pass_Result_triggered
 Parameter(s)  - void
 Return Type   - void
 Action        - This function creates Fail test report.
 *************************************************************************/
 void Home::on_actionSave_Fail_Result_triggered()
 {
    _qDebug("void Home::on_actionSave_Fail_Result_triggered()");
    if(ui->actionSave_Fail_Result->isEnabled() == false) return;

    QVector<QVector<QStringList>> data;
    QStringList testStatus;
    QDir myDir;
    QString dirPath;

    data.clear();
    getFailData(data,testStatus);
    if(data.count() == 0)
    {
        PLAY QMessageBox::warning(nullptr,MESSAGE_TITLE,"No Fail Data Found to save.\nPlease Run the Test and Save Again.");
        return;
    }

    dirPath = QDir::homePath() + "/Desktop";

    if(!myDir.exists(dirPath)) myDir.mkpath(dirPath);

    dirPath = dirPath + "/" + extractValueFromLabel(ui->label_testName->text()).replace(' ','_') +"_FAIL_" +
            QDateTime::currentDateTime().toString("dd_MM_yyyy") +  "_" +
            QDateTime::currentDateTime().toString("hh_mm_ss") + ".pdf";

    dirPath = QFileDialog :: getSaveFileName(this, tr("Save As"), dirPath, "Files(*.pdf)");
    _qDebugE("Saved File Path :[",dirPath,"]","");

    if(dirPath == "")
    {
        //PLAY QMessageBox::warning(nullptr,"Error!!\n","Report couldn't be saved due to empty file name.");
    }
    else
    {
        //Create and show a message box to show data processing
        QMessageBox* msgBox = new QMessageBox;

        msgBox->setWindowTitle(MESSAGE_TITLE);
        msgBox->setText("Please wait!! Report is being Generated ...");
        msgBox->setWindowFlags(Qt::CustomizeWindowHint);
        msgBox->setStandardButtons(nullptr);
        msgBox->show();
        timerObj->delayInms(1);

        generateReport(data,testStatus,dirPath);

        clearmem(msgBox);
    }
 }

/*************************************************************************
 Function Name - on_actionAuto_Save_Report_after_Test_triggered
 Parameter(s)  - void
 Return Type   - void
 Action        - This function handles for user selection on Auto_Save_Report .
 *************************************************************************/
 void Home::on_actionAuto_Save_Report_after_Test_triggered()
 {
    _qDebug("void Home::on_actionAuto_Save_Report_after_Test_triggered()");
    handleSelection(ui->actionAuto_Save_Report_after_Test,1);
 }

/*************************************************************************
 Function Name - on_actionAuto_Generate_Self_Check_Report_triggered
 Parameter(s)  - void
 Return Type   - void
 Action        - This function handles for user selection on
                 Auto_Generate_Self_Check_Report.
 *************************************************************************/
 void Home::on_actionAuto_Generate_Self_Check_Report_triggered()
 {
     _qDebug("void Home::on_actionAuto_Generate_Self_Check_Report_triggered()");
     handleSelection(ui->actionAuto_Generate_Self_Check_Report,2);
 }

 /*************************************************************************
  Function Name - on_actionEnable_Report_LandScape_Mode_triggered
  Parameter(s)  - void
  Return Type   - void
  Action        - This function handles for user selection on Enable_Report_LandScape_Mode.
  *************************************************************************/
 void Home::on_actionEnable_Report_LandScape_Mode_triggered()
 {
     _qDebug("void Home::on_actionEnable_Report_LandScape_Mode_triggered()");
     handleSelection(ui->actionEnable_Report_LandScape_Mode,3);
 }

/*************************************************************************
 Function Name - on_actionEnable_Keyboard_Action_triggered
 Parameter(s)  - void
 Return Type   - void
 Action        - This function handles for user selection on Enable_Keyboard.
 *************************************************************************/
 void Home::on_actionEnable_Keyboard_Action_triggered()
 {
    _qDebug("void Home::on_actionEnable_Keyboard_Action_triggered()");
    handleSelection(ui->actionEnable_Keyboard_Action,4);
 }

/*************************************************************************
 Function Name - on_actionAbout_triggered
 Parameter(s)  - void
 Return Type   - void
 Action        - This function shows about the GUI details.
 *************************************************************************/
 void Home::on_actionAbout_triggered()
 {
     _qDebug("void Home::on_actionAbout_triggered()");
     QWidget *aboutWidget = new QWidget;
     QLabel *label_pic = new QLabel;
     QString str;

     QPixmap pix = QPixmap(QDir::currentPath() +"/images/logoATL");
     pix = pix.scaled(110,110);
     label_pic->setPixmap(pix);

     QFont font("Times New Roman", 11, QFont::Bold);

     str = MESSAGE_TITLE;
     str += " GUI V1.0";
     QLabel *guiVersionLabel = new QLabel(str);
     guiVersionLabel->setFont(font);

     QLabel *dateTimeLabel = new QLabel("Built on November 15 2021 10:00 IST");
     dateTimeLabel->setFont(font);
     QString checksum = utilObj.getChecksumExe();
     QLabel *checksumLabel = new QLabel("Checksum " + checksum);
     checksumLabel->setFont(font);
     checksumLabel->setStyleSheet("QLabel { background-color :  #f0fe23 ; color : blue; }");

     QLabel *copyRightInfLabel = new QLabel("©2021 Ananth Technologies Limited, Inc. All rights reserved.");
     //copyRightInfLabel.setText("Copyright 2020 The Ananth Technologies Ltd. All right reserved");
     copyRightInfLabel->setFont(font);

     QPushButton *button_Close = new QPushButton("Close");
     //button_Close.setText("Close");
     QLabel *spaceLabel1 = new QLabel("                                                              ");
     QLabel *spaceLabel2 = new QLabel("                                                              ");

     QVBoxLayout *vBoxLayout1 = new QVBoxLayout;
     vBoxLayout1->addWidget(label_pic);

     QVBoxLayout *vBoxLayout2 = new QVBoxLayout;
     vBoxLayout2->addWidget(guiVersionLabel);
     vBoxLayout2->addWidget(dateTimeLabel);
     vBoxLayout2->addWidget(checksumLabel);
     vBoxLayout2->addWidget(copyRightInfLabel);

     QHBoxLayout *hBoxLayoutMain = new QHBoxLayout;
     hBoxLayoutMain->addLayout(vBoxLayout1);
     hBoxLayoutMain->addLayout(vBoxLayout2);

     QHBoxLayout *hBoxLayoutLast = new QHBoxLayout;
     hBoxLayoutLast->addWidget(spaceLabel1);
     hBoxLayoutLast->addWidget(spaceLabel2);
     hBoxLayoutLast->addWidget(button_Close);

     QVBoxLayout *vBoxLayout = new QVBoxLayout;;
     vBoxLayout->addLayout(hBoxLayoutMain);
     vBoxLayout->addLayout(hBoxLayoutLast);

     aboutWidget->setLayout(vBoxLayout);
     aboutWidget->setWindowTitle("About");
     aboutWidget->setWindowFlags(Qt::WindowTitleHint | Qt::WindowCloseButtonHint);
     aboutWidget->show();

     connect(button_Close , SIGNAL(clicked()), aboutWidget , SLOT(close()));
 }

/*************************************************************************
 Function Name - on_actionAbout_triggered
 Parameter(s)  - void
 Return Type   - void
 Action        - This function shows Relay Test GUI details.
 *************************************************************************/
 void Home::on_actionRelay_Test_triggered()
 {
    _qDebug("void Home::on_actionRelay_Test_triggered()");
    QString title = "RelayTest";

    relayTest = new RelayTest();

    if(relayTest == nullptr) {
        PLAY QMessageBox::warning(nullptr,"Error!!","RelayTest object creation failed.");
    }
    else {
        QMessageBox* msgBox = new QMessageBox;
        msgBox->setWindowTitle(title);
        msgBox->setText("Please wait!! "+title+" Window is being loaded...  ");
        msgBox->setWindowFlags(Qt::CustomizeWindowHint);
        msgBox->setStandardButtons(nullptr);
        msgBox->show();
        timerObj->delayInms(1);

        timerMeterCheckObj->stop();

        relayTest->setMsgTitle(title);
        relayTest->setWindowIcon(QIcon(":/Images/atl.ico"));

        G::handlerObj->pause();
        while(!G::handlerObj->isPaused()) timerObj->delayInms(1);

        if(relayTest->checkPS()) {
            connect(relayTest,SIGNAL(close()), this, SLOT(closeRelayTest()));

            hide();
            relayTest->setWindowTitle(title);
            relayTest->show();
        }
        else closeRelayTest();

        clearmem(msgBox);
    }
 }

/*************************************************************************
 Function Name - on_actionSelf_Test_triggered
 Parameter(s)  - void
 Return Type   - void
 Action        - This function shows about Self Test GUI details.
 *************************************************************************/
 void Home::on_actionSelf_Test_triggered()
 {
     _qDebug("void Home::on_actionSelf_Test_triggered()");
     QString title = "SelfTest";

     jigSelfTest = new JigSelfTest();

     if(jigSelfTest == nullptr) {
         PLAY QMessageBox::warning(nullptr,"Error!!","SelfTest Window open failed.");
     }
     else {
         QMessageBox* msgBox = new QMessageBox;
         msgBox->setWindowTitle(title);
         msgBox->setText("Please wait!! "+title+" Window is being loaded...  ");
         msgBox->setWindowFlags(Qt::CustomizeWindowHint);
         msgBox->setStandardButtons(nullptr);
         msgBox->show();
         timerObj->delayInms(1);

         QString userRep = ui->lineEdit_UserPerson->text();
         QString qaRep   = ui->lineEdit_QAperson->text();

         if(!userRep.count() || !qaRep.count())
         {
             clearmem(msgBox);
             PLAY QMessageBox::warning(nullptr,"Error!!\n","User Rep or QA Rep fields can't be empty.\nPlease try again.");
             clearmem(jigSelfTest);
             return;
         }

         timerMeterCheckObj->stop();

         jigSelfTest->setMsgTitle(title);
         jigSelfTest->setWindowIcon(QIcon(":/Images/atl.ico"));

         G::handlerObj->pause();
         while(!G::handlerObj->isPaused()) timerObj->delayInms(1);


         if(jigSelfTest->initSelfTest(userRep,qaRep,isAutoSaveSCReport)) {
             connect(jigSelfTest,SIGNAL(close()), this, SLOT(closeSelfTest()));

             hide();
             jigSelfTest->setWindowTitle(title);
             jigSelfTest->show();
         }
         else  closeSelfTest();

         clearmem(msgBox);
     }
 }

 void Home::indexChanged(int index) {
     _qDebug("void Home::indexChanged(int index)");

     if((encDec != nullptr) && (encDec->comboBox_table != nullptr)) {
        encDec->comboBox_table->clear();

        encDec->comboBox_table->addItems(encDec->dbTables.at(index));
        encDec->comboBox_table->setCurrentIndex(0);
     }
 }

 void Home::edPushButtonClicked()
 {
     _qDebug("void Home::edPushButtonClicked()");

     QString action = encDec->pushButton_ed->text().trimmed();
     QString message;
     bool isAllDbMode = false;
     bool isAllTableMode = false;
     bool status;
     bool isEncode = false;

     if(encDec->checkbox_dbSelectAll->isChecked()) {
        message = QString("Do you Want to %1 all tables in all DB?\n").arg(action);
        isAllDbMode = true;
     }
     else if(encDec->checkbox_tableSelectAll->isChecked()) {
        message = QString("Do you Want to %1 all tables in DB named [%2]?\n").arg(action).
                arg(encDec->comboBox_db->currentText());
        isAllTableMode = true;
     }
     else {
        message = QString("Do you Want to %1 the table [%2] in DB named [%3]?\n").arg(action).
                arg(encDec->comboBox_table->currentText()).arg(encDec->comboBox_db->currentText());
     }

     if(action.compare("encode",Qt::CaseInsensitive) == 0) isEncode = true;
     _qDebugE("action:",action,", isEncode:",isEncode);

     QMessageBox::StandardButton resBtn = QMessageBox::question( this, action,message,QMessageBox::No | QMessageBox::Yes);
     if (resBtn == QMessageBox::Yes)
     {
         if(isAllDbMode) {
             for(int i = 0;i < encDec->dbObj.count();i++) {
                 if(isEncode) status = encodeData(encDec->dbObj.at(i),encDec->dbTables.at(i));
                 else         status = decodeData(encDec->dbObj.at(i),encDec->dbTables.at(i));
                 if(!status) break;
             }
         }
         else if(isAllTableMode) {
             int index = encDec->comboBox_db->currentIndex();
             if(isEncode) status = encodeData(encDec->dbObj.at(index),encDec->dbTables.at(index));
             else         status = decodeData(encDec->dbObj.at(index),encDec->dbTables.at(index));
         }
         else {
             int index = encDec->comboBox_db->currentIndex();
             int index1 = encDec->comboBox_table->currentIndex();
             if(isEncode) status = encodeData(encDec->dbObj.at(index),encDec->dbTables.at(index).at(index1));
             else         status = decodeData(encDec->dbObj.at(index),encDec->dbTables.at(index).at(index1));
         }

         if(status) {
             if(isEncode) {INFO QMessageBox::information(nullptr,"Error!!\n","Data Encoded successfully in DB.");}
             else         {INFO QMessageBox::information(nullptr,"Error!!\n","Data Decoded successfully in DB.");}
         }
     }
 }

 void Home::edCancelPushButtonClicked()
 {
     _qDebug("void Home::edCancelPushButtonClicked()");
     QMessageBox::StandardButton resBtn = QMessageBox::question( this, "Cancel","Do you want to close ?\n",QMessageBox::No | QMessageBox::Yes);
     if (resBtn == QMessageBox::Yes)
     {
         clearmem(encDec->label_db);
         clearmem(encDec->comboBox_db);
         clearmem(encDec->checkbox_dbSelectAll);
         clearmem(encDec->label_table);
         clearmem(encDec->comboBox_table);
         clearmem(encDec->checkbox_tableSelectAll);
         clearmem(encDec->pushButton_ed);
         clearmem(encDec->pushButton_cancel);
         clearmem(encDec->label_blank);
         clearmem(encDec->hBoxLayout1);
         clearmem(encDec->hBoxLayout2);
         clearmem(encDec->hBoxLayout3);
         clearmem(encDec->vBoxLayout);
         clearmem(encDec->widget);

         for(Database* db : encDec->dbObj) {
             db->closeDB();
             clearmem(db);
         }
         encDec->dbObj.clear();
         encDec->dbTables.clear();
         clearmem(encDec);
     }
 }

void Home::on_actionENC_DB_triggered()
{
    _qDebug("void Home::on_actionENC_DB_triggered()");
    encDec = new EncDec;
    int height = 25;

    encDec->label_db = new QLabel("Select DB     : ");
    encDec->comboBox_db = new QComboBox;
    encDec->checkbox_dbSelectAll = new QCheckBox("");
    encDec->label_table = new QLabel("Select Table : ");
    encDec->comboBox_table = new QComboBox;
    encDec->checkbox_tableSelectAll = new QCheckBox("");
    encDec->pushButton_ed = new QPushButton(" Encode ");
    encDec->pushButton_cancel = new QPushButton(" Cancel ");
    encDec->label_blank = new QLabel("   ");

    encDec->label_db->setMinimumSize(30,height);
    encDec->label_table->setMinimumSize(30,height);
    encDec->comboBox_db->setMinimumSize(250,height);
    encDec->comboBox_table->setMinimumSize(250,height);
    encDec->checkbox_dbSelectAll->setMinimumSize(height,height);
    encDec->checkbox_tableSelectAll->setMinimumSize(height,height);
    encDec->pushButton_ed->setMaximumSize(180,height);
    encDec->pushButton_cancel->setMaximumSize(180,height);

    encDec->hBoxLayout1 = new QHBoxLayout;
    encDec->hBoxLayout2 = new QHBoxLayout;
    encDec->hBoxLayout3 = new QHBoxLayout;
    encDec->vBoxLayout = new QVBoxLayout;

    encDec->widget = new QWidget;

    encDec->widget->setWindowTitle("Encode");
    encDec->widget->setMaximumSize(400,150);//width,height
    encDec->widget->setMinimumSize(400,150);//width,height

    QStringList dbs = {"TMR_TRB.db","Login.db","Self_Test.db","METER_SETTINGS.db"};
    QVector<QStringList> selData;

    encDec->dbObj.clear();
    encDec->dbTables.clear();
    for(QString dbName : dbs) {
        Database* obj = new Database;
        QString connectionName = dbName.mid(0,dbName.indexOf("."))+"conn";
        QString userName = dbName.mid(0,dbName.indexOf("."))+"usr";

        encDec->dbObj.push_back(obj);

        if(obj->openDB(dbName,"localHost",userName,"password",connectionName)) {
            _qDebugE("DB:",dbName," opened successfully.","");
            encDec->dbTables.push_back(obj->getTableNames());
        }
        else {
            _qDebugE("Error!! DB:",dbName," open failed.","");
            encDec->dbTables.push_back(QStringList(""));
        }
    };
    _qDebugE("dbTables:",encDec->dbTables,"","");

    encDec->comboBox_db->addItems(dbs);
    encDec->comboBox_db->setCurrentIndex(0);
    encDec->comboBox_table->addItems(encDec->dbTables.at(0));
    connect(encDec->comboBox_db, SIGNAL(currentIndexChanged(int)), this, SLOT(indexChanged(int)));

    connect(encDec->pushButton_ed,SIGNAL(clicked()), this, SLOT(edPushButtonClicked()));
    connect(encDec->pushButton_cancel,SIGNAL(clicked()), this, SLOT(edCancelPushButtonClicked()));

    encDec->hBoxLayout1->addWidget(encDec->label_db);
    encDec->hBoxLayout1->addWidget(encDec->comboBox_db);
    encDec->hBoxLayout1->addWidget(encDec->checkbox_dbSelectAll);

    encDec->hBoxLayout2->addWidget(encDec->label_table);
    encDec->hBoxLayout2->addWidget(encDec->comboBox_table);
    encDec->hBoxLayout2->addWidget(encDec->checkbox_tableSelectAll);

    //encDec->hBoxLayout3->addWidget(encDec->label_blank);
    encDec->hBoxLayout3->addWidget(encDec->pushButton_ed);
    encDec->hBoxLayout3->addWidget(encDec->pushButton_cancel);

    encDec->vBoxLayout->addLayout(encDec->hBoxLayout1);
    encDec->vBoxLayout->addLayout(encDec->hBoxLayout2);
    encDec->vBoxLayout->addLayout(encDec->hBoxLayout3);

    encDec->widget->setLayout(encDec->vBoxLayout);
    encDec->widget->setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowMinMaxButtonsHint);//restore
    encDec->widget->setWindowModality(Qt::ApplicationModal);//Block other windows from receiving input events
    encDec->widget->show();
}

void Home::on_actionDEC_DB_triggered()
{
    _qDebug("void Home::on_actionDEC_DB_triggered()");
    encDec = new EncDec;
    int height = 25;

    encDec->label_db = new QLabel("Select DB     : ");
    encDec->comboBox_db = new QComboBox;
    encDec->checkbox_dbSelectAll = new QCheckBox("");
    encDec->label_table = new QLabel("Select Table : ");
    encDec->comboBox_table = new QComboBox;
    encDec->checkbox_tableSelectAll = new QCheckBox("");
    encDec->pushButton_ed = new QPushButton(" Decode ");
    encDec->pushButton_cancel = new QPushButton(" Cancel ");
    encDec->label_blank = new QLabel("   ");

    encDec->label_db->setMinimumSize(30,height);
    encDec->label_table->setMinimumSize(30,height);
    encDec->comboBox_db->setMinimumSize(250,height);
    encDec->comboBox_table->setMinimumSize(250,height);
    encDec->checkbox_dbSelectAll->setMinimumSize(height,height);
    encDec->checkbox_tableSelectAll->setMinimumSize(height,height);
    encDec->pushButton_ed->setMaximumSize(180,height);
    encDec->pushButton_cancel->setMaximumSize(180,height);

    encDec->hBoxLayout1 = new QHBoxLayout;
    encDec->hBoxLayout2 = new QHBoxLayout;
    encDec->hBoxLayout3 = new QHBoxLayout;
    encDec->vBoxLayout = new QVBoxLayout;

    encDec->widget = new QWidget;

    encDec->widget->setWindowTitle("Decode");
    encDec->widget->setMaximumSize(400,150);//width,height
    encDec->widget->setMinimumSize(400,150);//width,height

    QStringList dbs = {"TMR_TRB.db","Login.db","Self_Test.db","METER_SETTINGS.db"};
    QVector<QStringList> selData;

    encDec->dbObj.clear();
    encDec->dbTables.clear();
    for(QString dbName : dbs) {
        Database* obj = new Database;
        QString connectionName = dbName.mid(0,dbName.indexOf("."))+"conn";
        QString userName = dbName.mid(0,dbName.indexOf("."))+"usr";

        encDec->dbObj.push_back(obj);

        if(obj->openDB(dbName,"localHost",userName,"password",connectionName)) {
            _qDebugE("DB:",dbName," opened successfully.","");
            encDec->dbTables.push_back(obj->getTableNames());
        }
        else {
            _qDebugE("Error!! DB:",dbName," open failed.","");
            encDec->dbTables.push_back(QStringList(""));
        }
    };
    _qDebugE("dbTables:",encDec->dbTables,"","");

    encDec->comboBox_db->addItems(dbs);
    encDec->comboBox_db->setCurrentIndex(0);
    encDec->comboBox_table->addItems(encDec->dbTables.at(0));
    connect(encDec->comboBox_db, SIGNAL(currentIndexChanged(int)), this, SLOT(indexChanged(int)));

    connect(encDec->pushButton_ed,SIGNAL(clicked()), this, SLOT(edPushButtonClicked()));
    connect(encDec->pushButton_cancel,SIGNAL(clicked()), this, SLOT(edCancelPushButtonClicked()));

    encDec->hBoxLayout1->addWidget(encDec->label_db);
    encDec->hBoxLayout1->addWidget(encDec->comboBox_db);
    encDec->hBoxLayout1->addWidget(encDec->checkbox_dbSelectAll);

    encDec->hBoxLayout2->addWidget(encDec->label_table);
    encDec->hBoxLayout2->addWidget(encDec->comboBox_table);
    encDec->hBoxLayout2->addWidget(encDec->checkbox_tableSelectAll);

    //encDec->hBoxLayout3->addWidget(encDec->label_blank);
    encDec->hBoxLayout3->addWidget(encDec->pushButton_ed);
    encDec->hBoxLayout3->addWidget(encDec->pushButton_cancel);

    encDec->vBoxLayout->addLayout(encDec->hBoxLayout1);
    encDec->vBoxLayout->addLayout(encDec->hBoxLayout2);
    encDec->vBoxLayout->addLayout(encDec->hBoxLayout3);

    encDec->widget->setLayout(encDec->vBoxLayout);
    encDec->widget->setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowMinMaxButtonsHint);//restore
    encDec->widget->setWindowModality(Qt::ApplicationModal);//Block other windows from receiving input events
    encDec->widget->show();
}

void Home::on_comboBox_manualPolarity_currentIndexChanged(const QString &arg1)
{
    ui->label_TestStatus->clear();
    ui->label_TestStatus->setStyleSheet("");

    if(ui->label_testName->text().contains("standby",Qt::CaseInsensitive)) {
        if(manualData_StandBy.count() <= 0) {
            PLAY QMessageBox::warning(nullptr,"Error!!\n","No Standby Data Found");
            return;
        }

        updateManualTable(ui->tableWidget,manualData_StandBy.value(arg1),columnWidth);
    }
    else {
        if(manualData_Main.count() <= 0) {
            PLAY QMessageBox::warning(nullptr,"Error!!\n","No Main Data Found");
            return;
        }

        updateManualTable(ui->tableWidget,manualData_Main.value(arg1),columnWidth);
    }
}

void Home::on_actionSave_Table_triggered()
{
    int connectorIndex = INT_MAX;
    int expValueIndex  = INT_MAX;
    int measValueIndex = INT_MAX;
    int resultIndex    = INT_MAX;
    int toPinsIndex    = INT_MAX;

    for(uint8_t i = 0;i < ui->tableWidget->columnCount();i++)
    {
        if(     ui->tableWidget->horizontalHeaderItem(i)->text().compare("Measured_Value"     ) == 0) measValueIndex = i;
        else if(ui->tableWidget->horizontalHeaderItem(i)->text().compare("To Pins"            ) == 0) toPinsIndex    = i;
        else if(ui->tableWidget->horizontalHeaderItem(i)->text().compare("Expected Resistance") == 0) expValueIndex  = i;
        else if(ui->tableWidget->horizontalHeaderItem(i)->text().compare("Result"             ) == 0) resultIndex    = i;
    }

    for(uint8_t i = 0;i < hiddenDataHeaders.count();i++)
    {
        if(hiddenDataHeaders.at(i).compare("Outputs_High") == 0) { connectorIndex = i; break; }
    }

    if(connectorIndex == INT_MAX) {
        PLAY QMessageBox::warning(nullptr,"Error!!",QString("Outputs_High column is not present in RHS Table Header."));
        return;
    }
    if(measValueIndex == INT_MAX) {
        PLAY QMessageBox::warning(nullptr,"Error!!",QString("Measured_Value column is not present in Table Header."));
        return;
    }
    if(expValueIndex  == INT_MAX) {
        PLAY QMessageBox::warning(nullptr,"Error!!",QString("Expected Resistance column is not present in Table Header."));
        return;
    }
    if(resultIndex == INT_MAX) {
        PLAY QMessageBox::warning(nullptr,"Error!!",QString("Result column is not present in Table Header."));
        return;
    }
    if(hiddenDataHeaders.count() != hiddenData.count()) {
        PLAY QMessageBox::warning(nullptr,"Error!!","RHS data missing in Database.");
        return;
    }

    int rowCount = ui->tableWidget->rowCount();
    QStringList connectorLists;
    QStringList resultLists;
    QStringList remarkLists;
    QString result;
    QString connector;
    QString pin;
    int index;
    int rowNo;

    QMessageBox* msgBox = new QMessageBox;
    msgBox->setWindowTitle(MESSAGE_TITLE);
    msgBox->setText("Please wait!! The Data is being processed...  ");
    msgBox->setWindowFlags(Qt::CustomizeWindowHint);
    msgBox->setStandardButtons(nullptr);
    msgBox->show();
    timerObj->delayInms(1);

    for(rowNo = 0;rowNo < rowCount;rowNo++) {
        G::getConnPin(hiddenData.at(connectorIndex).at(rowNo),connector,pin,"/");
        _qDebugE("connector:",connector,", pin:",pin);

        if((connector.size() == 0) && (pin.size() == 0)) continue;

        if(connector.size() == 0) {
            PLAY QMessageBox::warning(nullptr,"Error!!",QString("Invalid Connector Number [%1].").arg(connector));
            break;
        }
        if(pin.size() == 0) {
            PLAY QMessageBox::warning(nullptr,"Error!!",QString("Invalid Pin Number [%1] in Connector [%2].").arg(pin).arg(connector));
            break;
        }

        result = ui->tableWidget->item(rowNo,resultIndex)->text();
        if(result.size() == 0) continue;

        index = connectorLists.indexOf(connector);

        if(index == -1) {
            connectorLists.push_back(connector);

            resultLists.push_back(result);
            remarkLists.push_back("");

            if(result.contains("fail",Qt::CaseInsensitive)) {
                if(toPinsIndex == INT_MAX) {
                    remarkLists.push_back(QString("[%1]:E[%2]:M[%3]").arg(pin)
                                          .arg(ui->tableWidget->item(rowNo,expValueIndex)->text())
                                          .arg(ui->tableWidget->item(rowNo,measValueIndex)->text()));
                }
                else {
                    QString toConnector,toPin;
                    G::getConnPin(ui->tableWidget->item(rowNo,toPinsIndex)->text(),toConnector,toPin," ");

                    if(connector != toConnector) {
                        PLAY QMessageBox::warning(nullptr,"Error!!",QString("FromConnector [%1] and ToConnector [%2] mismatching.")
                                                  .arg(connector).arg(toConnector));
                        break;
                    }
                    if(toPin.size() == 0) {
                        PLAY QMessageBox::warning(nullptr,"Error!!",QString("Invalid Pin Number [%1] in Connector [%2].").arg(toPin).arg(toConnector));
                        break;
                    }
                    remarkLists.push_back(QString("[%1->%2]:E[%3]:M[%4]").arg(pin).arg(toPin)
                                          .arg(ui->tableWidget->item(rowNo,expValueIndex)->text())
                                          .arg(ui->tableWidget->item(rowNo,measValueIndex)->text()));

                }
            }
        }
        else {
            if(result.contains("fail",Qt::CaseInsensitive)) {
                resultLists[index] = result;

                if(remarkLists[index].size() > 0) remarkLists[index].append(", ");
                if(toPinsIndex == INT_MAX) {
                    remarkLists[index].append(QString("[%1]:E[%2]:M[%3]").arg(pin)
                                          .arg(ui->tableWidget->item(rowNo,expValueIndex)->text())
                                          .arg(ui->tableWidget->item(rowNo,measValueIndex)->text()));
                }
                else {
                    QString toConnector,toPin;
                    G::getConnPin(ui->tableWidget->item(rowNo,toPinsIndex)->text(),toConnector,toPin," ");

                    if(connector != toConnector) {
                        PLAY QMessageBox::warning(nullptr,"Error!!",QString("FromConnector [%1] and ToConnector [%2] mismatching.")
                                                  .arg(connector).arg(toConnector));
                        break;
                    }
                    if(toPin.size() == 0) {
                        PLAY QMessageBox::warning(nullptr,"Error!!",QString("Invalid Pin Number [%1] in Connector [%2].").arg(toPin).arg(toConnector));
                        break;
                    }
                    remarkLists[index].append(QString("[%1->%2]:E[%3]:M[%4]").arg(pin).arg(toPin)
                                          .arg(ui->tableWidget->item(rowNo,expValueIndex)->text())
                                          .arg(ui->tableWidget->item(rowNo,measValueIndex)->text()));
                }
            }
        }
    } //for loop runs for number of rows

    clearmem(msgBox);
    timerObj->delayInms(10);

    if(rowNo < rowCount) return;

    QVector<QStringList> tableData;
    QStringList connectorData;
    QStringList headers = {"Connector_Number","Result","Remarks"};

    tableData.push_back(headers);

    for(int i = 0;i < connectorLists.count();i++) {
        connectorData.clear();
        connectorData.push_back(connectorLists.at(i));
        connectorData.push_back(resultLists.at(i));
        connectorData.push_back(remarkLists.at(i));
        tableData.push_back(connectorData);
    }

    if(tableData.count() < 1) {
        PLAY QMessageBox::warning(nullptr,"Error!!","Unable to find Data.");
        return;
    }
    statusData.push_back(ui->label_TestStatus->text());

    generateReport(QVector<QVector<QStringList>> (1,tableData),statusData,"");
}
