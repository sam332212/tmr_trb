/*************************************  FILE HEADER  *****************************************************************
*
*  File name - Global.cpp
*  Creation date and time - 07-JUN-2020 , SUN 07:00 AM IST
*  Author: - Manoj
*  Purpose of the file - All Global components will be defined .
*
**********************************************************************************************************************/
/********************************************************
 Inclusion of header file
 ********************************************************/
#include "Global.h"
#include <QtWidgets>

/********************************************************
 MACRO Definition
 ********************************************************/
#define MAP_TABLE_CELL_COUNT    2
#define MAP_TABLE_DOP_INDEX     1

#if GLOBAL_C_DEBUG
    #define _qDebug(s) qDebug()<<"\n-------- Inside "<<s<<" --------"
#else
    #define _qDebug(s); {}
#endif

#if GLOBAL_C_DEBUG_E
    #define _qDebugE(p,q,r,s) qDebug()<<p<<q<<r<<s
#else
    #define _qDebugE(p,q,r,s); {}
#endif

DMM*             G::dmmObj = nullptr;
IRMeter*         G::irMeterObj = nullptr;
PowerSupply*     G::psObj = nullptr;
DevicesHandler*  G::handlerObj = nullptr;
Pci1758*         G::pci1758Obj = nullptr;
Pci7444*         G::pci7444Obj = nullptr;
QTableWidget*    G::testTable = nullptr;
QLabel*          G::testResultStatus = nullptr;
QLabel*          G::testRunStatus = nullptr;
QLabel*          G::cycleDisplay = nullptr;
QPushButton*     G::runStopButton = nullptr;
QVector<QLabel*> G::psStatus;
QLabel*          G::testVoltage = nullptr;
QProgressBar*    G::progressBar = nullptr;
QTimer*          G::testTimer = nullptr;
QTimer*          G::cycleTimer = nullptr;
QMessageBox*     G::msgBox = nullptr;
QMessageBox*     G::testMsgBox = nullptr;
QString          G::appVersion = "";
QString          G::builtDate = "";
QString          G::expectedChecksum = "";
QString          G::softwareChecksum = "";
QString          G::startDate = "";
QString          G::endDate = "";
QString          G::startTime = "";
QString          G::endTime = "";

QFont   G::myFont;
QString G::testStartMessage = "";
QString G::testInterMediateMessage = "";
QString G::passSSLabel = "QLabel { background-color : #3AEA02;}";
QString G::failSSLabel = "QLabel { background-color : red;}";

double G::calibResistance = 0.0;
double G::calibVoltage    = 0.0;
long   G::measCount = 0;
long   G::testCount = 0;

bool G::stopTest = false;
bool G::checksumValidate = true;

int G::prevSlidebarPos = 0;
int G::curSlidebarPos = 0;
QSoundEffect G::effect;

/********************************************************
 Function Definition
 ********************************************************/
/*************************************************************************
 Function Name - Global
 Parameter(s)  - void
 Return Type   - void
 Action        - Creates the object.
 *************************************************************************/
Global::Global()
{
    _qDebug("Global Constructor");
}

/*************************************************************************
 Function Name - ~Global
 Parameter(s)  - void
 Return Type   - void
 Action        - Destroys the object.
 *************************************************************************/
 Global::~Global()
 {
    _qDebug("Global Destructor");
 }

/*************************************************************************
 Function Name - cleanUi
 Parameter(s)  - void
 Return Type   - void
 Action        - cleans Ui.
 *************************************************************************/
void Global::cleanUi()
{
    testResultStatus->setText("");
    testResultStatus->setStyleSheet("");
    testRunStatus->setStyleSheet("");//clear Label
    testRunStatus->clear();
    testVoltage->setText("");
    testVoltage->setHidden(true);
    testTable->setSelectionMode(QAbstractItemView::NoSelection);
    testTable->verticalScrollBar()->setValue(0);
    progressBar->setValue(0);

    setScrollToZero();
}

/*************************************************************************
 Function Name - setScrollToZero
 Parameter(s)  - void
 Return Type   - void
 Action        - sets scroll parameters to zero value.
 *************************************************************************/
void Global::setScrollToZero()
{
    prevSlidebarPos = 0;
    curSlidebarPos = 0;
}

/*************************************************************************
 Function Name - manageScrolling
 Parameter(s)  - int, int
 Return Type   - void
 Action        - Handles the user scrolling in table.
 *************************************************************************/
 void Global::manageScrolling(int index,int rowNo)
 {
     if(index == 0)
     {
         testTable->selectRow(rowNo);
         prevSlidebarPos = testTable->verticalScrollBar()->sliderPosition();
     }

     curSlidebarPos = testTable->verticalScrollBar()->sliderPosition();
     if(curSlidebarPos >= prevSlidebarPos){
         testTable->selectRow(rowNo);
         prevSlidebarPos = curSlidebarPos;
     }
     if(prevSlidebarPos > rowNo) prevSlidebarPos = curSlidebarPos;
 }

/*************************************************************************
 Function Name - manageScrolling
 Parameter(s)  - int, int
 Return Type   - void
 Action        - Handles the user scrolling in table.
 *************************************************************************/
 void Global::manageScrolling(QTableWidget *table,int index,int rowNo)
 {
     if(index == 0)
     {
         table->selectRow(rowNo);
         prevSlidebarPos = table->verticalScrollBar()->sliderPosition();
     }

     curSlidebarPos = table->verticalScrollBar()->sliderPosition();
     if(curSlidebarPos >= prevSlidebarPos){
         table->selectRow(rowNo);
         prevSlidebarPos = curSlidebarPos;
     }
     if(prevSlidebarPos > rowNo) prevSlidebarPos = curSlidebarPos;
 }

/*************************************************************************
 Function Name - enableTableHeaderUnderline
 Parameter(s)  - QTableWidget*
 Return Type   - void
 Action        - Adds horizontal line to Table Horizontal Header.
 *************************************************************************/
 void Global::enableTableHeaderUnderline(QTableWidget *table) {
     table->horizontalHeader()->setStyleSheet(
                 "QHeaderView::section{"
                     "border-top:1px solid #000103 ;"
                     "border-left:1px solid #000103 ;"
                     "border-right:1px solid #000103 ;"
                     "border-bottom: 1px solid #000103 ;"
                     "background-color:#f2f3fc;"
                     "padding:4px;"
                 "}"
                 "QTableCornerButton::section{"
                     "border-top:0px solid #D8D8D8;"
                     "border-left:1px solid #D8D8D8;"
                     "border-right:1px solid #D8D8D8;"
                     "border-bottom: 1px solid #D8D8D8;"
                     "background-color:white;"
                 "}"
                 );
 }

/*************************************************************************
 Function Name - updateProgressBar
 Parameter(s)  - QProgressBar*, int, int
 Return Type   - void
 Action        - Updates given QProgressBar as per the given data.
 *************************************************************************/
 void Global::updateProgressBar(QProgressBar* progressBar,int curCount,int maxCount) {
    progressBar->setValue(100.00 * (curCount + 1)/maxCount);
 }

/*************************************************************************
 Function Name - writeDOP
 Parameter(s)  - uint32_t, uint8_t, int
 Return Type   - int
 Action        - Sets DOP to 1 or 0. onOff::1=>DOP ON, onOff::0=>DOP OFF
                 Returns 1 if success else returns ErrorCode.
 *************************************************************************/
 int Global:: writeDOP(uint32_t dopNo , uint8_t onOff ,int pci)
 {
     _qDebug("int Global:: writeDOP(uint32_t dopNo , uint8_t onOff ,int pci)");
     int status = 1;

     if(pci == PCI::P1758) {
#if PCI_1758_EN
        status = pci1758Obj->writeDop(static_cast<int32>(dopNo-1),static_cast<uint8>(onOff));
        if(status != 1) {
            QMessageBox::warning(nullptr,"Error!!\n",pci1758Obj->getErrorLog());
        }
        return status;
#endif
     }
     else {
#if PCI_7444_EN
        status = pci7444Obj->writeDop(static_cast<uint8_t>(dopNo-1),static_cast<uint8_t>(onOff));
        if(status != 1) {
            QMessageBox::warning(nullptr,"Error!!\n",pci7444Obj->getErrorLog());
        }
        return status;
#endif
     }
     return status;
 }

 /*************************************************************************
  Function Name - writeDOP
  Parameter(s)  - QStringList, uint8_t, int
  Return Type   - int
  Action        - Sets DOPs to 1 or 0. onOff::1=>DOP ON, onOff::0=>DOP OFF.
                  Returns 1 if success else returns ErrorCode.
  *************************************************************************/
  int Global:: writeDOP(QStringList dopNos , uint8_t onOff,int pci)
  {
      _qDebug("int Global:: writeDOP(QStringList dopNos , uint8_t onOff,int pci)");
      int status = 1;
      uint32_t dopNo;

      if(pci == PCI::P1758) {
 #if PCI_1758_EN
          for(int i = 0;i < dopNos.count();i++)
          {
             dopNo = static_cast<uint32_t>(dopNos.at(i).toInt());
             status = pci1758Obj->writeDop(static_cast<int32>(dopNo-1),static_cast<uint8>(onOff));
             if(status != 1) {
                 QMessageBox::warning(nullptr,"Error!!\n",pci1758Obj->getErrorLog());
                 break;
             }
          }
         return status;
 #endif
      }
      else {
 #if PCI_7444_EN
          for(int i = 0;i < dopNos.count();i++)
          {
             dopNo = static_cast<uint32_t>(dopNos.at(i).toInt());
             status = pci7444Obj->writeDop(static_cast<uint8_t>(dopNo-1),static_cast<uint8_t>(onOff));
             if(status != 1) {
                 QMessageBox::warning(nullptr,"Error!!\n",pci7444Obj->getErrorLog());
                 break;
             }
          }
          return status;
 #endif
      }
      return status;
}

/*************************************************************************
 Function Name - writeDOP
 Parameter(s)  - QVector<uint32_t>, uint8_t, int
 Return Type   - int
 Action        - Sets DOPs to 1 or 0. onOff::1=>DOP ON, onOff::0=>DOP OFF.
                 Returns 1 if success else returns ErrorCode.
 *************************************************************************/
 int Global:: writeDOP(QVector<uint32_t> dopNos , uint8_t onOff,int pci)
 {
   _qDebug("int Global:: writeDOP(QVector<uint32_t> dopNos , uint8_t onOff,int pci)");
   int status = 1;
   uint32_t dopNo;

   if(pci == PCI::P1758) {
#if PCI_1758_EN
       for(int i = 0;i < dopNos.count();i++)
       {
          dopNo = dopNos.at(i);
          status = pci1758Obj->writeDop(static_cast<int32>(dopNo-1),static_cast<uint8>(onOff));
          if(status != 1) {
              QMessageBox::warning(nullptr,"Error!!\n",pci1758Obj->getErrorLog());
              break;
          }
       }
      return status;
#endif
   }
   else {
#if PCI_7444_EN
       for(int i = 0;i < dopNos.count();i++)
       {
          dopNo = dopNos.at(i);
          status = pci7444Obj->writeDop(static_cast<uint8_t>(dopNo-1),static_cast<uint8_t>(onOff));
          if(status != 1) {
              QMessageBox::warning(nullptr,"Error!!\n",pci7444Obj->getErrorLog());
              break;
          }
       }
       return status;
#endif
   }
   return status;
 }

 /*************************************************************************
  Function Name - readDIP
  Parameter(s)  - QStringList, uint8_t&, int
  Return Type   - int
  Action        - Reads given DIPs and saves the DIP status in given state variable.
                  Returns 1 if read success else returns ErrorCode.
  *************************************************************************/
  int Global:: readDIP(uint32_t dipNo , uint8_t& state ,int pci)
  {
      _qDebug("int Global:: readDIP(uint32_t dipNo , uint8_t& state ,int pci)");
      int status = 1;

      if(pci == PCI::P1758) {
 #if PCI_1758_EN
         status = pci1758Obj->readDip(static_cast<int32>(dipNo-1),&state);
         if(status != 1) {
             QMessageBox::warning(nullptr,"Error!!\n",pci1758Obj->getErrorLog());
         }
         return status;
 #endif
      }

      return status;
  }

 /*************************************************************************
  Function Name - readDIP
  Parameter(s)  - QStringList, QVector<uint8_t>&, int
  Return Type   - int
  Action        - Reads given DIPs and saves the individual DIP status in given
                  states buffer. Returns 1 if read success else returns ErrorCode.
  *************************************************************************/
  int Global:: readDIP(QStringList dipNos , QVector<uint8_t>& states, int pci)
  {
      _qDebug("int Global:: readDIP(QStringList dipNos , QVector<uint8_t>& states, int pci)");
      int status = 1;
      uint32_t dipNo;
      uint8_t state;

      states.clear();
      if(pci == PCI::P1758) {
 #if PCI_1758_EN
          for(int i = 0;i < dipNos.count();i++)
          {
             dipNo = static_cast<uint32_t>(dipNos.at(i).toInt());
             status = pci1758Obj->readDip(static_cast<int32>(dipNo-1),&state);
             states.push_back(state);
             if(status != 1) {
                 QMessageBox::warning(nullptr,"Error!!\n",pci1758Obj->getErrorLog());
                 break;
             }
          }
         return status;
 #endif
      }
      return status;
  }

 /*************************************************************************
  Function Name - readDIP
  Parameter(s)  - QVector<uint32_t>, QVector<uint8_t>&, int
  Return Type   - int
  Action        - Reads given DIPs and saves the individual DIP status in given
                  states buffer. Returns 1 if read success else returns ErrorCode.
  *************************************************************************/
  int Global:: readDIP(QVector<uint32_t> dipNos , QVector<uint8_t>& states, int pci)
  {
       _qDebug("int Global:: readDIP(QVector<uint32_t> dipNos , QVector<uint8_t>& states, int pci)");
       int status = 1;
       uint32_t dipNo;
       uint8_t state = 0;

       states.clear();
       if(pci == PCI::P1758) {
#if PCI_1758_EN
           for(int i = 0;i < dipNos.count();i++)
           {
              dipNo = dipNos.at(i);
              status = pci1758Obj->readDip(static_cast<int32>(dipNo-1),&state);
              states.push_back(state);
              if(status != 1) {
                  QMessageBox::warning(nullptr,"Error!!\n",pci1758Obj->getErrorLog());
                  break;
              }
           }
          return status;
#else
          for(int i = 0;i < dipNos.count();i++)  states.push_back(state);
#endif
       }
       return status;
  }
  /*************************************************************************
   Function Name - readDIP
   Parameter(s)  - QVector<uint32_t>, QVector<uint8_t>&, int
   Return Type   - int
   Action        - Reads given DIPs and saves the individual DIP status in given
                   states buffer with comma separated. Returns 1 if read
                   success else returns ErrorCode.
   *************************************************************************/
   int Global:: readDIP(QVector<uint32_t> dipNos , QString& states, int pci)
   {
        _qDebug("int Global:: readDIP(QVector<uint32_t> dipNos , QVector<uint8_t>& states, int pci)");
        int status = 1;
        uint32_t dipNo;
        uint8_t state = 0;

        states.clear();
        if(pci == PCI::P1758) {
 #if PCI_1758_EN
            for(int i = 0;i < dipNos.count();i++)
            {
               dipNo = dipNos.at(i);
               status = pci1758Obj->readDip(static_cast<int32>(dipNo-1),&state);
               if(states.size() > 0) states.append(",");
               states.append(QString::number(state));
               if(status != 1) {
                   QMessageBox::warning(nullptr,"Error!!\n",pci1758Obj->getErrorLog());
                   break;
               }
            }
           return status;
 #else
           for(int i = 0;i < dipNos.count();i++)  {
               if(states.size() > 0) states.append(",");
               states.append(QString::number(state));
           }
 #endif
        }
        return status;
   }

/*************************************************************************
 Function Name - getDop
 Parameter(s)  - const QVector<QStringList>, QString, uint32*
 Return Type   - bool
 Action        - Searches conn_pin(key) in given buffer "dataMap" and stores
                 the corresponding DOP value in dop. If key is found then returns
                 true else returns false.
 *************************************************************************/
 bool Global::getDop(const QVector<QStringList> dataMap,QString key,uint32* dop)
 {
     _qDebug("bool Global::getDop(const QVector<QStringList> dataMap,QString key,uint32* dop)");
     int index;

     for(index = 0;index < dataMap.count();index++)
     {
         if(dataMap.at(index).count() == MAP_TABLE_CELL_COUNT)
         {
             if(dataMap.at(index).at(0).compare(key) == 0)
             {
                 *dop = dataMap.at(index).at(MAP_TABLE_DOP_INDEX).toUInt();
                 break;
             }
         }
     }
     if(index == dataMap.count()) {
         QMessageBox::warning(nullptr,"Error!!\n","The UUT_con/Pin:["+key+"] couldn't be found in Database.");
         return false;
     }
     else return true;
 }

/*************************************************************************
 Function Name - getDops
 Parameter(s)  - const QVector<QStringList>, QString, uint32*
 Return Type   - bool
 Action        - Searches conn_pins(keys) in given buffer "dataMap" and stores
                 the corresponding DOP value in dops. If any one key is not
                 found then returns false else returns true.
 *************************************************************************/
 bool Global::getDops(const QVector<QStringList> dataMap,QString keys,QVector<uint32_t>& dops,QString splitter)
 {
     _qDebug("bool Global::getDops(const QVector<QStringList> dataMap,QString keys,QVector<uint32_t>& dops,QString splitter)");
     int index;
     bool ret = true;
     QStringList connPins;

     _qDebugE("dataMap:",dataMap,":","");

     dops.clear();
     if(keys.length() == 0) return ret;

     connPins = keys.split(splitter);
     _qDebugE("keys:",keys,"\nconnPins:",connPins);

     for(int i = 0;i < connPins.count();i++)
     {
         for(index = 0;index < dataMap.count();index++)
         {
             if(dataMap.at(index).at(0).compare(connPins.at(i)) == 0)
             {
                 dops.push_back(dataMap.at(index).at(MAP_TABLE_DOP_INDEX).toUInt());
                 break;
             }
         }
         if(index == dataMap.count()) {
             QMessageBox::warning(nullptr,"Error!!\n","The UUT_con/Pin:["+connPins.at(i)+"] couldn't be found in Database.");
             ret = false;
             break;
         }
     }

     return ret;
 }

/*************************************************************************
 Function Name - getDops
 Parameter(s)  - const QVector<QStringList>, QStringList, QStringList&
 Return Type   - bool
 Action        - Searches conn_pins in given buffer "dataMap" and stores
                 the corresponding DOP value in dops. If any one conn_pin is not
                 found then returns false else returns true.
 *************************************************************************/
 bool Global::getDops(const QVector<QStringList>& dataMap,QStringList connPins,QStringList& dops)
 {
     _qDebug("bool Global::getDops(const QVector<QStringList>& dataMap,QStringList connPins,QStringList dops)");
     int index;
     bool ret = true;

     _qDebugE("connPins:",connPins,"","");
     dops.clear();
     if(connPins.count() == 1 && connPins.at(0) == "") return ret;
     for(int i = 0;i < connPins.count();i++)
     {
         for(index = 0;index < dataMap.count();index++)
         {
             if(dataMap.at(index).at(0).compare(connPins.at(i)) == 0)
             {
                 dops.push_back(dataMap.at(index).at(MAP_TABLE_DOP_INDEX));
                 break;
             }
         }
         if(index == dataMap.count()) {
             QMessageBox::warning(nullptr,"Error!!\n","The UUT_con/Pin:["+connPins.at(i)+"] couldn't be found in Database.");
             ret = false;
             break;
         }
     }

     return ret;
 }

/*************************************************************************
  Function Name - onDops
  Parameter(s)  - const QMap<QString, uint32>&, QString, int
  Return Type   - bool
  Action        - Searches corresponding dops of given conn_pins in given
                  dataMap buffer and makes the DOPs ON. If the conn_pin is not
                  found or DOP ON failed then returns false else returns true.
  *************************************************************************/
  bool Global::onDops(const QMap<QString, uint32>& dopMap,QString connPins,int pci)
  {
      _qDebug("bool Global::onDops(const QMap<QString, uint32>& dataMap,QString connPins,int pci)");
      QVector<uint32_t> dops;
      QStringList uutConnPins = connPins.split(",");

      _qDebugE("connPins:",connPins,"","");
      if(connPins.count() == 0) return true;

      dops.clear();
      for(QString connPin : uutConnPins) {
        if(!dopMap.contains(connPin)) {
            QMessageBox::warning(nullptr,"Error!!\n","The UUT_con/Pin:["+connPin+"] couldn't be found in Database.");
            return false;
        }
        else {
            dops.push_back(dopMap.value(connPin));
        }
      }

      if(G::writeDOP(dops,ON,pci) != 1) return false;

      return true;
  }

/*************************************************************************
  Function Name - offDops
  Parameter(s)  - const QMap<QString, uint32>&, QString, int
  Return Type   - bool
  Action        - Searches corresponding dops of given conn_pins in given
                  dataMap buffer and makes the DOPs OFF. If the conn_pin is not
                  found or DOP OFF failed then returns false else returns true.
  *************************************************************************/
  bool Global::offDops(const QMap<QString, uint32>& dopMap,QString connPins,int pci)
  {
      _qDebug("bool Global::offDops(const QMap<QString, uint32>& dataMap,QString connPins,int pci)");
      QVector<uint32_t> dops;
      QStringList uutConnPins = connPins.split(",");

      _qDebugE("connPins:",connPins,"","");
      if(connPins.size() == 0) return true;

      dops.clear();
      for(QString connPin : uutConnPins) {
        if(!dopMap.contains(connPin)) {
            QMessageBox::warning(nullptr,"Error!!\n","The UUT_con/Pin:["+connPin+"] couldn't be found in Database.");
            return false;
        }
        else {
            dops.push_back(dopMap.value(connPin));
        }
      }

      if(G::writeDOP(dops,OFF,pci) != 1) return false;

      return true;
  }

/*************************************************************************
 Function Name - getConnPin
 Parameter(s)  - QString, QStringList&
 Return Type   - void
 Action        - Gets UUTConn/Pin from given buffer str and stores in given
                 buffer data.
 *************************************************************************/
 void Global::getConnPin(QString str,QStringList& data)
 {
     _qDebug("void Global::getConnPin(QString str,QStringList& data)");
     //Ex-J109-1A/(A,C,E,G,J,L,N,R,T,V,X,Z,b,d,f,h)
     QStringList strSplit;
     QStringList strSplit1;

     strSplit = str.split("/");

     data.clear();
     if(strSplit.count() > 1)
     {
         strSplit[0] = strSplit[0].trimmed();
         strSplit[1] = strSplit[1].trimmed();

         strSplit[1] = strSplit[1].remove('(');
         strSplit[1] = strSplit[1].remove(')');

         strSplit1 = strSplit.at(1).split(",");

         for(int i = 0;i < strSplit1.count();i++)
         {
             data.push_back(strSplit.at(0)+"/"+strSplit1.at(i));
         }
     }
 }

 /*************************************************************************
  Function Name - getConnPin
  Parameter(s)  - QString ,QString& , QString& , QString
  Return Type   - void
  Action        - Stores Conn and pin in conn and pin respectively from given
                  str by splitting on the basis of given splitter.
  *************************************************************************/
  void Global::getConnPin(QString str,QString& conn, QString& pin, QString splitter)
  {
      _qDebug("void Global::getConnPin(QString str,QStringList& data)");
      //Ex-J109-1A/(A) or J109-1A (A) or J109-1A/A
      conn = pin = "";

      QStringList strSplit = str.split(splitter);

      if(strSplit.count() < 2) return;

      conn = strSplit[0];
      strSplit[1] = strSplit[1].remove('(');
      pin = strSplit[1].remove(')');
  }

 /*************************************************************************
  Function Name - updateResult
  Parameter(s)  - bool, int, int, bool
  Return Type   - bool
  Action        - Updates result status in table cell. The status "fail" is
                  always overrides the status "pass". The  status indicates
                  current test status and testStatus indicates status of current
                  row_test before current test.
  *************************************************************************/
  bool Global::updateResult(bool status, int row, int column, bool testStatus) {
      QTableWidgetItem* item = nullptr;
      if(testStatus && !status) {
          item = testTable->item(row,column);
          if(item != nullptr) {
            item->setText(FAIL);
            item->setBackground(Qt::red);
          }
          else return false;
      }
      return true;
  }

/*************************************************************************
 Function Name - updateResultStatus
 Parameter(s)  - bool, bool&, bool&
 Return Type   - bool
 Action        - Updates result status in testResultStatus if not updated earlier.
                 The fail status has high priority than pass status. It means
                 if the previous status is fail then no new updation will happen,
                 but if the previous status is pass then new updation will happen
                 only if status is fail.
                 status: It indicates the current status
                 isPassUpdated: Global variable of the corresponding class which holds
                                the changes. If status is pass then it is updated to true.
                 isFailUpdated: Global variable of the corresponding class which holds
                                the changes. If status is fail then it is updated to true.
*************************************************************************/
  bool Global::updateResultStatus(bool status, bool &isPassUpdated, bool &isFailUpdated)
  {
      if(testResultStatus == nullptr) return false;

      if(status) {
          if(!isPassUpdated && !isFailUpdated) {
              testResultStatus->setText(PASS);
              testResultStatus->setStyleSheet("QLabel { background-color : #3AEA02;}");
              isPassUpdated = true;
          }
      }
      else {
          if(!isFailUpdated) {
              testResultStatus->setText(FAIL);
              testResultStatus->setStyleSheet("QLabel { background-color : red;}");
              isFailUpdated = true;
          }
      }
      return true;
  }

/*************************************************************************
 Function Name - updateResultStatus
 Parameter(s)  - QLabel*,bool, bool&, bool&
 Return Type   - bool
 Action        - Updates result status in UI label if not updated earlier.
                 The fail status has high priority than pass status. It means
                 if the previous status is fail then no new updation will happen,
                 but if the previous status is pass then new updation will happen
                 only if status is fail.
                 label: The Qwidget of type QLabel whose text and background color
                        is to be changed
                 status: It indicates the current status
                 isPassUpdated: Global variable of the corresponding class which holds
                                the changes. If status is pass then it is updated to true.
                 isFailUpdated: Global variable of the corresponding class which holds
                                the changes. If status is fail then it is updated to true.
 *************************************************************************/
 bool Global::updateResultStatus(QLabel* label,bool status, bool &isPassUpdated, bool &isFailUpdated)
 {
       if(label == nullptr) return false;

       if(status) {
           if(!isPassUpdated && !isFailUpdated) {
               label->setText(PASS);
               label->setStyleSheet("QLabel { background-color : #3AEA02;}");
               isPassUpdated = true;
           }
       }
       else {
           if(!isFailUpdated) {
               label->setText(FAIL);
               label->setStyleSheet("QLabel { background-color : red;}");
               isFailUpdated = true;
           }
       }
       return true;
 }

 /*************************************************************************
  Function Name - showInfoMessage
  Parameter(s)  - QString
  Return Type   - void
  Action        - Shows given info image in messagebox.
  *************************************************************************/
  void Global::showInfoMessage(QString imagePath)
  {
      if(imagePath.size() < 1) return;

      QMessageBox msgBox;
      msgBox.setWindowTitle("Information");
      msgBox.setIconPixmap(QPixmap(imagePath));
      msgBox.setWindowFlags(Qt::CustomizeWindowHint);
      msgBox.setDefaultButton(QMessageBox::Ok);
      playSound(SOUND_FILE);
      msgBox.exec();
  }

  /*************************************************************************
   Function Name - showInfoMessage
   Parameter(s)  - QString
   Return Type   - void
   Action        - Shows given info image in messagebox.
   *************************************************************************/
   void Global::showInfoMessage(QMessageBox* msgBox,QString title,QString message)
   {
       msgBox->setWindowTitle(title);
       msgBox->setText(message);
       msgBox->setWindowFlags(Qt::CustomizeWindowHint);
       msgBox->setStandardButtons(nullptr);
       msgBox->show();
   }

   /*************************************************************************
    Function Name - showInfoMessage
    Parameter(s)  - QString
    Return Type   - void
    Action        - Shows given info image in messagebox.
    *************************************************************************/
    void Global::showInfoMessage(QString title,QString message)
    {
        testMsgBox = new QMessageBox;
        testMsgBox->setWindowTitle(title);
        testMsgBox->setText(message);
        testMsgBox->setWindowFlags(Qt::CustomizeWindowHint);
        testMsgBox->setStandardButtons(nullptr);
        testMsgBox->show();
    }

 /*************************************************************************
  Function Name - closeTestMessage
  Parameter(s)  - void
  Return Type   - void
  Action        - deletes test messagebox object.
 *************************************************************************/
 void Global::closeTestMessage()
 {
     clearmem(testMsgBox);
 }

  /*************************************************************************
   Function Name - playSound
   Parameter(s)  - QString
   Return Type   - void
   Action        - Plays the given sound file.
   *************************************************************************/
  void Global::playSound(QString file)
  {
      effect.setSource(QUrl::fromLocalFile(file));
      //effect.setLoopCount(QSoundEffect::Infinite);
      effect.setVolume(0.8f);
      effect.setLoopCount(1);
      effect.play();
  }

  /*************************************************************************
   Function Name - enableRunButton
   Parameter(s)  - bool
   Return Type   - void
   Action        - enables/disables the run push button according to given status.
   *************************************************************************/
  void Global::enableRunButton(bool status)
  {
      runStopButton->setEnabled(status);
  }
